<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\Job\AuctionAutoCallService;

/**
 *
 */
const TYPE = ['first', 'called'];

/**
 * Class AuctionAutoCall
 *
 * @package App\Console\Commands
 */
class AuctionAutoCall extends Command
{
    /**
     * @var AuctionAutoCallService
     */
    protected $service;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:auction_auto_call {type?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auction auto call command with type argument value called';


    /**
     * AuctionAutoCall constructor.
     *
     * @param AuctionAutoCallService $service
     */
    public function __construct(AuctionAutoCallService $service)
    {
        parent::__construct();
        $this->service = $service;
    }


    /**
     * @throws \Exception
     */
    public function handle()
    {
        $autoCallFlg = ($this->arguments()['type'] == 'called') ? true : false;
        $this->service->execute($autoCallFlg);
    }
}
