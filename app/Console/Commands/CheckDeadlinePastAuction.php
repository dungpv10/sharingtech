<?php

namespace App\Console\Commands;

use App\Repositories\CommissionInfoRepositoryInterface;
use App\Repositories\DemandCorrespondsRepositoryInterface;
use App\Repositories\DemandInfoRepositoryInterface;
use App\Repositories\MSiteRepositoryInterface;
use App\Services\Command\CheckDeadlinePastAuctionService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use Exception;

class CheckDeadlinePastAuction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check_deadline_past_auction';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check deadline past auction';

    /**
     * @var DemandInfoRepositoryInterface
     */
    protected $demandInfoRepository;

    /**
     * @var CommissionInfoRepositoryInterface
     */
    protected $commissionInfoRepository;

    /**
     * @var MSiteRepositoryInterface
     */
    protected $mSiteRepository;

    /**
     * @var DemandCorrespondsRepositoryInterface
     */
    protected $demandCorrespondsRepository;

    /**
     * @var CheckDeadlinePastAuctionService
     */
    protected $checkDeadlinePastAuctionService;

    /**
     * Default user
     * @var string
     */
    protected $user = 'system';

    /**
     * CheckDeadlinePastAuction constructor.
     * @param DemandInfoRepositoryInterface $demandInfoRepository
     * @param CommissionInfoRepositoryInterface $commissionInfoRepository
     * @param MSiteRepositoryInterface $mSiteRepository
     * @param DemandCorrespondsRepositoryInterface $demandCorrespondsRepository
     * @param CheckDeadlinePastAuctionService $checkDeadlinePastAuctionService
     */
    public function __construct(
        DemandInfoRepositoryInterface $demandInfoRepository,
        CommissionInfoRepositoryInterface $commissionInfoRepository,
        MSiteRepositoryInterface $mSiteRepository,
        DemandCorrespondsRepositoryInterface $demandCorrespondsRepository,
        CheckDeadlinePastAuctionService $checkDeadlinePastAuctionService
    ) {
        parent::__construct();
        $this->demandInfoRepository = $demandInfoRepository;
        $this->commissionInfoRepository = $commissionInfoRepository;
        $this->mSiteRepository = $mSiteRepository;
        $this->demandCorrespondsRepository = $demandCorrespondsRepository;
        $this->checkDeadlinePastAuctionService = $checkDeadlinePastAuctionService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            logger(__METHOD__ . ": オークション流れ案件処理start \n");
            echo __METHOD__ . ": オークション流れ案件処理start \n";

            $subCommissionInfo = $this->commissionInfoRepository->subCommissionInfo();
            $data = $this->demandInfoRepository->commandCheckDeadlinePastAuction($subCommissionInfo);
            $tmp = [];
            foreach ($data as $key => $row) {
                $commissionType = $this->mSiteRepository->getCommissionTypeDiv($row->site_id);

                $row->commission_type_div = $commissionType->commission_type_div;

                // Manual selection is made in the order in which the bid was delivered
                $commissionInfos = $this->checkDeadlinePastAuctionService->getAuctionCommissionList($row);

                if ($row->selection_system == getDivValue('selection_type', 'AutomaticAuctionSelection')) {
                    $userId = 'AutomaticAuction';
                } else {
                    $userId = $this->user;
                }
                $row->modified_user_id = $userId;
                $row->modified = date('Y-m-d H:i:s');

                // In case of automatic selection (automatic bidding ceremony)
                $row->auction = 1;
                $row->selection_system = getDivValue('selection_type', 'manual_selection');
                $demandStatus = getDivValue('demand_status', 'no_selection');

                if (!empty($commissionInfos['commissionInfo'])) {
                    foreach ($commissionInfos['commissionInfo'] as $value) {
                        if ($value['lost_flg'] == 0) {
                            $demandStatus = getDivValue('demand_status', 'agency_before');
                            break;
                        }
                    }
                }

                $row->demand_status = $demandStatus;
                $row->priority = 0;
                $row = $row->toArray();
                $tmp['demandInfo'][$key]['demandInfo'] = $row;

                if (!empty($commissionInfos['commissionInfo'])) {
                    $tmp['demandInfo'][$key]['commissionInfo'] = $commissionInfos['commissionInfo'];
                }

                if (isset($commissionInfos['commissionInfo'])) {
                    foreach ($commissionInfos['commissionInfo'] as $commissionInfo) {
                        $tmp['commissionInfo'][] = $commissionInfo;
                    }
                }

                if (!empty($commissionInfos['corresponding_contens'][0])) {
                    $tmp['demandCorrespond'][] = array(
                        'demand_id' => $row['id'],
                        'corresponding_contens' => $commissionInfos['corresponding_contens'][0],
                        'responders' => '入札流れ',
                        'correspond_datetime' => date('Y-m-d H:i:s'),
                        'created_user_id' => $userId,
                        'created' => date('Y-m-d H:i:s'),
                        'modified_user_id' => $userId,
                        'modified' => date('Y-m-d H:i:s'),
                    );
                }

                if (!empty($commissionInfos['corresponding_contens'][1])) {
                    $tmp['demandCorrespond'][] = array(
                        'demand_id' => $row['id'],
                        'corresponding_contens' => $commissionInfos['corresponding_contens'][1],
                        'responders' => '自動選定',
                        'correspond_datetime' => date('Y-m-d H:i:s'),
                        'created_user_id' => $userId,
                        'created' => date('Y-m-d H:i:s'),
                        'modified_user_id' => $userId,
                        'modified' => date('Y-m-d H:i:s'),
                    );
                }
            }

            if (!empty($tmp['demandInfo'])) {
                $this->demandInfoRepository->insertOrUpdateMultiData($tmp['demandInfo']);
            }
            if (!empty($tmp['commissionInfo'])) {
                $this->commissionInfoRepository->insertOrUpdateMultiData($tmp['commissionInfo']);
            }

            if (!empty($tmp['demandCorrespond'])) {
                $this->demandCorrespondsRepository->insertOrUpdateMultiData($tmp['demandCorrespond']);
            }
            echo __METHOD__ . ": オークション流れ案件処理end \n";
            logger(__METHOD__ . ": オークション流れ案件処理end \n");
        } catch (Exception $e) {
            echo __METHOD__ . ': Error - ' . $e->getMessage();
            logger(__METHOD__ . ': Error - ' . $e->getMessage());
        }
    }
}
