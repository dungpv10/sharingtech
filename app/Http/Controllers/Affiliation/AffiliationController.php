<?php

namespace App\Http\Controllers\Affiliation;

use App\Http\Controllers\BaseController;
use App\Http\Requests\AddAgreementRequest;
use App\Http\Requests\Affiliation\RegistCorpRequest;
use App\Http\Requests\AgreementUploadFileFormRequest;
use App\Repositories\AffiliationCorrespondsRepositoryInterface;
use App\Repositories\AgreementAttachedFileRepositoryInterface;
use App\Repositories\AntisocialCheckRepositoryInterface;
use App\Repositories\AutoCommissionCorpRepositoryInterface;
use App\Repositories\CorpAgreementRepositoryInterface;
use App\Repositories\Eloquent\MCorpNewYearRepository;
use App\Repositories\Eloquent\MItemRepository;
use App\Repositories\MCorpCategoryRepositoryInterface;
use App\Repositories\MCorpRepositoryInterface;
use App\Repositories\MItemRepositoryInterface;
use App\Services\AddAgreementService;
use App\Services\Affiliation\AffiliationCorpService;
use App\Services\Affiliation\AffiliationInfoService;
use App\Services\Affiliation\AffiliationMCorpCategoryService;
use App\Services\Affiliation\AffiliationSearchService;
use App\Services\AffiliationService;
use App\Services\CommissionInfoService;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AffiliationController extends BaseController
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var AgreementAttachedFileRepositoryInterface
     */
    protected $agrAttachedFileRepository;

    /**
     * @var AntisocialCheckRepositoryInterface
     */
    protected $antisocialCheckRepository;

    /**
     * @var AutoCommissionCorpRepositoryInterface
     */
    protected $autoCommissionCorpRepository;

    /**
     * @var AffiliationCorrespondsRepositoryInterface
     */
    protected $affCorrespondRepository;

    /**
     * @var AffiliationCorpService
     */
    protected $affiliationCorpService;

    /**
     * @var AffiliationSearchService
     */
    protected $affiliationSearchService;

    /**
     * @var AffiliationService
     */
    protected $affiliationService;

    /**
     * @var AffiliationInfoService
     */
    protected $affiliationInfoService;

    /**
     * @var AddAgreementService
     */
    protected $addAgreementService;

    /**
     * @var AffiliationMCorpCategoryService
     */
    protected $affMCorpCategoryService;

    /**
     * @var CommissionInfoService
     */
    protected $commissionInfoService;

    /**
     * @var CorpAgreementRepositoryInterface
     */
    protected $corpAgreementRepository;

    /**
     * @var MCorpRepositoryInterface
     */
    protected $mCorpRepository;

    /**
     * @var MCorpCategoryRepositoryInterface
     */
    protected $mCorpCategoryRepository;

    /**
     * @var MItemRepositoryInterface
     */
    protected $mItemRepository;

    /**
     * AffiliationController constructor.
     *
     * @param Request $request
     * @param AgreementAttachedFileRepositoryInterface $agrAttachedFileRepository
     * @param AntisocialCheckRepositoryInterface $antisocialCheckRepository
     * @param AutoCommissionCorpRepositoryInterface $autoCommissionCorpRepository
     * @param AffiliationCorrespondsRepositoryInterface $affCorrespondRepository
     * @param AffiliationCorpService $affiliationCorpService
     * @param AffiliationSearchService $affiliationSearchService
     * @param AffiliationService $affiliationService
     * @param AffiliationInfoService $affiliationInfoService
     * @param AddAgreementService $addAgreementService
     * @param AffiliationMCorpCategoryService $affMCorpCategoryService
     * @param CommissionInfoService $commissionInfoService
     * @param CorpAgreementRepositoryInterface $corpAgreementRepository
     * @param MCorpRepositoryInterface $mCorpRepository
     * @param MCorpCategoryRepositoryInterface $mCorpCategoryRepository
     * @param MItemRepositoryInterface $mItemRepository
     */
    public function __construct(
        Request $request,
        AgreementAttachedFileRepositoryInterface $agrAttachedFileRepository,
        AntisocialCheckRepositoryInterface $antisocialCheckRepository,
        AutoCommissionCorpRepositoryInterface $autoCommissionCorpRepository,
        AffiliationCorrespondsRepositoryInterface $affCorrespondRepository,
        AffiliationCorpService $affiliationCorpService,
        AffiliationSearchService $affiliationSearchService,
        AffiliationService $affiliationService,
        AffiliationInfoService $affiliationInfoService,
        AddAgreementService $addAgreementService,
        AffiliationMCorpCategoryService $affMCorpCategoryService,
        CommissionInfoService $commissionInfoService,
        CorpAgreementRepositoryInterface $corpAgreementRepository,
        MCorpRepositoryInterface $mCorpRepository,
        MCorpCategoryRepositoryInterface $mCorpCategoryRepository,
        MItemRepositoryInterface $mItemRepository
    ) {
        parent::__construct();
        $this->middleware('affiliation.check.corpId');
        $this->request = $request;
        $this->agrAttachedFileRepository = $agrAttachedFileRepository;
        $this->antisocialCheckRepository = $antisocialCheckRepository;
        $this->autoCommissionCorpRepository = $autoCommissionCorpRepository;
        $this->affCorrespondRepository = $affCorrespondRepository;
        $this->affiliationCorpService = $affiliationCorpService;
        $this->affiliationSearchService = $affiliationSearchService;
        $this->affiliationService = $affiliationService;
        $this->affiliationInfoService = $affiliationInfoService;
        $this->addAgreementService = $addAgreementService;
        $this->affMCorpCategoryService = $affMCorpCategoryService;
        $this->commissionInfoService = $commissionInfoService;
        $this->corpAgreementRepository = $corpAgreementRepository;
        $this->mCorpRepository = $mCorpRepository;
        $this->mCorpCategoryRepository = $mCorpCategoryRepository;
        $this->mItemRepository = $mItemRepository;
    }

    /**
     * Index page
     *
     * @param null $phoneNumber
     * @param null $corpStatus
     * @return Factory|View
     */
    public function index($phoneNumber = null, $corpStatus = null)
    {
        $dataView = $this->affiliationSearchService->prepareDataForViewAffiliation();
        $instantSearch = -1;
        if (session()->has(__('affiliation.KEY_SESSION_FOR_AFFILIATION_SEARCH'))) {
            $instantSearch = 1;
            session()->forget(__('affiliation.KEY_SESSION_FOR_AFFILIATION_SEARCH'));
        }
        $dataView['phoneNumber'] = '';
        $dataView['showTableData'] = false;
        $dataView['corpJoinStatus'] = 1;
        if (!is_null($phoneNumber) && strlen(trim($phoneNumber)) > 0
            && !is_null($corpStatus) && strlen(trim($corpStatus)) > 0
        ) {
            $dataView['phoneNumber'] = $phoneNumber;
            $dataView['corpJoinStatus'] = $corpStatus;
        }

        return view(
            'affiliation.index',
            [
            'data' => $dataView,
            'instantSearch' => $instantSearch
            ]
        );
    }

    /**
     * Return view corp target area
     *
     * @param  integer $id
     * @param  integer $initPref
     * @return view
     */
    public function corpTargetArea($id = null, $initPref = null)
    {
        if ($id == null) {
            return back();
        }
        try {
            if (!ctype_digit($id)) {
                abort(404);
            }
            $data = $this->request->all();
            $result = $this->affiliationInfoService->getCorpTargetArea($id, $data, $initPref);
        } catch (Exception $e) {
            abort(404);
        }
        return view('affiliation.corptargetarea', $result);
    }

    /**
     * Get view add_agreement function
     *
     * @param null $corpId
     * @return Factory|View
     */
    public function getAddAgreement($corpId = null)
    {
        $mCorp = $this->mCorpRepository->getFirstById($corpId);
        $checkCorpId = false;
        if (!$mCorp) {
            $checkCorpId = true;
        }
        $checkCorpKind = $this->addAgreementService->checkCorpKind($mCorp['corp_kind']);
        $checkDisableFlg = $this->addAgreementService->checkDisableFlg(Auth::user()->auth);
        return view('affiliation.add_agreement', compact('mCorp', 'checkCorpKind', 'checkDisableFlg', 'checkCorpId'));
    }

    /**
     * Check and insert data
     *
     * @param AddAgreementRequest $request
     * @param integer|null $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws Exception
     */
    public function postAddAgreement(AddAgreementRequest $request, $id = null)
    {
        $userId = Auth::user()['user_id'];
        $corpId = $request->corp_id;
        $corpAgreement['kind'] = $request->kind;
        $corpAgreement['corp_id'] = $corpId;
        $corpAgreement['status'] = 'NotSigned';
        $corpAgreement['agreement_history_id'] = 1;
        $corpAgreement['ticket_no'] = 1;
        $corpAgreement['corp_kind'] = $request->corp_kind;
        if (isset($request->agreement_date)) {
            $corpAgreement['agreement_date'] = $request->agreement_date;
        }
        if (isset($request->agreement_flag)) {
            $corpAgreement['agreement_flag'] = $request->agreement_flag ? true : false;
        }
        if (isset($request->hansha_check)) {
            $corpAgreement['hansha_check'] = $request->hansha_check;
            $corpAgreement['hansha_check_user_id'] = $userId;
            $corpAgreement['hansha_check_date'] = date('Y-m-d H:i:s');
        }
        if (isset($request->transactions_law) && $request->transactions_law == 1) {
            $corpAgreement['transactions_law_user_id'] = $userId;
            $corpAgreement['transactions_law_date'] = date('Y-m-d H:i:s');
        }
        if (isset($request->acceptation) && $request->acceptation == 1) {
            $corpAgreement['status'] = 'Complete';
            $corpAgreement['acceptation_user_id'] = $userId;
            $corpAgreement['acceptation_date'] = date('Y-m-d H:i:s');
        }
        $this->addAgreementService->addAgreement($corpId, $corpAgreement);
        $request->session()->flash('box--success', trans('add_agreement.success'));
        return redirect()->route('affiliation.agreement.index', $id);
    }

    /**
     * Detail agreement
     *
     * @param  integer $corpId
     * @param  integer $agreementId
     * @return view
     */
    public function agreement($corpId, $agreementId = null)
    {
        $statusMsg = $this->corpAgreementRepository->getStatusMessage();
        $corpData = $this->mCorpRepository->find($corpId);
        $corpAgreementCnt = $this->corpAgreementRepository->getCountByCorpIdAndStatus($corpId);
        if (!empty($agreementId)) {
            $corpAgreement = $this->corpAgreementRepository->getFirstByCorpIdAndAgreementId(
                $corpId,
                $agreementId
            );
        } else {
            $corpAgreement = $this->corpAgreementRepository->getFirstByCorpIdAndAgreementId($corpId, null, true);
        }
        if (!$corpData) {
            return view('affiliation.agreement');
        }
        $corpAgreementList = $this->corpAgreementRepository->getAllByCorpId($corpId);
        $agreementAttachedFileCert = $this->agrAttachedFileRepository->getAllAgreementAttachedFileByCorpIdAndKind(
            $corpId,
            'Cert'
        );
        $lastAntisocialCheck = $this->antisocialCheckRepository->findHistoryByCorpId($corpId, 'first');
        $antisocialCheck = $this->antisocialCheckRepository->getResultList();
        $agreementProvisions = AffiliationService::getAgreementProvisions($corpAgreement);
        $role = Auth::user()['auth'];
        $isRoleAffiliation = AffiliationService::isRole($role, ['affiliation']);
        $disableFlg = !AffiliationService::isRole($role, ['system', 'admin']);
        $isRoleSystem = AffiliationService::isRole($role, ['system']);
        $agreementReportDownloadUrl = AffiliationService::getUrlDownloadByRoleAndStatusCorpAgreement(
            Auth::user()['auth'],
            $corpAgreement,
            $corpId
        );
        return view(
            'affiliation.agreement',
            compact(
                'corpData',
                'corpAgreement',
                'agreementAttachedFileCert',
                'agreementProvisions',
                'statusMsg',
                'corpAgreementCnt',
                'corpAgreementList',
                'lastAntisocialCheck',
                'agreementReportDownloadUrl',
                'role',
                'disableFlg',
                'antisocialCheck',
                'isRoleAffiliation',
                'isRoleSystem'
            )
        );
    }

    /**
     * Update agreement
     * @param  Request $request
     * @param  integer $corpId
     * @param  integer $agreementId
     * @return view
     */
    public function agreementUpdate(Request $request, $corpId, $agreementId = null)
    {
        $data = $request->all();
        try {
            $result = $this->affiliationService->agreementUpdate($data, $corpId, $agreementId);
            $request->session()->flash($result['class'], $result['message']);
            return redirect()->route(
                'affiliation.agreement.index',
                ['corpId' => $corpId, 'agreementId' => $agreementId]
            );
        } catch (Exception $ex) {
            $request->session()->flash('box--error', __('agreement.update_error'));
            return redirect()->route(
                'affiliation.agreement.index',
                ['corpId' => $corpId, 'agreementId' => $agreementId]
            );
        }
    }

    /**
     * Check auto commission
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function checkAutoCommission(Request $request)
    {
        try {
            if (!$request->isMethod('get')) {
                throw new Exception();
            }

            $data = $request->all();
            $areaCorpListCnt = $this->mCorpCategoryRepository->getCountAreaCorpListByCorpId($data['corp_id']);
            $autoCommissionCorpListCnt = $this->autoCommissionCorpRepository->countByCorpId($data['corp_id']);
            $result = true;
            if ($areaCorpListCnt != $autoCommissionCorpListCnt) {
                $result = false;
            }
            return response()->json($result);
        } catch (Exception $ex) {
            return response()->json(false);
        }
    }

    /**
     * Update reconfirmation
     *
     * @param  Request $request
     * @param  integer $corpId
     * @param  integer $agreementId
     * @return view
     */
    public function updateReconfirmation(Request $request, $corpId, $agreementId = null)
    {
        try {
            $result = $this->affiliationService->updateReconfirmation($corpId);
            $request->session()->flash($result['class'], $result['message']);
            return redirect()->route(
                'affiliation.agreement.index',
                ['corpId' => $corpId, 'agreementId' => $agreementId ? $agreementId : $result['agreement_id']]
            );
        } catch (Exception $ex) {
            $request->session()->flash('box--error', __('agreement.update_error'));
            return redirect()->route('affiliation.agreement.index', ['corpId' => $corpId]);
        }
    }

    /**
     * Agreement upload file
     *
     * @param  AgreementUploadFileFormRequest $request
     * @param  integer                        $corpId
     * @param  integer                        $corpAgreementId
     * @return view
     */
    public function agreementUploadFile(AgreementUploadFileFormRequest $request, $corpId, $corpAgreementId = null)
    {
        try {
            $result = $this->affiliationService->agreementUploadFile($request, $corpId, $corpAgreementId);
            $request->session()->flash($result['class'], $result['message']);
            return redirect()->route(
                'affiliation.agreement.index',
                ['corpId' => $corpId, 'agreementId' => $corpAgreementId]
            );
        } catch (Exception $ex) {
            $request->session()->flash('box--error', __('agreement.error_while_processing'));
            return redirect()->route('affiliation.agreement.index', ['corpId' => $corpId]);
        }
    }

    /**
     * Show page download agreement  file
     *
     * @param  integer $fileId
     * @return view
     */
    public function downloadAgreementFile($fileId)
    {
        $file = $this->agrAttachedFileRepository->findById($fileId);
        $path = str_replace(storage_path('upload/'), "", $file->path);
        return redirect()->route(
            'download.index',
            ['target' => base64_encode($path), 'filename' => base64_encode($file->name)]
        );
    }

    /**
     * Download agreement report
     *
     * @param  integer $corpId
     * @param  integer $agreementId
     * @return view
     */
    public function downloadAgreementReport($corpId, $agreementId = null)
    {
        return view('affiliation.agreement_download_report', ['corpId' => $corpId, 'agreementId' => $agreementId]);
    }

    /**
     * Download agreement terms
     *
     * @param  integer $corpId
     * @param  integer $agreementId
     * @return view
     */
    public function downloadAgreementTerms($corpId, $agreementId = null)
    {
        return view('affiliation.agreement_download_terms', ['corpId' => $corpId, 'agreementId' => $agreementId]);
    }

    /**
     * Detail Affiliation Category
     *
     * @param  integer $id
     * @return Factory|View
     * @throws NotFoundHttpException
     */
    public function category($id)
    {
        if (empty($id) || !is_numeric($id)) {
            throw new NotFoundHttpException();
        }

        //region Get Prepare Data
        $corpData = $this->affiliationCorpService->getMCorpData($id);
        if (!isset($corpData)) {
            throw new NotFoundHttpException();
        }
        $affiliationInfo = $this->affiliationCorpService->getAffiliationInfo($id);
        $genreList = $this->affMCorpCategoryService->getGenreList($id);
        $prefList = $this->affiliationCorpService->getPrefList($id);
        $mCorpSub = $this->affiliationCorpService->getMCorpSubByMCorpId($id);

        $auctionDeliveryStatusList = config('rits.auction_delivery_status');
        array_walk($auctionDeliveryStatusList, function (&$value, $key) {
            $value = __("rits_config.$value");
        });

        $prefectureDivList = config('rits.prefecture_div');
        array_walk($prefectureDivList, function (&$value, $key) {
            $value = __("rits_config.$value");
        });
        $mobilePhoneTypeList = $this->mItemRepository->getListByCategoryItem(MItemRepository::LIST_MOBILE_PHONE_TYPES); // Get List Mobile phone types
        $customerInfoContactList = $this->mItemRepository->getListByCategoryItem(MItemRepository::CUSTOMER_INFORMATION_CONTACT_METHOD); //Customer information contact
        $businessHolidayList = $this->mItemRepository->getListByCategoryItem(MItemRepository::HOLIDAYS); // Business Holiday List
        $vacationList = $this->mItemRepository->getListByCategoryItem(MItemRepository::LONG_HOLIDAYS); // Retrieve closed days
        $newYearStatusOptions = MCorpNewYearRepository::NEW_YEAR_STATUS_OPTIONS;
        $corpHolidays = $mCorpSub['holiday'];
        $corpDevelopmentResponse = $mCorpSub['developmentResponse'];
        //endregion

        //region Calculate Data
        $affiliationInfoCredit = null;
        $affiliationInfoCreditRemaining = null;
        if (!empty($affiliationInfo['credit_limit'])) {
            $affiliationInfoCredit = (int)$affiliationInfo['credit_limit'] + (int)$affiliationInfo['add_month_credit'];
            $affiliationInfoCreditRemaining = $affiliationInfoCredit -
                $this->commissionInfoService->checkCredit($id, null, true);
        }
        //endregion

        // Render view
        $viewData = [
            'id' => $id,
            'genreCustomAreaListA' => $genreList['genreCustomAreaListA'],
            'genreCustomAreaListB' => $genreList['genreCustomAreaListB'],
            'genreNormalAreaListA' => $genreList['genreNormalAreaListA'],
            'genreNormalAreaListB' => $genreList['genreNormalAreaListB'],
            'lastItemGenre' => $genreList['lastItemGenre'],
            'corpData' => $corpData,
            'prefList' => $prefList,
            'affiliationInfo' => $affiliationInfo,
            'auctionDeliveryStatusList' => $auctionDeliveryStatusList,
            'prefectureDivList' => $prefectureDivList,
            'mobilePhoneTypeList' => $mobilePhoneTypeList,
            'customerInfoContactList' => $customerInfoContactList,
            'businessHolidayList' => $businessHolidayList,
            'vacationList' => $vacationList,
            'corpHolidays' => $corpHolidays,
            'corpDevelopmentResponse' => $corpDevelopmentResponse,
            'newYearStatusOptions' => $newYearStatusOptions,
            'affiliationInfoCredit' =>
                isset($affiliationInfoCredit) ? number_format($affiliationInfoCredit) : null,
            'affiliationInfoCreditRemaining' =>
                isset($affiliationInfoCreditRemaining) ? number_format($affiliationInfoCreditRemaining) : null,
        ];


        return $this->renderView('affiliation.category', $viewData);
    }

    /**
     *  Regist/Update the Corp
     *
     * @param  $id
     * @param  RegistCorpRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function updateCorp($id, RegistCorpRequest $request)
    {
        $inputData = $request->all();
        $result = $this->affiliationCorpService->updateCorp($id, $inputData['data']);
        if (empty($result)) {
            return redirect()->back()
                ->with('error', trans('affiliation.m_corp_category_error_updating'));
        } else {
            if ($result['success']) {
                redirect()->back()->with('success', $result['message']);
            } else {
                if (isset($result['type']) && $result['type'] == 'field') {
                    redirect()->back()->withErrors($result['message']);
                } else {
                    redirect()->back()->with('error', $result['message']);
                }
            }
        }

        return redirect()->action(
            'Affiliation\AffiliationController@category',
            ['id' => $id]
        );
    }

    /**
     * Update status mCorp category
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function updateStatusMCorpCategory($id, Request $request)
    {
        $inputData = [
            'auction_status' => $request->get('auction_status')
        ];

        $result = $this->affMCorpCategoryService->updateStatusMCorpCategory($id, $inputData);
        if (!$result) {
            return redirect()->back()->withErrors(trans('affiliation.not_empty_affiliation_base_item'));
        }
        return redirect()->back()->with('success', __('common.updated_completed'));
    }

    /**
     * Search affiliation
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        try {
            $listResult = $this->affiliationSearchService->searchCorpByCondition($request);
        } catch (\Exception $exception) {
            $errorMessage = trans('mcorp_list.exception');
            return \response()->json($errorMessage);
        } catch (\Throwable $e) {
            $errorMessage = trans('mcorp_list.exception');
            return \response()->json($errorMessage);
        }
        return response()->view('affiliation.components.affiliation_table', ['result' => $listResult]);
    }

    /**
     * Download CSV affiliation
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function downloadCSVAffiliation(Request $request)
    {
        $file = $this->affiliationSearchService->getDataDownloadAffiliationCSV($request);
        if (!is_null($file)) {
            return $file->download('csv');
        }
        return redirect()->route('affiliation.index');
    }

    /**
     * Get affiliation history
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAffiliationHistoryInput($id)
    {
        $data = $this->affCorrespondRepository->find($id);
        return response()->json($data);
    }

    /**
     * Create affiliation history
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postAffiliationHistoryInput(Request $request, $id)
    {
        $data = $request->all();
        if (!empty($data['data_history']['responders'])
            && $data['data_history']['corresponding_contens'] <= 1000
            && !empty($data['data_history']['corresponding_contens'])) {
            $this->affCorrespondRepository->updateAffiliationCorrespondWithId($id, $data['data_history']);
        }

        return redirect()->route('affiliation.detail.edit', $data['affiliation_id']);
    }

    /**
     * Set session for back search of affiliation
     *
     * @return boolean[]
     */
    public function setSessionForBackAffiliationSearch()
    {
        session([__('affiliation.KEY_SESSION_FOR_AFFILIATION_SEARCH') => true]);

        return ['result' => true];
    }
}
