<?php

namespace App\Http\Controllers\Agreement;

use App\Models\AffiliationInfo;
use App\Models\CorpAgreement;
use App\Http\Controllers\Controller;
use App\Models\MCorp;
use App\Repositories\AffiliationInfoRepositoryInterface;
use App\Services\AgreementAdminService;
use Illuminate\Http\Request;

class AgreementAdminController extends Controller
{
    /**
     * @var AffiliationInfoRepositoryInterface
     */
    protected $repository;
    /**
     * @var AgreementAdminService
     */
    protected $service;

    /**
     * AgreementAdminController constructor.
     *
     * @param AffiliationInfoRepositoryInterface $affiliationInfoRepository
     * @param AgreementAdminService              $agreementAdminService
     */
    public function __construct(
        AffiliationInfoRepositoryInterface $affiliationInfoRepository,
        AgreementAdminService $agreementAdminService
    ) {
        $this->repository = $affiliationInfoRepository;
        $this->service = $agreementAdminService;
    }

    /**
     * Displays datatables front end view
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $corpKindLabel = MCorp::CORP_KIND;
        $agreementStatusItemLabel = $this->service->getAgreementStatusItemLabel();
        $listedKindLabel = AffiliationInfo::LISTED_KIND;
        $hanshaCheckStatusLabel = CorpAgreement::HANSHA_CHECK_STATUS;
        return view(
            'agreement.admin.dashboard',
            compact('corpKindLabel', 'agreementStatusItemLabel', 'listedKindLabel', 'hanshaCheckStatusLabel')
        );
    }

    /**
     * Process datatables ajax request.
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function dataProcessing(Request $request)
    {
        return $this->service->getTableDataWithCondition($request);
    }

    /**
     * @param Request $request
     * @throws \Exception
     */
    public function exportCsv(Request $request)
    {
        $this->service->exportFile($request, config("datacustom.file_type.csv"));
    }

    /**
     * @param Request $request
     * @throws \Exception
     */
    public function exportExcel(Request $request)
    {
        $this->service->exportFile($request, config("datacustom.file_type.excel"));
    }
}
