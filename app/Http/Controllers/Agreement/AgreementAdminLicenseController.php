<?php

namespace App\Http\Controllers\Agreement;

use App\Http\Controllers\Controller;
use App\Services\AgreementAdmin;
use App\Services\AgreementAdminLicenseService;
use Illuminate\Http\Request;
use App\Repositories\AgreementAdminLicenseRepositoryInterface;
use Illuminate\Support\Facades\Lang;

class AgreementAdminLicenseController extends Controller
{
    /**
     * @var AgreementAdminLicenseRepositoryInterface
     */
    protected $agreementLicenseRepository;
    /**
     * @var AgreementAdminLicenseService
     */
    protected $agreementLicenseService;

    /**
     * AgreementAdminLicenseController constructor.
     *
     * @param AgreementAdminLicenseRepositoryInterface $agreementLicenseRepository
     * @param AgreementAdminLicenseService             $agreementLicenseService
     */
    public function __construct(
        AgreementAdminLicenseRepositoryInterface $agreementLicenseRepository,
        AgreementAdminLicenseService $agreementLicenseService
    ) {
        $this->agreementLicenseRepository = $agreementLicenseRepository;
        $this->agreementLicenseService = $agreementLicenseService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getLicensePage()
    {
        return view('agreement.admin.license.index');
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function getLicenseData(Request $request)
    {
        return $this->agreementLicenseService->getAgreementLicenseData($request);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function addLicense(Request $request)
    {
        $this->agreementLicenseRepository->addLicense($request);
        $content = Lang::get('agreement_admin.registration_complete');
        return $this->getMessageResponseSuccess($content);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getLicenseDetail($id)
    {
        return $this->agreementLicenseRepository->getLicenseById($id);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function updateLicense(Request $request)
    {
        $this->agreementLicenseRepository->updateLicense($request);
        $content = Lang::get('agreement_admin.content_update_successfully');
        return $this->getMessageResponseSuccess($content);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteLicense($id)
    {
        try {
            $this->agreementLicenseRepository->deleteLicenseById($id);
            $content = Lang::get('agreement_admin.content_delete_successfully');
            return $this->getMessageResponseSuccess($content);
        } catch (\Exception $exception) {
            $message['type'] = 'ERROR';
            return $message;
        }
    }
}
