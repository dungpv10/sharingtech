<?php

namespace App\Http\Controllers\Agreement;

use App\Http\Controllers\Controller;
use App\Http\Requests\AgreementAttachFileRequest;
use App\Http\Requests\AgreementSystemRequest;
use App\Models\MCorpCategoriesTemp;
use App\Repositories\CorpAgreementRepositoryInterface;
use App\Repositories\CorpAgreementTempLinkRepositoryInterface;
use App\Repositories\MCorpCategoriesTempRepositoryInterface;
use App\Services\Affiliation\AffiliationCorpService;
use App\Services\AgreementSystemService;
use App\Services\AreaDialogService;
use App\Services\CategoryDialogService;
use App\Services\MAddress1Service;
use Illuminate\Http\Request;
use App\Services\Logic\AgreementSystemLogic;
use Illuminate\Support\Facades\Lang;

class AgreementSystemController extends Controller
{

    /**
     * @var CorpAgreementRepositoryInterface
     */
    protected $corpAgreementRepository;
    /**
     * @var AgreementSystemService
     */
    protected $agreementSystemService;
    /**
     * @var AffiliationCorpService
     */
    protected $affiliationCorpService;
    /**
     * @var CorpAgreementTempLinkRepositoryInterface
     */
    protected $corpAgreeTempLinkRepo;
    /**
     * @var MCorpCategoriesTempRepositoryInterface
     */
    protected $mCorpCategoriesTempRepository;
    /**
     * @var CategoryDialogService
     */
    protected $categoryDialogService;
    /**
     * @var AgreementSystemLogic
     */
    protected $agreementSystemLogic;
    /**
     * @var MAddress1Service
     */
    protected $mAddress1Service;
    /**
     * @var AreaDialogService
     */
    protected $areaDialogService;

    /**
     * AgreementSystemController constructor.
     * @param CorpAgreementRepositoryInterface $corpAgreementRepository
     * @param AgreementSystemService $agreementSystemBusiness
     * @param AffiliationCorpService $affiliationCorpService
     * @param CorpAgreementTempLinkRepositoryInterface $corpAgreeTempLinkRepo
     * @param MCorpCategoriesTempRepositoryInterface $mCorpCategoriesTempRepository
     * @param CategoryDialogService $categoryDialogService
     * @param AgreementSystemLogic $agreementSystemLogic
     * @param MAddress1Service $mAddress1Service
     * @param AreaDialogService $areaDialogService
     */
    public function __construct(
        CorpAgreementRepositoryInterface $corpAgreementRepository,
        AgreementSystemService $agreementSystemBusiness,
        AffiliationCorpService $affiliationCorpService,
        CorpAgreementTempLinkRepositoryInterface $corpAgreeTempLinkRepo,
        MCorpCategoriesTempRepositoryInterface $mCorpCategoriesTempRepository,
        CategoryDialogService $categoryDialogService,
        AgreementSystemLogic $agreementSystemLogic,
        MAddress1Service $mAddress1Service,
        AreaDialogService $areaDialogService
    ) {

        $this->corpAgreementRepository = $corpAgreementRepository;
        $this->agreementSystemService = $agreementSystemBusiness;
        $this->affiliationCorpService = $affiliationCorpService;
        $this->corpAgreeTempLinkRepo = $corpAgreeTempLinkRepo;
        $this->mCorpCategoriesTempRepository = $mCorpCategoriesTempRepository;
        $this->categoryDialogService = $categoryDialogService;
        $this->agreementSystemLogic = $agreementSystemLogic;
        $this->mAddress1Service = $mAddress1Service;
        $this->areaDialogService = $areaDialogService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function getStep0()
    {
        $mCorp = \Request::get('mCorp');
        $corpId = $mCorp->id;
        $corpAgreement = $this->corpAgreementRepository->getFirstByCorpIdAndAgreementId($corpId, null, true);
        if (is_null($corpAgreement)) {
            $this->agreementSystemLogic->initDataCorpAgreement($this->getUser());
        }
        $corpAgreementList = $this->corpAgreementRepository->getAllByCorpId($corpId, 'asc');
        return view('agreement.system.step0', compact('corpAgreementList'));
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function postStep0Proceed()
    {
        $this->agreementSystemService->step0Process($this->getUser());

        return redirect()->route('agreementSystem.getStep1');
    }

    /**
     * initial step1
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getStep1()
    {
        $currentStepName = Lang::get('agreement_system.step') . '[1] ' . Lang::get('progress.agreement_of_terms_conditions');
        $currentStep = 1;
        $mCorp = \Request::get('mCorp');
        $corpId = $mCorp->id;
        $corpAgreement = $this->corpAgreementRepository->findByCorpId($corpId);
        $corpAgreementId = $corpAgreement->id;
        $arrayProvision = $this->agreementSystemService->findCustomizedAgreementByCorpId($corpId);

        return view('agreement.system.step1', compact('currentStep', 'currentStepName', 'arrayProvision', 'corpAgreementId'));
    }

    /**
     * execute step1
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postStep1Proceed()
    {
        $this->agreementSystemService->step1Process($this->getUser());

        return redirect()->route('agreementSystem.getStep2');
    }

    /**
     * Company information input screen - initial step 2
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getStep2()
    {
        $currentStepName = trans('agreement_system.step') . '[2] ' . trans('progress.register_basic_information');
        $currentStep = 2;
        $mCorp = \Request::get('mCorp');
        $affiliationInfo = $this->affiliationCorpService->getAffiliationInfo($mCorp->id);
        $mCorpSubs = $this->affiliationCorpService->getMCorpSubByMCorpId($mCorp->id);
        $corpHolidays = $mCorpSubs['holiday'];
        $data = $this->agreementSystemService->getStep2($mCorp->responsibility);
        return view('agreement.system.step2', compact('currentStep', 'currentStepName', 'mCorp', 'affiliationInfo', 'corpHolidays', 'data'));
    }

    /**
     * Company information input screen - process step 2
     *
     * @param  AgreementSystemRequest $request
     * @return \Illuminate\Http\RedirectResponse|string
     */
    public function postStep2(AgreementSystemRequest $request)
    {
        try {
            $requestData = $this->agreementSystemService->convertRequestData($request->only('affiliationInfo', 'mCorp', 'holidays'));
            if ($this->agreementSystemService->updateData($requestData)) {
                $this->agreementSystemService->step2Process($this->getUser(), $requestData['mCorp']['corp_kind']);
                return redirect()->route('agreementSystem.getStep3');
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Genre selection screen - initial step3
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getStep3()
    {
        $currentStepName = Lang::get('agreement_system.step') . '[3] ' . Lang::get('progress.registering_available_compatible_genres_and_compatible_areas');
        $currentStep = 3;
        $corpId = \Request::get('mCorp')->id;

        $prefList = $this->agreementSystemService->getPrefList($corpId);
        $corpAgreementTempLink = $this->corpAgreeTempLinkRepo->findLatestByCorpId($corpId);
        $corpCategoryList = $this->mCorpCategoriesTempRepository->findAllByCorpIdAndTempIdWithFlag($corpId, $corpAgreementTempLink->id, false, false)->toArray();

        $viewData = [
            'currentStep' => $currentStep,
            'currentStepName' => $currentStepName,
            'prefList' => $prefList,
            'corpCategoryList' => $corpCategoryList];
        return view('agreement.system.step3', $viewData);
    }

    /**
     * Genre selection screen - process step3
     *
     * @return \Illuminate\Http\RedirectResponse|string
     */
    public function postStep3()
    {
        try {
            $this->agreementSystemService->step3Process($this->getUser());

            return redirect()->route('agreementSystem.getStep4');
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * initial step4
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getStep4()
    {
        $currentStepName = Lang::get('agreement_system.step') . '[4] ' . Lang::get('progress.registering_available_compatible_genres_and_compatible_areas');
        $currentStep = 4;
        $corpId = \Request::get('mCorp')->id;

        $corpAgreementTempLink = $this->corpAgreeTempLinkRepo->findLatestByCorpId($corpId);
        $corpCategoryList = $this->mCorpCategoriesTempRepository->findAllByCorpIdAndTempIdWithFlag($corpId, $corpAgreementTempLink->id, false, false)->toArray();

        $viewData = [
            'currentStep' => $currentStep,
            'corpCategoryList' => $corpCategoryList,
            'currentStepName' => $currentStepName];
        return view('agreement.system.step4', $viewData);
    }

    /**
     * Genre selection screen - process step4
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function postStep4()
    {
        $this->agreementSystemService->step4Process($this->getUser());
        return redirect()->route('agreementSystem.getStep5');
    }

    /**
     * initial step5
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getStep5()
    {
        $currentStepName = Lang::get('agreement_system.step') . '[5] ' . Lang::get('progress.title_step5_document');
        $currentStep = 5;
        $mCorp = \Request::get('mCorp');
        $corpId = $mCorp->id;
        $listFile = $this->agreementSystemService->getFileList($corpId);
        $corpAgreement = $this->corpAgreementRepository->findByCorpId($corpId);

        $viewData = [
            'currentStep' => $currentStep,
            'currentStepName' => $currentStepName,
            'fileList' => $listFile,
            'companyKind' => $mCorp->corp_kind,
            'corpAgreementId' => $corpAgreement->id];
        return view('agreement.system.step5', $viewData);
    }

    /**
     * get list categories of affiliation in agreement
     *
     * @return string
     * @throws \Throwable
     */
    public function postStep5()
    {
        $this->agreementSystemService->step5Process($this->getUser());
        return redirect()->route('agreementSystem.confirm');
    }

    /**
     * @return string
     * @throws \Throwable
     */
    public function getListCategoryDialog()
    {
        $corpId = \Request::get('mCorp')->id;
        $genreArray = $this->categoryDialogService->getListCategoryDialog($corpId);
        $genreGroupBase = $genreArray['genreGroupBase'];
        $genreGroupIntro = $genreArray['genreGroupIntro'];
        $genreGroupName = $genreArray['genreGroupName'];
        $genreNote = $this->categoryDialogService->getListGenreNote();
        $selectList = MCorpCategoriesTemp::SELECT_LIST;
        return view(
            'agreement.system.category_dialog_body',
            ['genreGroupBase' => $genreGroupBase,
            'genreGroupIntro' => $genreGroupIntro, 'genreGroupName' => $genreGroupName, 'genreNote' => $genreNote,
            'selectList' => $selectList]
        )->render();
    }

    /**
     * insert, update, delete these categories of affiliation in agreement
     *
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postListCategoryDialog(Request $request)
    {
        $data = $request->all();
        $corpId = \Request::get('mCorp')->id;
        $this->categoryDialogService->postListCategoryDialog($data, $corpId);
        return redirect()->route('agreementSystem.getStep3');
    }

    /**
     * get list provisions of affiliation in agreement
     *
     * @return string
     * @throws \Throwable
     */
    public function getListAreaDialog()
    {
        $corpId = \Request::get('mCorp')->id;
        $addressAreaList = $this->mAddress1Service->getListArea($corpId);
        return view('agreement.system.area_dialog_body', ['addressAreaList' => $addressAreaList])->render();
    }

    /**
     * insert, update, delete these provisions of affiliation in agreement
     *
     * @param  Request $request
     * @return string
     * @throws \Throwable
     */
    public function postAreaDialog(Request $request)
    {
        $corpId = \Request::get('mCorp')->id;
        $addressCd = $request->input('addressCd');
        $address1 = $request->input('address1');
        $postList = $this->areaDialogService->getListPostDialog($corpId, $addressCd);
        $viewMode = true;
        return view('agreement.system.post_dialog_body', ['postList' => $postList, 'address1' => $address1, 'addressCd' => $addressCd, 'viewMode' => $viewMode])->render();
    }

    /**
     * get list districts of affiliation in agreement
     *
     * @param  Request $request
     * @return string
     * @throws \Throwable
     */
    public function postViewAreaDialog(Request $request)
    {
        $corpId = \Request::get('mCorp')->id;
        $addressCd = $request->input('addressCd');
        $address1 = $request->input('address1');
        $postList = $this->areaDialogService->getListPostDialog($corpId, $addressCd);
        $viewMode = false;
        return view('agreement.system.post_dialog_body', ['postList' => $postList, 'address1' => $address1, 'addressCd' => $addressCd, 'viewMode' => $viewMode])->render();
    }

    /**
     * insert, update, delete these districts of affiliation in agreement
     *
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postPostDialog(Request $request)
    {
        $data = $request->all();
        $corpId = \Request::get('mCorp')->id;
        $this->areaDialogService->postListPostDialog($data, $corpId);
        return redirect()->route('agreementSystem.getStep3');
    }

    /**
     * delete file
     *
     * @param  Request $request
     * @return $this
     */
    public function deleteFile(Request $request)
    {
        $agreementAttachedFileId = $request->get('agreementAttachedFileId');
        $corpId = \Request::get('mCorp')->id;
        $errors = $this->agreementSystemService->deleteFile($corpId, $agreementAttachedFileId);
        return $this->getStep5()->withErrors($errors);
    }

    /**
     * store file
     *
     * @param  AgreementAttachFileRequest $request
     * @return $this
     */
    public function uploadFile(AgreementAttachFileRequest $request)
    {
        $fileUpload = $request->file('fileUpload');
        $corpAgreementId = $request->get('corpAgreementId');
        $corpId = \Request::get('mCorp')->id;
        $errors = $this->agreementSystemService->uploadFile($corpId, $corpAgreementId, $fileUpload);
        return $this->getStep5()->withErrors($errors);
    }

    /**
     * get file
     *
     * @param  Request $request
     * @return mixed
     */
    public function getThumbnail2(Request $request)
    {
        $agreementFileId = $request->get('agreementFileId');
        $fileAttach = $this->agreementSystemService->getThumbnail2($agreementFileId);
        $response = response()->make($fileAttach['content'], 200);
        $response->header("Content-Type", $fileAttach['contentType']);
        return $response;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getConfirm()
    {
        $currentStepName = Lang::get('agreement_system.step') . '[7] ' . Lang::get('progress.title_step_confirm');
        $currentStep = 7;

        $mCorp = \Request::get('mCorp');
        $corpId = $mCorp->id;

        $arrayProvision = $this->agreementSystemService->findCustomizedAgreementByCorpId($corpId);
        $affiliationInfo = $this->affiliationCorpService->getAffiliationInfo($corpId);
        $data = $this->agreementSystemService->getStep2($mCorp->responsibility);
        $mCorpSubs = $this->affiliationCorpService->getMCorpSubByMCorpId($mCorp->id);
        $corpHolidays = $mCorpSubs['holiday'];
        $prefList = $this->agreementSystemService->getPrefList($corpId);
        $corpAgreementTempLink = $this->corpAgreeTempLinkRepo->findLatestByCorpId($corpId);
        $corpCategoryList = $this->mCorpCategoriesTempRepository->findAllByCorpIdAndTempIdWithFlag($corpId, $corpAgreementTempLink->id, false, false)->toArray();
        $viewData = [
            'currentStep' => $currentStep,
            'currentStepName' => $currentStepName,
            'arrayProvision' => $arrayProvision,
            'affiliationInfo' => $affiliationInfo,
            'mCorp' => $mCorp, 'data' => $data,
            'corpHolidays' => $corpHolidays, 'prefList' => $prefList,
            'corpCategoryList' => $corpCategoryList];
        return view('agreement.system.confirm', $viewData);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postConfirm()
    {
        $corpAgreement = $this->corpAgreementRepository->getFirstByCorpIdAndAgreementId($this->getUser()->affiliation_id, null, true);
        $this->agreementSystemService->stepConfirmProcess($this->getUser());
        return redirect()->route('agreementSystem.getComplete', ['corpAgreementId' => $corpAgreement->id]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getComplete()
    {
        return view('agreement.system.complete');
    }
}
