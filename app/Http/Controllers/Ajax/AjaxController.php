<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use App\Repositories\AutoCommissionCorpRepositoryInterface;
use App\Repositories\ExclusionTimeRepositoryInterface;
use App\Repositories\MAnswerRepositoryInterface;
use App\Repositories\MCategoryRepositoryInterface;
use App\Repositories\MCorpRepositoryInterface;
use App\Repositories\MGeneralSearchRepositoryInterface;
use App\Repositories\MGenresRepositoryInterface;
use App\Repositories\MInquiryRepositoryInterface;
use App\Repositories\MPostRepositoryInterface;
use App\Repositories\MSiteCategoryRepositoryInterface;
use App\Repositories\MSiteGenresRepositoryInterface;
use App\Repositories\MSiteRepositoryInterface;
use App\Repositories\MUserRepositoryInterface;
use App\Repositories\SelectGenrePrefectureRepositoryInterface;
use App\Repositories\SelectGenreRepositoryInterface;
use App\Services\Ajax\AjaxService;
use App\Services\AutoCommissionCorpService;
use App\Services\Commission\CommissionDetailService;
use App\Services\CommissionService;
use App\Services\Credit\CreditService;
use App\Services\DemandInfoService;
use App\Services\GeneralSearchService;
use Auth;
use Exception;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    const BROWSE_COUNT_THRESHOLD = 5;
    const CACHED_STORE_TIME = 2000;
    /**
     * @var MPostRepositoryInterface
     */
    public $mPost;
    /**
     * @var ExclusionTimeRepositoryInterface
     */
    public $exclusionTimeRepo;
    /**
     * @var MGeneralSearchRepositoryInterface
     */
    public $mGeneralSearchRepo;
    /**
     * @var GeneralSearchService
     */
    public $generalSearchServices;
    /**
     * @var DemandInfoService
     */
    public $demandInfoService;
    /**
     * @var SelectGenreRepositoryInterface
     */
    public $selectGenreRepo;
    /**
     * @var SelectGenrePrefectureRepositoryInterface
     */
    public $selectGenrePrefectureRepo;
    /**
     * @var MCorpRepositoryInterface
     */
    public $mCorpRepo;
    /**
     * @var AjaxService
     */
    public $ajaxService;
    /**
     * @var MUserRepositoryInterface
     */
    public $mUserRepo;
    /**
     * @var MSiteGenresRepositoryInterface
     */
    public $mSiteGenresRepository;
    /**
     * @var MSiteCategoryRepositoryInterface
     */
    public $mSiteCategoryRepo;
    /**
     * @var MInquiryRepositoryInterface
     */
    public $mInquiryRepo;
    /**
     * @var MAnswerRepositoryInterface
     */
    public $mAnswerRepo;
    /**
     * @var Request
     */
    protected $request;
    /**
     * @var AutoCommissionCorpService
     */
    protected $autoCommissionCorpService;
    /**
     * @var MGenresRepositoryInterface
     */
    protected $mGenres;
    /**
     * @var CommissionDetailService
     */
    protected $commissionDetailService;
    /**
     * @var MCategoryRepositoryInterface
     */
    protected $mCategory;
    /**
     * @var MSiteRepositoryInterface
     */
    protected $mSiteRepo;
    /**
     * @var AutoCommissionCorpRepositoryInterface
     */
    protected $autoCommissionCorpRepo;
    /**
     * @var CommissionService
     */
    protected $commissionService;
    /**
     * @var CreditService
     */
    protected $creditService;
    /**
     * construct function
     *
     * @param Request                                  $request
     * @param MPostRepositoryInterface                 $mPost
     * @param AutoCommissionCorpService                $autoCommissionCorpService
     * @param MGenresRepositoryInterface               $mGenres
     * @param CommissionDetailService                  $commissionDetailService
     * @param ExclusionTimeRepositoryInterface         $exclusionTimeRepo
     * @param MGeneralSearchRepositoryInterface        $mGeneralSearchRepo
     * @param GeneralSearchService                     $generalSearchServices
     * @param MCategoryRepositoryInterface             $mCategory
     * @param MSiteRepositoryInterface                 $mSiteRepo
     * @param DemandInfoService                        $demandInfoService
     * @param SelectGenreRepositoryInterface           $selectGenreRepo
     * @param SelectGenrePrefectureRepositoryInterface $selectGenrePrefectureRepo
     * @param MCorpRepositoryInterface $mCorpRepo
     * @param AjaxService $ajaxService
     * @param MSiteGenresRepositoryInterface $mSiteGenresRepository
     * @param MUserRepositoryInterface $mUserRepository
     * @param MSiteCategoryRepositoryInterface $mSiteCategoryRepository
     * @param MInquiryRepositoryInterface $mInquiryRepository
     * @param MAnswerRepositoryInterface $mAnswerRepository
     * @param AutoCommissionCorpRepositoryInterface $autoCommissionCorpRepository
     * @param CommissionService $commissionService
     * @param CreditService $creditService
     */
    public function __construct(
        Request $request,
        MPostRepositoryInterface $mPost,
        AutoCommissionCorpService $autoCommissionCorpService,
        MGenresRepositoryInterface $mGenres,
        CommissionDetailService $commissionDetailService,
        ExclusionTimeRepositoryInterface $exclusionTimeRepo,
        MGeneralSearchRepositoryInterface $mGeneralSearchRepo,
        GeneralSearchService $generalSearchServices,
        MCategoryRepositoryInterface $mCategory,
        MSiteRepositoryInterface $mSiteRepo,
        DemandInfoService $demandInfoService,
        SelectGenreRepositoryInterface $selectGenreRepo,
        SelectGenrePrefectureRepositoryInterface $selectGenrePrefectureRepo,
        MCorpRepositoryInterface $mCorpRepo,
        AjaxService $ajaxService,
        MSiteGenresRepositoryInterface $mSiteGenresRepository,
        MUserRepositoryInterface $mUserRepository,
        MSiteCategoryRepositoryInterface $mSiteCategoryRepository,
        MInquiryRepositoryInterface $mInquiryRepository,
        MAnswerRepositoryInterface $mAnswerRepository,
        AutoCommissionCorpRepositoryInterface $autoCommissionCorpRepository,
        CommissionService $commissionService,
        CreditService $creditService
    ) {
        parent::__construct();
        $this->request = $request;
        $this->mPost = $mPost;
        $this->autoCommissionCorpService = $autoCommissionCorpService;
        $this->mGenres = $mGenres;
        $this->exclusionTimeRepo = $exclusionTimeRepo;
        $this->commissionDetailService = $commissionDetailService;
        $this->mGeneralSearchRepo = $mGeneralSearchRepo;
        $this->generalSearchServices = $generalSearchServices;
        $this->mCategory = $mCategory;
        $this->mSiteRepo = $mSiteRepo;
        $this->demandInfoService = $demandInfoService;
        $this->selectGenreRepo = $selectGenreRepo;
        $this->selectGenrePrefectureRepo = $selectGenrePrefectureRepo;
        $this->mCorpRepo = $mCorpRepo;
        $this->ajaxService = $ajaxService;
        $this->mSiteGenresRepository = $mSiteGenresRepository;
        $this->mUserRepo = $mUserRepository;
        $this->mSiteCategoryRepo = $mSiteCategoryRepository;
        $this->mInquiryRepo = $mInquiryRepository;
        $this->mAnswerRepo = $mAnswerRepository;
        $this->autoCommissionCorpRepo = $autoCommissionCorpRepository;
        $this->commissionService = $commissionService;
        $this->creditService = $creditService;
    }

    /**
     * search corp target area
     * @param integer $id
     * @param string $address1
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function searchCorpTargetArea($id = null, $address1 = null)
    {
        $checked = false;
        if ($this->request->input('checked') == true) {
            $checked = true;
        }
        $data = $this->mPost->searchCorpTargetArea($id, $address1);
        return view(
            'ajax.corp_target_area',
            [
            "list" => $data,
            'checked' => $checked
            ]
        );
    }

    /**
     * search address data by zip
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchAddressByZip(Request $request)
    {
        $zipCode = $request->query('zip');
        $zipCode = str_replace('-', '', $zipCode);
        $result = $this->mPost->searchAddressByZip($zipCode);
        return response()->json($result);
    }

    /**
     * get calendar view
     *
     * @param  Request $request
     * @return string
     * @throws \Throwable
     */
    public function getCalenderView(Request $request)
    {
        $year = $request->get('year');
        $month = $request->get('month');
        if (empty($year)) {
            $year = date('Y');
        }
        if (empty($month)) {
            $month = date('m');
        }

        return getDateWeekMonth($year, $month);
    }

    /**
     * get ExclusionTime
     *
     * @param  $pattern
     * @return string
     */
    public function exclusionPattern($pattern = null)
    {
        if (empty($pattern)) {
            return response()->json(
                [
                'result' => '',
                ]
            );
        }
        $day = '';
        $result = '';
        $data = $this->exclusionTimeRepo->findByPattern($pattern);
        $holiday = \Config::get('datacustom.holiday');
        array_walk(
            $holiday,
            function (&$value, $key) {
                $value = trans('divlist.' . $value);
            }
        );
        foreach ($holiday as $key => $val) {
            if (!empty($data['exclusion_day']) && judgeHoliday($data['exclusion_day'], $key)) {
                if (!empty($day)) {
                    $day .= ', ';
                }
                $day .= $val;
            }
        }
        if (!empty($data['exclusion_time_from'])) {
            $result .= trans('genre_detail.exclusion_time') . ' : ';
            $result .= $data['exclusion_time_from'] . trans('common.wavy_seal') . $data['exclusion_time_to'];
        }
        if (!empty($day)) {
            $result .= ' ' . trans('genre_detail.exclusion_date') . ' : ';
            $result .= $day;
        }
        return response()->json(
            [
            'result' => '【' . $result . '】',
            ]
        );
    }

    /**
     * get MGeneralSearch
     *
     * @return string
     * @throws Exception
     */
    public function searchMGeneralSearch()
    {
        $whereConditions = array();
        $orwhereConditions = array();
        try {
            switch (Auth::user()->auth) {
                case "popular":
                    array_push($whereConditions, ['auth_popular', '=', 1]);
                    break;
                case "admin":
                case 'system':
                    array_push($whereConditions, ['auth_admin', '=', 1]);
                    array_push($orwhereConditions, ['auth_popular', '=', 1]);
                    break;
                case 'accounting_admin':
                    array_push($whereConditions, ['auth_accounting_admin', '=', 1]);
                    array_push($orwhereConditions, ['auth_popular', '=', 1]);
                    break;
                case 'accounting':
                    array_push($whereConditions, ['auth_accounting', '=', 1]);
                    array_push($orwhereConditions, ['auth_popular', '=', 1]);
                    break;
                case 'affiliation':
                default:
            }
            if (!empty($whereConditions)) {
                $results = $this->mGeneralSearchRepo->findGeneralSearchAuth($whereConditions, $orwhereConditions);
            } else {
                $results = array();
            }
            return json_encode($results);
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param Request $request
     * @return string
     * @throws \Exception
     */
    public function csvPreview(Request $request)
    {
        try {
            $dataForm = $request->data;
            $dataForm['MGeneralSearch']['definition_name'] = "temporary";
            $dataForm['MGeneralSearch']['auth_admin'] = 1;
            $dataForm['MGeneralSearch']['id'] = "";
            $generalId = $this->generalSearchServices->saveGeneralSearchAjax($dataForm);
            $results = $this->generalSearchServices->getCsvPreview($generalId);
            $this->generalSearchServices->deleteGeneralSearchAll($generalId);
            return json_encode($results);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @extendDes all function for demand detail
     * @author Dung.PhamVan@nashtechglobal.com
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @des get category list by genre_id
     */
    public function getCategoryListByGenreId(Request $request)
    {
        $categories = $this->mCategory->getListStHide($request->get('genreId'));
        return response()->json(
            [
            'code' => 1,
            'data' => $categories,
            'message' => 'get successfully'
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getInquiryItemData(Request $request)
    {
        return response()->json(
            [
            'code' => 1,
            'message' => 'get data successfully',
            'data' => $this->mGenres->findById($request->get('genreId'))
            ]
        );
    }


    /**
     * @param null $demandId
     * @return \Illuminate\Http\JsonResponse
     */
    public function writeBrowse($demandId = null)
    {
        $cached = \Cache::get($demandId);
        $userId = \Auth::user()->user_id;

        if (!$cached) {
            $cached[] = [
                'user_id' => $userId,
                'last_date' => time()
            ];
            \Cache::put($demandId, $cached);
        } else {
            $checked = array_filter($cached, function($user) use ($userId){
                return $user['user_id'] == $userId;
            });

            if(empty($checked)){
                $cached[] = [
                    'user_id' => $userId,
                    'last_date' => time()
                ];
                \Cache::put($demandId, $cached);
            }
        }

        return $this->responseJson(1, 'cached demand ' . $demandId . ' with ' . count(\Cache::get($demandId)) . ' successfully', '');
    }


    /**
     * @param null $demandId
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function countBrowse($demandId = null)
    {
        $cached = \Cache::get($demandId);

        if (!$cached) {
            return $this->responseJson(0, 'get total current user successfully', 0);
        }
        $totalViews = count(array_filter($cached, function($user){
            return $user['last_date'] >= time() - self::BROWSE_COUNT_THRESHOLD;
        }));

        return $this->responseJson(1, 'get total current user successfully', $totalViews);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSiteData(Request $request)
    {
        $contentType = $request->get('content');
        $siteId = $request->get('site_id');

        $return = [
            'code' => 1,
            'message' => 'get data successfully'
        ];
        $data = $contentType == 1 ? $this->mSiteRepo->findById($siteId) : $this->mSiteRepo->getWithCommissionType($siteId);

        return response()->json(
            array_replace(
                $return,
                [
                'data' => $data
                ]
            )
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSelectionSystemList(Request $request)
    {
        $genreId = $request->get('genre_id');
        $address1 = $request->get('address1');

        $selectionSystem = '';

        if (!empty($address1)) {
            $genrePrefecture = $this->selectGenrePrefectureRepo->getByGenreIdAndPrefectureCd(
                [
                'genre_id' => $genreId,
                'address1' => $address1
                ]
            );

            if ($genrePrefecture) {
                $selectionSystem = $genrePrefecture->selection_type;
            }
        }

        if ($selectionSystem === '') {
            $selectGenre = $this->selectGenreRepo->findByGenreId($genreId);

            if ($selectGenre) {
                $selectionSystem = $selectGenre->select_type;
            }
        }

        return $this->responseJson(
            1,
            'get data successfully',
            [
            'selection_system' => $this->renderSelectionSystemOption($selectionSystem),
            'default_value' => $selectionSystem
            ]
        );
    }

    /**
     * @param $code
     * @param $message
     * @param $data
     * @return \Illuminate\Http\JsonResponse
     */
    private function responseJson($code, $message, $data)
    {
        return response()->json(
            [
            'code' => $code,
            'message' => $message,
            'data' => $data
            ]
        );
    }

    /**
     * @param $selection
     * @return array
     */
    private function renderSelectionSystemOption($selection)
    {
        $defaultSelection = getDivValue('selection_type', 'manual_selection');
        $option = [$selection => __('auto_commission_corp.' . config('rits.selection_type')[(int)$selection])];

        return $selection != $defaultSelection ?
            array_replace(
                $option,
                [
                    $defaultSelection => __('auto_commission_corp.' . config('rits.selection_type')['0'])
                ]
            ) : $option;
    }

    /**
     * @param $corpId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getMCorp(Request $request, $corpId, $categoryId = null)
    {
        $mCorp = $this->mCorpRepo->findAllAttribute($corpId);
        $feeData = $this->mCorpRepo->getFeeData($corpId, $categoryId);

        $feeCommission = $request->get('fee_data');
        return view('demand.m_corp_detail', compact('mCorp', 'feeData', 'feeCommission'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTravelExpenses(Request $request)
    {
        $genreId = $request->get('genre_id');
        $address = $request->get('address');
        $data = $this->selectGenrePrefectureRepo->getByGenreIdAndPrefectureCd(
            [
            'genre_id' => $genreId,
            'address1' => $address
            ]
        );
        $businessTripMount = $data ? $data->business_trip_amount : '';

        return $this->responseJson(
            1,
            '',
            [
            'business_trip_amount' => $businessTripMount
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkExistsAutoCommissionCorps(Request $request)
    {
        $siteId = $request->get('site_id');
        $genreId = $request->get('genre_id');
        $categoryId = $request->get('category_id');
        $prefectureCode = $request->get('prefecture_code');

        if (empty($request->except('_token'))) {
            return $this->responseJson(0, 'No data', 0);
        }

        $parseAddress = $this->ajaxService->parseData($prefectureCode);
        $jisCd = $this->mPost->getTargetArea($parseAddress);

        if ($jisCd == null) {
            return $this->responseJson(0, 'No data', 0);
        }
        $conditions = [
            'category_id' => $categoryId, 'jis_cd' => $prefectureCode
        ];
        $commissionCorps = $this->commissionService->getCorpList($conditions, '', false);

        $newCommissionCorps = $this->commissionService->getCorpList($conditions, 'new', false);

        $autoCommissionCorp = $this->autoCommissionCorpRepo->getByCategoryGenreAndPrefCd(
            ['pref_cd' => $prefectureCode, 'category_id' => $categoryId]
        );
        $result = 0;
        foreach ($autoCommissionCorp as $key => $value) {
            $targetCommissionCorp = null;
            foreach ($newCommissionCorps as $key => $newCorps) {
                if ($newCorps->id == $value->corp_id) {
                    $targetCommissionCorp = $newCorps;
                    break;
                }
            }
            $this->ajaxService->checkTargetCommissionCorp($commissionCorps, $targetCommissionCorp, $value);
            if ($targetCommissionCorp === null) {
                continue;
            }
            $resultCredit = Config::get('rits.CREDIT_NORMAL');
            $resultCredit = $this->getCredit($siteId, Config::get('rits.credit_check_exclusion_site_id'), $targetCommissionCorp['id'], $genreId, $resultCredit);

            if ($resultCredit == Config::get('rits.CREDIT_DANGER')) {
                continue;
            }
            $resultCrossSiteFlg = $this->mSiteRepo->getCrossSiteFlg(1);
            if (in_array($siteId, $resultCrossSiteFlg)) {
                continue;
            }
            if (in_array($siteId, (config('rits.arrSiteId')))) {
                continue;
            }
            $result = $value['AutoCommissionCorp']['process_type'];
            break;
        }

        return $this->responseJson(1, '', ['data' => $result]);
    }

    /**
     * @param $val
     * @param $arr
     * @param $targetId
     * @param $genreId
     * @param $result
     * @return int|string
     */
    private function getCredit($val, $arr, $targetId, $genreId, $result)
    {
        if (!in_array($val, $arr)) {
            $result = $this->creditService->checkCredit($targetId, $genreId, false, true);
        }
        return $result;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGenreListBySiteId(Request $request)
    {
        $siteId = $request->get('site_id');
        $data = $this->mSiteGenresRepository->getGenreBySiteStHide($siteId);
        return $this->responseJson(1, 'ok', $data);
    }

    /**
     * @des End functions for demand detail
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMsText(Request $request)
    {
        $result = ['message_failure' => __('commissioncorresponds.message_failure')];
        $res = $request->all();

        if ($res) {
            foreach ($res as $field => $vals) {
                foreach ($vals as $val) {
                    $key = str_replace('.', '_', $val);
                    $result[$field][$key] = __($val);
                }
            }
        }

        return response()->json($result);
    }

    /**
     * @param null $date
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getTaxRate($date = null)
    {
        $result = '';
        if (!empty($date)) {
            $date = str_replace("-", "/", $date);

            if (preg_match(
                '/^([1-9][0-9]{3})\/(0[1-9]{1}|1[0-2]{1})\/(0[1-9]{1}|[1-2]{1}[0-9]{1}|3[0-1]{1})$/',
                $date
            )
            ) {
                $result = $this->commissionDetailService->getTaxRates($date);
            }
        }

        return view(
            'ajax.tax_rate',
            [
            'tax_rate' => $result
            ]
        );
    }

    /**
     * @param null $date
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTaxRateOnly($date = null)
    {
        $result = '';

        if (!empty($date)) {
            $date = str_replace("-", "/", $date);

            if (preg_match(
                '/^([1-9][0-9]{3})\/(0[1-9]{1}|1[0-2]{1})\/(0[1-9]{1}|[1-2]{1}[0-9]{1}|3[0-1]{1})$/',
                $date
            )
            ) {
                $result = $this->commissionDetailService->getTaxRates($date);
            }
        }

        return response()->json($result);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function calculateBillInfo(Request $request)
    {
        $result = [];

        try {
            $data = $request->all();

            $result = $this->commissionDetailService->calcBillPrice(
                $data['CommissionInfo']['id'],
                $data['CommissionInfo']['commission_status'],
                !empty($data['CommissionInfo']['complete_date']) ? $data['CommissionInfo']['complete_date'] : null,
                isset($data['CommissionInfo']['construction_price_tax_exclude']) ? $data['CommissionInfo']['construction_price_tax_exclude'] : null
            );
        } catch (Exception $e) {
            logger($e->getMessage());
            $result['status'] = 500;
        }

        return response()->json($result);
    }

    /**
     * @param null $corpId
     * @param null $address1
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function searchTargetArea($corpId = null, $address1 = null)
    {
        $data = $this->mPost->searchTargetArea($corpId, $address1);
        return view(
            'ajax.target_area',
            [
            "list" => $data,
            ]
        );
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserList()
    {
        return $this->responseJson(1, 'get data successfully', $this->mUserRepo->getListUserForDropDown());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDefaultFee(Request $request)
    {
        $categoryId = $request->get('category_id');
        $defaultFee = $this->mCategory->getDefaultFee($categoryId);

        return $this->responseJson(1, 'get data successfully', ['default_fee' => $defaultFee]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCategoryList2(Request $request)
    {
        $categoryList = $this->mSiteGenresRepository->getMSiteGenresDropDownBySiteId($request->get('site_id'));

        return $this->responseJson(
            1,
            '',
            [
            'category_list' => $categoryList,
            'site_id' => $request->get('site_id')
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCommissionMaxLimit(Request $request)
    {
        $default = array(
            'manual_selection_limit' => 1,
            'auction_selection_limit' => 1
        );
        $mSite = $this->mSiteRepo->getSelectionLimit($request->get('m_site_id'));
        return $this->responseJson(
            1,
            '',
            array_merge($default, $mSite)
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function attentionData(Request $request)
    {
        $genreId = empty($request->get('genre_id')) ? null : $request->get('genre_id');
        $mGenreData = $this->mGenres->find($genreId);
        $attention = $mGenreData["MGenre"]['attention'];
        $data = str_replace(array("\r\n", "\r", "\n"), "<br />", $attention);
        return $this->responseJson(1, 'ok', $data);
    }

    /**
     * @param null $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function inquiryList($category = null)
    {
        $result = $this->mInquiryRepo->getListInquiryByCategory($category);
        if (count($result) > 0) {
            foreach ($result as &$arr) {
                $arr['answer'] = $this->mAnswerRepo->dropDownAnswer($arr['id'], true);
            }
        }
        return view('ajax.inquiry_list', ['data' => $result]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function commissionChange(Request $request)
    {
        $num = empty($request->get('num')) ? null : $request->get('num');
        $categoryId = empty($request->get('category_id')) ? null : $request->get('category_id');
        $corpId = empty($request->get('cord_id')) ? null : $request->get('cord_id');
        $data = $this->mCorpRepo->getCommissionChangeByCategoryIdAndCorpId($num, $categoryId, $corpId);
        return $this->responseJson(1, 'get data successfully', $data);
    }
}
