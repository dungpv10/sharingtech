<?php

namespace App\Http\Controllers\Auction;

use App\Http\Controllers\Controller;
use App\Http\Requests\AuctionRefusalRequest;
use App\Models\AuctionAgreementItem;
use App\Models\AuctionAgreementLink;
use App\Models\DemandInfo;
use App\Models\MTaxRate;
use App\Repositories\AffiliationAreaStatRepositoryInterface;
use App\Repositories\BillRepositoryInterface;
use App\Repositories\CommissionInfoRepositoryInterface;
use App\Repositories\MCategoryRepositoryInterface;
use App\Repositories\MCorpCategoryRepositoryInterface;
use App\Repositories\MCorpRepositoryInterface;
use App\Repositories\MSiteRepositoryInterface;
use App\Repositories\VisitTimeRepositoryInterface;
use Auth;
use Aws\CloudFront\Exception\Exception;
use Illuminate\Http\Request;
use App\Services\AuctionService;
use App\Repositories\AuctionInfoRepositoryInterface;
use App\Repositories\MTimeRepositoryInterface;
use App\Repositories\DemandInfoRepositoryInterface;
use App\Repositories\MGenresRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Models\MItem;

class AuctionController extends Controller
{
    /**
     * @var AuctionInfoRepositoryInterface
     */
    protected $auctionInfoRepository;
    /**
     * @var MTimeRepositoryInterface
     */
    protected $mTimeRepository;
    /**
     * @var DemandInfoRepositoryInterface
     */
    protected $demandInfoRepository;
    /**
     * @var MGenresRepositoryInterface
     */
    protected $mGenresRepository;
    /**
     * @var MSiteRepositoryInterface
     */
    protected $mSiteRepository;
    /**
     * @var VisitTimeRepositoryInterface
     */
    protected $visitTimeRepository;
    /**
     * @var MCorpCategoryRepositoryInterface
     */
    protected $mCorpCategoryRepository;
    /**
     * @var MCorpRepositoryInterface
     */
    protected $mCorpRepository;
    /**
     * @var MCategoryRepositoryInterface
     */
    protected $mCategoryRepository;
    /**
     * @var AffiliationAreaStatRepositoryInterface
     */
    protected $affiliationAreaStatRepo;
    /**
     * @var CommissionInfoRepositoryInterface
     */
    protected $commissionInfoRepo;
    /**
     * @var DemandInfo
     */
    protected $mDemandInfo;
    /**
     * @var BillRepositoryInterface
     */
    protected $billInfoRepository;
    /**
     * @var AuctionAgreementLink
     */
    protected $mAuctionAgreementLink;
    /**
     * @var AuctionService
     */
    protected $auctionService;

    /**
     * Instantiate a new controller instance.
     *
     * @param AuctionInfoRepositoryInterface         $auctionInfoRepository
     * @param MTimeRepositoryInterface               $mTimeRepository
     * @param DemandInfoRepositoryInterface          $demandInfoRepository
     * @param MGenresRepositoryInterface             $mGenresRepository
     * @param MSiteRepositoryInterface               $mSiteRepository
     * @param VisitTimeRepositoryInterface           $visitTimeRepository
     * @param MCorpCategoryRepositoryInterface       $mCorpCategoryRepository
     * @param MCategoryRepositoryInterface           $mCategoryRepository
     * @param AffiliationAreaStatRepositoryInterface $affiliationAreaStatRepo
     * @param CommissionInfoRepositoryInterface      $commissionInfoRepo
     * @param DemandInfo                             $mDemandInfo
     * @param BillRepositoryInterface                $billInfoRepository
     * @param AuctionAgreementLink                   $mAuctionAgreementLink
     * @param MCorpRepositoryInterface               $mCorpRepository
     * @param AuctionService                         $auctionService
     */
    public function __construct(
        AuctionInfoRepositoryInterface $auctionInfoRepository,
        MTimeRepositoryInterface $mTimeRepository,
        DemandInfoRepositoryInterface $demandInfoRepository,
        MGenresRepositoryInterface $mGenresRepository,
        MSiteRepositoryInterface $mSiteRepository,
        VisitTimeRepositoryInterface $visitTimeRepository,
        MCorpCategoryRepositoryInterface $mCorpCategoryRepository,
        MCategoryRepositoryInterface $mCategoryRepository,
        AffiliationAreaStatRepositoryInterface $affiliationAreaStatRepo,
        CommissionInfoRepositoryInterface $commissionInfoRepo,
        DemandInfo $mDemandInfo,
        BillRepositoryInterface $billInfoRepository,
        AuctionAgreementLink $mAuctionAgreementLink,
        MCorpRepositoryInterface $mCorpRepository,
        AuctionService $auctionService
    ) {
        parent::__construct();
        $this->auctionInfoRepository = $auctionInfoRepository;
        $this->mTimeRepository = $mTimeRepository;
        $this->demandInfoRepository = $demandInfoRepository;
        $this->mGenresRepository = $mGenresRepository;
        $this->mSiteRepository = $mSiteRepository;
        $this->visitTimeRepository = $visitTimeRepository;
        $this->mCorpCategoryRepository = $mCorpCategoryRepository;
        $this->mCategoryRepository = $mCategoryRepository;
        $this->affiliationAreaStatRepo = $affiliationAreaStatRepo;
        $this->commissionInfoRepo = $commissionInfoRepo;
        $this->mDemandInfo = $mDemandInfo;
        $this->billInfoRepository = $billInfoRepository;
        $this->mAuctionAgreementLink = $mAuctionAgreementLink;
        $this->mCorpRepository = $mCorpRepository;
        $this->auctionService = $auctionService;
    }

    /**
     * Show page index
     *
     * @param  Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $data = $request->all();
        return self::indexOrSearchAuction($data);
    }

    /**
     * show page index or search
     *
     * @param  array $dataRequest
     * @param  array $dataSession
     * @return Response
     */
    public function indexOrSearchAuction($dataRequest = null, $dataSession = null)
    {
        $orderBy = AuctionService::formatOptionSearch($dataRequest);
        $user = Auth::user();
        $auctionAlreadyData = [];
        $calendarEventData = [];
        if (AuctionService::isRole($user->auth, ['affiliation'])) {
            $auctionAlreadyData = $this->auctionInfoRepository->getAuctionAlreadyList($orderBy, $user->affiliation_id);
            $calendarEventData = AuctionService::getCalendarEventData($auctionAlreadyData);
        }
        $calendarEventData = json_encode($calendarEventData);
        $addressDisclosure = $this->mTimeRepository->getByItemCategory('address_disclosure');
        $telDisclosure = $this->mTimeRepository->getByItemCategory('tel_disclosure');
        $supportMessageTime = $this->mTimeRepository->getSupportMessageTime();
        $orderBy = AuctionService::formatOptionSearchUp($dataRequest);
        $results = $this->demandInfoRepository->searchDemandInfoList($dataSession, $orderBy);
        $listMGeners = $this->mGenresRepository->getList(true);
        $dataSort = AuctionService::getDataSort($dataRequest);
        $display = isset($dataSession['display']) && $dataSession['display'] == 1 ? false : true;
        $isRoleAffiliation = AuctionService::isRole($user['auth'], ['affiliation']);
        $buildingType = MItem::BUILDING_TYPE;
        return view(
            'auction.search',
            compact(
                'auctionAlreadyData',
                'calendarEventData',
                'results',
                'addressDisclosure',
                'telDisclosure',
                'supportMessageTime',
                'listMGeners',
                'dataSort',
                'isRoleAffiliation',
                'display',
                'buildingType'
            )
        );
    }

    /**
     * Show page search
     *
     * @param  Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        $dataSession = Session::get(self::$sessionKeyForAuctionSearch);
        $dataRequest = $request->all();
        return self::indexOrSearchAuction($dataRequest, $dataSession);
    }

    /**
     * Search auction
     *
     * @param  Request $request
     * @return Response|\Illuminate\Http\RedirectResponse
     */
    public function postSearch(Request $request)
    {
        try {
            $dataRequest = $request->all();
            Session::forget(self::$sessionKeyForAuctionSearch);
            Session::push(self::$sessionKeyForAuctionSearch, $dataRequest);
            return self::indexOrSearchAuction(null, $dataRequest);
        } catch (Exception $ex) {
            $request->session()->flash('alert-danger', trans('agreement.update_error'));
            return Redirect::back();
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteAuction(Request $request)
    {
        try {
            $dataRequest = $request->all();
            if ($this->auctionInfoRepository->deleteItemByListId($dataRequest)) {
                $request->session()->flash('alert-success', trans('auction.message_success'));
            } else {
                $request->session()->flash('alert-danger', trans('auction.message_failure'));
            }
            return Redirect::back();
        } catch (Exception $ex) {
            $request->session()->flash('alert-danger', trans('auction.message_failure'));
            return Redirect::back();
        }
    }

    /**
     * update commission
     *
     * @param  Request $request
     * @param  null    $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function support(Request $request, $id = null)
    {
        try {
            if ($request->isMethod('post')) {
                $data = $this->parsingData($request['data']);
                $demandData = $this->demandInfoRepository->getDemandById($data['demand_id'])->toarray();

                if (strtotime($demandData['auction_deadline_time']) <= strtotime(date('Y-m-d H:i:s'))) {
                    return view('auction.support', ['screen' => config('constant.refusal.support_past_time')]);
                }

                $resultEdit = $this->auctionService->editSupport($data);
                if ($resultEdit) {
                    $addressDisclosure = $this->mTimeRepository->getByItemCategory('address_disclosure');
                    $telDisclosure     = $this->mTimeRepository->getByItemCategory('tel_disclosure');
                    $auctionFee        = $this->auctionInfoRepository->getAuctionFee($data['id']);
                    $auctionProvisions = $this->auctionService->getItemAuctionAgreement();
                    $auctionProvisions = $this->auctionService->formatAuctionPolicy($auctionProvisions);
                    $popupStopFlag     = $this->auctionService->isPopupStopFlag(Auth::user()->affiliation_id);
                    $this->auctionService->updateAccumulatedInfoRegistDate($data['demand_id'], Auth::user()->affiliation_id);
                    if (!$popupStopFlag) {
                        return view(
                            'auction.component.modal-support',
                            ['address_disclosure'=> $addressDisclosure,
                            'demand_data' => $demandData, 'tel_disclosure' => $telDisclosure,
                            'auction_fee' => $auctionFee, 'auction_provisions'=> $auctionProvisions,
                            'screen'      => config('constant.refusal.support_already')
                            ]
                        );
                    }

                    return;
                }
                //update fail
                return view('auction.support', ['screen' => config('constant.refusal.update_fail')]);
            }
            //Return view support
            $auctionId = (int)$id;

            $commissionData = $this->auctionService->getCommissionDataSupport($auctionId);
            if (!empty($commissionData)) {
                return view('auction.support', ['results' => $commissionData, 'screen' => config('constant.refusal.deal_already')]);
            }

            $data = $this->auctionInfoRepository->getAuctionInfoDemandInfo($auctionId);

            if (strtotime($data->auction_deadline_time) <= strtotime(date('Y-m-d H:i:s'))) {
                return view('auction.support', ['screen' => config('constant.refusal.support_limit')]);
            }

            $data = $data->toarray();
            $visitList = $this->visitTimeRepository->findAllByDemandId($data['demand_id']);

            $auctionFee = $this->auctionInfoRepository->getAuctionFee($auctionId);
            $auctionProvisions = $this->auctionService->getItemAuctionAgreement();
            $auctionProvisions = $this->auctionService->formatAuctionPolicy($auctionProvisions);

            return view(
                'auction.support',
                ['data'=> $data, 'visit_list' => $visitList,
                'auction_fee' => $auctionFee, 'auction_provision' => $auctionProvisions, 'screen' => 5]
            );
        } catch (\Exception $exception) {
            $message = ['error' => trans('support.updateFail')];
            DB::rollback();
            Log::error($exception);
            return Redirect::back()->with($message);
        }
    }

    /**
     * update flag for m_corps
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function complete(Request $request)
    {
        $popupStopFlg = $request->get('popup_stop_flg');
        try {
            if ($popupStopFlg) {
                $this->auctionService->updatePopupStopFlg(Auth::user()->affiliation_id);
            }
            return response()->json(
                [
                'status' => 200,
                ]
            );
        } catch (\Exception $exception) {
            Log::info($exception);
            return response()->json(
                [
                'status' => 500,
                ]
            );
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateJbrStatus(Request $request)
    {
        $corpId = $request->get('corpId');
        try {
            $data = [
                'jbr_available_status' => 2
            ];
            $this->mCorpRepository->updateCorp($corpId, $data);
            return response()->json([
                'status' => 200,
                ]);
        } catch (\Exception $exception) {
            Log::info($exception);
            return response()->json(
                [
                'status' => 500,
                ]
            );
        }
    }


    /**
     * @param $data
     * @return mixed
     */
    private function parsingData($data)
    {
        $data['id']         = (int)$data['id'];
        $data['demand_id']  = (int)$data['demand_id'];
        $data['corp_id']    = (int)$data['corp_id'];
        $data['responders'] = isset($data['responders']) ? $data['responders'] : '';
        $data['agreement_check']      = isset($data['agreement_check']) ? (int)$data['agreement_check'] : 0;
        $data['demand_status']        = isset($data['demand_status']) ? (int)$data['demand_status'] : 0;
        $data['site_id']              = isset($data['site_id']) ? (int)$data['site_id'] : 0;
        $data['jbr_available_status'] = isset($data['jbr_available_status']) ? (int)$data['jbr_available_status'] : 0;
        if (isset($data['visit_time_id'])) {
            $data['visit_time_id'] = (int)$data['visit_time_id'];
        }

        return $data;
    }

    /**
     * @param $data
     * @return bool
     */
    private function editSupport($data)
    {
        try {
            $this->auctionInfoRepository->updateAuctionInfo($data);

            $demandData   = $this->demandInfoRepository->getDemandById($data['demand_id'])->toarray();

            $resultCategoryData = $this->mCorpCategoryRepository->findByCorpIdAndGenreIdAndCategoryId(
                $data['corp_id'],
                $demandData['genre_id'],
                $demandData['category_id']
            );

            $categoryData = isset($resultCategoryData) ? $resultCategoryData->toarray() : [];
            if (count($categoryData) == 0) {
                return false;
            }

            $createInfoCommission = $this->getInfoCommissionToCreate($categoryData, $data, $demandData);

            if (!$this->auctionService->checkEmptyCommissionFeeRate($createInfoCommission)
                || !$this->auctionService->checkCommissionIntroduce($createInfoCommission)
                || !$this->auctionService->checkCommissionStatusComplete($createInfoCommission)
                || !$this->auctionService->checkCommissionStatusOrderFail($createInfoCommission)
            ) {
                return false;
            }
            DB::beginTransaction();
            //Edit commission
            $newCommission = $this->commissionInfoRepo->save($createInfoCommission)->toarray();
            //Edit demand info
            $this->updateDemandInfo($data['demand_id']);
            //Create bill info
            $auctionFee = $this->auctionInfoRepository->getAuctionFee($data['id']);
            $this->createBillInfo($newCommission, $data, $auctionFee);
            //Create auction agreement link
            if ($auctionFee > 0) {
                $this->createAuctionAgreementLink($newCommission, $data, $auctionFee);
            }
            DB::commit();
            return true;
        } catch (\Exception $exception) {
            DB::rollback();
            return false;
        }
    }

    /**
     * @param $newCommission
     * @param $data
     * @param $auctionFee
     */
    private function createAuctionAgreementLink($newCommission, $data, $auctionFee)
    {
        $userLoginId = Auth::user()->user_id;
        $auctionLink = $this->mAuctionAgreementLink;

        $auctionLink->auction_id           = $data['id'];
        $auctionLink->corp_id              = $data['corp_id'];
        $auctionLink->auction_agreement_id = 1;
        $auctionLink->demand_id            = $data['demand_id'];
        $auctionLink->commission_id        = $newCommission['id'];
        $auctionLink->auction_fee          = $auctionFee;
        $auctionLink->agreement_check      = $data['agreement_check'];
        $auctionLink->responders           = $data['responders'];
        $auctionLink->modified             = date(config('constant.FullDateTimeFormat'), time());
        $auctionLink->created              = date(config('constant.FullDateTimeFormat'), time());
        $auctionLink->created_user_id      = $userLoginId;
        $auctionLink->modified_user_id     = $userLoginId;
        $auctionLink->save();
    }

    /**
     * @return array
     */
    private function getTaxRate()
    {
        return MTaxRate::where('start_date', '<=', date('Y-m-d H:i:s'))
            ->orWhere(
                [
                ['end_date' , '=', ''],
                ['end_date', '>=', date('Y-m-d H:i:s')]
                ]
            )->first()->toarray();
    }

    /**
     * @param $newCommission
     * @param null          $data
     * @param null          $auctionFee
     */
    private function createBillInfo($newCommission, $data = null, $auctionFee = null)
    {
        if ($newCommission['commission_type'] == getDivValue('commission_type', 'package_estimate')) {
            $billInfo = $this->getBillInfoToCreate($newCommission, true);
            $this->billInfoRepository->createBill($billInfo);
        }
        if ($auctionFee> 0) {
            $billInfo = $this->getBillInfoToCreate($newCommission, false, $data, $auctionFee);
            $this->billInfoRepository->createBill($billInfo);
        }
    }

    /**
     * @param $demandId
     */
    private function updateDemandInfo($demandId)
    {
        $this->mDemandInfo->where('id', $demandId)->update(
            [
            'push_stop_flg'  => 1,
            'demand_status'  => getDivValue('demand_status', 'information_sent'),
            'modified' => date(config('constant.FullDateTimeFormat'), time()),
            'modified_user_id' => Auth::user()->user_id
            ]
        );
    }

    /**
     * @param $newCommission
     * @param $hasTax
     * @param null          $data
     * @param null          $auctionFee
     * @return array
     */
    private function getBillInfoToCreate($newCommission, $hasTax, $data = null, $auctionFee = null)
    {
        $dataSave = [];
        $mTax = $this->getTaxRate();
        $taxRate = count($mTax)>0 ? floatval($mTax['tax_rate']) : 0;

        if ($hasTax) {
            $dataSave['commission_id']       = $newCommission['id'];
            $dataSave['demand_id']           = $newCommission['demand_id'];
            $dataSave['bill_status']         = getDivValue('bill_status', 'not_issue');
            $dataSave['comfirmed_fee_rate']  = 100;
            $dataSave['fee_target_price']    = isset($newCommission['corp_fee'])? $newCommission['corp_fee'] : 0;
            $dataSave['fee_tax_exclude']     = isset($newCommission['corp_fee'])? $newCommission['corp_fee'] : 0;
            $dataSave['tax']                 = floor($dataSave['fee_tax_exclude'] * $taxRate);
            $dataSave['total_bill_price']    = $dataSave['fee_tax_exclude'] + $dataSave['tax'];
            $dataSave['fee_payment_price']   = 0;
            $dataSave['fee_payment_balance'] = $dataSave['fee_tax_exclude'] + $dataSave['tax'];
            return $dataSave;
        }

        $dataSave['commission_id']       = $newCommission['id'];
        $dataSave['demand_id']           = $data['demand_id'];
        $dataSave['bill_status']         = 1;
        $dataSave['comfirmed_fee_rate']  = 100;
        $dataSave['fee_target_price']    = $auctionFee;
        $dataSave['fee_tax_exclude']     = $auctionFee;
        $dataSave['tax']                 = intval($dataSave['fee_tax_exclude'] * $taxRate);
        $dataSave['total_bill_price']    = $dataSave['fee_tax_exclude'] + $dataSave['tax'];
        $dataSave['fee_payment_price']   = 0;
        $dataSave['fee_payment_balance'] = $dataSave['total_bill_price'];
        $dataSave['auction_id']          = $data['id'];
        return $dataSave;
    }

    /**
     * @param $categoryData
     * @param $data
     * @param $demandData
     * @return array
     */
    private function getInfoCommissionToCreate($categoryData, $data, $demandData)
    {
        $edit = [];

        if ($categoryData['corp_commission_type'] != 2) {
            $commissionType   = getDivValue('commission_type', 'normal_commission');
            $commissionStatus = getDivValue('construction_status', 'progression');
        } else {
            $commissionType   = getDivValue('commission_type', 'package_estimate');
            $commissionStatus = getDivValue('construction_status', 'introduction');
            $categoryData['order_fee']      = $categoryData['introduce_fee'];
            $categoryData['order_fee_unit'] = 0;

            $edit['confirmd_fee_rate']   = 100;
            $edit['commission_fee_rate'] = 100;
            $edit['complete_date']       = date('Y/m/d');
        }

        if (empty($categoryData['order_fee']) || is_null($categoryData['order_fee_unit'])) {
            $mcCategoryData = $this->mCategoryRepository->getFeeData($demandData['category_id']);

            if (empty($mcCategoryData)) {
                $mcCategoryData = array('category_default_fee' => '', 'category_default_fee_unit' => '');
            } else {
                $mcCategoryData = $mcCategoryData->toarray();
            }
            $categoryData['order_fee']      = $mcCategoryData['category_default_fee'];
            $categoryData['order_fee_unit'] = $mcCategoryData['category_default_fee_unit'];
            $categoryData['note'] = '';
        }

        $edit['demand_id']                     = $data['demand_id'];
        $edit['corp_id']                       = $data['corp_id'];
        $edit['commit_flg']                    = 1;
        $edit['commission_type']               = $commissionType;
        $edit['commission_status']             = $commissionStatus;
        $edit['unit_price_calc_exclude']       = 0;
        $edit['commission_note_send_datetime'] = date('Y/m/d H:i', time());
        $edit['commission_visit_time_id']      = isset($data['visit_time_id']) ? $data['visit_time_id'] : 0;

        if ($categoryData['order_fee_unit'] == 0) {
            $edit['corp_fee'] = $categoryData['order_fee'];
        } elseif ($categoryData['order_fee_unit'] == 1) {
            $edit['commission_fee_rate'] = $categoryData['order_fee'];
        }
        $edit['business_trip_amount'] = !empty($data['business_trip_amount']) ? $data['business_trip_amount'] : 0 ;

        $affiliationAreaData = $this->affiliationAreaStatRepo->findByCorpIdAndGenerIdAndPrefecture($data['corp_id'], $demandData['genre_id'], $demandData['address1']);
        $affiliationAreaData = $this->changeTypeCollectionModel($affiliationAreaData);

        $edit['select_commission_unit_price_rank'] = $this->getSelectCommissionUnitPriceRank($affiliationAreaData['commission_unit_price_rank']);
        $edit['select_commission_unit_price'] = $this->getSelectCommissionUnitPrice($affiliationAreaData['commission_unit_price_category']);
        $edit['order_fee_unit'] = $categoryData['order_fee_unit'];

        return $edit;
    }

    /**
     * @param $data
     * @return string
     */
    private function getSelectCommissionUnitPriceRank($data)
    {
        return !empty($data) ? $data : '-' ;
    }

    /**
     * @param $data
     * @return int
     */
    private function getSelectCommissionUnitPrice($data)
    {
        return !empty($data) ? $data : 0 ;
    }

    /**
     * @param $data
     * @return null
     */
    private function changeTypeCollectionModel($data)
    {
        if (!empty($data)) {
            return $data->toarray();
        }
        return null;
    }

    /**
     * @return mixed
     */
    private function getItemAuctionAgreement()
    {
        return AuctionAgreementItem::find(1)->item;
    }

    /**
     * @param $auctionId
     * @return mixed|null
     */
    private function getCommissionData($auctionId)
    {
        $auction = $this->auctionInfoRepository->getAuctionById($auctionId);
        $demand  = $this->demandInfoRepository->getDemandById($auction->demand_id);
        $max = $this->mSiteRepository->findMaxLimit($demand);

        $currentNum = $this->auctionInfoRepository->findCurrentCommitNum($auctionId);

        if ($currentNum >= $max) {
            $lastCommission = $this->auctionInfoRepository->findLastCommissionCreated($auctionId);
            return $lastCommission;
        }
        return null;
    }

    /**
     * Function refusal for auction
     *
     * @param  $auctionId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function refusal($auctionId)
    {
        $commissionData = $this->auctionService->getCommissionData($auctionId);
        if (empty($commissionData)) {
            $data = $this->auctionInfoRepository->getAuctionInfoDemandInfo($auctionId);
            if (strtotime($data->auction_deadline_time) <= strtotime(date('Y-m-d H:i:s'))) {
                // Correspondence deadline has passed
                $demandStatus = null;
                $screen = config('constant.refusal.support_limit');
            } else {
                // If can deal with the case and no other trader exists
                $demandStatus = $data->demand_status;
                $screen = config('constant.refusal.deal_already');
            }
        } else {
            // When other companies have responded
            $demandStatus = null;
            $screen = config('constant.refusal.support_already');
            $commissionData = dateTimeFormat($commissionData, config('constant.datetime_format_jp'));
        }
        $data = [
            'demandStatus' => $demandStatus,
            'auctionId' => $auctionId,
            'commissionData' => $commissionData,
            'screen' => $screen
        ];
        return $data;
    }

    /**
     * @param AuctionRefusalRequest $request
     * @param $auctionId
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function postRefusal(AuctionRefusalRequest $request, $auctionId)
    {
        $data['refusal'] = $request->all();

        if ($data['refusal']['other_contents'] > 1000) {
            return redirect()->back()->withInput();
        } elseif (empty($data['refusal']['other_contents'])) {
            $data['refusal']['other_contents'] = '';
        }

        $data['auctionInfo'] = ['id' => $auctionId, 'refusal_flg' => 1];

        $auction = $this->auctionInfoRepository->find($auctionId);

        if ($auction->refusal_flg != 1) {
            if ($this->auctionService->editRefusal($data)) {
                $this->auctionService->updateAccumulatedInfoRefusalDate($auction->demand_id, $auction->corp_id);
            }
        }

        return redirect()->route('auction.index');
    }

    /**
     * show page proposal
     *
     * @param  integer $demandId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function proposal($demandId)
    {
        $user = Auth::user();
        if (AuctionService::isRole($user->auth, ['affiliation'])) {
            $auctionInfo = $this->auctionInfoRepository->getFirstByDemandIdAndCorpId($demandId, $user->affiliation_id);
            if (empty($auctionInfo->first_display_time)) {
                $auctionInfo->first_display_time = date('Y-m-d H:i');
                $this->auctionInfoRepository->save($auctionInfo);
            }
        }
        $demandInfo = $this->demandInfoRepository->find($demandId);
        return view('auction.proposal', compact('demandInfo'));
    }

    /**
     * ajax proposal json
     *
     * @param  integer $demandId
     * @return json
     */
    public function proposalJson($demandId)
    {
        $user = Auth::user();
        if (AuctionService::isRole($user->auth, ['affiliation'])) {
            $auctionInfo = $this->auctionInfoRepository->getFirstByDemandIdAndCorpId($demandId, $user->affiliation_id);
            if (empty($auctionInfo->first_display_time)) {
                $auctionInfo->first_display_time = date('Y-m-d H:i');
                $this->auctionInfoRepository->save($auctionInfo);
            }
        }
        $demandInfo = $this->demandInfoRepository->find($demandId);
        $result['id'] = $demandId;
        $result['contents'] = $demandInfo->contents;
        return response()->json($result);
    }
}
