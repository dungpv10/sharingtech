<?php

namespace App\Http\Controllers\Commission;

use App\Http\Controllers\Controller;

use App\Repositories\CommissionCorrespondsRepositoryInterface;
use App\Repositories\CommissionInfoRepositoryInterface;
use App\Repositories\ApprovalRepositoryInterface;
use App\Repositories\CommissionAppRepositoryInterface;
use App\Repositories\MUserRepositoryInterface;

use App\Http\Requests\AutoCommissionCorrespondsFormRequest;
use App\Http\Requests\CommissionApprovalRequest;
use App\Http\Requests\CommissionSearchRequest;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Exception;

use App\Services\CommissionService;
use App\Services\Commission\CommissionDetailService;
use App\Services\Commission\CommissionSupportService;
use App\Services\Commission\CommissionDataService;
use App\Services\Commission\CommissionFileService;
use App\Services\Commission\CommissionAppService;
use App\Services\Commission\CommissionSearchService;
use App\Services\Commission\CommissionExportCsvService;
use App\Services\Commission\CommissionApprovalService;
use App\Repositories\Eloquent\MItemRepository;
use App\Services\Commission\CommissionCorrespondService;

class CommissionController extends Controller
{
    /**
     * @var MUserRepositoryInterface
     */
    protected $mUserRepository;
    /**
     * @var CommissionInfoRepositoryInterface
     */
    protected $commissionInfoRepository;
    /**
     * @var CommissionCorrespondsRepositoryInterface
     */
    protected $commissionCorrespondsRepo;
    /**
     * @var CommissionAppRepositoryInterface
     */
    protected $commissionAppRepo;
    /**
     * @var ApprovalRepositoryInterface
     */
    protected $approvalRepository;
    /**
     * @var CommissionService
     */
    protected $commissionService;
    /**
     * @var CommissionCorrespondService
     */
    protected $commissionCorrespondService;
    /**
     * @var CommissionDetailService
     */
    protected $commissionDetailService;
    /**
     * @var CommissionSupportService
     */
    protected $commissionSupportService;
    /**
     * @var CommissionDataService
     */
    protected $commissionDataService;
    /**
     * @var CommissionFileService
     */
    protected $commissionFileService;
    /**
     * @var CommissionSearchService
     */
    protected $commissionSearchService;
    /**
     * @var CommissionExportCsvService
     */
    protected $commissionExportService;

    /**
     * @var CommissionAppService
     */
    protected $commissionAppService;
    /**
     * @var CommissionApprovalService
     */
    protected $commissionApprovalService;
    /**
     * CommissionController constructor.
     * @param CommissionCorrespondsRepositoryInterface $commissionCorrespondsRepo
     * @param CommissionInfoRepositoryInterface $commissionInfoRepository
     * @param MUserRepositoryInterface $mUserRepository
     * @param ApprovalRepositoryInterface $approvalRepository
     * @param CommissionAppRepositoryInterface $commissionAppRepository
     * @param CommissionService $commissionService
     * @param CommissionCorrespondService $commissionCorrespondService
     * @param CommissionDetailService $commissionDetailService
     * @param CommissionSupportService $commissionSupportService
     * @param CommissionDataService $commissionDataService
     * @param CommissionFileService $commissionFileService
     * @param CommissionAppService $commissionAppService
     * @param CommissionSearchService $commissionSearchService
     * @param CommissionExportCsvService $commissionExportService
     * @param CommissionApprovalService $commissionApprovalService
     */
    public function __construct(
        CommissionCorrespondsRepositoryInterface $commissionCorrespondsRepo,
        CommissionInfoRepositoryInterface $commissionInfoRepository,
        MUserRepositoryInterface $mUserRepository,
        ApprovalRepositoryInterface $approvalRepository,
        CommissionAppRepositoryInterface $commissionAppRepository,
        CommissionService $commissionService,
        CommissionCorrespondService $commissionCorrespondService,
        CommissionDetailService $commissionDetailService,
        CommissionSupportService $commissionSupportService,
        CommissionDataService $commissionDataService,
        CommissionFileService $commissionFileService,
        CommissionAppService $commissionAppService,
        CommissionSearchService $commissionSearchService,
        CommissionExportCsvService $commissionExportService,
        CommissionApprovalService $commissionApprovalService
    ) {
        parent::__construct();
        $this->commissionCorrespondsRepo = $commissionCorrespondsRepo;
        $this->commissionInfoRepository = $commissionInfoRepository;
        $this->mUserRepository = $mUserRepository;
        $this->approvalRepository = $approvalRepository;
        $this->commissionAppRepo = $commissionAppRepository;
        $this->commissionService = $commissionService;
        $this->commissionCorrespondService = $commissionCorrespondService;
        $this->commissionDetailService = $commissionDetailService;
        $this->commissionSupportService = $commissionSupportService;
        $this->commissionDataService = $commissionDataService;
        $this->commissionFileService = $commissionFileService;
        $this->commissionAppService = $commissionAppService;
        $this->commissionSearchService = $commissionSearchService;
        $this->commissionExportService = $commissionExportService;
        $this->commissionApprovalService = $commissionApprovalService;
    }

    /**
     * Get list commission information without search filter
     *
     * @param  Request $request
     * @param  null    $affiliationId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, $affiliationId = null)
    {
        $dataRequest = $request->all();
        $dataSession['isMobile'] = utilIsMobile($request->header('user-agent'));
        $request->session()->forget(self::$sessionKeyForCommissionSearch);

        return $this->indexOrSearchCommission($dataRequest, $dataSession, $affiliationId);
    }

    /**
     * Get list commission information with search filter
     *
     * @param  Request $request
     * @param  null    $affiliationId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request, $affiliationId = null)
    {
        $dataSession = $request->session()->get(self::$sessionKeyForCommissionSearch)[0];
        $dataRequest = $request->all();
        $dataSession['isMobile'] = utilIsMobile($request->header('user-agent'));

        return $this->indexOrSearchCommission($dataRequest, $dataSession, $affiliationId);
    }

    /**
     * Call action search or export csv.
     *
     * @param  CommissionSearchRequest $request
     * @param  null                    $affiliationId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws Exception
     */
    public function postSearch(CommissionSearchRequest $request, $affiliationId = null)
    {
        if (!empty($request->csv_out)) {
            return $this->export($request, $affiliationId);
        }
        $dataRequest = $request->all();
        $dataRequest['isMobile'] = utilIsMobile($request->header('user-agent'));
        $dataRequest['display'] = true;
        $request->session()->forget(self::$sessionKeyForCommissionSearch);
        $request->session()->push(self::$sessionKeyForCommissionSearch, $dataRequest);

        if ($affiliationId == 'none') {
            return redirect()->route('commission.search', ['affiliationId' => null]);
        } else {
            return $this->indexOrSearchCommission(null, $dataRequest, $affiliationId);
        }
    }

    /**
     * Get list data and export to csv file
     *
     * @param  Request $request
     * @param  null    $affiliationId
     * @return \Illuminate\Http\RedirectResponse
     * @throws Exception
     */
    public function export(Request $request, $affiliationId = null)
    {
        $dataRequest = $request->all();
        return $this->commissionExportService->exportCsv($dataRequest, $affiliationId);
    }

    /**
     * Description function:  get commission and userList to return view
     *
     * @param  integer $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $commission = $this->commissionCorrespondsRepo->find($id);
        $userList = $this->mUserRepository->getUser();

        return view(
            'commission.history_input',
            [
            'commission' => $commission,
            'userList' => $userList,
            ]
        );
    }

    /**
     * Description function: update table commission_corresponds
     *
     * @param  AutoCommissionCorrespondsFormRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(AutoCommissionCorrespondsFormRequest $request)
    {
        $data = [];
        $result = [];

        if ($request->isMethod('post')) {
            if ($request->has('id')) {
                $data['id'] = $request->input('id');
            }
            if ($request->has('rits_responders')) {
                $data['rits_responders'] = $request->rits_responders;
            }
            if ($request->has('responders')) {
                $data['responders'] = $request->responders;
            }
            $data['modified_user_id'] = Auth::user()['user_id'];
            $data['corresponding_contens'] = $request->corresponding_contens;
            $data['correspond_datetime'] = $request->correspond_datetime;
            if ($request->input('modified')) {
                $data['modified'] = $request->input('modified');
                $commission = $this->commissionCorrespondsRepo->find($data['id']);
                if ($data['modified'] != $commission['modified']) {
                    session()->flash('error', trans('commissioncorresponds.message_modified_not_check'));
                    $result['modified'] = trans('commissioncorresponds.message_modified_not_check');
                }
            }

            $this->commissionCorrespondsRepo->save($data);
            $result['success'] = trans('commissioncorresponds.message_successfully');
        }

        return response()->json($result);
    }


    /**
     * @param Request $request
     * @param null    $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws Exception
     */
    public function detail(Request $request, $id = null)
    {
        if (empty($id)) {
            return redirect()->route('commission.index');
        }

        $user = Auth::user();
        $auth = $user->auth;
        $affiliationId = $user->affiliation_id;
        $userId = $user->user_id;

        $this->commissionDetailService->updateCommissionData($id);
        $commissionData = $this->commissionDetailService->getCommissionData($id);

        if (empty($commissionData)) {
            abort(404);
        }

        if ($auth == 'affiliation') {
            if ($commissionData['CommissionInfo__lost_flg'] != 0
                || $commissionData['CommissionInfo__del_flg'] != 0
                || $commissionData['CommissionInfo__introduction_not'] != 0) {
                abort(404);
            }
        }

        $taxRate = $this->commissionDetailService->getTaxRates($commissionData['CommissionInfo__complete_date']);
        $fileUrl = $this->commissionFileService->getFileUrl($commissionData['DemandInfo__id']);
        $historyData = $this->commissionDetailService->getCommissionCorresponds($id);
        $demandAttachedFiles = $this->commissionDetailService->getDemandAttachedFiles($commissionData['DemandInfo__id']);

        /* JBR */
        $commissionData = $this->commissionDetailService->getDefaultJbrInfo($commissionData);
        /* support */
        $commissionData = $this->commissionSupportService->getSupport($id, $commissionData);
        /* address */
        $addressDisclosure = $this->commissionDetailService->getDisclosureData('address_disclosure');
        /* tel */
        $telDisclosure = $this->commissionDetailService->getDisclosureData('tel_disclosure');

        /* m_commission_alert_settings-tel */
        if (isset($commissionData['CommissionTelSupport']['correspond_status'])) {
            $mCommissionAlertSettingsTel = $this->commissionDetailService->mCommissionAlertSettings($commissionData['CommissionTelSupport']['correspond_status'], 0);
        }

        /* m_commission_alert_settings-visit */
        if (isset($commissionData['CommissionVisitSupport']['correspond_status'])) {
            $mCommissionAlertSettingsVisit = $this->commissionDetailService->mCommissionAlertSettings($commissionData['CommissionVisitSupport']['correspond_status'], 1);
        }

        /* m_commission_alert_settings-order */
        if (isset($commissionData['CommissionOrderSupport']['correspond_status'])) {
            $mCommissionAlertSettingsOrder = $this->commissionDetailService->mCommissionAlertSettings($commissionData['CommissionOrderSupport']['correspond_status'], 2);
        }

        /* tel - date */
        $dateTel = new \DateTime($commissionData['CommissionInfo__modified']);

        if (isset($mCommissionAlertSettingsTel['condition_value_min']) && isset($mCommissionAlertSettingsTel['rits_follow_datetime'])) {
            $dataTelList = $mCommissionAlertSettingsTel['condition_value_min'] + $mCommissionAlertSettingsTel['rits_follow_datetime'];
            $dateTel->add(new \DateInterval('PT' . $dataTelList . 'M'));
        }

        /* visit - date */
        $dateVisit = new \DateTime($commissionData['CommissionInfo__modified']);

        if (isset($mCommissionAlertSettingsVisit['condition_value_min']) && isset($mCommissionAlertSettingsVisit['rits_follow_datetime'])) {
            $dataVisitList = $mCommissionAlertSettingsVisit['condition_value_min'] + $mCommissionAlertSettingsVisit['rits_follow_datetime'];
            $dateVisit->add(new \DateInterval('PT' . $dataVisitList . 'M'));
        }

        /* order - date */
        $dateOrder = new \DateTime($commissionData['CommissionInfo__modified']);

        if (isset($mCommissionAlertSettingsOrder['condition_value_min']) && isset($mCommissionAlertSettingsOrder['rits_follow_datetime'])) {
            $dataOrderList = $mCommissionAlertSettingsOrder['condition_value_min'] + $mCommissionAlertSettingsOrder['rits_follow_datetime'];
            $dateOrder->add(new \DateInterval('PT' . $dataOrderList . 'M'));
        }

        $userList = $this->commissionDetailService->getMUsers();
        $mCommissionAlertSettingsTel['display_time'] = $dateTel->format('Y-m-d H:i:s');
        $mCommissionAlertSettingsVisit['display_time'] = $dateVisit->format('Y-m-d H:i:s');
        $mCommissionAlertSettingsOrder['display_time'] = $dateOrder->format('Y-m-d H:i:s');
        $mSites = $this->commissionDetailService->getMsiteById($commissionData['DemandInfo__site_id']);
        $visitTime = $this->commissionDetailService->getVisitTime($commissionData['CommissionInfo__commission_visit_time_id']);
        $resultTime = $this->getVisitTime($commissionData, $visitTime);

        if ($commissionData['CommissionInfo__lock_status'] == 1 && $auth == 'affiliation') {
            session()->flash('lock_status_invalid', trans('commissioncorresponds.error_inner'));
        }

        $categoryDefaultFee = '';

        if (empty($commissionData['DemandInfo__category_id']) == false) {
            $categoryDefaultFee = $this->commissionDataService->getDefaultFee($commissionData['DemandInfo__category_id']);
        }

        $notEnough = '';

        if ($auth == 'affiliation') {
            $notEnough = $this->commissionDataService->checkHaveEnoughData($affiliationId);
        }

        $applications = $this->commissionDataService->getApplicationData($id);
        $divValue = $this->commissionDataService->getDivValueList();
        $dropList = $this->commissionDataService->getDropList();

        return view(
            'commission.detail',
            [
            'id' => $id,
            'history_list' => $historyData,
            'results' => $commissionData,
            'demand_attached_files' => $demandAttachedFiles,
            'm_commission_alert_settings_tel' => $mCommissionAlertSettingsTel,
            'm_commission_alert_settings_visit' => $mCommissionAlertSettingsVisit,
            'm_commission_alert_settings_order' => $mCommissionAlertSettingsOrder,
            'user_list' => $userList,
            'user_id' => $userId,
            'user' => $user,
            'tax_rate' => $taxRate,
            'site_list' => $mSites,
            'address_disclosure' => $addressDisclosure,
            'tel_disclosure' => $telDisclosure,
            'visit_time' => $visitTime,
            'contact_desired_time_hope' => $resultTime['contact_desired_time_hope'],
            'contact_desired_time' => $resultTime['contact_desired_time'],
            'visit_time_display' => $resultTime['visit_time_display'],
            'visit_time_of_hope' => $resultTime['visit_time_of_hope'],
            'category_default_fee' => $categoryDefaultFee,
            'not_enough' => $notEnough,
            'applications' => $applications,
            'auth' => $auth,
            'affiliation_id' => $affiliationId,
            'file_url' => $fileUrl,
            'div_value' => $divValue,
            'drop_list' => $dropList,
            ]
        );
    }

    /**
     * @param Request $request
     * @param null    $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function regist(Request $request, $id = null)
    {
        $errorFlg = false;

        if ($request->isMethod('post')) {
            $data = $request->input('data');
            $files = $request->file('data');

            $commissionValidate = $this->commissionService->validateRegist($data['CommissionInfo']);
            $commissionCorrValidate = $this->commissionCorrespondService->validate($data['CommissionCorrespond']);

            if ($commissionValidate['check'] == false || $commissionCorrValidate['check'] == false) {
                $errorFlg = true;
            }

            if (!$errorFlg) {
                $result = $this->commissionDataService->regist($id, $data, $files);
            } else {
                session()->flash('error', trans('commission.check_input_item'));
            }
        }

        if (!$errorFlg && $result) {
            return redirect()->route('commission.detail', ['id' => $id]);
        }

        return redirect()->route('commission.detail', ['id' => $id])->withErrors($commissionValidate['validate'])->withInput();
    }

    /**
     * @param $id
     * @param $datetime
     * @param $status
     * @param $responder
     * @param $failReason
     * @param $contents
     * @param $hopeDatetime
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function registTelSupports($id, $datetime, $status, $responder, $failReason, $contents, $hopeDatetime)
    {
        $result = $this->commissionSupportService->registTelSupports($id, $datetime, $status, $responder, $failReason, $contents, $hopeDatetime);

        return view('commission.support', [
            'supports' => $result,
            'situation' => getDropList(MItemRepository::TELEPHONE_SUPPORT_STATUS),
            'prefix' => 'CommissionTelSupport'
        ]);
    }

    /**
     * @param $id
     * @param $datetime
     * @param $status
     * @param $responder
     * @param $failReason
     * @param $contents
     * @param $supportDatetime
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function registVisitSupports($id, $datetime, $status, $responder, $failReason, $contents, $supportDatetime)
    {
        $result = $this->commissionSupportService->registVisitSupports($id, $datetime, $status, $responder, $failReason, $contents, $supportDatetime);

        return view(
            'commission.support',
            [
            'supports' => $result,
            'situation' => getDropList(MItemRepository::VISIT_SUPPORT_STATUS),
            'prefix' => 'CommissionVisitSupport'
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function registOrderSupports(Request $request)
    {
        $result = $this->commissionSupportService->registOrderSupports($request->all());

        return view(
            'commission.support',
            [
            'supports' => $result,
            'situation' => getDropList(MItemRepository::ORDER_SUPPORT_STATUS),
            'prefix' => 'CommissionOrderSupport'
            ]
        );
    }

    /**
     * @param $id
     * @param $sup
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     * @throws Exception
     */
    public function listSupports($id, $sup)
    {
        $result = [];
        $situation = '';
        $prefix = '';

        try {
            if (!ctype_digit($id)) {
                throw new Exception(__('commission_detail.invalid_id'));
            }

            switch ($sup) {
                case 'tel':
                    $result = $this->commissionSupportService->getTelSupport($id, true);
                    $prefix = 'CommissionTelSupport';
                    $situation = getDropList(MItemRepository::TELEPHONE_SUPPORT_STATUS);
                    break;
                case 'visit':
                    $result = $this->commissionSupportService->getVisitSupport($id, true);
                    $prefix = 'CommissionVisitSupport';
                    $situation = getDropList(MItemRepository::VISIT_SUPPORT_STATUS);
                    break;
                case 'order':
                    $result = $this->commissionSupportService->getOrderSupport($id, true);
                    $prefix = 'CommissionOrderSupport';
                    $situation = getDropList(MItemRepository::ORDER_SUPPORT_STATUS);
                    break;
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return view(
            'commission.support',
            [
            'supports' => $result,
            'situation' => $situation,
            'prefix' => $prefix,
            ]
        );
    }

    /**
     * Update approvals.status when user approval or rejected
     *
     * @author Nguyen.DoNhu <Nguyen.DoNhu@Nashtechglobal.com>
     * @param  \App\Http\Requests\CommissionApprovalRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function approval(CommissionApprovalRequest $request)
    {
        try {
            /* Find approval */
            $approval = $this->approvalRepository->find($request->input('approval_id'));
            /* If approval not find, redirect 404 */
            if (!$approval) {
                abort(404);
            }

            $commissionApp = $this->commissionAppRepo->find($approval->relation_application_id);
            /* If application_user_id == this->user then redirect back with message */
            if ($approval->application_user_id == $this->getUser()->user_id) {
                $request->session()->flash('error_message', trans('commission.message_approval_error_user'));

                return redirect(route('report.applicationAdmin'));
            }

            $result = $this->commissionApprovalService->approval($approval->id, $commissionApp, $request->input('action_name'), $this->getUser());

            if ($result) {
                if ($request->input('action_name') == 'rejected') {
                    $request->session()->flash('success_message', trans('commission.message_rejected_success'));
                } else {
                    $request->session()->flash('success_message', trans('commission.message_approval_success'));
                }
            } else {
                $request->session()->flash('error_message', trans('commission.message_approval_error_update'));

                return redirect(route('report.applicationAdmin'));
            }

            return redirect()->route('commission.detail', ['id' => $commissionApp->commission_id, 'app' => '#app']);
        } catch (\Exception $exception) {
            abort('500');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function application(Request $request)
    {
        $data = $request->input('data');

        if ($request->isMethod('post')) {
            $this->commissionAppService->registApplication($data, $request);
        }

        return response()->json([
            'success' => true
        ]);
    }

    /**
     *
     * @param null $id
     * @return \Illuminate\Http\JsonResponse
     * @throws NotFoundHttpException
     */
    public function commissionFileDownload($id = null)
    {
        if (! ctype_digit($id)) {
            throw new NotFoundHttpException();
        }

        $demandAttachFile = $this->commissionDetailService->getDetailDemandAttachFile($id);

        if (file_exists($demandAttachFile['path'])) {
            return response()->download($demandAttachFile['path'], $demandAttachFile['name']);
        }

        echo __('commission_detail.no_file');
    }

    /**
     * Call service get list commission info.
     * @param null $dataRequest
     * @param null $dataSession
     * @param null $affiliationId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function indexOrSearchCommission($dataRequest = null, $dataSession = null, $affiliationId = null)
    {
        $dataResponse = $this->commissionSearchService->search($dataRequest, $dataSession, $affiliationId);

        if (count($dataResponse) > 0) {
            $viewName = $dataResponse['isMobile'] ? 'commission.index' : 'commission.index';

            return view($viewName, $dataResponse);
        } else {
            return abort(500);
        }
    }

    /**
     * Get visit time detail
     * @param array $commissionData
     * @param array $visitTime
     * @return array
     */
    private function getVisitTime($commissionData, $visitTime)
    {
        /* contact datetime */
        $contactDesiredTimeHope = '-';

        if (isset($commissionData['DemandInfo__contact_desired_time'])) {
            $contactDesiredTimeHope = dateTimeFormat($commissionData['DemandInfo__contact_desired_time']);
        } elseif (isset($commissionData['DemandInfo__contact_desired_time_from'])) {
            $contactDesiredTimeHope = dateTimeFormat($commissionData['DemandInfo__contact_desired_time_from']) . ' ~ ' . dateTimeFormat($commissionData['DemandInfo__contact_desired_time_to']);
        } elseif (isset($visitTime['visit_adjust_time'])) {
            $contactDesiredTimeHope = dateTimeFormat($visitTime['visit_adjust_time']);
        }

        $contactDesiredTime = '';

        if (isset($visitTime['visit_adjust_time'])) {
            $contactDesiredTime = dateTimeFormat($visitTime['visit_adjust_time']);
        } else {
            $contactDesiredTime = $contactDesiredTimeHope;
        }

        /* visit datetime */
        $visitTimeDisplay = '-';
        $visitTimeOfHope = '-';

        if (isset($commissionData['CommissionInfo__visit_desired_time'])) {
            $visitTimeDisplay = dateTimeFormat($commissionData['CommissionInfo__visit_desired_time']);
        } elseif (isset($visitTime['visit_time'])) {
            $visitTimeDisplay = dateTimeFormat($visitTime['visit_time']);
        } elseif (isset($visitTime['visit_time_from'])) {
            $visitTimeOfHope = dateTimeFormat($visitTime['visit_time_from']) . ' ~ ' . dateTimeFormat($visitTime['visit_time_to']);
        }

        if (isset($visitTime['visit_time'])) {
            $visitTimeOfHope = dateTimeFormat($visitTime['visit_time']);
        } elseif (isset($visitTime['visit_time_from'])) {
            $visitTimeOfHope = dateTimeFormat($visitTime['visit_time_from']) . ' ~ ' . dateTimeFormat($visitTime['visit_time_to']);
        }

        $result = [
            'contact_desired_time_hope' => $contactDesiredTimeHope,
            'contact_desired_time' => $contactDesiredTime,
            'visit_time_display' => $visitTimeDisplay,
            'visit_time_of_hope' => $visitTimeOfHope,
        ];

        return $result;
    }
}
