<?php

namespace App\Http\Controllers\DailyList;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

use Illuminate\Support\Facades\Storage;

class DailyListController extends Controller
{
    /**
     * index function call admin index view
     *
     * @return View
     */
    public function index()
    {
        $directories = Config::get('datacustom.daily_directory');
        $files = [];
        foreach ($directories as $key => $directory) {
            $files[$key] = [];
            if (Storage::disk('local')->exists($directory)) {
                foreach (Storage::disk('local')->allFiles($directory) as $file) {
                    $files[$key][] = [
                        "filename" => basename($file),
                        "path" => Storage::disk('local')->url($file)
                    ];
                }
            } else {
                Storage::disk('local')->makeDirectory($directory, 0775, true);
            }
        }
        return view('daily_list.index', ["files" => $files]);
    }
}
