<?php

namespace App\Http\Controllers\Demand;

use App\Http\Controllers\Controller;
use App\Http\Requests\AutoDemandCorrespondsFormRequest;
use App\Http\Requests\Demand\DemandInfoDataRequest;
use App\Models\DemandInfo;
use App\Models\MCorp;
use App\Models\MSite;
use App\Repositories\AuctionInfoRepositoryInterface;
use App\Repositories\AutoCommissionCorpRepositoryInterface;
use App\Repositories\DemandAttachedFileRepositoryInterface;
use App\Repositories\DemandCorrespondsRepositoryInterface;
use App\Repositories\DemandInfoRepositoryInterface;
use App\Repositories\MAnswerRepositoryInterface;
use App\Repositories\MCategoryRepositoryInterface;
use App\Repositories\MCorpRepositoryInterface;
use App\Repositories\MGenresRepositoryInterface;
use App\Repositories\MItemRepositoryInterface;
use App\Repositories\MSiteCategoryRepositoryInterface;
use App\Repositories\MSiteGenresRepositoryInterface;
use App\Repositories\MSiteRepositoryInterface;
use App\Repositories\MUserRepositoryInterface;
use App\Repositories\VisitTimeRepositoryInterface;
use App\Services\AuctionService;
use App\Services\CommissionService;
use App\Services\Credit\CreditService;
use App\Services\DemandCorrespondService;
use App\Services\DemandInfoService;
use App\Services\DemandInquiryAnswerService;
use App\Services\DemandService;
use App\Services\MPostService;
use App\Services\MSiteService;
use App\Services\PdfGenerator;
use App\Services\VisitTimeService;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class DemandController extends Controller
{
    /**
     * @var DemandCorrespondsRepositoryInterface
     */
    protected $demandCorrespondsRepository;
    /**
     * @var MUserRepositoryInterface
     */
    protected $mUserRepository;
    /**
     * @var AuctionInfoRepositoryInterface
     */
    protected $auctionInfoRepo;
    /**
     * @var MSiteService
     */
    protected $mSiteService;
    /**
     * @var MPostService
     */
    protected $mPostService;
    /**
     * @var AuctionService
     */
    protected $auctionService;
    /**
     * @var CommissionService
     */
    protected $commissionService;
    /**
     * @var MSiteRepositoryInterface
     */
    protected $mSite;
    /**
     * @var MItemRepositoryInterface
     */
    protected $mItemRepository;
    /**
     * @var MCorpRepositoryInterface
     */
    protected $mCorpRepo;
    /**
     * @var MCategoryRepositoryInterface
     */
    protected $mCateRepo;
    /**
     * @var DemandInfoRepositoryInterface
     */
    protected $demandInfoRepo;
    /**
     * @var DemandAttachedFileRepositoryInterface
     */
    protected $demandAttachedFileRepo;
    /**
     * @var DemandInfoService
     */
    protected $demandInfoService;
    /**
     * @var DemandInquiryAnswerService
     */
    protected $demandInquiryAnswerService;

    /**
     * @var CreditService
     */
    protected $creditService;
    /**
     * @var AutoCommissionCorpRepositoryInterface
     */
    protected $autoCommissionCorp;
    /**
     * @var VisitTimeRepositoryInterface
     */
    protected $visitTimeRepo;
    /**
     * @var VisitTimeService
     */
    protected $visitTimeService;
    /**
     * @var DemandCorrespondService
     */
    protected $demandCorrespondService;
    /**
     * @var MSiteGenresRepositoryInterface
     */
    protected $mSiteGenresRepository;
    /**
     * @var MAnswerRepositoryInterface
     */
    protected $mAnswerRepository;

    /**
     * @var MCorp
     */
    protected $mCorp;
    /**
     * @var mixed
     */
    protected $demandInfo;
    /**
     * @var MSiteCategoryRepositoryInterface
     */
    protected $mSiteCategoryRepo;
    /**
     * @var DemandService
     */
    protected $demandService;
    /**
     * @var MGenresRepositoryInterface
     */
    protected $mGenresRepository;
    /**
     * DemandController constructor.
     * @param DemandCorrespondsRepositoryInterface $demandCorrespondsRepository
     * @param MUserRepositoryInterface $mUserRepository
     * @param AuctionInfoRepositoryInterface $auctionInfoRepo
     * @param MSiteService $mSiteService
     * @param CommissionService $commissionService
     * @param MPostService $mPostService
     * @param AuctionService $auctionService
     * @param MSiteRepositoryInterface $mSite
     * @param MItemRepositoryInterface $mItemRepository
     * @param DemandInfoService $demandInfoService
     * @param DemandInquiryAnswerService $demandInquiryAnswerService
     * @param CreditService $creditService
     * @param MCategoryRepositoryInterface $mCateRepo
     * @param MCorpRepositoryInterface $mCorpRepo
     * @param DemandAttachedFileRepositoryInterface $demandAttachedFileRepo
     * @param DemandInfoRepositoryInterface         $demandInfoRepo
     * @param AutoCommissionCorpRepositoryInterface $autoCommissionCorpRepository
     * @param VisitTimeService $visitTimeService
     * @param DemandCorrespondService $demandCorrespondService
     * @param VisitTimeRepositoryInterface $visitTimeRepository
     * @param MSiteGenresRepositoryInterface $mSiteGenresRepository
     * @param MAnswerRepositoryInterface $mAnswerRepository
     * @param MCorp $mCorp
     * @param MSiteCategoryRepositoryInterface $mSiteCategoryRepo
     * @param DemandService $demandService
     * @param MGenresRepositoryInterface $mGenresRepository
     */
    public function __construct(
        DemandCorrespondsRepositoryInterface $demandCorrespondsRepository,
        MUserRepositoryInterface $mUserRepository,
        AuctionInfoRepositoryInterface $auctionInfoRepo,
        MSiteService $mSiteService,
        CommissionService $commissionService,
        MPostService $mPostService,
        AuctionService $auctionService,
        MSiteRepositoryInterface $mSite,
        MItemRepositoryInterface $mItemRepository,
        DemandInfoService $demandInfoService,
        DemandInquiryAnswerService $demandInquiryAnswerService,
        CreditService $creditService,
        MCategoryRepositoryInterface $mCateRepo,
        MCorpRepositoryInterface $mCorpRepo,
        DemandAttachedFileRepositoryInterface $demandAttachedFileRepo,
        DemandInfoRepositoryInterface $demandInfoRepo,
        AutoCommissionCorpRepositoryInterface $autoCommissionCorpRepository,
        VisitTimeService $visitTimeService,
        DemandCorrespondService $demandCorrespondService,
        VisitTimeRepositoryInterface $visitTimeRepository,
        MSiteGenresRepositoryInterface $mSiteGenresRepository,
        MAnswerRepositoryInterface $mAnswerRepository,
        MCorp $mCorp,
        MSiteCategoryRepositoryInterface $mSiteCategoryRepo,
        DemandService $demandService,
        MGenresRepositoryInterface $mGenresRepository
    ) {


        $this->demandCorrespondsRepository = $demandCorrespondsRepository;
        $this->mUserRepository = $mUserRepository;
        $this->auctionInfoRepo = $auctionInfoRepo;
        $this->mSiteService = $mSiteService;
        $this->commissionService = $commissionService;
        $this->mPostService = $mPostService;
        $this->auctionService = $auctionService;
        $this->mSite = $mSite;
        $this->mItemRepository = $mItemRepository;
        $this->demandInfoService = $demandInfoService;
        $this->creditService = $creditService;
        $this->autoCommissionCorp = $autoCommissionCorpRepository;
        $this->mCateRepo = $mCateRepo;
        $this->mCorpRepo = $mCorpRepo;
        $this->demandInfoRepo = $demandInfoRepo;
        $this->demandAttachedFileRepo = $demandAttachedFileRepo;
        $this->visitTimeRepo = $visitTimeRepository;
        $this->demandInquiryAnswerService = $demandInquiryAnswerService;
        $this->visitTimeService = $visitTimeService;
        $this->demandCorrespondService = $demandCorrespondService;
        $this->mSiteGenresRepository = $mSiteGenresRepository;
        $this->mAnswerRepository = $mAnswerRepository;
        $this->mCorp = $mCorp;
        $this->mSiteCategoryRepo = $mSiteCategoryRepo;
        $this->demandService = $demandService;
        $this->mGenresRepository = $mGenresRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $demandInfo = $this->dbquery->select("demand_model.sql", ['id' => 974875]);
        $pdfGenerator = app(PdfGenerator::class);
        $pdfGenerator->commission();
        utilGetDropList("IPHONE");

        return view('home');
    }

    /**
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function historyInput($id = null)
    {
        $demand = $this->demandCorrespondsRepository->find($id);
        $userList = $this->mUserRepository->getUser();

        return view(
            'demand.history_input',
            [
            'demand' => $demand ?? (new DemandInfo()),
            'userList' => $userList,
            ]
        );
    }

    /**
     * @param AutoDemandCorrespondsFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @des update history_input
     */
    public function update(AutoDemandCorrespondsFormRequest $request)
    {
        $data = [];
        if ($request->isMethod('post')) {
            if ($request->has('id')) {
                $data['id'] = $request->input('id');
            }
            $data['responders'] = $request->responders;
            $data['corresponding_contens'] = $request->corresponding_contens;
            $data['correspond_datetime'] = $request->correspond_datetime;
            if ($request->input('modified')) {
                $data['modified'] = $request->input('modified');
                $demand = $this->demandCorrespondsRepository->find($data['id']);
                if ($data['modified'] != $demand['modified']) {
                    return redirect()->back()->with('modified', $data['modified']);
                }
            }

            $rslt = $this->demandCorrespondsRepository->save($data);
            if ($rslt) {
                $request->session()->flash('success', Lang::get('demand.message_successfully'));
            }
        }

        return redirect()->back();
    }

    /**
     * @param null $demandId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function auctionDetail($demandId = null)
    {
        $results = $this->auctionInfoRepo->getListByDemandId($demandId);
        return view('demand.auction_detail', [
            'results' => $results
            ]);
    }

    /**
     * @param $data
     * @param $requestMerge
     * @param int          $defaultValue
     * @return mixed
     */
    private function mergeData($data, $requestMerge, $defaultValue = 0)
    {
        foreach ($requestMerge as $merge) {
            if (!isset($data['demandInfo'][$merge])) {
                $data['demandInfo'][$merge] = $defaultValue;
            }
        }

        return $data;
    }

    /**
     * @param $data
     * @return mixed
     */
    private function mergeZero($data)
    {
        $requestMerge = [
            'nighttime_takeover', 'mail_demand', 'cross_sell_implement', 'cross_sell_call', 'riro_kureka',
            'remand', 'sms_reorder', 'corp_change', 'low_accuracy', 'do_auction', 'follow', 'auction'
        ];
        return $this->mergeData($data, $requestMerge);
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function regist(DemandInfoDataRequest $request)
    {
        $data = $request->except('_token', '_method');
        $data['not-send'] = $data['not-send'] ?? 0;
        $data['demandInfo'] = $this->demandInfoService->addDefaultValueForDemand($data['demandInfo']);
        $commissionData = isset($data['commissionInfo']) ? $data['commissionInfo'] : [];
        $data['commissionInfo'] = $this->demandInfoService->makeCommissionInfoData($commissionData);
        // process data not quick order fail
        $processData = $this->demandInfoService->processDataWithoutQuickOrder($data);
        if (!$processData) {
            // redirect back to regist demand
            return $this->backToDetail($data);
        } else {
            $data = $processData;
        }
        // DemandInfo Remove the leading and trailing spaces of each item
        $data['demandInfo'] = $this->demandInfoService->replaceSpace($data['demandInfo']);
        //One-touch loss
        $quickOrderFail = false;
        // check do_auction
        $data['demandInfo'] = $this->demandInfoService->checkDemanInfoDoAuction($data['demandInfo']);
        // check auto_selecttion
        $data['demandInfo'] = $this->demandInfoService->checkDoAutoSelection($data['demandInfo']);
        // update demand info if has quick order fail
        $data = $this->demandInfoService->processQuickOrderFail($data);
        /*validate commission type div*/
        $cTypeDiv = $data['demandInfo']['commission_type_div'];
        $commissionExits = array_key_exists('commissionInfo', $data);
        $demandStatus = $data['demandInfo']['demand_status'];
        // validate thaihv
        $errFlg = false;


        if(!empty($request->get('commissionInfo') && $request->get('send_commission_info') == 1)) {
            if(!$this->commissionService->checkCommissionFlgCount($data)){
                \Log::debug('checkCommissionFlgCount');
                session()->flash('error_commission_flg_count', __('demand.check_input_confirmation'));
                $errFlg = true;
            }
        }

        /* validate demand info */
        if (!$this->demandInfoService->validateDemandInfo($data)) {
            \Log::debug('validateDemandInfo');
            $errFlg = true;
        }
        /* validate visit time */
        if ($this->demandInfoService->validateDemandInquiryAnswer($data)
            || $this->visitTimeService->processValidateVisitTime($data)
        ) {
            \Log::debug('validateDemandInquiryAnswer, processValidateVisitTime');
            $errFlg = true;
        }
        if ($this->demandInfoService->checkErrSendCommissionInfo($data)) {
            \Log::debug('checkErrSendCommissionInfo');
            $errFlg = true;
        }
         /* validate commission info */
        if (isset($data['commissionInfo'])) {
            if (!$this->commissionService->validate($data['commissionInfo'], $data['demandInfo']['demand_status'])) {
                \Log::debug('commissionService->validate');
                $errFlg = true;
            }
            if (!$this->commissionService->validateCheckLimitCommitFlg($data)) {
                \Log::debug('commissionService->validateCheckLimitCommitFlg');
                session()->flash('error_msg_input', __('demand.commitFlgLimit'));
                $errFlg = true;
            }
        }
        /* validate demand correspond */
        if (isset($data['demandCorrespond']) && $data['send_commission_info'] == 0) {
            if (!$this->demandCorrespondService->validate($data['demandCorrespond'])) {
                \Log::debug('demandCorrespondService->validate');
                $errFlg = true;
            }
        }

        if (!$this->demandInfoService->validateCommissionTypeDiv($cTypeDiv, $commissionExits, $demandStatus)) {
            session()->flash('error_msg_input', __('demand.notEmptyIntroduceInfo'));
            \Log::debug('validateCommissionTypeDiv');
            return $this->backToDetail($data);
        }
        if (!$this->demandInfoService->checkModifiedDemand($data['demandInfo'])) {
            session()->flash('error_msg_input', __('demand.modifiedNotCheck'));
            \Log::debug('checkModifiedDemand');
            return $this->backToDetail($data);
        }

        if (!$this->demandInfoService->validateSelectSystemType($data)) {
            \Log::debug('validateSelectSystemType');
            return $this->backToDetail($data);
        }
        if ($errFlg) {
            session()->flash('error_msg_input', __('demand.input_requried'));
            \Log::debug('errFlg');
            return $this->backToDetail($data);
        }

        $commissionInfo = array_key_exists('commissionInfo', $data) ? $data['commissionInfo'] : [];
        $visitTime = array_key_exists('visitTime', $data) ? $data['visitTime'] : [];
        $introduceInfo = array_key_exists('introduceInfo', $data) ? $data['introduceInfo'] : [];
        $demandCorrespond = array_key_exists('demandCorrespond', $data) ? $data['demandCorrespond'] : [];
        // Auction selection only
        $allData = $this->demandInfoService->processAuctionSelection($data);
        $data = $allData['data'];
        $auctionFlg = $allData['auctionFlg'];
        $auctionNoneFlg = $allData['auctionNoneFlg'];
        $hasStartTimeErr = $allData['hasStartTimeErr'];
        // Build process data with selection system
        $data = $this->demandInfoService->processDataWithSelectionSystem($data);
        // Holds the parties with errors
        $errorNo = [];
        try {
            // Transaction start
            DB::beginTransaction();
            // Register deal information
            $data['demandInfo'] = $this->demandInfoService->updateDemand($data['demandInfo']);
            //Register a hearing item by case
            $this->demandInquiryAnswerService->updateDemandInquiryAnswer($data);
            // Register visit date
            $this->visitTimeService->updateVisitTime($data);
            // Registration of destination information
            $errorNo = $this->commissionService->updateCommission($data);
            // Registration of introduction destination information
            $this->commissionService->updateIntroduce($data);
            // Register auction information
             $demandId = $data['demandInfo']['id'];
            // Move existing process to Component, move to AuctionInfoUtil -> update_auction_infos
            $this->auctionService->updateAuctionInfos($demandId, $data, false);
            //Create a case correspondence history
            $this->demandCorrespondService->updateDemandCorrespond($data);
            // $this->set('regist_enabled', $regist_enabled);
            // $errFlg = $errFlg && !$quickOrderFail;
            if (empty($errorNo)) {
                DB::commit();
                if (!$demandId) {
                    // At the time of new registration
                    $demandId = $this->demandInfoRepo->getMaxIdInsert();
                }
                //When the "transmission of agency information" button is pressed
                if (isset($data['send_commission_info']) && $data['send_commission_info'] == 1) {
                    $this->commissionService->sendNotify($demandId);
                }
            } else {
                DB::rollback();
                $errorMessage = __('demand.not_email_and_fax');

                foreach ($errorNo as $key => $val) {
                    $errorMessage .= ' [取次先' . $val . ']';
                }
                session()->flash('error_msg_input', $errorMessage);
                return $this->backToDetail($data);
            }
        } catch (Exception $e) {
             DB::rollback();
            \Log::error($e->getMessage());
            \Log::error('__order fail___' . $quickOrderFail);
            session()->flash('error_msg_input', __('demand.input_requried'));
            return $this->backToDetail($data);
        }
        // Send mail / fax for update
        if ($data['send_commission_info'] == 1 && isset($data['commissionInfo'])) {
            \Log::debug('___ start process mail and fax ___');
            $mailAndFaxs = $this->demandInfoService->getMailAndFaxByCorpData($data['commissionInfo']);
            $faxList = $mailAndFaxs['faxList'];
            $mailList = $mailAndFaxs['mailList'];
            $demandInfo = $data['demandInfo'];
            $mailInfo = $this->demandInfoService->getMailData($demandInfo['id']);
            $result = $this->demandInfoService->sendMail($demandInfo, $mailList, $mailInfo);
            $result = $this->demandInfoService->sendFax($demandInfo, $faxList, $mailInfo);
            if (!$result) {
                session()->flash('error_msg_input', __('demand.send_error'));
                \Log::debug('___ fail send email and fax ___');
                return $this->backToDetail($data);
            }
        }

        if ($data['send_commission_info'] == 1) {
            $this->commissionService->updateCommissionSendMailFax($data);
        }

        if ($hasStartTimeErr) {
            // Opportunity creation date and desired date are reversed
            session()->flash('error_msg_input', __('demand.start_time_error'));
        } elseif (!$auctionNoneFlg) {
            // 0 franchise stores
            session()->flash('error_msg_input', __('demand.aff_nothing'));
        } elseif (!$auctionFlg) {
            //Off hours
            session()->flash('error_msg_input', __('demand.auction_ng_update'));
        }
        return redirect()->route('demand.detail', $data['demandInfo']['id'])->with('message', __('demand.msg_success'));
    }


    /**
     * @param $data
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    private function backToDetail($data)
    {
        $errs = [];
        if (session('errors')) {
            $errs = array_merge($errs, session('errors'));
        }
        if (session('demand_errors')) {
            $errs = array_merge($errs, session('demand_errors'));
        }
        if (session('error_msg_input')) {
            $errs['error_msg_input'] = session('error_msg_input');
        }
        if (!isset($data['demandInfo']['id']) || empty($data['demandInfo']['id'])) {
            return redirect()->route('demand.get.create')->withInput($data)->withErrors($errs);
        }
        if (isset($data['restore_at_error']) && !empty($data['restore_at_error'])) {
            $data = array_replace_recursive($data, $data['restore_at_error']);
        }
        $commissionInfo = array_key_exists('commissionInfo', $data) ? $data['commissionInfo'] : [];
        $visitTime = array_key_exists('visitTime', $data) ? $data['visitTime'] : [];
        $introduceInfo = array_key_exists('introduceInfo', $data) ? $data['introduceInfo'] : [];
        $demandCorrespond = array_key_exists('demandCorrespond', $data) ? $data['demandCorrespond'] : [];
        $mInquiry = [];
        if (isset($data['mInquiry'])) {
            foreach ($data['mInquiry'] as $key => $val) {
                $mInquiry[$key] = $val;
                $mInquiry[$key] += ['demandInquiryAnswer'=>$data['demandInquiryAnswer'][$key]];
            }
        }

        $data['demandCorrespond'] = $data['demandCorrespond'] ?? [];

        $registEnable = false;
        if (array_key_exists('id', $data['demandInfo'])) {
            $dispData = $this->setDemand($data['demandInfo']['id']);
            if (file_exists(config('datacustom.estimate_file_path') . 'estimate_' . $data['demandInfo']['id'])) {
                $estimateFileUrl = '/download/estimate/estimate_' . $data['demandInfo']['id'] . '.pdf';
            }

            if (file_exists(config('datacustom.receipt_file_path') . 'receipt_' . $data['demandInfo']['id'] . '.pdf')) {
                $receiptFileUrl = '/download/receipt/receipt_' . $data['demandInfo']['id'] . '.pdf';
            }
            $registEnable = true;
        } else {
            $data['demandInfo']['same_customer_demand_url'] =
            route('demand.detail', $data['demandInfo']['source_demand_id']);
        }
        $dispData['demandInfo'] = $data['demandInfo'];
        $dispData['visitTime'] = $visitTime;
        $dispData['commissionInfo'] = $commissionInfo;
        $dispData['mInquiry'] = $mInquiry;
        $dispData['demandCorrespond'] = $data['demandCorrespond'];

        $answerList = array();
        for ($i = 0; $i < count($dispData['mInquiry']); $i++) {
            $answerList[$i] = $this->mAnswerRepository->dropDownAnswer(
                $dispData['mInquiry'][$i]['demandInquiryAnswer']['inquiry_id']
            );
        }
        $dispData['mAnswer'] = $answerList;
        $dispData['regist'] = "regist";

        $dispData['commissionInfo_tmp_corp_id'] = $data['CommissionInfo_tmp_corp_id'] ?? '';
        $siteList = $this->mSite->getListMSitesForDropDown();
        $genreList = $this->mSiteGenresRepository->getGenreBySiteStHide($data['demandInfo']['site_id']);

        $userList = $this->mUserRepository->getListUserForDropDown();
        $categoryList = $this->mCateRepo->getDropListCategory(
            $data['demandInfo']['genre_id'],
            !empty($data['demandInfo']['id'])
        );

        $crossSellSourceSite = [];
        if (isset($data['demandInfo']['cross_sell_source_site'])) {
            $crossSellSourceSite = $this->mSiteGenresRepository->getMSiteGenresDropDownBySiteId(
                $data['demandInfo']['cross_sell_source_site']
            );
        }

        $bidSituation = false;
        if (!empty($data['demandInfo']['id'])) {
            $AuctionInfo_data = $this->auctionInfoRepo->getAuctionInfoDemandInfo($data['demandInfo']['id']);
            if (!empty($AuctionInfo_data)) {
                $bidSituation = true;
            }
        }

        $selectionSystemList = $this->demandInfoService->getSelectionSystemList();

        if (isset($data['demandInfo']['id'])) {
            return redirect()->back()->withErrors($errs)->withInput($data)->with('disableLimit', 'disabled');
        }
        return view('demand.detail', compact(
            'dispData',
            'estimateFileUrl',
            'receiptFileUrl',
            'registEnable',
            'siteList',
            'genreList',
            'userList',
            'categoryList',
            'crossSellSourceSite',
            'bidSituation',
            'selectionSystemList'
        ));
    }

    /**
     * @param Request $request
     * @param int $customerTel
     * @param int $siteTel
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function cti(Request $request, $customerTel = null, $siteTel = null)
    {
        $ctiCustomerTel = '';
        if (empty($siteTel)) {
            return redirect()->route('demand.get.create');
        }
        $customerTelSave = '';
        if ($customerTel == '0') {
            $customerTel_save= '0';
            $customerTel= '';
        }

        if ($customerTel == '非通知') {
            $customerTelSave= '非通知';
            $customerTel= '';
        }

        $demandInfo = $this->demandInfoService->setPreDemand($customerTel, $siteTel);
        if (!empty($demandInfo['demand_status']) && $demandInfo['demand_status'] != 0) {
            $ctiCustomerTel = $customerTel;
        }
        $demandInfo['demand_status'] = 0;
        if ($customerTelSave == '非通知') {
            $demandInfo['customer_tel'] = '非通知';
        }

        return redirect()->route('demand.get.create')->with('ctiDemandInfo', $demandInfo);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $userDropDownList = $this->mUserRepository->getListUserForDropDown();
        $mSiteDropDownList = $this->mSite->getListMSitesForDropDown();
        $mItemsDropDownList = $this->mItemRepository->getMItemListByItemCategory('建物種別');
        $priorityDropDownList = $this->demandInfoService->getPriorityTranslate();
        $prefectureDiv = $this->demandInfoService->translatePrefecture();
        $stClaimDropDownList = $this->mItemRepository->getMItemListByItemCategory('STクレーム');
        $jbrWorkContentDropDownList = $this->mItemRepository->getMItemListByItemCategory('[JBR様]作業内容');
        $specialMeasureDropDownList = $this->mItemRepository->getMItemListByItemCategory('案件特別施策');
        $demandStatusDropDownList = $this->mItemRepository->getMItemListByItemCategory('案件状況');
        $orderFailReasonDropDownList = $this->mItemRepository->getMItemListByItemCategory('案件失注理由');
        $acceptanceStatusDropDownList = $this->mItemRepository->getMItemListByItemCategory('受付ステータス');
        $quickOrderFailReasonDropDownList = $this->mItemRepository->getMItemListByItemCategory('ワンタッチ失注理由');
        $vacationLabels = $this->mItemRepository->getMItemList('長期休業日', date('Y/m/d'));
        $selectionSystemList = $this->demandInfoService->getSelectionSystemList();
        if (isset($request->old('demandInfo')['site_id'])) {
            $genresDropDownList = $this->mSiteGenresRepository->getGenreBySiteStHide($request->old('demandInfo')['site_id']);
        }
        if (isset($request->old('demandInfo')['genre_id'])) {
            $categoriesDropDownList =
            $this->mCateRepo->getListCategoriesForDropDown($request->old('demandInfo')['genre_id']);
        }


        return view(
            'demand.create',
            compact(
                'userDropDownList',
                'mSiteDropDownList',
                'mItemsDropDownList',
                'priorityDropDownList',
                'prefectureDiv',
                'stClaimDropDownList',
                'jbrWorkContentDropDownList',
                'specialMeasureDropDownList',
                'demandStatusDropDownList',
                'orderFailReasonDropDownList',
                'acceptanceStatusDropDownList',
                'quickOrderFailReasonDropDownList',
                'vacationLabels',
                'selectionSystemList',
                'genresDropDownList',
                'categoriesDropDownList'
            )
        );
    }

    /**
     * clone a demand
     *
     * @param  null $demandId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function copy($demandId = null)
    {
        if ($demandId == null) {
            return;
        }

        $data = self::setDemand($demandId);

        if (array_key_exists('MInquiry', $data)) {
            $answer_list = array();
            for ($i = 0; $i < count($data['MInquiry']); $i++) {
                $answer_list[$i] = $this->__set_answer($data['MInquiry'][$i]['id']);
            }
            $data += array('MAnswer' => $answer_list);
        }

        if (array_key_exists('DemandInfo', $data) && array_key_exists('id', $data['DemandInfo'])) {
            unset($data['DemandInfo']['id']);
        }

        unset($data['DemandInfo']['selection_system_before']);
        unset($data['CommissionInfo']);
        unset($data['IntroduceInfo']);
        $data['DemandInfo']['demand_status'] = 1;
        $data['DemandInfo']['demand_status_before'] = $data['DemandInfo']['demand_status'];
        unset($data['DemandInfo']['auction']);

        $selectionSystemList = $this->demandService->getSelectionSystemList(
            $data['DemandInfo']['genre_id'],
            $data['DemandInfo']['address1']
        );
        unset($data['DemandInfo']['riro_kureka']);

        $mSiteDropDownList = $this->mSite->getListMSitesForDropDown();
        $userDropDownList = $this->mUserRepository->getListUserForDropDown();

        if (array_key_exists('DemandInfo', $data)) {
            $genresDropDownList = $this->mGenresRepository->getListForDropDown($data['DemandInfo']['site_id']);
            $categoriesDropDownList = $this->mCateRepo->getListCategoriesForDropDown($data['DemandInfo']['genre_id']);
            $mSiteGenresDropDownList =
            $this->mSiteGenresRepository->getMSiteGenresDropDownBySiteId($data['DemandInfo']['genre_id']);
            $mItemsDropDownList = $this->mItemRepository->getMItemListByItemCategory('建物種別');

            // TODO: Dung check lai
            $crossSellGenreList = $this->mSiteGenresRepository->getGenreBySite($data['DemandInfo']['cross_sell_source_site']);
            $crossSellCategoryList =
            $this->mSiteCategoryRepo->getCategoriesBySite($data['DemandInfo']['cross_sell_source_site']);
            // TODO END
        } else {
            $genresDropDownList = $categoriesDropDownList = $mSiteGenresDropDownList = $mItemsDropDownList = [];
        }

        $demand = new DemandInfo($data['DemandInfo']);

        $mSiteDropDownList = $this->mSite->getListMSitesForDropDown();
        $userDropDownList = $this->mUserRepository->getListUserForDropDown();
        $genresDropDownList = $this->mSiteGenresRepository->getGenreBySiteStHide($demand->site_id);
        $categoriesDropDownList = $this->mCateRepo->getListCategoriesForDropDown($demand->genre_id);
        $prefectureDiv = $this->demandInfoService->translatePrefecture();
        $mItemsDropDownList = $this->mItemRepository->getMItemListByItemCategory('建物種別');
        $priorityDropDownList = $this->demandInfoService->getPriorityTranslate();
        $specialMeasureDropDownList = $this->mItemRepository->getMItemListByItemCategory('案件特別施策');
        $stClaimDropDownList = $this->mItemRepository->getMItemListByItemCategory('STクレーム');
        $jbrWorkContentDropDownList = $this->mItemRepository->getMItemListByItemCategory('[JBR様]作業内容');
        $orderFailReasonDropDownList = $this->mItemRepository->getMItemListByItemCategory('案件失注理由');
        $acceptanceStatusDropDownList = $this->mItemRepository->getMItemListByItemCategory('受付ステータス');
        $quickOrderFailReasonDropDownList = $this->mItemRepository->getMItemListByItemCategory('ワンタッチ失注理由');
        $mSiteGenresDropDownList = $this->mSiteGenresRepository->getMSiteGenresDropDownBySiteId($demand->site_id);
        $demandStatusDropDownList = $this->mItemRepository->getMItemListByItemCategory('案件状況');
        $customerTel = $this->demandInfoRepo->checkIdenticallyCustomer($demand->customer_tel);
        $enableSiteId = in_array($demandId, [861, 863, 889, 890, 1312, 1313, 1314]);
        return view(
            'demand.detail',
            compact(
                'selection_system_list',
                'mSiteDropDownList',
                'userDropDownList',
                'genresDropDownList',
                'categoriesDropDownList',
                'mSiteGenresDropDownList',
                'mItemsDropDownList',
                'crossSellCategoryList',
                'prefectureDiv',
                'selectionSystemList',
                'priorityDropDownList',
                'specialMeasureDropDownList',
                'stClaimDropDownList',
                'jbrWorkContentDropDownList',
                'demandStatusDropDownList',
                'orderFailReasonDropDownList',
                'acceptanceStatusDropDownList',
                'quickOrderFailReasonDropDownList',
                'demand',
                'customerTel',
                'enableSiteId'
            )
        );
    }

    /**
     * @param $demandId
     * @return mixed
     */
    private function setDemand($demandId)
    {

        $results = $this->demandInfoRepo->findById($demandId);

        if (array_key_exists('commission_info', $results)) {
            foreach ($results['commission_info'] as $k => $value) {
                // ジャンル・カテゴリが空の場合の為に修正
                $data = self::getHolidayData($value['corp_id']);

                $categoryData = self::getFeeData($value['corp_id'], $results['category_id']);

                if (empty($categoryData)) {
                    $categoryData['MCorpCategory'] = array('order_fee' => '' , 'order_fee_unit' => '', 'note' => '');
                }

                $data['MCorpCategory'] = $categoryData['MCorpCategory'];
                $results['CommissionInfo'][$k]['AffiliationInfo']['attention'] = $data['AffiliationInfo']['attention'];
                $results['CommissionInfo'][$k]['MCorp']['holiday'] = $data['MCorp']['holiday'];
                $results['CommissionInfo'][$k]['MCorpNewYear'] = $data['MCorpNewYear'];
                $results['CommissionInfo'][$k]['MCorpCategory']['order_fee'] = $data['MCorpCategory']['order_fee'];
                $results['CommissionInfo'][$k]['MCorpCategory']['order_fee_unit'] =
                    $data['MCorpCategory']['order_fee_unit'];

                $results['CommissionInfo'][$k]['MCorpCategory']['note'] = $data['MCorpCategory']['note'];
                $results['CommissionInfo'][$k]['MCorpCategory']['introduce_fee'] =
                    $data['MCorpCategory']['introduce_fee'];

                $results['CommissionInfo'][$k]['MCorpCategory']['corp_commission_type'] =
                    $data['MCorpCategory']['corp_commission_type'];
            }
        }

        // get data of visit time
        $data = $this->visitTimeRepo->findListByDemandId($demandId);

        $visitTimeData = '';
        $vsIndex = 0;

        foreach ($data as $key => $value) {
            if ($key === 'VisitTime') {
                $results['VisitTime'][$vsIndex]['id'] = $value['id'];
                $results['VisitTime'][$vsIndex]['visit_time'] = $value['visit_time'];
                $results['VisitTime'][$vsIndex]['is_visit_time_range_flg'] = $value['is_visit_time_range_flg'];
                $results['VisitTime'][$vsIndex]['visit_time_from'] = $value['visit_time_from'];
                $results['VisitTime'][$vsIndex]['visit_time_to'] = $value['visit_time_to'];
                $results['VisitTime'][$vsIndex]['visit_adjust_time'] = $value['visit_adjust_time'];
                $results['VisitTime'][$vsIndex]['visit_time_before'] =
                    ($value['is_visit_time_range_flg'] == 0) ? $value['visit_time'] : $value['visit_time_from'];
                $results['VisitTime'][$vsIndex]['commit_flg'] = !empty($data['AuctionInfo']['id'])? 1 : 0 ;
                $vsIndex++;
            }

            if ($key == 'AuctionInfo' && !empty($value['id'])) {
                $visitTimeData = $data['VisitTime']['visit_time'];
            }
        }

        $results['DemandInfo']['priority_before'] = $results['DemandInfo']['priority'];
        $results['DemandInfo']['contact_desired_time_before'] = $results['DemandInfo']['contact_desired_time'];
        $results['DemandInfo']['selection_system_before'] = $results['DemandInfo']['selection_system'];
        $results['DemandInfo']['demand_status_before'] = $results['DemandInfo']['demand_status'];

        $results['DemandInfo']['follow_tel_date'] = '';

        if (!empty($visitTimeData)) {
            // フォロー時間設定を取得
            $follow_data = $this->MTime->find('all', array('conditions' => array('item_category' => 'follow_tel')));
            $results['DemandInfo']['follow_tel_date'] =
                $this->MTime->getFollowTimeWithData(
                    $results['DemandInfo']['auction_start_time'],
                    $visitTimeData,
                    $follow_data
                );
        }

        return $results;
    }

    /**
     * @author Hao.NguyenHuu@nt
     * @param $corpId
     * @return array
     */
    private function getHolidayData($corpId)
    {
        $holiday = $this->mCorpRepo->getHolidays($corpId);
        $results = $this->mCorpRepo->getHolidayListByCorpId($corpId);
        $results['MCorp'] = [
            'holiday' => !empty($holiday) ? $holiday[0]->array_to_string : ''
        ];

        return $results;
    }

    /**
     * @param $corpId
     * @param $categoryId
     * @return array|mixed
     */
    private function getFeeData($corpId, $categoryId)
    {
        $results = [];

        if (empty($categoryId)) {
            return $results;
        }

        $results = $this->mCorpRepo->getByCorpIdAndCategoryId($corpId, $categoryId);

        return $results;
    }

    /**
     * Remove demand info
     *
     * @param  integer $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $this->demandInfoRepo->deleteByDemandId($id);

        return redirect()->route('demandlist.search');
    }

    /**
     * クロスセル専用
     *
     * @param  integer $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function cross($id = null)
    {
        if (empty($id)) {
            return redirect()->route('demand.detail');
        }

        // If there is a matter ID
        // Acquire copy source case data
        $data = $this->setDemand($id);

        if (array_key_exists('MInquiry', $data)) {
            // Acquisition of interview answer item data
            $answerList = array();

            for ($i = 0; $i < count($data['MInquiry']); $i++) {
                $answerList[$i] = $this->mAnswerRepository->dropDownAnswer($data['MInquiry'][$i]['id']);
            }

            $data += array('MAnswer' => $answerList);
        }

        // Assign site name to original site
        $data['DemandInfo']['cross_sell_source_site'] =
            isset($data['DemandInfo']['site_id']) ? $data['DemandInfo']['site_id'] : '';

        // Specify the cross-selling matter for the site name 861
        $site = $this->mSite->getSiteByName('クロスセル案件');
        $data['DemandInfo']['site_id'] = isset($site['id']) ? $site['id'] : '';

        // Assign genre to original genre
        $data['DemandInfo']['cross_sell_source_genre'] =
            isset($data['DemandInfo']['genre_id']) ? $data['DemandInfo']['genre_id'] : '';
        $data['DemandInfo']['genre_id'] = '0';
        $data['DemandInfo']['category_id'] = '0';

        // Substitute the matter ID into the original case number
        $data['DemandInfo']['source_demand_id'] = isset($data['DemandInfo']['id']) ? $data['DemandInfo']['id'] : '';

        // Assign Proposal Number to display URL, same customer case URL
        $data['DemandInfo']['same_customer_demand_url'] = route('demand.detail', ['id' => $data['DemandInfo']['id']]);
        $data['DemandInfo']['contents'] = '';
        // Remove case ID

        if (isset($data['DemandInfo']) && isset($data['DemandInfo']['id'])) {
            unset($data['DemandInfo']['id']);
        }

        // Clear selection method just before
        unset($data['DemandInfo']['selection_system_before']);

        // Do not copy agency data and referral data
        unset($data['CommissionInfo']);
        unset($data['IntroduceInfo']);

        // "Unselected" status of cases
        $data['DemandInfo']['demand_status'] = 1;
        $data['DemandInfo']['demand_status_before'] = $data['DemandInfo']['demand_status'];
        unset($data['DemandInfo']['auction']);

        $selectionSystemList = $this->demandService->getSelectionSystemList(
            $data['DemandInfo']['genre_id'],
            $data['DemandInfo']['address1']
        );

        // Retrieve the list for dropdown
        $siteList = $this->mSite->getList();
        $userList = $this->mUserRepository->dropDownUserList();

        // Category list, 【cross selling】 former category list
        $genreList = [];
        $categoryList = [];
        $crossSellGenreList = [];
        $crossSellCategoryList = [];

        if (isset($data['DemandInfo'])) {
            $genreList = $this->mSiteGenresRepository->getGenreBySiteStHide($data['DemandInfo']['site_id']);
            $genreList = $genreList ? $genreList->toArray() : [];

            $categoryList = $this->mCateRepo->getListStHide($data['DemandInfo']['genre_id']);
            $categoryList = $categoryList ? $categoryList->toArray() : [];

            $crossSellGenreList = $this->mSiteGenresRepository->getGenreBySite($data['DemandInfo']['cross_sell_source_site']);
            $crossSellGenreList = $crossSellGenreList ? $crossSellGenreList->toArray() : [];

            $siteId = null;

            if (!empty($data['DemandInfo']['cross_sell_source_site'])) {
                $site = $this->mSite->find($data['DemandInfo']['cross_sell_source_site']);

                if ($site->cross_site_flg == 1) {
                    $siteId = $site->id;
                }
            }

            $crossSellCategoryList = $this->mSiteCategoryRepo->getCategoriesBySite($siteId);
        }

        $demand = new DemandInfo($data['DemandInfo']);

        $mSiteDropDownList = $this->mSite->getListMSitesForDropDown();
        $userDropDownList = $this->mUserRepository->getListUserForDropDown();
        $genresDropDownList = $this->mSiteGenresRepository->getGenreBySiteStHide($demand->site_id);
        $categoriesDropDownList = $this->mCateRepo->getListCategoriesForDropDown($demand->genre_id);
        $prefectureDiv = $this->demandInfoService->translatePrefecture();
        $mItemsDropDownList = $this->mItemRepository->getMItemListByItemCategory('建物種別');
        $priorityDropDownList = $this->demandInfoService->getPriorityTranslate();
        $specialMeasureDropDownList = $this->mItemRepository->getMItemListByItemCategory('案件特別施策');
        $stClaimDropDownList = $this->mItemRepository->getMItemListByItemCategory('STクレーム');
        $jbrWorkContentDropDownList = $this->mItemRepository->getMItemListByItemCategory('[JBR様]作業内容');
        $orderFailReasonDropDownList = $this->mItemRepository->getMItemListByItemCategory('案件失注理由');
        $acceptanceStatusDropDownList = $this->mItemRepository->getMItemListByItemCategory('受付ステータス');
        $quickOrderFailReasonDropDownList = $this->mItemRepository->getMItemListByItemCategory('ワンタッチ失注理由');
        $mSiteGenresDropDownList = $this->mSiteGenresRepository->getMSiteGenresDropDownBySiteId($demand->site_id);
        $demandStatusDropDownList = $this->mItemRepository->getMItemListByItemCategory('案件状況');

        $customerTel = $this->demandInfoRepo->checkIdenticallyCustomer($demand->customer_tel);
        // Progress situation active control
        $registEnabled = false;
        // Clear
        $clearEnabled = true;
        // Re-display button activation control
        $againEnabled = false;
        $enableSiteId = in_array($id, [861, 863, 889, 890, 1312, 1313, 1314]);
        return view('demand.detail', [
            'regist_enabled' => $registEnabled,
            'clear_enabled' => $clearEnabled,
            'again_enabled' => $againEnabled,
            'selection_system_list' => $selectionSystemList,
            'site_list' => $siteList,
            'user_list' => $userList,
            'genre_list' => $genreList,
            'category_list' => $categoryList,
            'cross_sell_genre_list' => $crossSellGenreList,
            'cross_sell_category_list' => $crossSellCategoryList,
            'userDropDownList' => $userDropDownList,
            'mSiteDropDownList' => $mSiteDropDownList,
            'genresDropDownList' => $genresDropDownList,
            'categoriesDropDownList' => $categoriesDropDownList,
            'mSiteGenresDropDownList' => $mSiteGenresDropDownList,
            'prefectureDiv' => $prefectureDiv,
            'mItemsDropDownList' => $mItemsDropDownList,
            'selectionSystemList' => $selectionSystemList,
            'priorityDropDownList' => $priorityDropDownList,
            'specialMeasureDropDownList' => $specialMeasureDropDownList,
            'stClaimDropDownList' => $stClaimDropDownList,
            'jbrWorkContentDropDownList' => $jbrWorkContentDropDownList,
            'demandStatusDropDownList' => $demandStatusDropDownList,
            'orderFailReasonDropDownList' => $orderFailReasonDropDownList,
            'acceptanceStatusDropDownList' => $acceptanceStatusDropDownList,
            'quickOrderFailReasonDropDownList' => $quickOrderFailReasonDropDownList,
            'demand' => $demand,
            'customerTel' => $customerTel,
            'enableSiteId' => $enableSiteId
        ]);
    }

    /**
     * Download file attach in demand
     *
     * @param  integer $id
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function demandFileDownload($id = null)
    {
        if (!ctype_digit($id)) {
            abort(404);
        }

        $fileAttach = $this->demandAttachedFileRepo->findId($id);

        // file not found 404
        if (!$fileAttach) {
            abort(404);
        }

        // download file
        if (file_exists($fileAttach->path)) {
            $headers = [
                'Content-type' => 'application/octet-stream',
                'Content-disposition' => 'attachment; filename=' . $fileAttach->name,
                'Pragma' => 'no-cache',
                'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
                'Expires' => '0'
            ];

            return response()->download($fileAttach->path, $fileAttach->name, $headers);
        } else {
            abort(404);
        }
    }
}
