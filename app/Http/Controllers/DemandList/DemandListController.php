<?php

namespace App\Http\Controllers\DemandList;

use App\Http\Controllers\Controller;
use App\Http\Requests\Demand\DemandAttachedFileRequest;
use App\Repositories\DemandAttachedFileRepositoryInterface;


use App\Repositories\MCategoryRepositoryInterface;
use App\Repositories\MGenresRepositoryInterface;
use App\Repositories\MSiteGenresRepositoryInterface;
use App\Repositories\Eloquent\MItemRepository;
use App\Repositories\MSiteRepositoryInterface;
use App\Repositories\MCorpRepositoryInterface;
use App\Repositories\DemandInfoRepositoryInterface;
use App\Repositories\MItemRepositoryInterface;
use App\Models\DemandInfo;
use App\Repositories\MUserRepositoryInterface;
use App\Repositories\SelectGenrePrefectureRepositoryInterface;
use App\Repositories\SelectGenreRepositoryInterface;
use App\Services\UploadFile\UploadFile;
use Aws\CloudFront\Exception\Exception;
use Illuminate\Http\Request;
use App\Services\ExportService;
use App\Http\Requests\DemandListRequest;
use App\Services\DemandInfoService;
use App\Services\MItemService;
use DB;
use Auth;
use App\Services\DemandService;

class DemandListController extends Controller
{
    /**
     * @var boolean
     */
    public $defaultDisplay = false;
    /**
     * @var Request
     */
    protected $request;
    /**
     * @var DemandInfoRepositoryInterface
     */
    public $demandinfoRepository;
    /**
     * @var MItemRepositoryInterface
     */
    public $mItemRepo;
    /**
     * @var MCorpRepositoryInterface
     */
    public $mCorpRepo;
    /**
     * @var MSiteRepositoryInterface
     */
    public $mSite;
    /**
     * @var DemandInfoService
     */
    public $demandInfoService;
    /**
     * @var MUserRepositoryInterface
     */
    protected $mUserRepository;
    /**
     * @var MGenresRepositoryInterface
     */
    protected $mGenresRepository;
    /**
     * @var MCategoryRepositoryInterface
     */
    protected $mCategoryRepository;
    /**
     * @var MSiteGenresRepositoryInterface
     */
    protected $mSiteGenresRepository;
    /**
     * @var SelectGenrePrefectureRepositoryInterface
     */
    protected $selectGenrePrefectureRepository;
    /**
     * @var SelectGenreRepositoryInterface
     */
    protected $selectGenreRepository;
    /**
     * @var DemandAttachedFileRepositoryInterface
     */
    protected $demandAttachedFileRepository;

    /**
     * @var MItemService
     */
    public $mItemService;

    const NON_NOTIFICATION = '非通知';

    /**
     * @var DemandService
     */
    protected $demandService;
    /**
     * @var ExportService
     */
    protected $exportService;
    /**
     *
     * @var $siteIdEnable
     */
    public static $siteIdEnable = [861, 863, 889, 890, 1312, 1313, 1314];
    /**
     * construct function
     * @param Request $request
     * @param DemandInfoRepositoryInterface $demandinfoRepository
     * @param MCorpRepositoryInterface $mCorpRepo
     * @param MSiteRepositoryInterface $mSite
     * @param DemandInfoService $demandInfoService
     * @param MUserRepositoryInterface $mUserRepository
     * @param MGenresRepositoryInterface $mGenresRepository
     * @param MCategoryRepositoryInterface $mCategoryRepository
     * @param MSiteRepositoryInterface $mSiteRepository
     * @param MSiteGenresRepositoryInterface $mSiteGenresRepository
     * @param SelectGenrePrefectureRepositoryInterface $selectGenrePrefectureRepository
     * @param SelectGenreRepositoryInterface $selectGenreRepository
     * @param DemandAttachedFileRepositoryInterface $demandAttachedFileRepository
     * @param MItemService $mItemService
     * @param MItemRepositoryInterface $mItemRepo
     * @param DemandService $demandService
     */
    public function __construct(
        Request $request,
        DemandInfoRepositoryInterface $demandinfoRepository,
        MCorpRepositoryInterface $mCorpRepo,
        MSiteRepositoryInterface $mSite,
        DemandInfoService $demandInfoService,
        MUserRepositoryInterface $mUserRepository,
        MGenresRepositoryInterface $mGenresRepository,
        MCategoryRepositoryInterface $mCategoryRepository,
        MSiteRepositoryInterface $mSiteRepository,
        MSiteGenresRepositoryInterface $mSiteGenresRepository,
        SelectGenrePrefectureRepositoryInterface $selectGenrePrefectureRepository,
        SelectGenreRepositoryInterface $selectGenreRepository,
        DemandAttachedFileRepositoryInterface $demandAttachedFileRepository,
        MItemService $mItemService,
        MItemRepositoryInterface $mItemRepo,
        DemandService $demandService,
        ExportService $exportService
    ) {
        parent::__construct();
        $this->request = $request;
        $this->demandinfoRepository = $demandinfoRepository;
        $this->mCorpRepo = $mCorpRepo;
        $this->mSite = $mSite;
        $this->mItemRepo = $mItemRepo;
        $this->demandInfoService = $demandInfoService;
        $this->mUserRepository = $mUserRepository;
        $this->mGenresRepository = $mGenresRepository;
        $this->mCategoryRepository = $mCategoryRepository;
        $this->mSiteGenresRepository = $mSiteGenresRepository;
        $this->selectGenrePrefectureRepository = $selectGenrePrefectureRepository;
        $this->selectGenreRepository = $selectGenreRepository;
        $this->demandAttachedFileRepository = $demandAttachedFileRepository;
        $this->mItemService = $mItemService;
        $this->demandService = $demandService;
        $this->exportService = $exportService;
    }

    /**
     * index function
     *
     * @param  Request $request
     * @return view
     */
    public function index(Request $request)
    {
        $this->defaultDisplay = true;
        $data = $request->query();
        if ($request->isMethod('get')) {
            if (!empty($data['cti'])) {
                $sessionKeyForAffiliationSearch = self::$sessionKeyForAffiliationSearch;
                $sessionKeyForDemandSearch = self::$sessionKeyForDemandSearch;
                $this->demandInfoService->checkAffiliationResults($request, $data, $sessionKeyForAffiliationSearch, $sessionKeyForDemandSearch);
            } elseif (!empty($data['customer_tel'])) {
                $request->session()->put(self::$sessionKeyForDemandSearch, $data);
                return redirect()->route('demandlist.search');
            }
        }
        return view(
            'demand_list.index',
            [
            "defaultDisplay" => $this->defaultDisplay,
            'siteLists' => $this->mSite->getList(),
            'itemLists' => $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::PROPOSAL_STATUS))
            ]
        );
    }

    /**
     * search demand function
     *
     * @param  integer $id
     * @return view
     */
    public function search($id = null)
    {
        $data = [];

        if (!empty($id)) {
            $listCorp = $this->getMCorp($id);
            $this->request->merge(
                [
                'corp_name' => $listCorp[0]['corp_name'],
                'corp_name_kana' => $listCorp[0]['corp_name_kana']
                ]
            );
            $data = $this->demandListPost();
        }
        try {
            if ($this->request->isMethod('get')) {
                $data = $this->demandListGet();
            }
        } catch (Exception $e) {
            abort('404');
        }
        $demandInfos = $this->demandinfoRepository->getDemandInfo($data);
        return view('demand_list.index', [
            "defaultDisplay" => $this->defaultDisplay,
            "demandInfos" => $demandInfos,
            "auth" => Auth::user()->auth,
            'siteLists' => $this->mSite->getList(),
            'itemLists' => $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::PROPOSAL_STATUS)),
            'conditions' => $data
            ]);
    }

    /**
     * @param DemandListRequest $demandrequest
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function searchDemand(DemandListRequest $demandrequest)
    {
        try {
            if ($demandrequest->isMethod('post')) {
                $data = $this->demandListPost();
            }
        } catch (Exception $e) {
            abort('404');
        }
        if ($demandrequest->input('submit') != '' && $demandrequest->input('submit') == 'csv' && (Auth::user()->auth == 'system'
            || Auth::user()->auth == 'admin' || Auth::user()->auth == 'accounting_admin')
        ) {
            $dataList = $this->demandInfoService->convertDataCsv($this->demandinfoRepository->getDataCsv($data));
            $fileName = trans('demandlist.csv_demand_name') . '_' . Auth::user()->user_id.'.csv';
            $fieldList = DemandInfo::csvFormat();
            return $this->exportService->exportCsv($fileName, $fieldList['default'], $dataList);
        } else {
            $demandInfos = $this->demandinfoRepository->getDemandInfo($data);
            //Sanitize::clean before show
            return view(
                'demand_list.index',
                [
                "defaultDisplay" => $this->defaultDisplay,
                "demandInfos" => $demandInfos,
                "auth" => Auth::user()->auth,
                'siteLists' => $this->mSite->getList(),
                'itemLists' => $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::PROPOSAL_STATUS)),
                'conditions' => $data
                ]
            );
        }
    }

    /**
     * get corp by id function
     *
     * @param  integer $id
     * @return array
     */
    private function getMCorp($id = null)
    {
        return $this->mCorpRepo->getFirstById($id, true);
    }

    /**
     * set again data request post function
     *
     * @return array
     */
    private function demandListPost()
    {

        $data = $this->request->all();
        if (isset($data['data'])) {
            $data = $data['data'];
        }
        unset($data['csv_out']);
        $this->request->session()->forget(self::$sessionKeyForDemandSearch);
        $this->request->session()->put(self::$sessionKeyForDemandSearch, $data);

        return $data;
    }

    /**
     * set again data request get function
     *
     * @return array
     */
    private function demandListGet()
    {
        $data = $this->request->session()->get(self::$sessionKeyForDemandSearch);
        if (!empty($data)) {
            $data += array('page' => $this->request->input('page'));
        }
        return $data;
    }

    /**
     * @author Dung.PhamVan@nashtechglobal.com
     * @param Request $request
     * @param null    $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDetail(Request $request, $id)
    {
        $demand = $this->demandinfoRepository->getDemandByIdWithRelations($id);
        if (!$demand) {
            return redirect()->route('demand.get.create');
        }

        $customerTel = $this->demandinfoRepository->checkIdenticallyCustomer($demand->customer_tel);
        $userDropDownList = $this->mUserRepository->getListUserForDropDown();
        $genresDropDownList = $this->mSiteGenresRepository->getGenreBySiteStHide($demand->site_id);

        $categoriesDropDownList = $this->mCategoryRepository->getListCategoriesForDropDown($demand->genre_id);
        $mSiteDropDownList = $this->mSite->getListMSitesForDropDown();
        $mSiteGenresDropDownList = $this->mSiteGenresRepository->getMSiteGenresDropDownBySiteId($demand->site_id);
        $mItemsDropDownList = $this->mItemRepo->getMItemListByItemCategory('建物種別');
        $prefectureDiv = $this->demandInfoService->translatePrefecture();
        $selectionSystemList = $this->demandInfoService->getSelectionSystemList($demand);

        $priorityDropDownList = $this->demandInfoService->getPriorityTranslate();
        $stClaimDropDownList = $this->mItemRepo->getMItemListByItemCategory('STクレーム');
        $jbrWorkContentDropDownList = $this->mItemRepo->getMItemListByItemCategory('[JBR様]作業内容');
        $specialMeasureDropDownList = $this->mItemRepo->getMItemListByItemCategory('案件特別施策');
        $demandStatusDropDownList = $this->mItemRepo->getMItemListByItemCategory('案件状況');
        $orderFailReasonDropDownList = $this->mItemRepo->getMItemListByItemCategory('案件失注理由');
        $acceptanceStatusDropDownList = $this->mItemRepo->getMItemListByItemCategory('受付ステータス');
        $quickOrderFailReasonDropDownList = $this->mItemRepo->getMItemListByItemCategory('ワンタッチ失注理由');
        $vacationLabels = $this->mItemRepo->getMItemList('長期休業日', date('Y/m/d'));

        $rankList = $this->demandService->getGenreByRank($demand->site_id);
        $enableSiteId = in_array($demand->site_id, [861, 863, 889, 890, 1312, 1313, 1314]);
        return view('demand.detail', compact(
            'demand',
            'userDropDownList',
            'genresDropDownList',
            'categoriesDropDownList',
            'mSiteDropDownList',
            'mSiteGenresDropDownList',
            'mItemsDropDownList',
            'prefectureDiv',
            'selectionSystemList',
            'priorityDropDownList',
            'stClaimDropDownList',
            'jbrWorkContentDropDownList',
            'specialMeasureDropDownList',
            'demandStatusDropDownList',
            'orderFailReasonDropDownList',
            'acceptanceStatusDropDownList',
            'quickOrderFailReasonDropDownList',
            'vacationLabels',
            'holidays',
            'rankList',
            'customerTel',
            'enableSiteId'
        ));
    }


    /**
     * @param $id
     * @return mixed
     */
    public function deleteAttachedFile($id)
    {
        $demandAttachedFile = $this->demandAttachedFileRepository->findId($id);
        $demandAttachedFile->delete();

        return redirect()->back()->withMessage(__('demand.delete_file_success'));
    }

    /**
     * @param DemandAttachedFileRequest $request
     * @param null                      $demandId
     * @return $this
     * @throws \Exception
     * @throws \Throwable
     */
    public function uploadAttachedFile(DemandAttachedFileRequest $request, $demandId = null)
    {
        if (!$request->has('demand_attached_file')) {
            return redirect()->back()->withErrors([__('demand.the_file_field_is_required')]);
        }

        $uploadFiles = $request->file('demand_attached_file');

        foreach ($uploadFiles as $key => $file) {
            DB::transaction(
                function () use ($demandId, $request, $file, $key) {
                    try {
                        $currentDate = date('Y-m-d H:i:s');
                        $userId = Auth::user()->id;
                        $pathUpload = public_path('rits-files/' . $demandId);

                        $nextIndex = $this->demandAttachedFileRepository->getNextIndex();
                        $fileUpload = new UploadFile($file, $pathUpload, $nextIndex);

                        $demandAttachedFile = $this->demandAttachedFileRepository->create(
                            [
                            'id' => $nextIndex,
                            'demand_id' => $demandId,
                            'path' => '',
                            'name' => $fileUpload->getOriginalName(),
                            'content_type' => $fileUpload->getMiMeType(),
                            'create_date' => $currentDate,
                            'create_user_id' => $userId,
                            'update_date' => $currentDate,
                            'update_user_id' => $userId
                            ]
                        );
                        $demandAttachedFile->path = $pathUpload . '/' . $fileUpload->upload();
                        $demandAttachedFile->save();
                    } catch (Exception $e) {
                        return redirect()->withMessage('has something wrong with upload file ' . $key . '');
                    }
                }
            );
        }
        return redirect()->back()->withMessage(__('demand.upload_file_success'));
    }
}
