<?php

namespace App\Http\Controllers\ProgressManagement;

use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\ProgressManagementService;
use App\Services\ProgCorpService;
use App\Services\ProgressManagementItemService;
use App\Repositories\ProgDemandInfoRepositoryInterface;
use App\Repositories\ProgImportFilesRepositoryInterface;
use App\Models\ProgDemandInfo;
use App\Models\MItem;
use App\Models\ProgCorp;
use App\Repositories\MCorpRepositoryInterface;
use App\Repositories\ProgDemandInfoTmpRepositoryInterface;
use App\Repositories\ProgAddDemandInfoTmpRepositoryInterface;
use App\Repositories\ProgDemandInfoOtherTmpRepositoryInterface;
use App\Services\PMUpdateConfirmService;

class ProgressManagementController extends Controller
{
    //business service
    /**
     * @var ProgressManagementService
     */
    public $pMService;
    /**
     * @var ProgCorpService
     */
    public $pCorpService;
    /**
     * @var ProgImportFilesRepositoryInterface
     */
    protected $progImportFile;
    /**
     * @var ProgDemandInfoRepositoryInterface
     */
    protected $progDemandInfo;
    /**
     * @var MCorpRepositoryInterface
     */
    protected $mCorpRepository;

    /**
     * ProgressManagementController constructor.
     * @param ProgressManagementService $pMService
     * @param ProgCorpService $pCorpService
     * @param ProgImportFilesRepositoryInterface $progImportFile
     * @param ProgDemandInfoRepositoryInterface $progDemandInfo
     * @param MCorpRepositoryInterface $mCorpRepository
     */
    public function __construct(
        ProgressManagementService $pMService,
        ProgCorpService $pCorpService,
        ProgImportFilesRepositoryInterface $progImportFile,
        ProgDemandInfoRepositoryInterface $progDemandInfo,
        MCorpRepositoryInterface $mCorpRepository
    ) {
        parent::__construct();
        $this->pMService = $pMService;
        $this->pCorpService = $pCorpService;
        $this->progImportFile = $progImportFile;
        $this->progDemandInfo = $progDemandInfo;
        $this->mCorpRepository = $mCorpRepository;
    }

    /**
     * index progress demand info
     *
     * @author thaihv
     * @param  Request $request http request
     * @return collection
     */
    public function index(Request $request)
    {
        $progFiles = $this->pMService->getImportFileNotDelete();
        if (session('message_del')) {
            $request->session()->flash('message', session('message_del'));
            $request->session()->forget('message_del');
        }
        $confirmDelete = __('progress_management.index.confirm_delete');
        return view('progress_management.index')->with('progFiles', $progFiles)->with('confirmDelete', $confirmDelete);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Request $request
     * @param  $pCorpId
     * @return \Illuminate\Http\Response
     */
    public function adminDemandDetail(Request $request, $pCorpId)
    {
        $pageTitle = '進捗管理システム';
        $pDemandInfos = $this->pMService->adminDemandDetail($pCorpId);
        $addDemandData = $this->pMService->getProgAddDemandInfos($pCorpId);
        $limitAddDemand = config('datacustom.limit_prog_add_demand_info');
        $pageInfo = $this->pMService->getPageInfo($pDemandInfos);
        $progCorp = $pDemandInfos->first()->progCorp;
        $demandTypeUpdate = $progCorp->getDemandTypeUpdate();
        $hidClass = $progCorp->progress_flag == ProgCorp::MAIL_COLLECTED ? 'd-none' : '';
        $guideTitle = $this->pMService->getGuideTitle($progCorp->progress_flag);
        $pmCommissionStatus = ProgDemandInfo::PM_COMMISSION_STATUS;
        array_unshift($pmCommissionStatus, '');
        $diffFllags = ProgDemandInfo::PM_DIFF_LIST;
        $tax = ProgDemandInfo::PM_TAX;
        $cmOrderFailReasonList = $this->pMService->getMItemList(MItem::CONMISSION_ORDER_FAIL_REASON);
        if (session('reacquisition')) {
            $request->session()->flash('message', session('reacquisition'));
            $request->session()->forget('reacquisition');
        }
        if (session('demandAdd')) {
            $request->session()->flash('message', session('demandAdd'));
            $request->session()->forget('demandAdd');
        }
        if (session('multipleUpdate')) {
            $request->session()->flash('message', session('multipleUpdate'));
            $request->session()->forget('multipleUpdate');
        }
        if (session('singleUpdate')) {
            $request->session()->flash('message', session('singleUpdate'));
            $request->session()->forget('singleUpdate');
        }
        $cmOrderFailReasonUpdate = ['' => ''];
        foreach ($cmOrderFailReasonList as $value) {
            $cmOrderFailReasonUpdate[$value['id']] = $value['category_name'];
        }
        return view('progress_management.admin_demand_detail')->with(
            compact(
                'pageTitle',
                'pDemandInfos',
                'addDemandData',
                'limitAddDemand',
                'guideTitle',
                'pCorpId',
                'progCorp',
                'pageInfo',
                'pmCommissionStatus',
                'diffFllags',
                'cmOrderFailReasonUpdate',
                'hidClass',
                'demandTypeUpdate',
                'tax'
            )
        );
    }

    /**
     * update progress demand info
     *
     * @author thaihv
     * @param  Request $request      http request
     * @param  integer $pDMandInfoId progress_demand_info_id
     * @return json status and message
     */
    public function updateProgDemandInfo(Request $request, $pDMandInfoId)
    {
        $pDemandInfo = $request->all();
        $userUpdateInfo = $this->pMService->getUserAgent($request);
        $pDemandInfo = array_merge($userUpdateInfo, $pDemandInfo);
        $updated = $this->pMService->updateProgDemandInfo($pDMandInfoId, $pDemandInfo);
        if ($updated) {
            $progDemandInfo = $this->pMService->findWithCommissionById($pDMandInfoId);
            $this->pMService->updateCommissionInfos([$progDemandInfo], 'admin');
            $this->pMService->insertLog([$progDemandInfo]);
            session(['singleUpdate' => __('progress_management.update_message')]);
            return response()->json(['code' => 200, 'message' => __('progress_management.update_message')]);
        }
        session(['singleUpdate' => __('progress_management.update_all_fail')]);
        return response()->json(['code' => 500, 'message' => __('progress_management.update_all_fail')]);
    }

    /**
     * update progress demand info
     *
     * @author thaihv
     * @param  Request $request http request
     * @return json status and message
     */
    public function multipleUpdate(Request $request)
    {
        $pDemandInfos = $request->get('pData');
        if (empty($pDemandInfos)) {
            return response()->json(['code' => 5001, 'message' => __('progress_management.update_all_success')]);
        }
        if (!is_array($pDemandInfos) && $pDemandInfos == true) {
            session(['multipleUpdate' => __('progress_management.update_all_success')]);
        }
        $userUpdateInfo = $this->pMService->getUserAgent($request);
        $numOfRowUpdated = 0;
        $progIds = [];
        foreach ($pDemandInfos as $value) {
            $pDemandInfo = array_merge($userUpdateInfo, $value);
            $pDMandInfoId = $pDemandInfo['pId'];
            $progIds[] = $pDemandInfo['pId'];
            unset($pDemandInfo['pId']);
            $updated = $this->pMService->updateProgDemandInfo($pDMandInfoId, $pDemandInfo);
            if ($updated) {
                $numOfRowUpdated++;
            }
        }
        $rowMessage = ' (' . $numOfRowUpdated . '/' . count($pDemandInfos);
        if ($numOfRowUpdated > 0) {
            // update log
            $progDemandInfos = $this->pMService->findByIds($progIds);
            if ($progDemandInfos) {
                $this->pMService->insertLog($progDemandInfos);
            }
            session(['multipleUpdate' => __('progress_management.update_all_success')]);
            return response()->json(
                [
                'code' => 200,
                'message' => __('progress_management.update_all_success') . $rowMessage
                ]
            );
        }
        session(['multipleUpdate' => __('progress_management.update_all_success')]);
        return response()->json(
            [
            'code' => 500,
            'message' => __('progress_management.update_all_success') . $rowMessage
            ]
        );
    }

    /**
     * reacquisition
     *
     * @author thaihv
     * @param  Request $request      http request
     * @param  integer $pDMandInfoId progress_demand_info_id
     * @return json status and message
     */
    public function reacquisition(Request $request, $pDMandInfoId)
    {
        $userUpdateInfo = $this->pMService->getUserAgent($request);
        $updated = $this->pMService->reacquisition($pDMandInfoId, $userUpdateInfo);
        if ($updated) {
            session(['reacquisition' => __('progress_management.reacqure_success')]);
            return response()->json(['code' => 200, 'message' => __('progress_management.reacqure_success')]);
        }
        session(['reacquisition' => __('progress_management.reacqure_fail')]);
        return response()->json(['code' => 500, 'message' => __('progress_management.reacqure_fail')]);
    }

    /**
     * add progress ad demand info
     *
     * @author thaihv
     * @param  Request $request http request
     * @param  integer $pCorpId pcorp id
     * @return json status and message
     */
    public function progAddDemandInfoDetail(Request $request, $pCorpId)
    {
        $proCorp = $this->pCorpService->getProgCorpById($pCorpId);
        if (!$proCorp) {
            return response()->json(['code' => 500, 'message' => __('progress_management.corp_not_found')]);
        }
        $data = $request->get('pData');
        $userAgent = $this->pMService->getUserAgent($request);
        foreach ($data as $key => $value) {
            $pData = array_merge($value, $userAgent);
            $pData['prog_corp_id'] = $proCorp->id;
            $pData['prog_import_file_id'] = $proCorp->prog_import_file_id;
            $pData['corp_id'] = $proCorp->corp_id;
            $data[$key] = $pData;
        }
        $insertedIds = $this->pMService->insertUpdateProgAddDemandInfo($data);
        if (!empty($insertedIds)) {
            $progAdds = $this->pMService->getProgAddDemandByIds($insertedIds);
            $this->pMService->insertLog($progAdds, 'add_demand_info');
            session(['demandAdd' => __('progress_management.add_demand_success')]);
            return response()->json(['code' => 200, 'message' => __('progress_management.add_demand_success')]);
        }
        session(['demandAdd' => __('progress_management.add_demand_fail')]);
        return response()->json(['code' => 500, 'message' => __('progress_management.add_demand_fail')]);
    }

    /**
     * get user agent
     *
     * @param  Request $request http request
     * @return array info of user
     */
    private function getUserAgent($request)
    {
        $userUpdateInfo['ip_address_update'] = $request->ip();
        $userUpdateInfo['user_agent_update'] = $request->header('User-Agent');
        $userUpdateInfo['modified'] = date('Y-m-d H:i:s');
        $userUpdateInfo['modified_user_id'] = auth()->user()->user_id;

        return $userUpdateInfo;
    }

    /**
     * Get data to show info in delete commission screen based on fileId
     *
     * @param  $fileId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getDeleteCommissionInfos($fileId)
    {
        $title = trans('progress_management.delete_commission_infos.title');

        if (empty($fileId) || !ctype_digit($fileId)) {
            return Redirect::back();
        } else {
            $file = $this->progImportFile->findOrFail($fileId);
            return view('progress_management.delete_commission_infos', ['title' => $title, 'file' => $file]);
        }
    }

    /**
     * Action delete commission info related to fileId
     *
     * @param  DeleteCommissionInfoRequest $request
     * @param  $fileId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postDeleteCommissionInfos(DeleteCommissionInfoRequest $request, $fileId)
    {
        // Get list id of prog_demand want to delete
        $delTarget = $request->get('delete_ids');

        if (empty($delTarget)) {
            $message = ['error' => trans('progress_management.delete_commission_infos.lack_input_message')];
        } else {
            $message = $this->pMService->deleteCommissionInfos($delTarget, $fileId);
        }

        return Redirect::back()->with($message)->withInput();
    }

    /**
     * get import_commission_infos data
     *
     * @param  integer $fileId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getImportCommissionInfos($fileId = null)
    {
        $progImportFile = $this->progImportFile->find($fileId);
        if (empty($fileId) || !ctype_digit($fileId) || !count($progImportFile)) {
            $errorMessage = trans('mcorp_list.exception');
            return \view('partials.errors', compact('errorMessage'));
        }
        $lock = \Config::get('datacustom.lock');
        return view('progress_management.import_commission_infos', compact('progImportFile', 'lock'));
    }

    /**
     * update import_commission_infos data
     *
     * @param  ImportCommissionInfosRequest $request
     * @param  integer                      $fileId
     * @return string
     */
    public function postImportCommissionInfos(ImportCommissionInfosRequest $request, $fileId)
    {
        try {
            if ($this->pMService->postImportCommissionInfos($request->all(), $fileId)) {
                $request->session()->flash(
                    'box--success',
                    trans('progress_management.import_commission_corp.insert_success')
                );
                return back()->with('oldValue', $request->all());
            } else {
                $request->session()->flash(
                    'box--error',
                    trans('progress_management.import_commission_corp.insert_error')
                );
                return back()->with('oldValue', $request->all());
            }
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }

    /**
     * get progress corp
     *
     * @author thaihv
     * @param  Request $request
     * @param  $fileId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function corpIndex(Request $request, $fileId)
    {

        $importFile = $this->progImportFile->findNotDeleteById($fileId);
        if (!$importFile) {
            return redirect()->route('progress.management.index')->with('message', __('progress_management.err_msg'));
        }
        $searchQuery = $request->all();
        foreach ($searchQuery as $key => $value) {
            if ($key != 'corp_name') {
                $searchQuery[$key] = trim($value);
            }
        }
        if (!empty($searchQuery)) {
            session(['searchQuery' => json_encode($searchQuery)]);
        } else {
            session()->forget('searchQuery');
        }
        if (session('updateProgCorp')) {
            $message = session('updateProgCorp');
            $request->session()->flash('message', $message);
            $request->session()->forget('updateProgCorp');
        }
        $progCorps = $this->pCorpService->getProgCorpWithHoliday($fileId, $searchQuery);
        $progressFlagList = getDropList(MItem::PROGRESS_STATUS_CATEGORY);
        $notReplyFlagList = getDropList(MItem::PROGRESS_STATUS_REPLY_RESULT_CATEGORY);
        $contactTypeList = getDropList(MItem::PROGRESS_DELIVERY_CATEGORY);
        $callBackPhoneFlag = getDropList(MItem::PROGRESS_BACK_PHONE_CATEGORY);
        // js data
        $baseUrl = route('progress.corpIndex', $fileId);
        $confirmUpdateMsg = __('progress_management.confirm_update');
        $mailEmptyMsg = __('progress_management.mail_empty_message');
        $willSendEmailMsg = __('progress_management.will_send_email');
        $willSendFaxMsg = __('progress_management.will_send_fax');
        $faxEmptyMsg = __('progress_management.fax_empty_message');
        $faxNumberMsg = __('progress_management.fax_number_msg');
        $mailNumberMsg = __('progress_management.mail_number_msg');
        $willSendFaxMail = __('progress_management.will_send_fax_mail');
        $sendBulkMail = __('progress_management.send_bulk_mail');
        $sendBulkFax = __('progress_management.send_bulk_fax');
        $noChecked = __('progress_management.no_checked');
        $emailNotEnter = __('progress_management.email_not_enter');
        $faxNotEnter = __('progress_management.fax_not_enter');
        $bulkMailFax = __('progress_management.send_bulk_mail_fax');
        $corpIdNull = __('progress_management.corp_id_null');

        return view(
            'progress_management.corp_index',
            compact(
                'importFile',
                'progCorps',
                'progressFlagList',
                'notReplyFlagList',
                'contactTypeList',
                'callBackPhoneFlag',
                'searchQuery',
                'confirmUpdateMsg',
                'mailEmptyMsg',
                'willSendEmailMsg',
                'willSendFaxMsg',
                'faxEmptyMsg',
                'faxNumberMsg',
                'willSendFaxMail',
                'baseUrl',
                'sendBulkMail',
                'noChecked',
                'emailNotEnter',
                'sendBulkFax',
                'faxNotEnter',
                'bulkMailFax',
                'corpIdNull',
                'fileId',
                'mailNumberMsg'
            )
        );
    }

    /**
     * update progress corp
     *
     * @author thaihv
     * @param  Request    $request
     * @param  $fileId
     * @param  $progCorpId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateProgressCorp(Request $request, $fileId, $progCorpId)
    {
        $searchQuery = json_decode(session()->pull('searchQuery'));
        if (empty($searchQuery)) {
            $searchQuery = [];
        }
        $searchQuery = (array)$searchQuery;
        $searchQuery['fileId'] = $fileId;
        $progCorpInput = $request->except('_token', '_method');
        $corpName = $progCorpInput['official_corp_name'];
        unset($progCorpInput['official_corp_name']);
        $updated = $this->pCorpService->updateProgressCorp($progCorpId, $progCorpInput);
        if ($updated) {
            $message = __('progress_management.update_prog_success', ['coprName' => $corpName]);
        } else {
            $message = __('progress_management.update_prog_fail', ['coprName' => $corpName]);
        }
        session(['updateProgCorp' =>  $message]);
        return response()->json(['status' => 200, 'message'=> $message]);
    }

    /**
     * send corp email
     *
     * @author thaihv
     * @param  Request $request
     * @param  integer $fileId  prog_import file id
     * @param  integer $pCorpId prog_corps id
     * @return json           status and message
     */
    public function sendEmail(Request $request, $fileId, $pCorpId)
    {
        $corpRequestData = $request->all();
        $count = $this->pCorpService->sendEmail($pCorpId, $corpRequestData);
        if ($count > 0) {
            $saved = $this->pCorpService->updateAfterSendEmail($pCorpId, $count);
            if ($saved) {
                $request->session()->flash(
                    'message',
                    __('progress_management.email_send_success', ['corpName' => $corpRequestData['name']])
                );
                return response()->json(['status' => 200, 'message' => 'send email and save success']);
            }
            $request->session()->flash(
                'message',
                __('progress_management.email_send_fail', ['corpName' => $corpRequestData['name']])
            );
            return response()->json(['status' => 500, 'message' => 'save prog info fail']);
        }
        $request->session()->flash(
            'message',
            __('progress_management.email_send_fail', ['corpName' => $corpRequestData['name']])
        );
        return response()->json(['status' => 500, 'message' => 'send email fail']);
    }

    /**
     * send corp fax
     *
     * @author thaihv
     * @param  Request $request
     * @param  integer $fileId  prog_import file id
     * @param  integer $pCorpId prog_corps id
     * @return json           status and message
     */
    public function sendFax(Request $request, $fileId, $pCorpId)
    {
        set_time_limit(0);
        $corpRequestData = $request->all();
        $count = $this->pCorpService->sendFax($pCorpId, $corpRequestData);
        if ($count > 0) {
            $saved = $this->pCorpService->updateAfterSendFax($pCorpId, $count);
            if ($saved) {
                $request->session()->flash(
                    'message',
                    __('progress_management.fax_send_success', ['corpName' => $corpRequestData['name']])
                );
                return response()->json(['status' => 200, 'message' => 'send fax and save success']);
            }
            $request->session()->flash(
                'message',
                __('progress_management.fax_send_fail', ['corpName' => $corpRequestData['name']])
            );
            return response()->json(['status' => 500, 'message' => 'save prog info fail']);
        }
        $request->session()->flash(
            'message',
            __('progress_management.fax_send_fail', ['corpName' => $corpRequestData['name']])
        );
        return response()->json(['status' => 500, 'message' => 'send fax fail']);
    }

    /**
     * send fax and email
     *
     * @param  Request $request
     * @param  integer $fileId  prog_import_file
     * @param  integer $pCorpId prog_corps id
     * @return boolean
     */
    public function sendMailFax(Request $request, $fileId, $pCorpId)
    {
        $corpRequestData = $request->all();
        $faxsData = [];
        $faxsData['name'] = $corpRequestData['name'];
        $faxsData['faxs'] = $corpRequestData['faxs'];

        $mailData = [];
        $mailData['name'] = $corpRequestData['name'];
        $mailData['emails'] = $corpRequestData['emails'];

        // send email

        $countEmail = $this->pCorpService->sendEmail($pCorpId, $mailData);
        $success = true;
        $message = '';
        if ($countEmail > 0) {
            $saved = $this->pCorpService->updateAfterSendEmail($pCorpId, $countEmail);
            if ($saved) {
                $message .= __('progress_management.email_send_success', ['corpName' => $mailData['name']]) . '<br />';
            } else {
                $success = false;
                $message .= __('progress_management.email_send_fail', ['corpName' => $mailData['name']]) . '<br />';
            }
        }
        $countFax = $this->pCorpService->sendFax($pCorpId, $faxsData);
        if ($countFax > 0) {
            $saved = $this->pCorpService->updateAfterSendFax($pCorpId, $countFax);
            if ($saved) {
                $message .= __('progress_management.fax_send_success', ['corpName' => $faxsData['name']]) . '<br />';
            } else {
                $success = false;
                $message .= __('progress_management.fax_send_fail', ['corpName' => $faxsData['name']]) . '<br />';
            }
        }
        $request->session()->flash('message', $message);
        if ($success) {
            return response()->json(['status' => 200, 'message' => 'sent and update successfully']);
        }
        return response()->json(['status' => 500, 'message' => 'sent and update failed']);
    }

    /**
     * send bulk email
     *
     * @author thaihv
     * @param  Request $request
     * @param  integer $fileId  prog_corps id
     * @return json
     */
    public function sendMultipleEmail(Request $request, $fileId)
    {
        set_time_limit(0);
        $dataMail = $request->get('data');
        $total = 0;
        foreach ($dataMail as $val) {
            $pCorpId = $val['pCorpId'];
            $mails = [
                'name' => $val['name'],
                'emails' => $val['emails']
            ];

            $count = $this->pCorpService->sendEmail($pCorpId, $mails);
            if ($count > 0) {
                $total += $count;
                $saved = $this->pCorpService->updateAfterSendEmail($pCorpId, $count);
                if ($saved) {
                    $request->session()->flash('message', __('progress_management.sent_bulk_mail'));
                }
            }
        }
        return response()->json(['status' => '200', 'message' => 'total: ' . $total]);
    }

    /**
     * send bulk email fax
     *
     * @author thaihv
     * @param  Request $request
     * @param  integer $fileId  prog_corps id
     * @return json
     */
    public function sendMultipleFax(Request $request, $fileId)
    {
        set_time_limit(0);
        $dataFax = $request->get('data');
        $total = 0;
        foreach ($dataFax as $val) {
            $pCorpId = $val['pCorpId'];
            $faxs = [
                'name' => $val['name'],
                'faxs' => $val['faxs']
            ];

            $count = $this->pCorpService->sendFax($pCorpId, $faxs);
            if ($count > 0) {
                $total += $count;
                $saved = $this->pCorpService->updateAfterSendFax($pCorpId, $count);
                if ($saved) {
                    $request->session()->flash('message', __('progress_management.sent_bulk_fax'));
                }
            }
        }
        return response()->json(['status' => '200', 'message' => 'total: ' . $total]);
    }

    /**
     * send bulk email and fax
     *
     * @author thaihv
     * @param  Request $request
     * @param  integer $fileId  prog_corps id
     * @return json
     */
    public function sendMultipleMailFax(Request $request, $fileId)
    {
        set_time_limit(0);
        $dataFax = $request->get('faxs');
        $dataMail = $request->get('mails');
        $totalMail = 0;
        $totalFax = 0;
        // send fax data
        $messageMail = '';
        $messageFax = '';
        foreach ($dataFax as $val) {
            $pCorpId = $val['pCorpId'];
            $faxs = [
                'name' => $val['name'],
                'faxs' => $val['faxs']
            ];

            $count = $this->pCorpService->sendFax($pCorpId, $faxs);
            if ($count > 0) {
                $totalFax += $count;
                $saved = $this->pCorpService->updateAfterSendFax($pCorpId, $count);
                if ($saved) {
                    $messageMail = __('progress_management.sent_bulk_fax') . '<br />';
                }
            }
        }
        // send mail data
        foreach ($dataMail as $val) {
            $pCorpId = $val['pCorpId'];
            $mails = [
                'name' => $val['name'],
                'emails' => $val['emails']
            ];

            $count = $this->pCorpService->sendEmail($pCorpId, $mails);
            if ($count > 0) {
                $totalMail += $count;
                $saved = $this->pCorpService->updateAfterSendEmail($pCorpId, $count);
                if ($saved) {
                    $messageFax = __('progress_management.sent_bulk_mail') . '<br />';
                }
            }
        }
        $request->session()->flash('message', $messageMail . $messageFax);
        return response()->json([
            'status' => '200',
            'message' => 'totalMail: ' . $totalMail . '. totalFax: ' . $totalFax
        ]);
    }

    /**
     * [outputCSV description]
     *
     * @author thaihv
     * @param  Request $request
     * @param  integer $fileId prog_corps id
     * @return \Illuminate\Contracts\View\Factory|
     * \Illuminate\View\View|\Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function outputCSV(Request $request, $fileId)
    {
        set_time_limit(0);
        $file = $this->pCorpService->outputCSV($fileId);
        if (!$file) {
            return view('errors.404');
        }
        return $file;
    }

    /**
     * @param $pCorpId
     * @return bool|\Illuminate\Contracts\View\Factory|
     * \Illuminate\View\View|
     * \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function outputFileCSV($pCorpId)
    {
        set_time_limit(0);
        $file = $this->pCorpService->outputCSV($pCorpId, 'file');
        if (!$file) {
            return view('errors.404');
        }
        return $file;
    }

    /**
     * [outputPDF description]
     *
     * @author thaihv
     * @param  Request $request
     * @param  $pCorpId
     * @return file
     */
    public function outputPDF(Request $request, $pCorpId)
    {
        set_time_limit(0);
        ini_set("pcre.backtrack_limit", "5000000");
        $type = 'D'; // D will do download, F for storage
        $file = $this->pCorpService->outputPDF($pCorpId, $type);
        if (!$file) {
            return view('errors.404');
        }
        return $file;
    }

    /**
     * get prog item for edit (table prog_items)
     * @param ProgressManagementItemService $itemService
     * @return view
     */
    public function itemEdit(ProgressManagementItemService $itemService)
    {
        $item = $itemService->getProgressItem();
        if ($item !== null) {
            return view('progress_management.item_edit')->with(compact('item'));
        } else {
            abort(404);
        }
    }

    /**
     * update prog_item edited (table prog_items)
     * @param $progressItemId
     * @param Request                       $request
     * @param ProgressManagementItemService $itemService
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateItemEdit($progressItemId, Request $request, ProgressManagementItemService $itemService)
    {
        $data = $request->all();
        $result = $itemService->updateItem($progressItemId, $data);
        //update successfully
        if ($result == true) {
            $message['type'] = 'success';
            $message['text'] = trans('progress_management.item_update.success_message');
        } else { // update fail
            $message['type'] = 'error';
            $message['text'] = trans('progress_management.item_update.error_message');
        }
        return redirect()->back()->with('flash_message', $message);
    }

    /**
     * @param $fileId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function fileDelete($fileId)
    {
        $message = $this->pMService->deleteFile($fileId);
        return redirect()->route('progress.management.index')->with('message_del', $message);
    }

    /**
     * show page update  confirm
     *
     * @param  integer $progImportFileId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws Exception
     */
    public function showUpdateConfirm($progImportFileId = null)
    {
        try {
            $corpId         = Auth::user()->affiliation_id;
            $corpInfo       = $this->mCorpRepository->getFirstMCorp($corpId);
            $officialCorpName = !empty($corpInfo->official_corp_name) ? $corpInfo->official_corp_name : '';
            $progCorp       = $this->progCorpRepository->findFirstByCorpIdAndFileId($corpId, $progImportFileId);
            $progDemandInfo = $this->progDemandInfoTmpRepo->getByProgCorpId($progCorp);
            if (!count($progDemandInfo)) {
                throw new Exception();
            }
            $progAddDemandInfo  = $this->progAddDemandInfoTmpRepo->getByProgCorpId($progCorp);
            $progImportFile     = $this->progDemandInfoOtherTmpRepo->findByProgCorpId($progCorp);
            $pmCommissionStatus = ProgDemandInfo::PM_COMMISSION_STATUS;
            $diffFllags         = ProgDemandInfo::PM_DIFF_LIST;
            $reasonList         = getDropList(config('rits.commission_order_fail_reason'));
            $demandTypeList     = config('rits.demand_type_list');
            return view(
                'progress_management.update_confirm',
                compact(
                    'progDemandInfo',
                    'progAddDemandInfo',
                    'progImportFile',
                    'pmCommissionStatus',
                    'diffFllags',
                    'reasonList',
                    'demandTypeList',
                    'progImportFileId',
                    'officialCorpName'
                )
            );
        } catch (Exception $exception) {
            $errorMessage = trans('pm_update_confirm.exception');
            return view('partials.errors', compact('errorMessage'));
        }
    }

    /**
     * update confirm
     *
     * @param  Request  $request
     * @param  interger $progImportFileId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function updateConfirm(Request $request, $progImportFileId = null)
    {
        try {
            $data = $request->all();
            $result = $this->pmUpdateConfirmService->updateConfirm($data, $progImportFileId);
            if ($result) {
                return redirect()->route('progress_management.show.update_end');
            }
            $errorMessage = trans('pm_update_confirm.exception');
            return view('partials.errors', compact('errorMessage'));
        } catch (Exception $exception) {
            $errorMessage = trans('pm_update_confirm.exception');
            return view('partials.errors', compact('errorMessage'));
        }
    }

    /**
     * show page update end
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showUpdateEnd()
    {
        $corpId    = Auth::user()->affiliation_id;
        $corpInfo  = $this->mCorpRepository->getFirstMCorp($corpId);
        $pageTitle = !empty($corpInfo->official_corp_name) ? $corpInfo->official_corp_name : '';
        return view('progress_management.update_end', compact('pageTitle'));
    }

    /**
     * @param Request $request
     * @param null    $fileId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function affDemandDetail(Request $request, $fileId = null)
    {
        //todo check fileId exists
        $tax = ProgDemandInfo::PM_TAX;
        $corpId = Auth::user()->affiliation_id;
        $diffFlags = ProgDemandInfo::PM_DIFF_LIST_DEMAND_DETAIL;
        $commissionStatusList = ProgDemandInfo::PM_COMMISSION_STATUS;
        array_unshift($commissionStatusList, '');
        $flag = true;

        $pc = $this->pCorpService->getProgCorpByFlag($corpId, $fileId, 2);

        $corpInfo  = $this->mCorpRepository->findMcorp($corpId);
        if (empty($pc)) {
            $flag = false;
            return view(
                'progress_management.demand_detail.aff_demand_detail',
                [
                'flag'     => $flag,
                'corpInfo' => $corpInfo,
                'id'       => $fileId
                ]
            );
        }

        $progTmp  = $this->pCorpService->getTmp($pc->id);

        $progData = $this->pCorpService->getProgDemandInfos($pc->id);

        $pageInfo = $this->pMService->getPageInfo($progData['ProgDemandInfoPaginate']);
        $dataPaginate = $progData['ProgDemandInfoPaginate'];

        foreach ($progData['ProgDemandInfo'] as $key => $val) {
            foreach ($progTmp as $tmp) {
                if ($tmp['prog_demand_info_id'] == $val['id']) {
                    $progData['ProgDemandInfo'][$key] = $tmp;
                    break;
                }
            }
        }

        $data['ProgDemandInfo'] = $progData['ProgDemandInfo'];

        if ($progTmp) {
            $data['ProgAddDemandInfo'] = $this->pCorpService->getTmp($pc->id, 'add_demand_info');
            if (empty($data['ProgAddDemandInfo']) && !empty($progData['ProgAddDemandInfo'])) {
                    $data['ProgAddDemandInfo'] = $progData['ProgAddDemandInfo'];
            }

            $data['ProgImportFile'] = $this->pCorpService->getTmp($pc->id, 'file');
            $data['ProgDemandInfoOther'] = $this->pCorpService->getTmp($pc->id, 'other');
        }

        $pageTitle = $corpInfo->official_corp_name.'様 案件一覧';
        $pi        = $this->pCorpService->getProgItem();
        $commissionOrderFailReasonList  = getDropList(MItem::COMMISSION_ORDER_FAIL_REASON);
        array_unshift($commissionOrderFailReasonList, "");

        if ($request->ajax()) {
            $dataPost = $request->all();
            $this->pMService->setTmp($dataPost, $dataPost['prog_corp_id']);
            //case submit Session
            if (isset($dataPost['submitSession'])) {
                return '';
            }
            //case next and back button
            return view(
                'progress_management.component.ajax_form_demand_detail',
                [
                'tax'       => $tax,
                'id'        => $fileId,
                'corpInfo'  => $corpInfo,
                'pageTitle' => $pageTitle,
                'prog_corp_id' => $pc->id,
                'pi'        => $pi,
                'dataPaginate' => $dataPaginate,
                'pageInfo'     => $pageInfo,
                'data'      => $data,
                'diffFlags' => $diffFlags,
                'commissionStatus' => $commissionStatusList,
                'commissionOrderFailReasonList' => $commissionOrderFailReasonList,
                'flag'       => $flag,
                ]
            );
        }

        if (!$request->ajax() && $request->isMethod('post')) {
            //case last submit
            $dataPost = $request->all();
            $this->pMService->setTmp($dataPost, $dataPost['prog_corp_id']);
            return redirect()->route(
                'progress_management.show.update_confirm',
                ['progImportFileId' => $dataPost['ProgImportFile']['file_id']]
            );
        }

        return view(
            'progress_management.demand_detail.aff_demand_detail',
            [
            'tax'       => $tax,
            'id'        => $fileId,
            'corpInfo'  => $corpInfo,
            'pageTitle' => $pageTitle,
            'prog_corp_id' => $pc->id,
            'pi'        => $pi,
            'dataPaginate' => $dataPaginate,
            'pageInfo'     => $pageInfo,
            'data'      => $data,
            'diffFlags' => $diffFlags,
            'commissionStatus' => $commissionStatusList,
            'commissionOrderFailReasonList' => $commissionOrderFailReasonList,
            'flag'       => $flag,
            ]
        );
    }
}
