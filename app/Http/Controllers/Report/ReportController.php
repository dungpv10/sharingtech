<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use App\Models\AntisocialCheck;
use App\Models\CommissionInfo;
use App\Models\MCorpCategoriesTemp;
use App\Repositories\AntisocialCheckRepositoryInterface;
use App\Repositories\ApprovalRepositoryInterface;
use App\Repositories\CommissionInfoRepositoryInterface;
use App\Repositories\DemandInfoRepositoryInterface;
use App\Repositories\MCorpCategoriesTempRepositoryInterface;
use App\Repositories\MCorpRepositoryInterface;
use App\Repositories\MGenresRepositoryInterface;
use App\Services\ExportService;
use App\Services\MItemService;
use App\Services\Report\ReportCorpCategoryGroupApplicationService;
use App\Services\Report\ReportCorpCommissionService;
use App\Services\Report\ReportDevSearchService;
use App\Services\Report\ReportRealTimeService;
use App\Services\ReportJbrCommissionService;
use App\Services\ReportService;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ReportController extends Controller
{
    const CSVOUT = 'CSV出力';
    /**
     * @var AntisocialCheckRepositoryInterface
     */
    public $antisocialcheckRepository;
    /**
     * @var CommissionInfoRepositoryInterface
     */
    protected $commissionRepository;
    /**
     * @var MCorpCategoriesTempRepositoryInterface
     */
    protected $mCorpCategoriesTempRepo;
    /**
     * @var ApprovalRepositoryInterface
     */
    protected $approvalRepo;
    /**
     * @var DemandInfoRepositoryInterface
     */
    protected $demandInfoRepository;
    /**
     * @var ReportJbrCommissionService
     */
    protected $reportCommissionService;
    /**
     * @var ReportCorpCommissionService
     */
    protected $reportCorpCommissionService;
    /**
     * @var ReportRealTimeService
     */
    protected $reportRealTimeService;
    /**
     * @var ReportCorpCategoryGroupApplicationService
     */
    protected $reportCorpCateGroupAppService;
    /**
     * @var MCorpRepositoryInterface
     */
    protected $mCorpRepository;
    /**
     * @var MGenresRepositoryInterface
     */
    protected $genreRepo;
    /**
     * @var ReportDevSearchService
     */
    protected $reportDevSearchService;
    /**
     * @var MItemService
     */
    protected $mItemService;
    /**
     * @var ReportService
     */
    protected $service;
    /**
     * @var ExportService
     */
    protected $exportService;

    /**
     * ReportController constructor.
     *
     * @param AntisocialCheckRepositoryInterface        $antisocialcheckRepository
     * @param CommissionInfoRepositoryInterface         $commissionRepository
     * @param ReportJbrCommissionService                $reportCommissionService
     * @param DemandInfoRepositoryInterface             $demandInfoRepository
     * @param ReportCorpCommissionService               $reportCorpCommissionService
     * @param ApprovalRepositoryInterface               $approvalRepository
     * @param MCorpCategoriesTempRepositoryInterface    $mCorpCategoriesTempRepo
     * @param ReportRealTimeService                     $reportRealTimeService
     * @param ReportCorpCategoryGroupApplicationService $reportCorpCateGroupAppService
     * @param MCorpRepositoryInterface $mCorpRepository
     * @param MGenresRepositoryInterface $mGenresRepository
     * @param ReportDevSearchService $devSearchService
     * @param MItemService $mItemService
     * @param ReportService $service
     * @param ExportService $exportService
     */
    public function __construct(
        AntisocialCheckRepositoryInterface $antisocialcheckRepository,
        CommissionInfoRepositoryInterface $commissionRepository,
        ReportJbrCommissionService $reportCommissionService,
        DemandInfoRepositoryInterface $demandInfoRepository,
        ReportCorpCommissionService $reportCorpCommissionService,
        ApprovalRepositoryInterface $approvalRepository,
        MCorpCategoriesTempRepositoryInterface $mCorpCategoriesTempRepo,
        ReportRealTimeService $reportRealTimeService,
        ReportCorpCategoryGroupApplicationService $reportCorpCateGroupAppService,
        MCorpRepositoryInterface $mCorpRepository,
        MGenresRepositoryInterface $mGenresRepository,
        ReportDevSearchService $devSearchService,
        MItemService $mItemService,
        ReportService $service,
        ExportService $exportService
    ) {
        parent::__construct();
        $this->antisocialcheckRepository = $antisocialcheckRepository;
        $this->commissionRepository = $commissionRepository;
        $this->reportCommissionService = $reportCommissionService;
        $this->demandInfoRepository = $demandInfoRepository;
        $this->reportCorpCommissionService = $reportCorpCommissionService;
        $this->approvalRepo = $approvalRepository;
        $this->mCorpCategoriesTempRepo = $mCorpCategoriesTempRepo;
        $this->mCorpRepository = $mCorpRepository;
        $this->genreRepo = $mGenresRepository;
        $this->reportDevSearchService = $devSearchService;
        $this->reportRealTimeService = $reportRealTimeService;
        $this->reportCorpCateGroupAppService = $reportCorpCateGroupAppService;
        $this->mItemService = $mItemService;
        $this->service = $service;
        $this->exportService = $exportService;
    }

    /**
     * @param $amount
     * @return string
     */
    public static function yenFormatJbr($amount)
    {
        if (is_numeric($amount)) {
            return __('report_sales_support.money_correspond.yen2') . number_format($amount);
        } else {
            return __('report_sales_support.money_correspond.yen2') . '0';
        }
    }

    /**
     * @return view
     */
    public function index()
    {
        return view('report.index');
    }

    /**
     * get data antisocial follow to view
     *
     * @param  Request $request
     * @return view
     * @throws \Throwable
     */
    public function antisocialFollow(Request $request)
    {
        $results = $this->antisocialcheckRepository->getAntisocialList();
        $isUpdateAuthority = $this->antisocialcheckRepository->isUpdateAuthority(Auth::user()->auth);
        if ($request->ajax()) {
            return response()->json(
                view(
                    'report.components.antisocial_follow_table',
                    [
                    "results" => $results,
                    "isUpdateAuthority" => $isUpdateAuthority,
                    ]
                )->render()
            );
        }

        return view(
            'report.antisocial_follow',
            [
            "results" => $results,
            "isUpdateAuthority" => $isUpdateAuthority,
            ]
        );
    }

    /**
     * post data antisocial follow
     *
     * @param  Request $request
     * @return null
     */
    public function antisocialFollowUpdate(Request $request)
    {
        $dataparam = $request->all();
        if (isset($dataparam['csv_out'])) {
            $fileName = trans('antisocial_follow.antisocial_follow') . '_' . Auth::user()->user_id.'.csv';
            $fieldList = AntisocialCheck::csvFormat();
            $dataList = $this->antisocialcheckRepository->getDataCsv();
            return $this->exportService->exportCsv($fileName, $fieldList, $dataList);
        }
        if (isset($dataparam['update'])) {
            $result = $this->antisocialcheckRepository->updateDataAntisocialFollow($dataparam, Auth::user()->auth);
            if ($result) {
                $request->session()->flash('Update', trans('aff_corptargetarea.update'));
            } else {
                $request->session()->flash('InputError', trans('bill.output_message_no_data'));
            }
        }

        return back();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function jbrReceiptFollow()
    {
        return view('report.jbr_receipt_follow');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function getListReceiptFollow(Request $request)
    {
        $fromDate = $request->get('from_date');
        $toDate = $request->get('to_date');
        $order = $request->get('order');
        $nameColumn = $request->get('nameColumn');
        try {
            $results = $this->orderList(
                $this->commissionRepository->getListJbrReceiptFollow($fromDate, $toDate, true),
                $order,
                $nameColumn
            );
            $numberCorp = $this->commissionRepository->getListJbrReceiptFollow(
                $fromDate,
                $toDate,
                false
            )->groupBy('m_corps.id')->limit(trans('report_jbr.limitRecord'))->get()->count();
            $numberCorp = trans('report_jbr.number_corp') . $numberCorp;

            if ($request->ajax()) {
                return view('report.component_jbr_receipt', compact('results', 'numberCorp'));
            }

            return view('report.jbr_receipt_follow', compact('results', 'numberCorp'));
        } catch (\Exception $exception) {
            Log::error($exception);
        }
    }

    /**
     * @param $listJbr
     * @param $order
     * @param $nameColumn
     * @return mixed
     */
    private function orderList($listJbr, $order, $nameColumn)
    {
        $order = !empty($order) ? $order : 'asc';
        $nameColumn = !empty($nameColumn) ? $nameColumn : 'commission_infos.follow_date';

        return $listJbr->orderBy($nameColumn, $order)->paginate(\Config::get('datacustom.report_number_row'));
    }

    /**
     * @param Request $request
     * @return string
     */
    public function getCsvListReceiptFollow(Request $request)
    {
        $fromDate = $request->get('from_date');
        $toDate = $request->get('to_date');
        try {
            $listJbr = [];
            $fieldList = CommissionInfo::csvFormat();

            $fileName = trans('report_jbr.receipt_follow_csv').'_'.date('YmdHis', time());

            $listJbr = $this->commissionRepository->getListJbrReceiptFollow(
                $fromDate,
                $toDate,
                true
            )->limit(trans('report_jbr.limitRecord'))->orderBy(
                'commission_infos.follow_date',
                'asc'
            )->get()->toArray();
            $csvData = $this->reportCommissionService->setCsvData($listJbr, $fieldList);

            return Excel::create(
                $fileName,
                function ($excel) use ($csvData, $fileName) {
                    $excel->sheet(
                        $fileName,
                        function ($sheet) use ($csvData) {
                            $sheet->fromArray($csvData);
                        }
                    );
                }
            )->download('csv');
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    /**
     * get data for report corp agreement category
     *
     * @return view
     */
    public function corpAgreementCategory()
    {
        $dataResult = $this->mCorpCategoriesTempRepo->getCorpAgreementCategory();

        return view(
            'report.corp_agreement_category',
            [
            'dataResult' => $dataResult,
            ]
        );
    }

    /**
     * get data for report corp agreement category by ajax
     * @param $request
     * @return view
     * @throws \Throwable
     */
    public function corpAgreementCategoryAjax(Request $request)
    {
        if ($request->ajax()) {
            $results = $this->mCorpCategoriesTempRepo->getCorpAgreementCategory();
            if (count($results) == 0) {
                return response()->json(view('report.components.error')->render());
            }
            try {
                return response()->json(view('report.components.corp_agreement_category_table', [
                    "dataResult" => $results
                ])->render());
            } catch (Exception $e) {
                return $e->getMessage();
            }
        }
    }
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function exportCsvCorpAgreementCategory(Request $request)
    {
        if ($request->input('csv_out') === self::CSVOUT) {
            $dataList = $this->mCorpCategoriesTempRepo->getCsvCorpAgreementCategory();

            $fileName = trans('report_corp_agreement_category.name_csv') . '_' . Auth::user()->user_id.'.csv';

            $fieldList = MCorpCategoriesTemp::csvFormat();
            $exportService = new ExportService();
            return $exportService->exportCsv($fileName, $fieldList, $dataList);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function auctionFall(Request $request)
    {
        $data['url'] = '/report/auction_fall';
        $data['named'] = $request->input('named');
        $request->session()->forget(self::$sessionKeyForReport);
        $request->session()->put(self::$sessionKeyForReport, $data);

        $dataResult = $this->demandInfoRepository->getDemandForReport(
            $request->input('sort'),
            $request->input('direction')
        );

        return view(
            'report.auction_fall',
            [
            "results" => $dataResult,
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function auctionFallTable(Request $request)
    {
        $dataResult = $this->demandInfoRepository->getDemandForReport(
            $request->input('sort'),
            $request->input('direction')
        );

        return view(
            'report.auction_fall_table',
            [
            "results" => $dataResult,
            ]
        );
    }

    /**
     * @auth Dung.PhamVan@nashtechglobal.com
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getJbrCommission(Request $request)
    {
        $orderRequest = $request->all();
        $sortOptions = config('report.commissionSortOption');
        $sortDefault = [
            ['name' => 'corp_name', 'sort' => 'asc'],
            ['name' => 'contact_desired_time', 'sort' => 'asc'],
            ['name' => 'commission_rank', 'sort' => 'asc'],
            ['name' => '', 'sort' => ''],
        ];

        $arrayCombine = [];
        if (isset($orderRequest['order_by']) && !is_array($orderRequest['order_by']) && !is_array($orderRequest['sort_by'])) {
            $arrayCombine[$orderRequest['order_by']] = $orderRequest['sort_by'];
        } else {
            $orderBy = $orderRequest['order_by'] ?? [];
            $sortBy = $orderRequest['sort_by'] ?? [];

            foreach (array_keys($orderBy) as $key) {
                if (!array_key_exists($key, $sortBy)) {
                    $sortBy[$key] = '';
                }
            }

            $arrayCombine = count($orderRequest) != 0 ? array_combine($orderBy, $sortBy) : [];
        }

        $reportData = $this->demandInfoRepository->getJbrCommissionReport($arrayCombine)->appends($orderRequest);
        $totalRecord = $this->demandInfoRepository->totalRecordCommissionReport($arrayCombine);

        return view('report.jbr_commission', compact('sortOptions', 'sortDefault', 'reportData', 'totalRecord'));
    }

    /**
     * show page jbr ongoing
     *
     * @param  Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function jbrOngoing(Request $request)
    {
        $data = $request->all();
        $detailSort = ReportService::fomartDetailSortJbrOngoing($data);
        $results = $this->demandInfoRepository->getJbrOngoing($detailSort);
        if ($request->ajax()) {
            return view('report.component.ongoing', compact('results'));
        }

        return view('report.jbr_ongoing', compact('results'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function indexCorpCommission()
    {
        $sortOptions = config('report.sortOptions');

        $strGetParam = [
            'demand_follow_date' => '',
            'detect_contact_desired_time' => '',
            'commission_rank' => '',
            'site_id' => '',
            'corp_name' => '',
            'business_day' => '',
            'first_commission' => '',
            'user_name' => '',
            'modified' => '',
            'auction' => '',
            'cross_sell_implement' => '',
        ];

        $filterOptions = config('report.filterOptions');
        $contactRequest = config('report.contactRequest');
        $genreRank = config('report.genreRank');
        $dayOfTheWeek = config('report.dayOfTheWeek');
        $historyUpdate = config('report.historyUpdate');

        $defaultOrder = [
            'order' => [
                1 => 'corp_name',
                2 => 'contact_desired_time',
                3 => 'commission_rank',
            ],
            'direction' => [
                1 => 'asc',
                2 => 'asc',
                3 => 'asc',
            ],
        ];

        return view(
            'report.corp_commission.corp_commission',
            [
            'sortOptions' => $sortOptions,
            'strGetParam' => $strGetParam,
            'filterOptions' => $filterOptions,
            'contactRequest' => $contactRequest,
            'genreRank' => $genreRank,
            'dayOfTheWeek' => $dayOfTheWeek,
            'historyUpdate' => $historyUpdate,
            'defaultOrder' => json_encode($defaultOrder),
            'results' => null,
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function searchCorpCommission(Request $request)
    {
        $buildFilter = [];
        $order = [];
        for ($i = 1; $i < 5; $i++) {
            if ($request->get('order' . $i)) {
                if ($request->get('order' . $i) === 'address1') {
                    $key = 'm_address1.address1';
                } elseif ($request->get('order' . $i) === 'item_id') {
                    $key = 'demand_infos.id';
                } else {
                    $key = $request->get('order' . $i);
                }
                $order[$key] = ($request->get('direction' . $i)) ? $request->get('direction' . $i) : 'asc';
            }
        }

        // A follow-up date
        if ($request->get('filter_demand_follow_date')) {
            $buildFilter['demand_infos.follow_date'] = $request->get('filter_demand_follow_date');
        }

        // Requested date and time
        if ($request->get('filter_detect_contact_desired_time')) {
            $buildFilter['demand_infos.contact_desired_time'] = $request->get('filter_detect_contact_desired_time');
        }

        // Genre Rank
        if ($request->get('filter_commission_rank')) {
            $buildFilter['m_genres.commission_rank'] = $request->get('filter_commission_rank');
        }

        // Site name
        if ($request->get('filter_site_name')) {
            $buildFilter['m_sites.site_name'] = $request->get('filter_site_name');
        }

        // Partner 1
        if ($request->get('filter_corp_name')) {
            $buildFilter['m_corps.corp_name'] = $request->get('filter_corp_name');
        }

        // Business day
        if ($request->get('filter_holiday')) {
            $buildFilter['demand_infos.holiday'] = $request->get('filter_holiday');
        }

        // Initial check
        if ($request->get('filter_first_commission')) {
            $buildFilter['commission_infos.first_commission'] = $request->get('filter_first_commission');
        }

        // Last history updated person
        if ($request->get('filter_user_name')) {
            $buildFilter['m_users.user_name'] = $request->get('filter_user_name');
        }

        // History update time
        if ($request->get('filter_modified')) {
            $buildFilter['commission_infos.modified'] = $request->get('filter_modified');
        }

        // Bid drop
        if ($request->get('filter_auction')) {
            $buildFilter['demand_infos.auction'] = $request->get('filter_auction');
        }

        // Cross-cell acquisition
        if ($request->get('filter_cross_sell_implement')) {
            $buildFilter['demand_infos.cross_sell_implement'] = $request->get('filter_cross_sell_implement');
        }

        $data = $this->reportCorpCommissionService->getCorpCommissionPaginationCondition($buildFilter, $order);

        return response()->json(
            \view(
                'report.corp_commission.show_report',
                [
                'results' => $data,
                ]
            )->render()
        );
    }

    /**
     * @param Request $request
     */
    public function registerCorpCommission(Request $request)
    {
        // Pending for confirm with BrSE
    }

    /**
     * @param Request $request
     */
    public function deleteCorpCommission(Request $request)
    {
        // Pending for confirm with BrSE
    }

    /**
     * Display page report/application_admin
     *
     * @author Nguyen.DoNhu <Nguyen.DoNhu@Nashtechglobal.com>
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function applicationAdmin()
    {
        $results = $this->approvalRepo->getApprovalForReport();
        $ir = getDropList(MItemRepository::IRREGULAR_REASON);

        return view(
            'report.application_admin',
            [
            "results" => $results,
            "ir" => $ir,
            ]
        );
    }

    /**
     * Show page report/development
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function development(Request $request)
    {
        $genres = $this->genreRepo->getListSelectBox([['valid_flg', 1]]);
        $prefecture = getDivList('rits.prefecture_div', 'rits_config');
        $genreId = $request->session()->get("genre_id", null);
        $address = $request->session()->get("address", null);

        return view(
            'report.development',
            [
            "genres" => $genres,
            "prefecture" => $prefecture,
            "genreId" => $genreId,
            "address" => $address,
            ]
        );
    }

    /**
     * Display table report, get params from session
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getDevelopmentSearch(Request $request)
    {
        if (!$request->hasSession() || $request->session()->get("genre_id", null) == null) {
            $request->session()->flash('error', trans('report_development_search.genre_id_required'));

            return redirect()->route("report.development");
        }

        $genreId = $request->session()->get("genre_id");
        $address = $request->session()->get("address");
        $noAttackList = $this->reportDevSearchService->getMCropUnattended($genreId);
        $advanceList = $this->reportDevSearchService->getMCropAdvance($genreId);
        if ($address == null || empty($address)) {
            $genres = $this->genreRepo->getListSelectBox([['valid_flg', 1]]);
            $prefecture = getDivList('rits.prefecture_div', 'rits_config');

            return view(
                'report.development_search',
                [
                "genres" => $genres,
                "prefecture" => $prefecture,
                "noAttackList" => $noAttackList,
                "advanceList" => $advanceList,
                "genreId" => $genreId,
                "flag" => false,
                ]
            );
        } else {
            return view(
                'report.development_search',
                [
                "genreId" => $genreId,
                "address" => $address,
                "advanceList" => $advanceList,
                "noAttackList" => $noAttackList,
                "flag" => true,
                ]
            );
        }
    }

    /**
     * Display table report, get params from url
     *
     * @param  $status
     * @param  $address
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDevelopmentSearchByParams($status, $address)
    {
        $genreId = Session::get("genre_id");
        $noAttackList = [];
        $advanceList = [];
        if ($status == 1) {
            $noAttackList = $this->reportDevSearchService->getMCropUnattended($genreId);
        }
        if ($status == 2) {
            $advanceList = $this->reportDevSearchService->getMCropAdvance($genreId);
        }

        return view(
            'report.development_search',
            [
            "genreId" => $genreId,
            "address" => $address,
            "advanceList" => $advanceList,
            "noAttackList" => $noAttackList,
            "status" => $status,
            "flag" => true,
            ]
        );
    }

    /**
     * Get data for datatable
     *
     * @param  \Illuminate\Http\Request $request
     * @return mixed
     * @throws \Exception
     */
    public function getDevelopmentSearchData(Request $request)
    {
        $genreId = $request->session()->get("genre_id");
        $address = $request->query("address", null);
        if ($address == null) {
            $address = $request->session()->get("address");
        }
        $status = $request->query("status", null);

        return $this->reportDevSearchService->getMCropForDatatable($genreId, $address, $status);
    }

    /**
     * Bind genre_id and address to session
     *
     * @param  \App\Http\Requests\ReportDevelopmentSearchRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postDevelopmentSearch(ReportDevelopmentSearchRequest $request)
    {
        $request->session()->put("genre_id", $request->input("genre_id"));
        if ($request->has("address")) {
            $request->session()->put("address", $request->input("address"));
        }

        return redirect()->route('report.development.search');
    }

    /**
     * Corp selection
     *
     * @param  Request $request
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function indexCorpSelection(Request $request)
    {
        $page = config('datacustom.report_number_row');
        $sort = $request->get('sort');
        $direction = $request->get('direction');
        $pageLink = ($request->get('page')) ? '&page=' . $request->get('page') : '';
        $results = $this->reportCorpCommissionService->getCorpSelectionPaginationCondition($page, $sort, $direction);

        return view('report.corp_selection', ['results' => $results, 'pageLink' => $pageLink]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function applicationAnswer()
    {
        return view('report.application_answer');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     * @throws \Throwable
     */
    public function applicationAnswerAjax(Request $request)
    {
        if ($request->ajax()) {
            $results = $this->approvalRepo->getApplicationAnswer();
            if (count($results) == 0) {
                return response()->json(view('report.components.error')->render());
            }
            try {
                return response()->json(
                    view(
                        'report.components.application_answer_table',
                        [
                        "results" => $results,
                        'application' => MItemRepository::APPLICATION,
                        'irregularReason' => MItemRepository::IRREGULAR_REASON,
                        ]
                    )->render()
                );
            } catch (Exception $e) {
                return $e->getMessage();
            }
        }
    }

    /**
     *
     */
    public function applicationAnswerCsv()
    {
        $dataList = $this->approvalRepo->getApplicationAnswerCsv();
        $fileName = mb_convert_encoding(
            trans('application_answer.application_answer') . '_' . Auth::user()->user_id,
            'SJIS-win',
            'UTF-8'
        );
        $fieldList = Approval::csvFormat();
        $csvExport = app(CSVExport::class);
        $csvExport->download($fieldList['default'], $fileName, $dataList);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function realTimeReport()
    {
        $results = $this->reportRealTimeService->getDataRealTime();

        return view('report.real_time_report', ['results' => $results]);
    }

    /**
     * Export data of report corp category group application answer
     *
     * @param Request $request
     */
    public function exportCorpCategoryGroupApplicationAnswer(Request $request)
    {
        try {
            $params['corp_id'] = $request->get('corp_id');
            $params['corp_name'] = $request->get('corp_name');
            $params['group_id'] = $request->get('group_id');
            $params['application_date_from'] = $request->get('application_date_from');
            $params['application_date_to'] = $request->get('application_date_to');

            $dataList = $this->reportCorpCateGroupAppService->getDataExportCsvCorpCateGroupApp($params);
            $fileName = mb_convert_encoding(
                trans('report_corp_cate_group_app_answer.corp_category_application_answer') . '_' . Auth::user()->user_id,
                'SJIS-win',
                'UTF-8'
            );
            $fieldList = CorpCategoryGroupApplication::csvFormat();
            $csvExport = app(CSVExport::class);
            $csvExport->download($fieldList, $fileName, $dataList);
        } catch (Exception $e) {
            logger(__METHOD__ . '- Error - ' . $e->getMessage());
        }
    }

    /**
     * Show data report corp category group application answer
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function corpCategoryGroupApplicationAnswer()
    {
        $params = [];
        $propriety = getDropList(MItemRepository::APPLICATION) + [0 => MItemRepository::PARTIAL_APPROVAL_OR_REJECTION];
        $results = $this->reportCorpCateGroupAppService->searchCorpCategoryGroupApplication($params);

        return view(
            'report.corp_category_group_application_answer.corp_category_group_application_answer',
            [
            'results' => $results,
            'propriety' => $propriety,
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function searchCorpCategoryGroupApplicationAnswer(Request $request)
    {
        $params['corp_id'] = $request->get('corp_id');
        $params['corp_name'] = $request->get('corp_name');
        $params['group_id'] = $request->get('group_id');
        $params['application_date_from'] = $request->get('application_date_from');
        $params['application_date_to'] = $request->get('application_date_to');

        $propriety = getDropList(MItemRepository::APPLICATION) + [0 => MItemRepository::PARTIAL_APPROVAL_OR_REJECTION];

        $results = $this->reportCorpCateGroupAppService->searchCorpCategoryGroupApplication($params);

        return response()->json(
            \view(
                'report.corp_category_group_application_answer.show_report',
                [
                'results' => $results,
                'propriety' => $propriety,
                ]
            )->render()
        );
    }

    /**
     * Show data report corp category group application answer
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function corpCategoryGroupApplicationAdmin()
    {
        $results = $this->reportCorpCateGroupAppService->corpCategoryGroupApplicationAdmin();

        return view('report.corp_category_group_application_admin', ['results' => $results]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function unsentList(Request $request)
    {
        $requestData = $request->all();
        $responseData = $this->service->getUnSentList($requestData);
        return view('report.unsent_list', $responseData);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function salesSupport(Request $request)
    {
        $params = $request->all();
        $sortParams = [];

        if (!isset($params['last_step_status'])) {
            $params['last_step_status'] = [3, 6, 7];
        }

        if (isset($params['data'])) {
            $sortParams['sort'] = isset($params['data']['sort']) ? $params['data']['sort'] : '';
            $sortParams['direction'] = isset($params['data']['direction']) ? $params['data']['direction'] : '';
        }

        $mGenreList = $this->genreRepo->getListGenres(true, true);
        $supportKindLabel = config('report.support_kind_label');

        $categories = [
            __('report_sales_support.tel_correspon'),
            __('report_sales_support.visit_correspon'),
            __('report_sales_support.order_correspon'),
            __('report_sales_support.tel_reason'),
            __('report_sales_support.visit_reason'),
            __('report_sales_support.order_reason'),
        ];

        $items = $this->mItemService->getMultiList($categories);

        try {
            $results = $this->commissionRepository->getSalesSupport($params, $sortParams);
        } catch (Exception $e) {
            abort('404');
        }

        return view(
            'report.sales_support',
            [
            'm_genre_list' => $mGenreList,
            'support_kind_label' => $supportKindLabel,
            'items' => $items,
            'results' => $results,
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateSalesSupport(Request $request)
    {
        $exclusionStatus = $request->get('exclusion_status');
        $userId = Auth::user()->user_id;

        foreach ($exclusionStatus as $id => $status) {
            $saveData = [
                'id' => $id,
                're_commission_exclusion_status' => $status,
            ];

            if ($status > 0) {
                $saveData['re_commission_exclusion_user_id'] = $userId;
                $saveData['re_commission_exclusion_datetime'] = date('Y-m-d H:i:s');
            }

            $this->commissionRepository->save($saveData);
        }

        $request->session()->flash('success', __('report_sales_support.message_successfully'));

        return redirect()->action('Report\ReportController@salesSupport');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function addition(Request $request)
    {
        $requestData = $request->all();
        $responseData = $this->service->getAdditionList($requestData);
        return view('report.addition', $responseData);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function additionUpdate(Request $request)
    {
        $requestData = $request->all();
        $updateResult = $this->service->updateAdditionInfo($requestData);
        $request->session()->flash('alert-' . $updateResult['type'], $updateResult['message']);
        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function additionExportCSV(Request $request)
    {
        $dataExport = $this->service->getDataCSV();
        if (count($dataExport) > 0) {
            $fieldList = $this->service->getFieldListExportCSV();
            $fileName = trans('report_addition.csv_file_name') . '_' . Auth::user()->user_id . '.csv';
            $data = [];
            foreach ($dataExport as $k => $item) {
                foreach ($fieldList as $key => $val) {
                    $data[$k][$key] = '';
                    if (isset($dataExport[$k][$key])) {
                        $data[$k][$key] = $dataExport[$k][$key];
                    }
                }
            }
            return $this->exportService->exportCsv($fileName, $fieldList, $data);
        } else {
            $request->session()->flash('alert-warning', trans('report_addition.export_no_data'));
            return redirect()->back();
        }
    }
}
