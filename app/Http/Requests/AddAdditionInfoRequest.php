<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory as ValidationFactory;

class AddAdditionInfoRequest extends FormRequest
{
    /**
     * AddAdditionInfoRequest constructor.
     *
     * @param ValidationFactory $validationFactory
     */
    public function __construct(ValidationFactory $validationFactory)
    {
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'customer_name' => 'required',
            'construction_price_tax_exclude' => 'required|numeric',
            'complete_date' => 'required|date_format:Y/m/d',
            'demand_type_update' => 'required'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'customer_name.required'                        => trans('addition.customer_name').trans('addition.required'),
            'construction_price_tax_exclude.required'   => trans('addition.construction_price_tax_exclude').trans('addition.required'),
            'complete_date.required'                    => trans('addition.complete_date').trans('addition.required'),
            'complete_date.date_format'                    => trans('addition.complete_date').trans('addition.date_format'),
            'demand_type_update.required'               => trans('addition.demand_type_update').trans('addition.required')
        ];
    }
}
