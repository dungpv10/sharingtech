<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


use Illuminate\Support\Facades\Input;

class AutoMTimeFormRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $rules['item_hour_date9'] = 'required|numeric';
        $rules['item_hour_date10'] = 'required|numeric';
        $rules['item_hour_date11'] = 'required|numeric';
        $rules['item_hour_date12'] = 'required|numeric';
        $rules['item_hour_date13'] = 'required|numeric';
        $rules['item_hour_date14'] = 'required|numeric';
        $rules['item_hour_date15'] = 'required|numeric';
        for ($i = 1; $i <= 6; $i++) {
            if (empty(Input::get('item_hour_date' . $i))) {
                if (Input::get('item_category' . $i) != 'send_mail') {
                    if ((!empty(Input::get('item_type' . $i)) && Input::get('item_type' . $i) == 0 && empty(Input::get('item_minute_date' . $i))) || (!empty(Input::get('item_type' . $i)) && Input::get('item_type' . $i) == 1)) {
                        $rules['item_hour_date'. $i] = 'required';
                        $rules['item_minute_date'. $i] = 'required';
                    }
                }
            }
            if (empty(Input::get('item_minute_date' . $i))) {
                if (Input::get('item_category' . $i) != 'send_mail') {
                    if ((!empty(Input::get('item_type' . $i)) && Input::get('item_type' . $i) == 0 && empty(Input::get('item_hour_date' . $i))) || (!empty(Input::get('item_type' . $i)) && Input::get('item_type' . $i) == 2)) {
                        $rules['item_hour_date'. $i] = 'required';
                        $rules['item_minute_date'. $i] = 'required';
                    }
                }
            }
        }
        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'item_hour_date1.required' => 'mtime.msg_error_required',
            'item_minute_date1.required' => 'mtime.msg_error_required',
            'item_hour_date2.required' => 'mtime.msg_error_required',
            'item_minute_date2.required' => 'mtime.msg_error_required',
            'item_hour_date3.required' => 'mtime.msg_error_required',
            'item_minute_date3.required' => 'mtime.msg_error_required',
            'item_hour_date4.required' => 'mtime.msg_error_required',
            'item_minute_date4.required' => 'mtime.msg_error_required',
            'item_hour_date5.required' => 'mtime.msg_error_required',
            'item_minute_date5.required' => 'mtime.msg_error_required',
            'item_hour_date6.required' => 'mtime.msg_error_required',
            'item_minute_date6.required' => 'mtime.msg_error_required',
            'item_hour_date9.required' => 'mtime.msg_error_required',
            'item_hour_date9.numeric' => 'mtime.msg_error_numberic',
            'item_hour_date10.required' => 'mtime.msg_error_required',
            'item_hour_date10.numeric' => 'mtime.msg_error_numberic',
            'item_hour_date11.required' => 'mtime.msg_error_required',
            'item_hour_date11.numeric' => 'mtime.msg_error_numberic',
            'item_hour_date12.required' => 'mtime.msg_error_required',
            'item_hour_date12.numeric' => 'mtime.msg_error_numberic',
            'item_hour_date13.required' => 'mtime.msg_error_required',
            'item_hour_date13.numeric' => 'mtime.msg_error_numberic',
            'item_hour_date14.required' => 'mtime.msg_error_required',
            'item_hour_date14.numeric' => 'mtime.msg_error_numberic',
            'item_hour_date15.required' => 'mtime.msg_error_required',
            'item_hour_date15.numeric' => 'mtime.msg_error_numberic',
        ];
    }
}
