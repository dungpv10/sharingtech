<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FaxEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @param $mailData
     * @param $data
     * @param $header
     */
    public function __construct($mailData, $data, $header)
    {
        $this->mailData = $mailData;
        $this->data = $data;
        $this->header = $header;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $header = $this->header;
        $this->withSwiftMessage(
            function ($message) use ($header) {
                $contentType = $message->getHeaders()->get('Content-type');
                $contentType->setValue($header);
            }
        );
        return $this
            ->view('email_template.fax_body')
            ->from($this->mailData['from'])
            ->subject($this->mailData['subject'])
            ->bcc($this->mailData['bcc'])
            ->with(['data' => $this->data]);
    }
}
