<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccumulatedInformations extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'accumulated_informations';

    /**
     * @var boolean
     */
    public $timestamps = false;
}
