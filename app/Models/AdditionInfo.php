<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdditionInfo extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'addition_infos';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    protected $editable = ['corp_id', 'demand_id', 'customer_name', 'genre_id', 'construction_price_tax_exclude', 'complete_date', 'falsity_flg', 'demand_flg', 'demand_type_update', 'note', 'del_flg', 'memo', 'modified_user_id', 'updated_at'];

    /**
     * @var boolean
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function genres()
    {
        return $this->belongsTo('App\Models\MGenre', 'genre_id');
    }

    /**
     * @return array
     */
    public function getEditableColumns()
    {
        return $this->editable;
    }

    /**
     * @return array
     */
    public static function csvFieldList()
    {
        $csvFields = [];
        foreach (\Schema::getColumnListing('addition_infos') as $column) {
            $csvFields[] = "addition_infos." . $column . " AS addition_infos" . '_' . $column;
        }
        $addColumn = [
            'm_genres.genre_name',
        ];

        foreach ($addColumn as $column) {
            $field = explode(".", $column);
            $table = $field[0];
            $columnName = $field[1];
            $csvFields[] = "$table." . $columnName . " AS $table" . '_' . $columnName;
        }
        return $csvFields;
    }
}
