<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AffiliationAreaStat extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'affiliation_area_stats';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;

    const AFF_TRANSACTION = 5; // Increase the upper limit because it overflows memory when outputting all items
}
