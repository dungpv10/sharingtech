<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AffiliationCorrespond extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'affiliation_corresponds';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;
}
