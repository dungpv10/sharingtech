<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AffiliationStat extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'affiliation_stats';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;
}
