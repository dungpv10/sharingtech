<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Agreement extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'agreement';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;

    /**
     * @return $this
     */
    public function agreementProvision()
    {
        return $this->hasMany('App\Models\AgreementProvision', 'agreement_id', 'id')->orderBy('sort_no');
    }
}
