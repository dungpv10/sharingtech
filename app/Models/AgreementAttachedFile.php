<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgreementAttachedFile extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'agreement_attached_file';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;

    const CERT = 'Cert';
    const LICENSE = 'License';

    const KIND_CERT = [self::CERT => '身分証明書、登記謄本'];
    const KIND_LICENSE = [self::LICENSE => 'ライセンス'];

    /**
     * @return bool
     */
    public function isFileTypePdf()
    {
        if ($this->content_type != null && strpos($this->content_type, 'pdf')) {
            return true;
        }
        return false;
    }
}
