<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgreementCustomize extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'agreement_customize';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;
    /**
     * * value of table_kind field in table
     */
    const AGREEMENT_PROVISIONS = 'AgreementProvisions';
    /**
     *
     */
    const AGREEMENT_PROVISIONS_ITEM = 'AgreementProvisionsItem';
    /**
     *
     */
    const TABLE_KIND_LABEL = [
        self::AGREEMENT_PROVISIONS => '条文',
        self::AGREEMENT_PROVISIONS_ITEM => '項目'
    ];
    /**
     * value of edit_kind field in table
     */
    const DELETE = 'Delete';
    /**
     *
     */
    const UPDATE = 'Update';
    /**
     *
     */
    const ADD = 'Add';
    /**
     *
     */
    const EDIT_KIND_LABEL = [
        self::ADD => '追加',
        self::UPDATE => '更新',
        self::DELETE => '削除'
    ];

    /**
     * @return mixed|string
     */
    public function getCustomizeProvisionKey()
    {
        if ($this->original_provisions_id != 0) {
            return $this->original_provisions_id;
        } else {
            return "c" . $this->customize_provisions_id;
        }
    }

    /**
     * @return mixed|string
     */
    public function getCustomizeItemKey()
    {
        if ($this->original_item_id != 0) {
            return $this->original_item_id;
        } else {
            return "c" . $this->customize_item_id;
        }
    }
}
