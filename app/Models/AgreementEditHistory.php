<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgreementEditHistory extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'agreement_edit_history';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;
}
