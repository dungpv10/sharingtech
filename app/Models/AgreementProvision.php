<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgreementProvision extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'agreement_provisions';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function agreement()
    {
        return $this->belongsTo('App\Models\Agreement', 'id', 'agreement_id');
    }

    /**
     * @return $this
     */
    public function agreementProvisionItem()
    {
        return $this->hasMany('App\Models\AgreementProvisionItem', 'agreement_provisions_id', 'id')->orderBy('sort_no');
    }

    /**
     * @return string
     */
    public function getContentAndAllItems()
    {
        $content = (checkIsNullOrEmptyStr($this->provisions) ? "null" : $this->provisions) . " \n";
        $items = $this->agreementProvisionItem;
        foreach ($items as $item) {
            $content .= " " . (checkIsNullOrEmptyStr($item->item) ? "null" : $item->item) . " \n";
        }
        return $content;
    }
}
