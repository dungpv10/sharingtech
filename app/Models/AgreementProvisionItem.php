<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgreementProvisionItem extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'agreement_provisions_item';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;

    /**
     * @var mixed
     */
    protected $customize_key;

    /**
     * @return mixed
     */
    public function getCustomizeItemKey()
    {
        if (is_null($this['customize_key']) || empty($this['customize_key'])) {
            return $this->id;
        } else {
            return $this['customize_key'];
        }
    }
}
