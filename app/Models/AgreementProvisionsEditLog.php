<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgreementProvisionsEditLog extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'agreement_provisions_edit_logs';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;
}
