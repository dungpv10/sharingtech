<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class AntisocialCheck extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'antisocial_checks';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;

    /**
     * get result list
     *
     * @return array
     */
    public function getResultList()
    {
        return [
            'None' => trans('antisocial_follow.none'),
            'OK' => trans('antisocial_follow.ok'),
            'NG' => trans('antisocial_follow.ng'),
            'Inadequate' => trans('antisocial_follow.inadequate')
        ];
    }

    /**
     * @return array
     */
    public static function csvFormat()
    {
        return array(
            'mcorp_id' => trans('antisocial_follow.mcorp_id'),
            'official_corp_name' => trans('antisocial_follow.official_corp_name'),
            'corp_name_kana' => trans('antisocial_follow.corp_name_kana'),
            'max' => trans('antisocial_follow.last_antisocial_check_date'),
            'commission_dial' => trans('antisocial_follow.commission_dial'),
        );
    }
}
