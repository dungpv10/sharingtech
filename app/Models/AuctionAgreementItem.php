<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AuctionAgreementItem extends Model
{
    /**
     * @var string
     */
    protected $table = 'auction_agreement_provisions_items';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;
}
