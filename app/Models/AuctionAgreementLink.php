<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AuctionAgreementLink extends Model
{
    /**
     * @var string
     */
    protected $table = 'auction_agreement_links';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;
}
