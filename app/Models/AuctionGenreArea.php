<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AuctionGenreArea extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'auction_genre_areas';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;
}
