<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AuctionInfo extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'auction_infos';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @var boolean
     */
    public $timestamps = false;
}
