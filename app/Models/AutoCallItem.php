<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AutoCallItem extends Model
{
    /**
     * @var string
     */
    protected $table = 'auto_call_items';
    /**
     * @var array
     */
    protected $guarded = [];
    /**
     * @var boolean
     */
    public $timestamps = false;
}
