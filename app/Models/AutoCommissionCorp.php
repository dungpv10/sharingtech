<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AutoCommissionCorp extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'auto_commission_corp';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function mCorps()
    {
        return $this->hasMany(MCorp::class, 'corp_id', 'id');
    }
}
