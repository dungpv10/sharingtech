<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BillInfo extends Model
{
    /**
     * @var array
     */
    public static $csvFormat = array(
        'MCorp__id' => '企業コード',
        'MCorp__official_corp_name' => '企業名',
        'BillInfo__demand_id' => '案件ID',
        'BillInfo__commission_id' => '取次ID',
        'MItem__item_name' => '取次形態',
        'CommissionInfo__tel_commission_datetime' => '電話取次日時',
        'CommissionInfo__complete_date' => '施工完了日',
        'DemandInfo__customer_name' => 'お客様名',
        'DemandInfo__riro_kureka' => 'リロ・クレカ案件',
        'BillInfo__fee_target_price' => '手数料対象金額',
        'BillInfo__comfirmed_fee_rate' => '確定手数料率',
        'BillInfo__fee_tax_exclude' => '手数料(税抜)',
        'BillInfo__tax' => '消費税',
        'BillInfo__insurance_price' => '保険料金額',
        'BillInfo__fee_billing_date' => '手数料請求日',
        'BillInfo__fee_payment_balance' => '手数料入金残高',
    );
    /**
     * @var string
     */
    protected $table = 'bill_infos';
    /**
     * @var boolean
     */
    public $timestamps = false;
}
