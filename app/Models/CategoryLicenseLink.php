<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryLicenseLink extends Model
{

    /**
     * @var string
     */
    protected $table = 'category_license_link';

    /**
     * @var boolean
     */
    public $timestamps = false;
}
