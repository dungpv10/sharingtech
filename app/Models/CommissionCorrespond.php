<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommissionCorrespond extends Model
{
    /**
     * @var string
     */
    protected $table = 'commission_corresponds';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;
}
