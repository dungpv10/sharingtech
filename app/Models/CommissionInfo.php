<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommissionInfo extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'commission_infos';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    const UPDATED_AT = 'modified';
    const CREATED_AT = 'created';

    /**
     * @var boolean
     */
    public $timestamps = false;

    /**
     * @return $this
     */
    public function mCorp()
    {
        return $this->belongsTo('App\Models\MCorp', 'corp_id', 'id')->with('affiliationInfo');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function demandInfo()
    {
        return $this->belongsTo('App\Models\DemandInfo', 'demand_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mItem()
    {
        return $this->belongsTo('App\Models\MItem', 'commission_status', 'item_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function billInfos()
    {
        return $this->hasMany('App\Models\BillInfo', 'commission_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function billInfo()
    {
        return $this->hasOne('App\Models\BillInfo', 'commission_id', 'id');
    }

    /**
     * @return array
     */
    public static function csvFormat()
    {
        return array(
            'demand_id' => trans('jbr_receipt_follow.demand_id'),
            'official_corp_name' => trans('jbr_receipt_follow.official_corp_name'),
            'genre_name' => trans('jbr_receipt_follow.genre_name'),
            'commission_id' => trans('jbr_receipt_follow.commission_id'),
            'jbr_order_no' => trans('jbr_receipt_follow.jbr_order_no'),
            'customer_name' => trans('jbr_receipt_follow.customer_name'),
            'complete_date' => trans('jbr_receipt_follow.complete_date'),
            'construction_price_tax_include' => trans('jbr_receipt_follow.construction_price_tax_include'),
            'MItem_item_name' => trans('jbr_receipt_follow.MItem_item_name'),
            'MItem2_item_name' => trans('jbr_receipt_follow.MItem2_item_name'),
        );
    }


    /**
     * @author Dung.PhamVan@nashtechglobal.com
     */
    public function mCorpAndMCorpNewYear()
    {
        return $this->mCorp()->with('mCorpNewYear');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function affiliationInfo()
    {
        return $this->hasOne('App\Models\AffiliationInfo', 'corp_id', 'corp_id');
    }

    /**
     * @return false|string
     */
    public function getCommissionNoteSendDatetimeFormatAttribute()
    {
        return dateTimeFormat($this->getAttribute('commission_note_send_datetime'));
    }

    /**
     * @return string
     */
    public function getOrderFeeUnitLabelAttribute()
    {
        $categoryDefaultFee = MCategory::getDefaultFee();
        $feeUnit = $this->getAttribute('order_fee_unit') ?? $categoryDefaultFee;

        return $this->getAttribute('commission_fee_rate') && $feeUnit == 1 ? '取次時手数料率' : '取次先手数料';
    }

    /**
     * @return string
     */
    public function getCorpCommissionTypeDispAttribute()
    {

        return $this->getAttribute('commission_type') == 1 ? '紹介' : '成約';
    }

    /**
     * @return mixed|string
     */
    public function getFeeUnit()
    {
        $categoryDefaultFee = MCategory::getDefaultFee();
        return $this->getAttribute('order_fee_unit') ?? $categoryDefaultFee;
    }

    /**
     * @return array
     */
    public static function csvFieldList()
    {
        $csvFields = [];
        foreach (\Schema::getColumnListing('demand_infos') as $column) {
            $csvFields[] = "demand_infos." . $column . " AS demand_infos" . '_' . $column;
        }
        $addColumn = [
            'commission_infos.id',
            'commission_infos.commit_flg',
            'commission_infos.commission_type',
            'commission_infos.appointers',
            'commission_infos.first_commission',
            'commission_infos.corp_fee',
            'commission_infos.attention',
            'commission_infos.commission_dial',
            'commission_infos.tel_commission_datetime',
            'commission_infos.tel_commission_person',
            'commission_infos.commission_fee_rate',
            'commission_infos.commission_note_send_datetime',
            'commission_infos.commission_note_sender',
            'commission_infos.commission_status',
            'commission_infos.commission_order_fail_reason',
            'commission_infos.complete_date',
            'commission_infos.order_fail_date',
            'commission_infos.estimate_price_tax_exclude',
            'commission_infos.construction_price_tax_exclude',
            'commission_infos.construction_price_tax_include',
            'commission_infos.deduction_tax_include',
            'commission_infos.deduction_tax_exclude',
            'commission_infos.confirmd_fee_rate',
            'commission_infos.unit_price_calc_exclude',
            'commission_infos.report_note',
            'commission_infos.corp_id',
            'commission_infos.send_mail_fax',
            'commission_infos.send_mail_fax_datetime',
            'm_corps.corp_name',
            'm_corps.official_corp_name',
            'm_sites.site_name',
            'm_genres.genre_name',
            'm_categories.category_name'
        ];

        foreach ($addColumn as $column) {
            $field = explode(".", $column);
            $table = $field[0];
            $columnName = $field[1];
            $csvFields[] = "$table." . $columnName . " AS $table" . '_' . $columnName;
        }
        return $csvFields;
    }

    /**
     * @return array
     */
    public static function csvFieldListFormat()
    {
        return [
            'demand_infos_id' => trans('commissioninfos.csv_field.demand_infos_id'),
            'demand_infos_follow_date' => trans('commissioninfos.csv_field.demand_infos_follow_date'),
            'demand_infos_demand_status' => trans('commissioninfos.csv_field.demand_infos_demand_status'),
            'demand_infos_order_fail_reason' => trans('commissioninfos.csv_field.demand_infos_order_fail_reason'),
            'demand_infos_mail_demand' => trans('commissioninfos.csv_field.demand_infos_mail_demand'),
            'demand_infos_nighttime_takeover' => trans('commissioninfos.csv_field.demand_infos_nighttime_takeover'),
            'demand_infos_low_accuracy' => trans('commissioninfos.csv_field.demand_infos_low_accuracy'),
            'demand_infos_remand' => trans('commissioninfos.csv_field.demand_infos_remand'),
            'demand_infos_immediately' => trans('commissioninfos.csv_field.demand_infos_immediately'),
            'demand_infos_corp_change' => trans('commissioninfos.csv_field.demand_infos_corp_change'),
            'demand_infos_receive_datetime' => trans('commissioninfos.csv_field.demand_infos_receive_datetime'),
            'demand_infos_site_name' => trans('commissioninfos.csv_field.demand_infos_site_name'),
            'demand_infos_genre_name' => trans('commissioninfos.csv_field.demand_infos_genre_name'),
            'demand_infos_category_name' => trans('commissioninfos.csv_field.demand_infos_category_name'),
            'demand_infos_cross_sell_source_site' => trans('commissioninfos.csv_field.demand_infos_cross_sell_source_site'),
            'demand_infos_cross_sell_source_genre' => trans('commissioninfos.csv_field.demand_infos_cross_sell_source_genre'),
            'demand_infos_cross_sell_source_category' => trans('commissioninfos.csv_field.demand_infos_cross_sell_source_category'),
            'demand_infos_source_demand_id' => trans('commissioninfos.csv_field.demand_infos_source_demand_id'),
            'demand_infos_same_customer_demand_url' => trans('commissioninfos.csv_field.demand_infos_same_customer_demand_url'),
            'demand_infos_receptionist' => trans('commissioninfos.csv_field.demand_infos_receptionist'),
            'demand_infos_customer_name' => trans('commissioninfos.csv_field.demand_infos_customer_name'),
            'demand_infos_customer_corp_name' => trans('commissioninfos.csv_field.demand_infos_customer_corp_name'),
            'demand_infos_customer_tel' => trans('commissioninfos.csv_field.demand_infos_customer_tel'),
            'demand_infos_customer_mailaddress' => trans('commissioninfos.csv_field.demand_infos_customer_mailaddress'),
            'demand_infos_postcode' => trans('commissioninfos.csv_field.demand_infos_postcode'),
            'demand_infos_address1' => trans('commissioninfos.csv_field.demand_infos_address1'),
            'demand_infos_address2' => trans('commissioninfos.csv_field.demand_infos_address2'),
            'demand_infos_address3' => trans('commissioninfos.csv_field.demand_infos_address3'),
            'demand_infos_tel1' => trans('commissioninfos.csv_field.demand_infos_tel1'),
            'demand_infos_tel2' => trans('commissioninfos.csv_field.demand_infos_tel2'),
            'demand_infos_contents' => trans('commissioninfos.csv_field.demand_infos_contents'),
            'demand_infos_contact_desired_time' => trans('commissioninfos.csv_field.demand_infos_contact_desired_time'),
            'demand_infos_selection_system' => trans('commissioninfos.csv_field.demand_infos_selection_system'),
            'demand_infos_pet_tombstone_demand' => trans('commissioninfos.csv_field.demand_infos_pet_tombstone_demand'),
            'demand_infos_sms_demand' => trans('commissioninfos.csv_field.demand_infos_sms_demand'),
            'demand_infos_order_no_marriage' => trans('commissioninfos.csv_field.demand_infos_order_no_marriage'),
            'demand_infos_jbr_order_no' => trans('commissioninfos.csv_field.demand_infos_jbr_order_no'),
            'demand_infos_jbr_work_contents' => trans('commissioninfos.csv_field.demand_infos_jbr_work_contents'),
            'demand_infos_jbr_category' => trans('commissioninfos.csv_field.demand_infos_jbr_category'),
            'demand_infos_mail' => trans('commissioninfos.csv_field.demand_infos_mail'),
            'demand_infos_order_date' => trans('commissioninfos.csv_field.demand_infos_order_date'),
            'demand_infos_complete_date' => trans('commissioninfos.csv_field.demand_infos_complete_date'),
            'demand_infos_order_fail_date' => trans('commissioninfos.csv_field.demand_infos_order_fail_date'),
            'demand_infos_jbr_estimate_status' => trans('commissioninfos.csv_field.demand_infos_jbr_estimate_status'),
            'demand_infos_jbr_receipt_status' => trans('commissioninfos.csv_field.demand_infos_jbr_receipt_status'),
            'demand_infos_acceptance_status' => trans('commissioninfos.csv_field.demand_infos_acceptance_status'),
            'demand_infos_nitoryu_flg' => trans('commissioninfos.csv_field.demand_infos_nitoryu_flg'),
            'commission_infos_id' => trans('commissioninfos.csv_field.commission_infos_id'),
            'commission_infos_corp_id' => trans('commissioninfos.csv_field.commission_infos_corp_id'),
            'm_corps_corp_name' => trans('commissioninfos.csv_field.m_corps_corp_name'),
            'm_corps_official_corp_name' => trans('commissioninfos.csv_field.m_corps_official_corp_name'),
            'commission_infos_commit_flg' => trans('commissioninfos.csv_field.commission_infos_commit_flg'),
            'commission_infos_commission_type' => trans('commissioninfos.csv_field.commission_infos_commission_type'),
            'commission_infos_appointers' => trans('commissioninfos.csv_field.commission_infos_appointers'),
            'commission_infos_first_commission' => trans('commissioninfos.csv_field.commission_infos_first_commission'),
            'commission_infos_corp_fee' => trans('commissioninfos.csv_field.commission_infos_corp_fee'),
            'commission_infos_attention' => trans('commissioninfos.csv_field.commission_infos_attention'),
            'commission_infos_commission_dial' => trans('commissioninfos.csv_field.commission_infos_commission_dial'),
            'commission_infos_tel_commission_datetime' => trans('commissioninfos.csv_field.commission_infos_tel_commission_datetime'),
            'commission_infos_tel_commission_person' => trans('commissioninfos.csv_field.commission_infos_tel_commission_person'),
            'commission_infos_commission_fee_rate' => trans('commissioninfos.csv_field.commission_infos_commission_fee_rate'),
            'commission_infos_commission_note_send_datetime' => trans('commissioninfos.csv_field.commission_infos_commission_note_send_datetime'),
            'commission_infos_commission_note_sender' => trans('commissioninfos.csv_field.commission_infos_commission_note_sender'),
            'commission_infos_send_mail_fax' => trans('commissioninfos.csv_field.commission_infos_send_mail_fax'),
            'commission_infos_send_mail_fax_datetime' => trans('commissioninfos.csv_field.commission_infos_send_mail_fax_datetime'),
            'commission_infos_commission_status' => trans('commissioninfos.csv_field.commission_infos_commission_status'),
            'commission_infos_commission_order_fail_reason' => trans('commissioninfos.csv_field.commission_infos_commission_order_fail_reason'),
            'commission_infos_complete_date' => trans('commissioninfos.csv_field.commission_infos_complete_date'),
            'commission_infos_order_fail_date' => trans('commissioninfos.csv_field.commission_infos_order_fail_date'),
            'commission_infos_estimate_price_tax_exclude' => trans('commissioninfos.csv_field.commission_infos_estimate_price_tax_exclude'),
            'commission_infos_construction_price_tax_exclude' => trans('commissioninfos.csv_field.commission_infos_construction_price_tax_exclude'),
            'commission_infos_construction_price_tax_include' => trans('commissioninfos.csv_field.commission_infos_construction_price_tax_include'),
            'commission_infos_deduction_tax_include' => trans('commissioninfos.csv_field.commission_infos_deduction_tax_include'),
            'commission_infos_deduction_tax_exclude' => trans('commissioninfos.csv_field.commission_infos_deduction_tax_exclude'),
            'commission_infos_confirmd_fee_rate' => trans('commissioninfos.csv_field.commission_infos_confirmd_fee_rate'),
            'commission_infos_unit_price_calc_exclude' => trans('commissioninfos.csv_field.commission_infos_unit_price_calc_exclude'),
            'commission_infos_report_note' => trans('commissioninfos.csv_field.commission_infos_report_note'),
        ];
    }
}
