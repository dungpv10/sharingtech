<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommissionTelSupport extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'commission_tel_supports';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;
}
