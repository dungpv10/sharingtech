<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class CommissionVisit extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'commission_visit_supports';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;
}
