<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CorpAgreement extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'corp_agreement';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;

    const STEP0 = 'Step0';
    const STEP1 = 'Step1';
    const STEP2 = 'Step2';
    const STEP3 = 'Step3';
    const STEP4 = 'Step4';
    const STEP5 = 'Step5';
    const STEP6 = 'Step6';
    const CONFIRM = 'Confirm';
    const REVIEW = 'Review';
    const PASS_BACK = 'PassBack';
    const COMPLETE = 'Complete';
    const NOT_SIGNED = 'NotSigned';
    const RECONFIRMATION = 'Reconfirmation';
    const RESIGNING = 'Resigning';
    const APPLICATION = 'Application';

    const AGREEMENT_STATUS = [
        null => '',
        self::STEP0 => '未確認',
        self::STEP1 => '契約内容確認中',
        self::STEP2 => '契約内容確認中',
        self::STEP3 => '契約内容確認中',
        self::STEP4 => '契約内容確認中',
        self::STEP5 => '契約内容確認中',
        self::STEP6 => '契約内容確認中',
        self::CONFIRM => '契約内容最終確認',
        self::APPLICATION => '同意申請完了',
        self::REVIEW => '申請審査中',
        self::PASS_BACK => '差戻し中',
        self::COMPLETE => '契約完了',
        self::NOT_SIGNED => '未締結',
        self::RECONFIRMATION => '再確認',
        self::RESIGNING => '再契約申請'
    ];

    const NONE = 'None';
    const OK = 'OK';
    const NG = 'NG';
    const INADEQUATE = 'Inadequate';

    const HANSHA_CHECK_STATUS = [
        self::NONE => '未実施',
        self::OK => '実施済みOK',
        self::NG => '実施済みNG',
        self::INADEQUATE => '書類不備'
    ];

    /**
     * get status message
     *
     * @return array
     */
    public function getStatusMessage()
    {
        return [
            'Step0' => '未確認',
            'Step1' => '契約内容確認中',
            'Step2' => '契約内容確認中',
            'Step3' => '契約内容確認中',
            'Step4' => '契約内容確認中',
            'Step5' => '契約内容確認中',
            'Step6' => '契約内容確認中',
            'Confirm' => '契約内容最終確認',
            'Application' => '同意申請完了',
            'Review' => '申請審査中',
            'PassBack' => '差戻し中',
            'Complete' => '契約完了',
            'NotSigned' => '未締結',
            'Reconfirmation' => '契約再確認申請',
            'Resigning' => '再契約申請'
        ];
    }
}
