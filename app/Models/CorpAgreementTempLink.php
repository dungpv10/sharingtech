<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CorpAgreementTempLink extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'corp_agreement_temp_link';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function corpAgreement()
    {
        return $this->belongsTo('App\Models\CorpAgreement', 'corp_agreement_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function mCorpCategoriesTemps()
    {
        return $this->hasMany('App\Models\MCorpCategoriesTemp', 'temp_id', 'id');
    }
}
