<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CorpCategoryApplication extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'corp_category_applications';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;
}
