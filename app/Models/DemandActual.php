<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DemandActual
 *
 * @package App\Models
 */
class DemandActual extends Model
{
    /**
     * @var string
     */
    protected $table = 'demand_actuals';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;
}
