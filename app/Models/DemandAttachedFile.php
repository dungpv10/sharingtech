<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DemandAttachedFile extends Model
{

    /**
     * @var string
     */
    public $table = 'demand_attached_files';
    /**
     * @var string
     */
    public $primaryKey = 'id';
    /**
     * @var boolean
     */
    public $timestamps = false;
    /**
     * @var array
     */
    protected $fillable = [
        'id', 'demand_id', 'path', 'name', 'content_type', 'create_date','create_user_id',
        'update_date', 'update_user_id', 'delete_date', 'delete_flag'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
}
