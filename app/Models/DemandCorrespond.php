<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DemandCorrespond extends Model
{
    //
    /**
     * @var string
     */
    protected $table = 'demand_corresponds';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @var boolean
     */
    public $timestamps = false;

    /**
     * @author Dung.PhamVan@nashtechglobal.com
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function MUser()
    {
        return $this->belongsTo(MUser::class, 'responders', 'id');
    }

    /**
     * @author Dung.PhamVan@nashtechglobal.com
     * @return false|string
     * @des get user_name attribute
     */
    public function getUserNameAttribute()
    {
        $responder = $this->getAttribute('responders');

        return (!ctype_digit($responder) || !$this->MUser) ? $responder : $this->MUser->user_name;
    }

    /**
     * @author Dung.PhamVan@nashtechglobal.com
     * @return false|string
     * @des get correspond_date_time_format attribute
     */
    public function getCorrespondDateTimeFormatAttribute()
    {

        return dateTimeFormat($this->getAttribute('correspond_datetime'));
    }
}
