<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DemandForecast
 *
 * @package App\Models
 */
class DemandForecast extends Model
{
    /**
     * @var string
     */
    protected $table = 'demand_forecasts';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;
}
