<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DemandInfo extends Model
{
    const AU_DROP_DOWN_START_YEAR = 2015; // auction setting base year
    const DEMAND_INFO_AUCTIONED = 1; // auction field in DB when auctioned.
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    //define format csv
    const CSV_AUCTION_SETTING_HEADER = [
        'prefecture_name' => '都道府県',
        'year_count' => '累計件数',
        'year_flowing_ratio' => '累計率',
        'month_count' => '件数',
        'month_flowing_ratio' => '率'
    ];

    private $visitTimeList;

    /**
     * @var array
     */
    public $conditionForOrder = [
        'auction' => 'demand_infos.auction',
        'modified2' => 'commission_infos.modified',
        'user_name' => 'm_user_name',
        'holiday' => '((ARRAY_TO_STRING(ARRAY(SELECT "mi"."item_name"
                                        FROM m_corp_subs mcs INNER JOIN m_items mi ON "mi"."item_category" = "mcs"."item_category"
                                        AND "mi"."item_id" = "mcs"."item_id" WHERE "mcs"."item_category" = \'休業日\'
                                        AND "mcs"."corp_id" = "m_corps"."id"), \',\')))',
        'visit_time' => 'visit_time_view.visit_time',
        'demand_follow_date' => '(case when "demand_infos"."follow_date" is null then \'Z\'
                                        when "demand_infos"."follow_date" = \'\' then \'Y\'
                                        else "demand_infos"."follow_date" end)',
        'commission_rank'=> 'm_genres.commission_rank',
        'detect_contact_desired_time' => '(CASE WHEN "visit_time_view"."is_visit_time_range_flg" = 1
                THEN "visit_time_view"."visit_adjust_time" WHEN "demand_infos"."is_contact_time_range_flg" = 1
                THEN "demand_infos"."contact_desired_time_from" ELSE "demand_infos"."contact_desired_time" END)',
        'corp_name' => 'm_corps.corp_name',
        'site_id' => 'demand_infos.site_id',
        'customer_name' => 'demand_infos.customer_name',
        'address' => 'demand_infos.address1',
        'contactable' => '(CASE "m_corps"."contactable_support24hour"
                                        WHEN 1 THEN \'24H対応\'
                                        ELSE "m_corps"."contactable_time_from" || \'～\' || "m_corps"."contactable_time_to" END)',
        'first_commission' => 'commission_infos.first_commission',
        'demand_id' => 'demand_infos.id',
        'commission_dial' => 'm_corps.commission_dial',
        'follow_date' => 'demand_infos.follow_date'
    ];

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * define default format csv
     *
     * @return array
     */
    public static function csvFormat()
    {
        return array(
            'default' => array(
                'id' => trans('demandinfo_table.demand_id'),
                'follow_date' => trans('demandinfo_table.follow_date'),
                'demand_status' => trans('demandinfo_table.demand_status'),
                'order_fail_reason' => trans('demandinfo_table.order_fail_reason'),
                'mail_demand' => trans('demandinfo_table.mail_demand'),
                'nighttime_takeover' => trans('demandinfo_table.nighttime_takeover'),
                'low_accuracy' => trans('demandinfo_table.low_accuracy'),
                'remand' => trans('demandinfo_table.remand'),
                'immediately' => trans('demandinfo_table.immediately'),
                'corp_change' => trans('demandinfo_table.corp_change'),
                'receive_datetime' => trans('demandinfo_table.receive_datetime'),
                'site_name' => trans('demandinfo_table.site_name'),
                'genre_name' => trans('demandinfo_table.genre_name'),
                'category_name' => trans('demandinfo_table.category_name'),
                'cross_sell_source_site' => trans('demandinfo_table.cross_sell_source_site'),
                'cross_sell_source_genre' => trans('demandinfo_table.cross_sell_source_genre'),
                'cross_sell_source_category' => trans('demandinfo_table.cross_sell_source_category'),
                'source_demand_id' => trans('demandinfo_table.source_demand_id'),
                'same_customer_demand_url' => trans('demandinfo_table.same_customer_demand_url'),
                'receptionist' => trans('demandinfo_table.receptionist'),
                'customer_name' => trans('demandinfo_table.customer_name'),
                'customer_corp_name' => trans('demandinfo_table.customer_corp_name'),
                'customer_tel' => trans('demandinfo_table.customer_tel'),
                'customer_mailaddress' => trans('demandinfo_table.customer_mailaddress'),
                'postcode' => trans('demandinfo_table.postcode'),
                'address1' => trans('demandinfo_table.address1'),
                'address2' => trans('demandinfo_table.address2'),
                'address3' => trans('demandinfo_table.address3'),
                'tel1' => trans('demandinfo_table.tel1'),
                'tel2' => trans('demandinfo_table.tel2'),
                'contents' => trans('demandinfo_table.contents'),
                'contact_desired_time' => trans('demandinfo_table.contact_desired_time'),
                'selection_system' => trans('demandinfo_table.selection_system'),
                'pet_tombstone_demand' => trans('demandinfo_table.pet_tombstone_demand'),
                'sms_demand' => trans('demandinfo_table.sms_demand'),
                'order_no_marriage' => trans('demandinfo_table.order_no_marriage'),
                'jbr_order_no' => trans('demandinfo_table.jbr_order_no'),
                'jbr_work_contents' => trans('demandinfo_table.jbr_work_contents'),
                'jbr_category' => trans('demandinfo_table.jbr_category'),
                'mail' => trans('demandinfo_table.mail'),
                'order_date' => trans('demandinfo_table.order_date'),
                'complete_date' => trans('demandinfo_table.complete_date'),
                'order_fail_date' => trans('demandinfo_table.order_fail_date'),
                'jbr_estimate_status' => trans('demandinfo_table.jbr_estimate_status'),
                'jbr_receipt_status' => trans('demandinfo_table.jbr_receipt_status'),
                'acceptance_status' => trans('demandinfo_table.acceptance_status'),
                'nitoryu_flg' => trans('demandinfo_table.nitoryu_flg'),
                'id' => trans('demandinfo_table.id'),
                'corp_id' => trans('demandinfo_table.corp_id'),
                'corp_name' => trans('demandinfo_table.corp_name'),
                'official_corp_name' => trans('demandinfo_table.official_corp_name'),
                'commit_flg' => trans('demandinfo_table.commit_flg'),
                'commission_type' => trans('demandinfo_table.commission_type'),
                'appointers' => trans('demandinfo_table.appointers'),
                'first_commission' => trans('demandinfo_table.first_commission'),
                'corp_fee' => trans('demandinfo_table.corp_fee'),
                'attention' => trans('demandinfo_table.attention'),
                'commission_dial' => trans('demandinfo_table.commission_dial'),
                'tel_commission_datetime' => trans('demandinfo_table.tel_commission_datetime'),
                'tel_commission_person' => trans('demandinfo_table.tel_commission_person'),
                'commission_fee_rate' => trans('demandinfo_table.commission_fee_rate'),
                'commission_note_send_datetime' => trans('demandinfo_table.commission_note_send_datetime'),
                'commission_note_sender' => trans('demandinfo_table.commission_note_sender'),
                'send_mail_fax' => trans('demandinfo_table.send_mail_fax'),
                'send_mail_fax_datetime' => trans('demandinfo_table.send_mail_fax_datetime'),
                'commission_status' => trans('demandinfo_table.commission_status'),
                'commission_order_fail_reason' => trans('demandinfo_table.commission_order_fail_reason'),
                'complete_date' => trans('demandinfo_table.complete_date'),
                'order_fail_date' => trans('demandinfo_table.order_fail_date'),
                'estimate_price_tax_exclude' => trans('demandinfo_table.estimate_price_tax_exclude'),
                'construction_price_tax_exclude' => trans('demandinfo_table.construction_price_tax_exclude'),
                'construction_price_tax_include' => trans('demandinfo_table.construction_price_tax_include'),
                'deduction_tax_include' => trans('demandinfo_table.deduction_tax_include'),
                'deduction_tax_exclude' => trans('demandinfo_table.deduction_tax_exclude'),
                'confirmd_fee_rate' => trans('demandinfo_table.confirmd_fee_rate'),
                'unit_price_calc_exclude' => trans('demandinfo_table.unit_price_calc_exclude'),
                'report_note' => trans('demandinfo_table.report_note'),
            ),
        );
    }

    // declare constant selection type for selection_system field
    const SELECTION_TYPE = ['manual' => 0, 'auto' => 4, 'auction' => 2, 'auto_auction' => 3];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'demand_infos';
    /**
     * @var boolean
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mCategory()
    {
        return $this->belongsTo('App\Models\MCategory', 'category_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function billInfos()
    {
        return $this->hasMany('App\Models\BillInfo', 'demand_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mGenres()
    {
        return $this->belongsTo('App\Models\MGenre', 'genre_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function auctionInfo()
    {
        return $this->hasMany("App\Models\AuctionInfo", "demand_id", "id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mSite()
    {
        return $this->belongsTo('App\Models\MSite', 'site_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mUser()
    {
        return $this->belongsTo('App\Models\MUser', 'receptionist', 'id');
    }

    /**
     * @author Dung.PhamVan@nashtechglobal.com
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function visitTimes()
    {
        return $this->hasMany(VisitTime::class, 'demand_id', 'id')->orderBy('id', 'asc');
    }

    /**
     * @return $this
     */
    public function demandCorresponds()
    {
        return $this->hasMany(DemandCorrespond::class, 'demand_id', 'id')->orderBy('correspond_datetime', 'desc');
    }

    /**
     * @return $this
     */
    public function commissionInfos()
    {
        return $this->commissionInfo()->where('del_flg', 0)->orderBy('id', 'asc')
            ->with('mCorpAndMCorpNewYear');
    }

    /**
     * Get over limit time attribute
     *
     * @return integer
     */
    public function getOverLimitTimeAttribute()
    {
        if (!isset($this->mGenres) || !in_array($this->getAttribute('demand_status'), [1, 2, 3]) || ($this->mGenres && $this->mGenres->commission_limit_time < 1)) {
            return 0;
        }

        $limitTimeStamp = strtotime($this->getAttribute('receive_datetime') . '+ ' . (int)$this->mGenres->commission_limit_time . ' minute');
        $overLimitTimeStamp = strtotime("now") - $limitTimeStamp;

        return $overLimitTimeStamp;
    }

    /**
     * Get over limit time format attribute
     *
     * @return mixed
     */
    public function getOverLimitTimeFormatAttribute()
    {
        $overLimitTime = $this->getAttribute('over_limit_time');

        return ($overLimitTime > 0) ? round($overLimitTime / 3600) . '時間'. round($overLimitTime % 3600 / 60). '分' : '';
    }

    /**
     * @return DemandInfo
     */
    public function commissions()
    {
        return $this->commissionInfos();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function commissionInfo()
    {
        return $this->hasMany(CommissionInfo::class, 'demand_id', 'id');
    }

    /**
     * @return $this
     */
    public function introduceInfo()
    {
        return $this->hasMany(CommissionInfo::class, 'demand_id', 'id')
            ->where('commission_infos.commission_type', '1')
            ->orderBy('commission_infos.id', 'asc');
    }

    /**
     * @return $this
     */
    public function demandCorrespondHistory()
    {
        return $this->hasMany(DemandCorrespond::class, 'demand_id', 'id')
            ->orderBy('demand_corresponds.id', 'desc');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function demandInquiryAnswers()
    {
        return $this->hasMany(DemandInquiryAns::class, 'demand_id', 'id');
    }

    /**
     * @return $this
     */
    public function demandAttachedFiles()
    {

        return $this->hasMany(DemandAttachedFile::class, 'demand_id', 'id')->orderBy('id', 'asc');
    }


    /**
     * @return mixed|string
     */
    public function getStatusNameAttribute()
    {
        $demandStatus = $this->getAttribute('demand_status');
        $item = MItem::where('item_category', '案件状況')
            ->where('item_id', $demandStatus)
            ->first();
        return $item ? $item->item_name : '';
    }

    /**
     * @return mixed|string
     */
    public function getMSiteNameAttribute()
    {
        $siteId = $this->getAttribute('site_id');
        $mSite = MSite::find($siteId);
        return $mSite ? $mSite->site_name : '';
    }

    /**
     * @return false|string
     */
    public function getReceiveDateTimeFormatAttribute()
    {
        return dateTimeFormat($this->getAttribute('receive_datetime'));
    }

    /**
     * @return false|string
     */
    public function getAuctionDeadlineTimeFormatAttribute()
    {
        return dateTimeFormat($this->getAttribute('auction_deadline_time'));
    }

    /**
     * @return false|string
     */
    public function getFollowTelDateFormatAttribute()
    {
        return dateTimeFormat($this->getAttribute('follow_tel_date'));
    }

    /**
     *
     */
    public function getCommissionLimitoverTimeFormatAttribute()
    {
        return formatSec($this->getAttribute('commission_limitover_time') * 60);
    }

    /**
     * @return $this
     */
    public function commissionInfoMail()
    {
        return $this->hasMany(CommissionInfo::class, 'demand_id', 'id')
            ->where('commit_flg', 1)
            ->where('del_flg', '!=', 1)
            ->where('introduction_not', '!=', 1)
            ->where('lost_flg', '!=', 1);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function commissionWord()
    {
        return $this->hasMany(CommissionInfo::class, 'demand_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function inquiries()
    {
        return $this->hasMany('App\Models\DemandInquiryAns', 'demand_id', 'id');
    }

    // original field in DB

    /**
     * @var array
     */
    public $demandFields = [
        'id'                         => null,
        'follow_date'                => null,
        'demand_status'              => null,
        'order_fail_reason'          => null,
        'mail_demand'                => null,
        'nighttime_takeover'         => null,
        'low_accuracy'               => null,
        'remand'                     => null,
        'immediately'                => null,
        'corp_change'                => null,
        'order_loss'                 => null,
        'receive_datetime'           => null,
        'site_id'                    => null,
        'genre_id'                   => null,
        'category_id'                => null,
        'cross_sell_source_site'     => null,
        'cross_sell_source_category' => null,
        'receptionist'               => null,
        'customer_name'              => null,
        'customer_tel'               => null,
        'customer_mailaddress'       => null,
        'postcode'                   => null,
        'address1'                   => null,
        'address2'                   => null,
        'address3'                   => null,
        'address4'                   => null,
        'building'                   => null,
        'room'                       => null,
        'tel1'                       => null,
        'tel2'                       => null,
        'contents'                   => null,
        'contact_desired_time'       => null,
        'jbr_order_no'               => null,
        'jbr_work_contents'          => null,
        'jbr_category'               => null,
        'mail'                       => null,
        'order_date'                 => null,
        'complete_date'              => null,
        'order_fail_date'            => null,
        'jbr_estimate_status'        => null,
        'jbr_receipt_status'         => null,
        'modified_user_id'           => null,
        'created_user_id'            => null,
        'development_request'        => null,
        'share_notice'               => null,
        'sms_send_agreement'         => null,
        'del_flg'                    => 0,
        'riro_kureka'                => 0,
        'cross_sell_source_genre'    => null,
        'order_no_marriage'          => null,
        'source_demand_id'           => null,
        'reservation_demand'         => 0,
        'same_customer_demand_url'   => null,
        'upload_estimate_file_name'  => null,
        'upload_receipt_file_name'   => null,
        'pet_tombstone_demand'       => null,
        'sms_demand'                 => 0,
        'cost_customer_question'     => 0,
        'cost_from'                  => null,
        'cost_to'                    => null,
        'special_measures'           => 0,
        'cost_customer_answer'       => null,
        'call_back_time'             => null,
        'auction'                    => 0,
        'follow'                     => 0,
        'business_trip_amount'       => null,
        'auction_deadline_time'      => null,
        'selection_system'           => null,
        'priority'                   => null,
        'push_stop_flg'              => null,
        'auction_start_time'         => null,
        'acceptance_status'          => null,
        'construction_class'         => null,
        'cross_sell_implement'       => null,
        'commission_limitover_time'  => null,
        'lock_flag'                  => null,
        'lock_date'                  => null,
        'lock_user_id'               => null,
        'contact_desired_time_from'  => null,
        'contact_desired_time_to'    => null,
        'is_contact_time_range_flg'  => null,
        'customer_corp_name'         => null,
        'jbr_receipt_price'          => null,
        'nitoryu_flg'                => 0,
        'cross_sell_call'            => null,
        'sms_reorder'                => 0,
        'st_claim'                   => 0,
    ];

    /**
     * build visit time
     * @author thaihv
     * @param  array $old   user input
     * @param  int $idx   position
     * @param  string $field (visit_time_from, visit_time_to, ajuidment)
     * @return string        time value
     */
    public function buildVisittime($old, $idx, $field)
    {
        $txtTime = '';
        if (is_null($this->visitTimeList)) {
            $this->visitTimeList = $this->visitTimes->toArray();
        }
        $txtTime = isset($old[$idx][$field]) ? $old[$idx][$field] : (isset(($this->visitTimeList)[$idx]) ? ($this->visitTimeList)[$idx][$field] : '');
        return $txtTime;
    }
}
