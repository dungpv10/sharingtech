<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DemandInquiryAns extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'demand_inquiry_answers';
    /**
     * @var boolean
     */
    public $timestamps = false;
    /**
     * @var array
     */
    protected $guarded = ['id'];
    const UPDATED_AT = 'modified';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mInquiry()
    {
        return $this->belongsTo('App\Models\MInquiry', 'inquiry_id', 'id');
    }
}
