<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExclusionTime extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'exclusion_times';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;

    /**
     * @var fillable
     */
    public $fillable = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function auctionGenreAreas()
    {
        return $this->hasMany('App\Models\AuctionGenreArea', 'exclusion_pattern', 'pattern');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function auctionGenres()
    {
        return $this->hasMany('App\Models\AuctionGenre', 'exclusion_pattern', 'pattern');
    }
}
