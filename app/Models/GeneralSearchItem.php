<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GeneralSearchItem extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'general_search_items';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;
}
