<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class License extends Model
{

    /**
     * @var string
     */
    protected $table = 'license';

    const HAVE_TO = '必須';

    /**
     * @var boolean
     */
    public $timestamps = false;
}
