<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MAddress1 extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'm_address1';
}
