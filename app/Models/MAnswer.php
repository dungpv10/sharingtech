<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MAnswer extends Model
{
    /**
     * @var string
     */
    protected $table = 'm_answers';
}
