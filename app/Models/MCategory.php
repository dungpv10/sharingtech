<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MCategory extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'm_categories';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;

    const DISABLE_FLG_TRUE = true;
    const DISABLE_FLG_FALSE = false;

    const AND_LICENSE_CONDITION = 1;
    const OR_LICENSE_CONDITION = 2;
    const LICENSE_CONDITION_TYPE = [
        self::AND_LICENSE_CONDITION => 'AND条件',
        self::OR_LICENSE_CONDITION => 'OR条件'
    ];

    const STOP_CATEGORY = '取次STOPカテゴリ';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function mGenres()
    {
        return $this->hasOne('App\Models\MGenre', 'id', 'genre_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function mSiteCategory()
    {
        return $this->hasMany(MSiteCateogry::class, 'category_id', 'id');
    }

    /**
     * @return mixed|string
     */
    public static function getDefaultFee()
    {
        $mCategory = self::orderBy('id', 'asc')->first();
        return $mCategory ? $mCategory->category_default_fee_unit : '';
    }
}
