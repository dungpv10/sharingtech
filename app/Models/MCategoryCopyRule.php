<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MCategoryCopyRule extends Model
{
    /**
     * @var string
     */
    protected $table = 'm_category_copyrules';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;
}
