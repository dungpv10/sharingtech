<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MCommissionAlertSetting extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'm_commission_alert_settings';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;
}
