<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MCommissionType extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'm_commission_types';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;
}
