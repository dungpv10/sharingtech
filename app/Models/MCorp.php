<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MCorp extends Model
{
    const CORP = 'Corp';
    const PERSON = 'Person';
    const CORP_KIND = [
        null => '',
        self::CORP => '法人',
        self::PERSON => '個人'
    ];
    const MEMBER_STATE_ACCESSION = 1;
    const AUCTION_STATUS_ONE = 1;
    const AUCTION_STATUS_TWO = 2;
    const AUCTION_STATUS_THREE = 3;
    const COMMISSION_STATUS_IMPROGRESS = 1;
    const COMMISSION_STATUS_ORDER = 2;
    const COMMISSION_STATUS_COMPLETE = 3;
    const COMMISSION_STATUS_FAIL = 4;

    const LISTED = 'listed';
    const UNLISTED = 'unlisted';
    const LISTED_KIND = [
        null => '',
        self::LISTED => '上場企業',
        self::UNLISTED => '非上場企業'
    ];

    const MOBILE_TEL_TYPE = '携帯電話タイプ';
    const COORDINATION_METHOD = '顧客情報連絡手段';

    const METHOD_6 = 'メール＋アプリ(推奨)';
    const METHOD_1 = 'メール＋FAX';
    const METHOD_7 = 'メール＋FAX＋アプリ';
    const METHOD_2 = 'メール';
    const METHOD_3 = 'FAX';
    const METHOD_4 = '専用フォーム';
    const METHOD_5 = 'その他';

    const METHOD_NUM_1 = '1';
    const METHOD_NUM_2 = '2';
    const METHOD_NUM_3 = '3';
    const METHOD_NUM_4 = '4';
    const METHOD_NUM_5 = '5';
    const METHOD_NUM_6 = '6';
    const METHOD_NUM_7 = '7';

    const COORDINATION_METHOD_LIST = [
        '' => '--なし--',
        self::METHOD_NUM_6 => self::METHOD_6,
        self::METHOD_NUM_1 => self::METHOD_1,
        self::METHOD_NUM_7 => self::METHOD_7,
        self::METHOD_NUM_2 => self::METHOD_2,
        self::METHOD_NUM_3 => self::METHOD_3,
        self::METHOD_NUM_4 => self::METHOD_4,
        self::METHOD_NUM_5 => self::METHOD_5,
    ];

    /**
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'm_corps';
    /**
     * @var array
     */
    protected $guarded = [];
    // custom attribulte @thaihv
    /**
     * @var array
     */
    protected $attributes = ['holidays' => '', 'in_progress' => 0, 'in_order' => 0, 'complete' => 0, 'failed' => 0];

    /**
     * @return mixed|string
     */
    public function getHolidaysAttribute()
    {
        return isset($this->attributes['holidays']) ? $this->attributes['holidays'] : '';
    }

    /**
     * @param $string
     */
    public function setHolidaysAttribute($string)
    {
        $this->attributes['holidays'] = $string;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function affiliationInfos()
    {
        return $this->hasMany('App\Models\AffiliationInfo', 'corp_id', 'id');
    }

    /**
     * @return mixed|string
     */
    public function getInProgressAttribute()
    {
        return isset($this->attributes['in_progress']) ? $this->attributes['in_progress'] : '';
    }

    /**
     * @param $count
     */
    public function setInProgressAttribute($count)
    {
        $this->attributes['in_progress'] = $count;
    }

    /**
     * @return mixed|string
     */
    public function getInOrderAttribute()
    {
        return isset($this->attributes['in_order']) ? $this->attributes['in_order'] : '';
    }

    /**
     * @param $count
     */
    public function setInOrderAttribute($count)
    {
        $this->attributes['in_order'] = $count;
    }

    /**
     * @return mixed|string
     */
    public function getCompleteAttribute()
    {
        return isset($this->attributes['complete']) ? $this->attributes['complete'] : '';
    }

    /**
     * @param $count
     */
    public function setCompleteAttribute($count)
    {
        $this->attributes['complete'] = $count;
    }

    /**
     * @return mixed|string
     */
    public function getFailedAttribute()
    {
        return isset($this->attributes['failed']) ? $this->attributes['failed'] : '';
    }

    /**
     * @param $count
     */
    public function setFailedAttribute($count)
    {
        $this->attributes['failed'] = $count;
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function commissionInfos()
    {
        return $this->hasMany('App\Models\CommissionInfo', 'corp_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mItem()
    {
        return $this->belongsTo('App\Models\MItem', 'coordination_method', 'item_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function mCorpCategory()
    {
        return $this->hasMany('App\Models\MCorpCategory', 'corp_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function mCorpNewYear()
    {
        return $this->hasOne(MCorpNewYear::class, 'corp_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function affiliationInfo()
    {
        return $this->hasOne(AffiliationInfo::class, 'corp_id', 'id');
    }

    /**
     * Get first holiday attribute
     *
     * @return string
     */
    public function getHoliday1Attribute()
    {
        $holiday = \DB::select(
            'SELECT ARRAY_TO_STRING(ARRAY( SELECT item_name FROM m_items m_items INNER JOIN m_corp_subs
              ON "m_corp_subs"."item_category" = "m_items"."item_category" JOIN m_corps ON "m_corp_subs"."corp_id" = "m_corps"."id"
              AND "m_corp_subs"."item_id" = "m_items"."item_id" WHERE "m_corp_subs"."item_category" = \'休業日\'
              AND m_corps.id='. $this->getAttribute('id') .'
              ORDER BY "m_items"."sort_order" ASC ),\'｜\')'
        );

        return $holiday ? $holiday[0]->array_to_string : '';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function affiliationStats()
    {
        return $this->hasMany('App\Models\AffiliationStat', 'corp_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function affiliationAreaStats()
    {
        return $this->hasMany('App\Models\AffiliationAreaStat', 'corp_id', 'id');
    }

    /**
     * @return $this
     */
    public function commissionStatusInProgress()
    {
        return $this->hasMany('App\Models\CommissionInfo', 'corp_id', 'id')->where('commission_status', self::COMMISSION_STATUS_IMPROGRESS);
    }

    /**
     * @return $this
     */
    public function commissionStatusInOrder()
    {
        return $this->hasMany('App\Models\CommissionInfo', 'corp_id', 'id')->where('commission_status', self::COMMISSION_STATUS_ORDER);
    }

    /**
     * @return $this
     */
    public function commissionStatusInComplete()
    {
        return $this->hasMany('App\Models\CommissionInfo', 'corp_id', 'id')->where('commission_status', self::COMMISSION_STATUS_COMPLETE);
    }

    /**
     * @return $this
     */
    public function commissionStatusInFail()
    {
        return $this->hasMany('App\Models\CommissionInfo', 'corp_id', 'id')->where('commission_status', self::COMMISSION_STATUS_FAIL);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function mTargetArea()
    {
        return $this->hasMany('App\Models\MTargetArea');
    }

    /**
     * @return array
     */
    public function getEmailByArrayAttribute()
    {
        $mail = $this->getAttribute('mailaddress_pc');
        return explode(';', $mail);
    }

    /**
     * @return array
     */
    public function getEmailMobileByArrayAttribute()
    {
        $mail = $this->getAttribute('mailaddress_mobile');
        return explode(';', $mail);
    }

    /**
     * @return mixed
     */
    public function getTextCoordinationAttribute()
    {
        $coordinationMethod = $this->getAttribute('coordination_method');
        $result = MItem::where('item_category', '=', '顧客情報連絡手段')
            ->where('item_id', '=', $coordinationMethod)->first();
        return $result->item_name;
    }

    /**
     * @return $this
     */
    public function mCorpCategoryWithCondition()
    {
        return $this->MCorpCategory()->where('category_id', $this->getAttribute('coordination_method'));
    }

    /**
     * @return string
     */
    public function getOrderFeeCommissionAttribute()
    {
        $corpMission = $this->mCorpCategoryWithCondition()->first();

        $corpMissionType = $corpMission ? $corpMission->corp_commission_type : 0;

        if ($corpMissionType != 2) {
            $orderFee = $corpMission->order_fee ?? 0;
            $orderFeeUnit =$corpMission->order_fee_unit ?? 0;
            $corpCommissionType = '成約';
        } else {
            $orderFee = $corpMission->introduce_fee;
            $orderFeeUnit = 0;
            $corpCommissionType = '紹介';
        }

        return $orderFeeUnit == 0
            ? $corpCommissionType . ($orderFee ? yenFormat2($orderFee) : "")
            : $corpCommissionType . ($orderFee ? $orderFee . '%' : '');
    }

    /**
     * @return mixed|string
     */
    public function getCategoryNoteAttribute()
    {
        $mCorp = $this->mCorpCategoryWithCondition()->first();
        return $mCorp ? $mCorp->note : '';
    }

    /**
     * Get address1 JP attribute
     *
     * @return array
     */
    public function getAddress1JpAttribute()
    {
        return getDivTextJP('prefecture_div', $this->attributes['address1']);
    }

    /**
     * @return mixed
     */
    public function getNoteOrCategoryNoteAttribute()
    {
        return $this->attributes['note'] != '' ? $this->attributes['note'] : $this->mCorpCategory->note;
    }
}
