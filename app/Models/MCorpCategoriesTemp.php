<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MCorpCategoriesTemp extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'm_corp_categories_temp';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;

    const DELETE_FLAG_TRUE = true;
    const DELETE_FLAG_FALSE = false;

    /**
     * @return array
     */
    public static function csvFormat()
    {
        return array(
            'id' => trans('report_corp_agreement_category.history_id'),
            'corp_agreement_id' => trans('report_corp_agreement_category.contract_id'),
            'm_corps_id' => trans('report_corp_agreement_category.company_id'),
            'official_corp_name' => trans('report_corp_agreement_category.formal_member_store_name'),
            'm_genres_id' => trans('report_corp_agreement_category.genre_id'),
            'genre_name' => trans('report_corp_agreement_category.genre_name'),
            'm_categories_id' => trans('report_corp_agreement_category.category_id'),
            'category_name' => trans('report_corp_agreement_category.category_name'),
            'order_fee' => trans('report_corp_agreement_category.order_receiving_fee'),
            'custom_order_fee_unit' => trans('report_corp_agreement_category.order_commission_unit_price'),
            'introduce_fee' => trans('report_corp_agreement_category.referral_fee'),
            'note' => trans('report_corp_agreement_category.remarks'),
            'select_list' => trans('report_corp_agreement_category.expertise'),
            'custom_corp_commission_type' => trans('report_corp_agreement_category.order_form'),
            'custom_action_type' => trans('report_corp_agreement_category.update_type'),
            'custom_action' => trans('report_corp_agreement_category.update_contents'),
            'modified' => trans('report_corp_agreement_category.update_date_and_time'),
        );
    }
    const SELECT_LIST = [
        '' => 'なし',
        'A' => 'A',
        'B' => 'B',
        'C' => 'C'
    ];
}
