<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MCorpCategory extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'm_corp_categories';

    /**
     * @var array
     */
    protected $fillable = ['*'];

    /**
     * @var boolean
     */
    public $timestamps = false;

    /**
     * get array field category
     *
     * @return array
     */
    public function getArrayFieldCategory()
    {
        return [
            'corp_id',
            'genre_id',
            'category_id',
            'order_fee',
            'order_fee_unit',
            'introduce_fee',
            'note',
            'select_list',
            'select_genre_category',
            'target_area_type',
            'modified_user_id',
            'modified',
            'created_user_id',
            'created',
            'corp_commission_type'
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function mTargetAreas()
    {
        return $this->hasMany('App\Models\MTargetArea', 'corp_category_id', 'id');
    }
}
