<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MCorpNewYear extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'm_corp_new_years';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;

    /**
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }
}
