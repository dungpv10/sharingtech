<?php

namespace App\Models;

class MCorpSub extends Base
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'm_corp_subs';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;

    /**
     *
     */
    const HOLIDAY = '休業日';
}
