<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MCorpsNoticeInfo extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    /**
     * @var string
     */
    protected $table = 'm_corps_notice_infos';

    /**
     * @var array
     */
    protected $guarded = [];
}
