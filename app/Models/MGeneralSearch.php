<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MGeneralSearch extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'm_general_searches';

    /**
     * @var array
     */
    protected $fillable = ['*'];

    /**
     * @var boolean
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function gsCondition()
    {
        return $this->hasMany('App\Models\GeneralSearchCondition', 'general_search_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function gsItem()
    {
        return $this->hasMany('App\Models\GeneralSearchItem', 'general_search_id', 'id');
    }
}
