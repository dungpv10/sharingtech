<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class MGenre extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'm_genres';

    /**
     * @var boolean
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function additionInfos()
    {
        return $this->hasMany('App\Models\AdditionInfo');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function mCategories()
    {
        return $this->hasMany('App\Models\MCategory', 'genre_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function selectGenres()
    {
        return $this->hasMany('App\Models\SelectGenres', 'genre_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function mSiteGenres()
    {
        return $this->hasMany(MSiteGenres::class, 'genre_id', 'id');
    }
}
