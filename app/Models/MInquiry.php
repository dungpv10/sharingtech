<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MInquiry extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'm_inquiries';
    /**
     * @var boolean
     */
    public $timestamps = false;
}
