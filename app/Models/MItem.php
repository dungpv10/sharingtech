<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MItem extends Model
{
    const CONMISSION_ORDER_FAIL_REASON = '取次失注理由';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'm_items';

    /**
     * @var array
     */
    protected $guarded = [];

    const PROGRESS_STATUS_CATEGORY = '進捗表状況';
    const PROGRESS_STATUS_REPLY_RESULT_CATEGORY = '進捗表_未返信理由';
    const PROGRESS_DELIVERY_CATEGORY = '進捗表送付方法';
    const PROGRESS_BACK_PHONE_CATEGORY = '進捗表_架電後フラグ';
    const COMMISSION_ORDER_FAIL_REASON = '取次失注理由';
    const BUILDING_TYPE = '建物種別';
    /**
     * get list by query function
     *
     * @param  string $query
     * @return object
     */
    public function scopeGetList($query)
    {
        return $query->where('id', '>', 100);
    }

    /**
     * get list genres function
     * @param string $category
     * @return array
     */
    public function getList($category)
    {
        $results = [];
        $list = self::where('item_category', $category)
                    ->where('enabled_start', '<=', date('Y/m/d'))
                    ->where(function ($query) {
                        $query->where('enabled_end', '>=', date('Y/m/d'))
                            ->orWhere('enabled_end', null);
                    })->orderBy('sort_order', 'asc')->get();

        foreach ($list as $val) {
            $results[$val['item_id']] = $val['item_name'];
        }

        return $results;
    }

    /**
     * get list genres item name function
     *
     * @param  string  $category
     * @param  integer $value
     * @return string
     */
    public function getListText($category, $value)
    {
        $results = MItem::where('item_category', '=', $category)
            ->where('item_id', '=', $value)
            ->orderBy('item_id', 'asc')->get()->toarray();
        if (isset($results[0]['item_name'])) {
            return $results[0]['item_name'];
        } else {
            return;
        }
    }
}
