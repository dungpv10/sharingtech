<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MPost extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'm_posts';

    /**
     * @var array
     */
    protected $fillable = ['*'];

    /**
     * @var boolean
     */
    public $timestamps = false;
}
