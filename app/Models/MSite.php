<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MSite extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'm_sites';

    const CROSS_FLAG = 1;

    /**
     * get list site function
     *
     * @return array
     */
    public function getList()
    {
        $list = MSite::select('site_name', 'id')->orderBy('site_name', 'asc')->get()->toarray();
        $results = array();
        foreach ($list as $val) {
            $results[$val['id']] = $val['site_name'];
        }
        return $results;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mCommissionType()
    {
        return $this->belongsTo(MCommissionType::class, 'commission_type', 'id');
    }
}
