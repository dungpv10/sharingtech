<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * MSiteCateogry
 *
 * @author cuongnguyenx
 */
class MSiteCateogry extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'm_site_categories';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mCategory()
    {
        return $this->belongsTo(MCategory::class, 'category_id', 'id');
    }
}
