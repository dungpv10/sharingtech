<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MSiteGenres extends Model
{
    /**
     * @var string
     */
    public $table = 'm_site_genres';

    /**
     * @return $this
     */
    public function mGenre()
    {
        return $this->belongsTo(MGenre::class, 'genre_id', 'id')->select(['genre_name', 'id']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mSite()
    {
        return $this->belongsTo(MSite::class, 'site_id', 'id');
    }
}
