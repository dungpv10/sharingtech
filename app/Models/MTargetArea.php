<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MTargetArea extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'm_target_areas';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;
}
