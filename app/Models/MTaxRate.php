<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MTaxRate extends Model
{
    /**
     * @var string
     */
    protected $table = 'm_tax_rates';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;
}
