<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MTime extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'm_times';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;
}
