<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User;

class MUser extends User
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'm_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'user_name', 'password', 'auth', 'affiliation_id'];

    /**
     * @var boolean
     */
    public $timestamps = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * @param $roles
     * @return bool
     */
    public function hasRole($roles)
    {
        $auth = $this->auth;
        if (in_array($auth, $roles)) {
            return true;
        }

        return false;
    }

    /**
     * Description function: get user has column auth != affiliation
     *
     * @return array
     */
    public function dropDownUser()
    {
        $list = MUser::where('auth', '!=', 'affiliation')
            ->orderBy('user_name', 'asc')->get()->toarray();
        $results = array();
        foreach ($list as $val) {
            $results[$val['id']] = $val['user_name'];
        }
        return $results;
    }

    /**
     * @return bool
     */
    public function isPoster()
    {
        return in_array($this->auth, ['system', 'admin', 'accounting_admin']);
    }

    /**
     * @return bool
     */
    public function isReader()
    {
        return $this->auth == 'affiliation';
    }


    /**
     * @auth Dung.PhamVan@nashtechglobal.com
     */
    public function getListUserForDropDown()
    {
        return $this->where('auth', '!=', 'affiliation')
            ->select('user_name', 'id')
            ->pluck('user_name', 'id')->toArray();
    }

    /**
     * @return bool
     */
    public function isSystem()
    {
        return $this->auth == 'system';
    }

    /**
     * @return array
     */
    public static function csvFormat()
    {
        return array(
            'user_name' => trans('user_search.user_name'),
            'corp_id' => trans('user_search.corp_id'),
            'official_corp_name' => trans('user_search.official_corp_name'),
            'auth' => trans('user_search.auth'),
            'last_login_date' => trans('user_search.last_login_date'),
        );
    }
}
