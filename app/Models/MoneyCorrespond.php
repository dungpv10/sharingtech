<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MoneyCorrespond extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    /**
     * @var string
     */
    protected $table = 'money_corresponds';

    /**
     * @var array
     */
    protected $guarded = [];
}
