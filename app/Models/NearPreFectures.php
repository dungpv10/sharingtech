<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NearPreFectures extends Model
{
    /**
     * @var string
     */
    protected $table = "near_prefectures";

    /**
     * @var array
     */
    protected $fillable = ['*'];
}
