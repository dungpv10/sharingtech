<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\NotCorrespondItem;

class NotCorrespond extends Model
{
    /**
     * @var string
     */
    protected $table = 'not_corresponds';
    /**
     * @var array
     */
    protected $guarded = [];
    /**
     * @var boolean
     */
    public $timestamps = false;

    /**
     *
     */
    public function findNotCorrespond()
    {
    }
}
