<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotCorrespondItem extends Model
{
    /**
     * @var string
     */
    protected $table = 'not_correspond_items';

    /**
     * @var array
     */
    protected $fillable = ['*'];

    /**
     * @var boolean
     */
    public $timestamps = false;

    /**
     * @return array
     */
    public function getEditableColumns()
    {
        return [
            'immediate_lower_limit',
            'large_lower_limit',
            'midium_lower_limit',
            'small_lower_limit',
            'immediate_date',
            'modified_user_id',
            'modified'
        ];
    }
}
