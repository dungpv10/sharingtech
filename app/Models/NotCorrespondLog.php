<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\NotCorrespondItem;

class NotCorrespondLog extends Model
{
    /**
     * @var string
     */
    protected $table = 'not_correspond_logs';
    /**
     * @var array
     */
    protected $guarded = [];
    /**
     * @var boolean
     */
    public $timestamps = false;
}
