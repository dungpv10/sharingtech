<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NoticeInfo extends Model
{
    const CREATED_AT = "created";
    const UPDATED_AT = "modified";

    /**
     * @var string
     */
    protected $table = 'notice_infos';
    /**
     * @var array
     */
    protected $guarded = [];
}
