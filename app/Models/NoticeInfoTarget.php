<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NoticeInfoTarget extends Model
{
    const CREATED_AT = "created";
    const UPDATED_AT = "modified";

    /**
     * @var string
     */
    protected $table = 'notice_info_targets';
    /**
     * @var array
     */
    protected $guarded = [];
}
