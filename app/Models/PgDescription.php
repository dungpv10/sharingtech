<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PgDescription extends Model
{
    /**
     * @var string
     */
    protected $table = 'pg_description';
    /**
     * @var array
     */
    protected $guarded = [];
}
