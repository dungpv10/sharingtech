<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProgAddDemandInfo extends Model
{
    const UPDATED_AT = 'modified';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function progCorp()
    {
        return $this->belongsTo('\App\Models\ProgCorp', 'prog_corp_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mCorp()
    {
        return $this->belongsTo('\App\Models\MCorp', 'corp_id', 'id');
    }
}
