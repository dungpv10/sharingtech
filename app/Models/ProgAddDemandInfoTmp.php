<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProgAddDemandInfoTmp extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'prog_add_demand_info_tmps';

    /**
     * @var array
     */
    protected $guarded = ['id'];
    /**
     * map updated_at of eloquent with modified
     */
    const UPDATED_AT = 'modified';
}
