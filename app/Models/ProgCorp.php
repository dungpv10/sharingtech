<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProgCorp extends Model
{
    /**
     * @var string
     */
    protected $table = 'prog_corps';
    /**
     * @var array
     */
    protected $guarded = ['id'];
    const UPDATED_AT = 'modified';
    const MAIL_COLLECTED = 7; // progress flag status
    const LIMIT_ADD_DEMAND_INFO = 30;
    const DEMAND_TYPE_UPDATE_EDITABLE = [
        1 => '復活案件（過去に失注になったお客様より再度問い合わせがあった場合）',
        2 => '追加施工（問い合わせのあったジャンルと同時に、別ジャンルの受注をした場合）',
        3 => 'その他'
    ];
    const DEMAND_TYPE_UPDATE_READ_ONLY = [
        1=>'復活案件',
        2=>'追加施工',
        3=>'その他'
    ];
    const STORAGE_PATH = 'prog_corps/';
    // custom attribulte @thaihv
    /**
     * @var array
     */
    protected $attributes = ['holidays' => ''];

    /**
     * @return mixed|string
     */
    public function getHolidaysAttribute()
    {
        return isset($this->attributes['holidays']) ? $this->attributes['holidays'] : '';
    }

    /**
     * @param $string
     */
    public function setHolidaysAttribute($string)
    {
        $this->attributes['holidays'] = $string;
    }
    // end custom attribute
    /**
    /**
     * Get the prog_demand_infos for the prog_corp.
     */
    public function progAddDemandInfos()
    {
        return $this->hasMany('App\Models\ProgAddDemandInfo', 'prog_corp_id', 'id');
    }

    /**
     * Get the prog_import_file for the prog_corp.
     */
    public function progImportFile()
    {
        return $this->belongsTo('App\Models\ProgImportFile', 'prog_import_file_id', 'id');
    }
    /**
     * Get the prog_import_file for the prog_corp.
     */
    public function progDemandInfo()
    {
        return $this->hasMany('App\Models\ProgDemandInfo', 'prog_corp_id', 'id')->orderBy('receive_datetime');
    }

    /**
     * Get the mCorp for the prog_corp.
     */
    public function mCorp()
    {
        return $this->belongsTo('App\Models\MCorp', 'corp_id', 'id');
    }
    /**
     * get demand type update by progress flag
     *
     * @return array list type update
     */
    public function getDemandTypeUpdate()
    {
        return $this->progress_flag == self::MAIL_COLLECTED ? self::DEMAND_TYPE_UPDATE_READ_ONLY : self::DEMAND_TYPE_UPDATE_EDITABLE;
    }
}
