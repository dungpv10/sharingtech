<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProgDemandInfo extends Model
{
    // map updated_at of eloquent with modified
    const UPDATED_AT = 'modified';
    const PM_TAX = 8;
    const PM_COMMISSION_STATUS = [1=>'進行中', 2 => '受注', 3 => '施工完了', 4 => '失注'];
    const PM_DIFF_LIST = [1=> '--', 2 => '変更なし', 3 => '変更あり'];
    const PM_DIFF_LIST_DEMAND_DETAIL = [1=> '--', 2 => '変更はない', 3 => '変更がある'];
    const DEMAND_TYPE_UPDATE_EDITABLE = [
        1 => '復活案件（過去に失注になったお客様より再度問い合わせがあった場合）',
        2 => '追加施工（問い合わせのあったジャンルと同時に、別ジャンルの受注をした場合）',
        3 => 'その他'
    ];
    const DEMAND_TYPE_UPDATE_READ_ONLY = [
        1=>'復活案件',
        2=>'追加施工',
        3=>'その他'
    ];


    const CSV_FIELD = [
        'corp_name' => '企業名', //m_corps
        'official_corp_name' => '正式名',//m_corps
        'corp_id' => '施工会社番号',
        'commission_dial' => '取次要ダイヤル',//m_corps
        'mail_address' => '進捗表送付先(メール)', //prog_corps
        'fax' => '進捗表送付(FAX)',//prog_corps
        'contact_type' => '取次状況',//prog_corps
        'unit_cost' => '単価',//prog_corps
        'progress_flag' => '進捗表状況',//prog_corps
        'demand_id' =>'案件コード',
        'commission_id' =>'取次ID',
        'receive_datetime' =>'受信日時',
        'customer_name' =>'(ひらがなフルネーム)',
        'category_name' =>'カテゴリ',
        'fee' =>'手数料率(手数料金額)',
        'complete_date' => '施工完了日[失注日](インポート時)',
        'construction_price_tax_exclude' => '施工金額（税抜）(インポート時)',
        'commission_status' =>'進捗状況(インポート時)',
        'diff_flg' =>'情報相違',
        'complete_date_update' =>'施工完了日[失注日](業者返送時)',
        'construction_price_tax_exclude_update' =>'施工金額（税抜）(業者返送時)',
        'commission_status_update' =>'進捗状況(業者返送時)',
        'fee_target_price' =>'手数料対象金額',
        'commission_order_fail_reason_update' =>'失注理由',
        'comment_update' =>'備考欄',
        'koujo' => '控除金額',//prog_corps
        'collect_date' => '回収日',//prog_corps
        'sf_register_date' => '未送信（焦げ付き）',//prog_corps
        'contact_type' => '送付方法',//prog_corps
        'last_send_date' => '最新送信日',//prog_corps
        'mail_count' => 'メール送付回数',//prog_corps
        'fax_count' => 'ＦＡＸ送付回数',//prog_corps
        'note' =>'後追い履歴',//prog_corps //カスタム項目
        'not_replay_flag' => '未返信理由',//prog_corps
        'fee_billing_date' => '手数料請求日',//請求データ参照
        'genre_name' => 'ジャンル',
        'tel1' => '連絡先①',//m_corps
        'agree_flag' =>'同意チェック',
        'ip_address_update' =>'IPアドレス',
        'user_agent_update' =>'ユーザーエージェント',
        'host_name_update' =>'ホスト名',
    ];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'prog_demand_infos';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function progCorp()
    {
        return $this->belongsTo('\App\Models\ProgCorp', 'prog_corp_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mCorp()
    {
        return $this->belongsTo('\App\Models\MCorp', 'corp_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rogDemandInfoTmps()
    {
        return $this->hasMany('App\Models\ProgDemandInfoTmp', 'prog_demand_info_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function progAddDemandInfo()
    {
        return $this->hasMany('App\Models\ProgAddDemandInfo', 'demand_id_update', 'demand_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function commissionInfo()
    {
        return $this->belongsTo('App\Models\CommissionInfo', 'commission_id', 'id');
    }
}
