<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProgDemandInfoLog extends Model
{
    /**
     * @var array
     */
    protected $guarded = ['id'];
}
