<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProgDemandInfoOtherTmp extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'prog_demand_info_other_tmps';

    /**
     * @var boolean
     */
    public $timestamps = false;
    /**
     * @var array
     */
    protected $guarded = ['id'];
}
