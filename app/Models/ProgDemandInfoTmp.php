<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProgDemandInfoTmp extends Model
{
    /**
     * @var string
     */
    protected $table = 'prog_demand_info_tmps';
    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * map updated_at of eloquent with modified
     */
    const UPDATED_AT = 'modified';
}
