<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProgImportFile extends Model
{
    const UPDATED_AT = 'modified';
       /**
        * The table associated with the model.
        *
        * @var string
        */
    protected $table = 'prog_import_files';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'file_name',
        'original_file_name',
        'import_date',
        'delete_flag',
        'modified_user_id',
        'lock_flag',
        'release_flag',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}
