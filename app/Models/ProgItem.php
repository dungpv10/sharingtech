<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProgItem extends Model
{
    const EDIT_DATE = 6;
    /**
     * @var string
     */
    protected $table = 'prog_items';

    /**
     * @var boolean
     */
    public $timestamps = false;

    /**
     * @return array
     */
    public function getEditableColumns()
    {
        return [
            'up_text',
            'down_text',
            'return_limit',
            'modified',
            'modified_user_id',
        ];
    }
}
