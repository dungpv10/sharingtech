<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Refusals extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'refusals';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;
}
