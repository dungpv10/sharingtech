<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReputationChecks extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'reputation_checks';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;
}
