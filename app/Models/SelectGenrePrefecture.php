<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SelectGenrePrefecture extends Model
{
    /**
     * @var string
     */
    public $table = 'select_genre_prefectures';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;
}
