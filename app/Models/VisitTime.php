<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VisitTime extends Model
{
    /**
     * @var string
     */
    protected $table = 'visit_times';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Get field
     *
     * @return array
     */
    public static function getField()
    {
        return [
            'demand_id' => null,
            'visit_time' => null,
            'modified_user_id' => null,
            'modified' => null,
            'created_user_id' => null,
            'created' => null,
            'is_visit_time_range_flg' => 0,
            'visit_time_from' => null,
            'visit_time_to' => null,
            'visit_adjust_time' => null
        ];
    }

    /**
     * Demand info relations
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function demandInfo()
    {
        return $this->belongsTo(DemandInfo::class, 'demand_id', 'id');
    }
}
