<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Weather
 *
 * @package App\Models
 */
class Weather extends Model
{
    /**
     * @var string
     */
    protected $table = 'weathers';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;
}
