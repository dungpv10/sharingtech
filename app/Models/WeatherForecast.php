<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class WeatherForecast
 *
 * @package App\Models
 */
class WeatherForecast extends Model
{
    /**
     * @var string
     */
    protected $table = 'weather_forecasts';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var boolean
     */
    public $timestamps = false;
}
