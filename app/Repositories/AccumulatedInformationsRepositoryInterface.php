<?php

namespace App\Repositories;

interface AccumulatedInformationsRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $corpId
     * @param $demandId
     * @return mixed
     */
    public function getInfos($corpId, $demandId);

    /**
     * @param $demandId
     * @return mixed
     */
    public function getAllInfos($demandId);

    /**
     * @author Nguyen.DoNhu <Nguyen.DoNhu@Nashtechglobal.com>
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updateOrCreate($id, $data);

    /**
     * @author Nguyen.DoNhu <Nguyen.DoNhu@Nashtechglobal.com>
     * @param integer $corpId
     * @param integer $demandId
     * @return mixed
     */
    public function getInfoByFlag($corpId, $demandId);
}
