<?php

namespace App\Repositories;

interface AdditionInfoRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $data
     * @return \App\Models\Base
     */
    public function save($data);

    /**
     * @param $fields
     * @param $orderBy
     * @param $conditions
     * @return mixed
     */
    public function getReportAdditionList($fields, $orderBy, $conditions);
}
