<?php

namespace App\Repositories;

interface AffiliationCorrespondsRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * Acquisition of member store correspondence history
     *
     * @param  array $conditions
     * @return \Illuminate\Support\Collection
     */
    public function getAffiliationCorrespond($conditions = []);

    /**
     * Update affiliation correspond with id
     *
     * @param  $id
     * @param  $data
     * @return mixed|static
     */
    public function updateAffiliationCorrespondWithId($id, $data);
}
