<?php

namespace App\Repositories;

interface AffiliationInfoRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param integer $corpId
     * @return array
     */
    public function findAffiliationInfoByCorpId($corpId);

    /**
     * @param $data
     * @return mixed
     */
    public function updateByCorpId($data);

    /**
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updateById($id, $data);

    /**
     * Get list corp_id in week
     * Use in AffiliationInfoService
     *
     * @author Nguyen.DoNhu <Nguyen.DoNhu@Nashtechglobal.com>
     * @return mixed
     */
    public function getCommissionWeekCountOfAffiliation();

    /**
     * Get list corp_id by status
     * Use in AffiliationInfoService
     *
     * @author Nguyen.DoNhu <Nguyen.DoNhu@Nashtechglobal.com>
     * @param  $status
     * @return mixed
     */
    public function getReceiptCountInitialize($status);

    /**
     * Get affiliation_infos join shell_work_result
     * Use in AffiliationInfoService
     *
     * @author Nguyen.DoNhu <Nguyen.DoNhu@Nashtechglobal.com>
     * @return mixed
     */
    public function getWithJoinShellWork();

    /**
     * Get list corp_id
     * Use in AffiliationInfoService
     *
     * @author Nguyen.DoNhu <Nguyen.DoNhu@Nashtechglobal.com>
     * @return mixed
     */
    public function getReceiptRateInitialize();
}
