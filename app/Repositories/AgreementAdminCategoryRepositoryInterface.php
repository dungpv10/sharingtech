<?php

namespace App\Repositories;

interface AgreementAdminCategoryRepositoryInterface
{
    /**
     * @return mixed
     */
    public function getAllJoinedMCategory();

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);
}
