<?php

namespace App\Repositories;

interface AgreementCustomizeRepositoryInterface
{
    /**
     * @return mixed
     */
    public function getAllAgreementCustomize();

    /**
     * @param $corpId
     * @param $deleteFlag
     * @return mixed
     */
    public function findAgreementCustomizeByCorpId($corpId, $deleteFlag);

    /**
     * @param $id
     * @return mixed
     */
    public function deleteById($id);

    /**
     * @param $id
     * @return mixed
     */
    public function findById($id);

    /**
     * @param $data
     * @return mixed
     */
    public function saveAgreementCustomize($data);

    /**
     * @param $fieldId
     * @param $field
     * @param $tableKind
     * @return mixed
     */
    public function findLastestCustomize($fieldId, $field, $tableKind);

}
