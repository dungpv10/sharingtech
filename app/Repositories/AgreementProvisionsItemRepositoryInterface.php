<?php


namespace App\Repositories;

interface AgreementProvisionsItemRepositoryInterface
{
    /**
     * @param $column
     * @param $value
     * @return mixed
     */
    public function deleteByColumn($column, $value);

    /**
     * @param $id
     * @return mixed
     */
    public function findById($id);
}
