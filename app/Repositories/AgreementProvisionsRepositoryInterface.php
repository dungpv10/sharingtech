<?php

namespace App\Repositories;

interface AgreementProvisionsRepositoryInterface
{

    /**
     * @param $id
     * @param $deleteFlag
     * @return mixed
     */
    public function findAgreementById($id, $deleteFlag);

    /**
     * @param $agreementId
     * @param $deleteFlag
     * @return mixed
     */
    public function findAgreementProvisionsByAgreementId($agreementId, $deleteFlag);

    /**
     * @param $id
     * @return mixed
     */
    public function findById($id);
}
