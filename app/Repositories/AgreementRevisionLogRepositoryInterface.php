<?php

namespace App\Repositories;

interface AgreementRevisionLogRepositoryInterface
{
    /**
     * @return mixed
     */
    public function getAllContractTermsRevisionHistoryJoinMUserWithoutContent();

    /**
     * @param $id
     * @return mixed
     */
    public function findByIdJoinWithMUser($id);

    /**
     * @param $data
     * @return mixed
     */
    public function insert($data);

    /**
     * @return mixed
     */
    public function getFirstAgreementRevisionLog();

    /**
     * @return mixed
     */
    public function getMaxAgreementRevisionLogId();
}
