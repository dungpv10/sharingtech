<?php

namespace App\Repositories;

interface ApprovalRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @return mixed
     */
    public function getApplicationAnswer();

    /**
     * @return mixed
     */
    public function getApprovalForReport();

    /**
     * @param $groupId
     * @return mixed
     */
    public function getApprovalForCropCategoryAppAdmin($groupId);

    /**
     * @param $groupId
     * @param $approvalIds
     * @param $userId
     * @return mixed
     */
    public function getApprovalForCropCategoryAppAdminService($groupId, $approvalIds, $userId);

    /**
     * @return mixed
     */
    public function getApplicationAnswerCsv();

    /**
     * @param \App\Models\Base $data
     * @return \App\Models\Base
     */
    public function save($data);

    /**
     * @param $groupId
     * @return mixed
     */
    public function getApprovalForCropCategoryAppAnswer($groupId);

    /**
     * @return mixed
     */
    public function getCorpCategoryGroupApplicationAdmin();

    /**
     * @param $id
     * @param $action
     * @return mixed
     */
    public function updateStatus($id, $action);
}
