<?php

namespace App\Repositories;

interface AuctionGenreAreaRepositoryInterface extends SingleKeyModelRepositoryInterface
{

    /**
     * @param $genreId
     * @param $prefCd
     * @return mixed
     */
    public function getFirstByGenreIdAndPrefCd($genreId, $prefCd);

    /**
     * @param null $data
     * @return mixed
     */
    public function saveData($data = null);
}
