<?php

namespace App\Repositories;

interface AuctionInfoRepositoryInterface extends SingleKeyModelRepositoryInterface
{

    /**
     * get list
     *
     * @param $orderBy
     * @param $affiliationId
     * @return mixed
     */
    public function getAuctionAlreadyList($orderBy, $affiliationId);

    /**
     * get by id
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * count by id and commit flag
     * @param integer $id
     * @param integer $commitFlg
     * @return mixed
     */
    public function countByIdAndCommissionCommitFlag($id, $commitFlg = 1);

    /**
     * get by id and commit flag
     * @param $id
     * @return mixed
     */
    public function getByIdAndCommissionCommitFlag($id, $commitFlg = 1);

    /**
     * @param $auctionId
     * @return mixed
     */
    public function getAuctionInfoDemandInfo($auctionId);

    /**
     * @param $auctionId
     * @return mixed
     */
    public function getAuctionFee($auctionId);

    /**
     * @param $dataAuction
     * @return mixed
     */
    public function updateAuctionInfo($dataAuction);

    /**
     * @param $auctionId
     * @return mixed
     */
    public function getFirstById($auctionId);

    /**
     * @param $data
     * @param $flags
     * @return mixed
     */
    public function updateFlag($data, $flags = ['refusal_flg']);

    /**
     * @param $dataRequest
     * @return mixed
     */
    public function deleteItemByListId($dataRequest);

    /**
     * find by demand id and corp id
     *
     * @param  integer $demandId
     * @param  integer $corpId
     * @return object
     */
    public function getFirstByDemandIdAndCorpId($demandId, $corpId);

    /**
     * @param null $demandId
     * @return mixed
     */
    public function getListByDemandId($demandId = null);

    /**
     * @param $id
     * @param $data
     * @return mixed
     */
    public function saveAuction($id, $data);

    /**
     * @param $data
     * @return mixed
     */
    public function saveAuctions($data);
}
