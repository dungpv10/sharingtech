<?php

namespace App\Repositories;

interface AuctionRankingRepoInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $endDate
     * @param $startDate
     * @return mixed
     */
    public function getData($endDate, $startDate);

    /**
     * @param $endDate
     * @param $startDate
     * @return mixed
     */
    public function getDataPaginateAuctionRanking($endDate, $startDate);

    /**
     * @param $endDate
     * @param $startDate
     * @return mixed
     */
    public function getDataCSVAuctionRanking($endDate, $startDate);

    /**
     * @param $endDate
     * @param $startDate
     * @return mixed
     */
    public function getCountAuctionRanking($endDate, $startDate);
}
