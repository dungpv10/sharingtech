<?php

namespace App\Repositories;

interface AutoCallRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @return mixed
     */
    public function getItem();

    /**
     * @param \App\Models\Base $data
     * @return \App\Models\Base
     */
    public function save($data);
}
