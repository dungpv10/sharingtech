<?php

namespace App\Repositories;

interface AutoCommissionCorpRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * count auto commission corp list by corpId
     * @param  integer $corpId
     * @return integer
     */
    public function countByCorpId($corpId);

    /**
     * @param $jisCd
     * @param $categoryId
     * @param $corpId
     * @return mixed
     */
    public function deleteBy($jisCd, $categoryId, $corpId);

    /**
     * @param $data
     * @param null $type
     * @param bool $checkRequest
     * @return mixed
     */
    public function getByCategoryGenreAndPrefCd($data, $type = null, $checkRequest = true);

    /**
     * @param $arrGenreId
     * @return mixed
     */
    public function findByGenreId($arrGenreId);

    /**
     * @param $listCateId
     * @param $listPrefId
     * @return mixed
     */
    public function deleteByCateAndPref($listCateId, $listPrefId);

    /**
     * @param $listCorpCommission
     * @param $listCorpSelect
     * @param $listCate
     * @param $listJiscd
     * @return mixed
     */
    public function addCorpInfor($listCorpCommission, $listCorpSelect, $listCate, $listJiscd);
}
