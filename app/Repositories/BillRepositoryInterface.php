<?php

namespace App\Repositories;

interface BillRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function getBlankModel();

    /**
     * @param $fromDate
     * @param $toDate
     * @return mixed
     */
    public function getDataByCondition($fromDate, $toDate);

    /**
     * create bill_infos
     *
     * @param  $data
     * @return mixed
     */
    public function insertData($data);

    /**
     * search bill_info data by conditions
     * @param $data
     * @return mixed
     */
    public function searchByConditions($data);

    /**
     * find modified column
     *
     * @param  $id
     * @return mixed
     */
    public function findModified($id);

    /**
     * update record
     *
     * @param  $id
     * @param  $data
     * @return mixed
     */
    public function updateMultiRecord($id, $data);

    /**
     * get bill_info lists
     *
     * @param  $ids
     * @return mixed
     */
    public function getDownloadList($ids);

    /**
     * get bill info data
     *
     * @param  $ids
     * @param $mCorpId
     * @param  $billStatus
     * @return mixed
     */
    public function getPastIssueList($ids, $mCorpId, $billStatus);

    /**
     * @param $auctionId
     * @return mixed
     */
    public function findByAuctionId($auctionId);

    /**
     * @param $billId
     * @return mixed
     */
    public function getData($billId);

    /**
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updateData($id, $data);

    /**
     * delete multi records
     * @param integer $demandId
     * @param array $ids
     * @return mixed
     */
    public function deleteByDemandIdAndIds($demandId, $ids);

    /**
     * get first data by demand id and auction id
     * @param integer $demandId
     * @param null|integer $auctionId
     * @return \Illuminate\Database\Eloquent\Model|null|object|static
     */
    public function getByDemandIdAuctionId($demandId, $auctionId = null);

    /**
     * count data by demand id and commission id
     * @param integer $demandId
     * @param integer $commissionId
     * @return int
     */
    public function countByDemandIdAndCommissionId($demandId, $commissionId);
}
