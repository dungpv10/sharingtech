<?php

namespace App\Repositories;

interface CategoryLicenseLinkRepositoryInterface
{
    /**
     * @return mixed
     */
    public function getLicenseIdsByCategoryId($id);

    /**
     * @param $categoryId
     * @param $licenseId
     * @return mixed
     */
    public function deleteByCategoryIdAndLicenseId($categoryId, $licenseId);
}
