<?php

namespace App\Repositories;

interface CommissionAppRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $commissionId
     * @return mixed
     */
    public function findByCommissionId($commissionId);

    /**
     * @param $data
     * @return mixed
     */
    public function saveApp($data);
}
