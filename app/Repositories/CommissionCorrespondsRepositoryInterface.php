<?php

namespace App\Repositories;

interface CommissionCorrespondsRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function save($data);

    /**
     * @param $id
     * @return mixed
     */
    public function findById($id);
}
