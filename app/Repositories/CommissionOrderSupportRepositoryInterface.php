<?php

namespace App\Repositories;

interface CommissionOrderSupportRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $data
     * @return \App\Models\Base
     */
    public function save($data);

    /**
     * @param $commissionId
     * @return mixed
     */
    public function findByCommissionId($commissionId);
}
