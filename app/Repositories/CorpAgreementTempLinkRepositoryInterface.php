<?php

namespace App\Repositories;

interface CorpAgreementTempLinkRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $corpId
     * @param $corpAgreementId
     * @return mixed
     */
    public function getItemByCorpIdAndCorpAgreementId($corpId, $corpAgreementId);

    /**
     * @param \App\Models\Base $data
     * @return \App\Models\Base
     */
    public function save($data);

    /**
     * @param $corpId
     * @return mixed
     */
    public function getTempLink($corpId);

    /**
     * @param $corpId
     * @param $corpAgreementId
     * @return mixed
     */
    public function insertAgreementTempLink($corpId, $corpAgreementId);

    /**
     * @param $corpId
     * @param $tempId
     * @return mixed
     */
    public function getFirstByCorpId($corpId, $tempId);

    /**
     * @param $data
     * @return mixed
     */
    public function updateByTempLink($data);

    /**
     * @param $corpId
     * @param $corpAgreementId
     * @return mixed
     */
    public function getByCorpIdAndCorpAgreementId($corpId, $corpAgreementId);

    /**
     * @param $corpId
     * @param $corpAgreementId
     * @return mixed
     */
    public function getFirstByCorpIdAndCorpAgreementId($corpId, $corpAgreementId);

    /**
     * @param $corpId
     * @return mixed
     */
    public function getByCorpIdWith2Record($corpId);

    /**
     * @param $corpId
     * @return mixed
     */
    public function getFirstIdByCorpId($corpId);

    /**
     * @param $corpId
     * @param $limit
     * @return mixed
     */
    public function getItemByCorpIdAndLimit($corpId, $limit);

    /**
     * @param $corpId
     * @return mixed
     */
    public function findLatestByCorpId($corpId);

    /**
     * @param $corpId
     * @param $corpAgreementId
     * @return mixed
     */
    public function insertAndGetIdBack($corpId, $corpAgreementId);

    /**
     * @param $cropId
     * @return mixed
     */
    public function getByCropIdWithRelation($cropId);
}
