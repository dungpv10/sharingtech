<?php

namespace App\Repositories;

interface CorpCategoryGroupApplicationRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $params
     * @return mixed
     */
    public function searchCorpCategoryGroupApplication($params);

    /**
     * @param $params
     * @return mixed
     */
    public function getDataExportCorpCateGroupApp($params);
}
