<?php

namespace App\Repositories;

interface DemandCorrespondsRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function save($data);
}
