<?php

namespace App\Repositories;

interface DemandInfoRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * Search demand info list
     *
     * @param  array $data
     * @return array object
     */
    public function searchDemandInfoList($data = null);

    /**
     * Counting demand_info by genres
     *
     * @param $genreId
     * @param null $auctioned
     * @param null $year
     * @param null $month
     * @param array $systemType
     * @return mixed
     */
    public function getDemandbyGenreId($genreId, $auctioned = null, $year = null, $month = null, $systemType = []);

    /**
     * @param $data
     * @return mixed
     */
    public function getQueryDemandInfo($data);

    /**
     * @param $data
     * @return mixed
     */
    public function getDemandInfo($data);

    /**
     * @param $demandId
     * @return mixed
     */
    public function getDemandById($demandId);

    /**
     * @param null $sort
     * @param null $direction
     * @return mixed
     */
    public function getDemandForReport($sort = null, $direction = null);

    /**
     * @param $subQueryForHearNum
     * @return mixed
     */
    public function joinQueryGetReportCorpCommission($subQueryForHearNum);

    /**
     * @return mixed
     */
    public function joinQueryGetReportCorpSelection();

    /**
     * @return mixed
     */
    public function getAllFields();

    /**
     * @param $subQueryForDemandStatus
     * @return mixed
     */
    public function findReportDemandStatus($subQueryForDemandStatus);

    /**
     * @param $subQueryForHearNum
     * @return mixed
     */
    public function getRealTimeReportHearLossNum1($subQueryForHearNum);

    /**
     * @param $subQueryForHearNum
     * @return mixed
     */
    public function getRealTimeReportHearLossNum2($subQueryForHearNum);

    /**
     * @param \App\Models\Base $data
     * @return \App\Models\Base
     */
    public function save($data);

    /**
     * @param $id
     * @return mixed
     */
    public function softDelete($id);

    /**
     * @param $demandId
     * @return mixed
     */
    public function getLimitoverTime($demandId);

    /**
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updateDemandData($id, $data);

    /**
     * @param $field
     * @param $sortData
     * @return mixed
     */
    public function getDataUnSentList($field, $sortData);

    /**
     * @param $id
     * @return mixed
     */
    public function deleteByDemandId($id);

    /**
     * @param $id
     * @return mixed
     */
    public function findById($id);

    /**
     * @param $setting
     * @return mixed
     */
    public function getDemandInfos($setting);

    /**
     * @param $data
     * @return mixed
     */
    public function updateExecuteFollowDate($data);

    /**
     * Check deadline past auction base on sub query
     *
     * @param $subQuery
     * @return mixed
     */
    public function commandCheckDeadlinePastAuction($subQuery);

    /**
     * Get list auction auto call.
     *
     * @param  $autoCallFlg
     * @return mixed
     */
    public function getAutoCallList($autoCallFlg);

    /**
     * Get demand info
     * @param $customerTel
     * @return mixed
     */
    public function getFirstDemandByTel($customerTel);

    /**
     * @param array $orders
     * @return mixed
     */
    public function getJbrCommissionReport($orders = []);

    /**
     * @param array $orders
     * @return mixed
     */
    public function totalRecordCommissionReport($orders = []);

    /**
     * get jbr ongoing
     *
     * @param  array $data
     * @return object
     */
    public function getJbrOngoing($data);

    /*
     * @param integer $demandInfoId
     * @return \Illuminate\Database\Eloquent\Model|null|object|static
     */
    public function getMailData($demandInfoId);
}
