<?php

namespace App\Repositories;

interface DemandInquiryAnsRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $demandId
     * @return mixed
     */
    public function getDemandInquiryWithMInquiryByDemand($demandId);

    /**
     * @param $id
     * @return mixed
     */
    public function findById($id);

    /**
     * @param $demandId
     * @param $inquiryId
     * @return mixed
     */
    public function getDemandAnswerByDemandIdAndInquiryId($demandId, $inquiryId);
}
