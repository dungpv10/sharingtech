<?php

namespace App\Repositories;

interface DeviceInfoRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $deviceToken
     * @return mixed
     */
    public function findByDeviceToken($deviceToken);

    /**
     * @param \App\Models\Base $data
     * @return \App\Models\Base
     */
    public function save($data);

    /**
     * @param $userId
     * @return mixed
     */
    public function getDeviceInfoByUserId($userId);
}
