<?php

namespace App\Repositories\Eloquent;

use App\Models\AuctionGenre;
use App\Repositories\AuctionGenreRepositoryInterface;
use DB;

class AuctionGenreRepository extends SingleKeyModelRepository implements AuctionGenreRepositoryInterface
{
    /**
     * @var AuctionGenre
     */
    protected $model;

    /**
     * AuctionGenreRepository constructor.
     *
     * @param AuctionGenre $model
     */
    public function __construct(AuctionGenre $model)
    {
        $this->model = $model;
    }

    /**
     * get data by id
     *
     * @param  integer $genreId
     * @return array
     */
    public function getFirstByGenreId($genreId = null)
    {
        return $this->model->where('genre_id', '=', $genreId)->first();
    }

    /**
     * save auction genre
     *
     * @param  array $data
     * @return boolean
     */
    public function saveAuctionGenre($data)
    {
        try {
            DB::beginTransaction();
            DB::table('auction_genres')->where('id', '=', $data['id'])->update($data);
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            return false;
        }
    }
}
