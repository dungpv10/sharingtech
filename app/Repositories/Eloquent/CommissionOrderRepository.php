<?php

namespace App\Repositories\Eloquent;

use App\Models\CommissionOrder;
use App\Repositories\CommissionOrderRepositoryInterface;

class CommissionOrderRepository extends SingleKeyModelRepository implements CommissionOrderRepositoryInterface
{
    /**
     * @var CommissionOrder
     */
    protected $model;

    /**
     * CommissionOrderRepository constructor.
     *
     * @param CommissionOrder $model
     */
    public function __construct(CommissionOrder $model)
    {
        $this->model = $model;
    }

    /**
     * @return \App\Models\Base|CommissionOrder|\Illuminate\Database\Eloquent\Model
     */
    public function getBlankModel()
    {
        return new CommissionOrder();
    }
}
