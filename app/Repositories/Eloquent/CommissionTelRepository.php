<?php

namespace App\Repositories\Eloquent;

use App\Models\CommissionTel;
use App\Repositories\CommissionTelRepositoryInterface;

class CommissionTelRepository extends SingleKeyModelRepository implements CommissionTelRepositoryInterface
{
    /**
     * @var CommissionTel
     */
    protected $model;

    /**
     * CommissionTelRepository constructor.
     *
     * @param CommissionTel $model
     */
    public function __construct(CommissionTel $model)
    {
        $this->model = $model;
    }

    /**
     * @return \App\Models\Base|CommissionTel|\Illuminate\Database\Eloquent\Model
     */
    public function getBlankModel()
    {
        return new CommissionTel();
    }
}
