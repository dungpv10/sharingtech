<?php

namespace App\Repositories\Eloquent;

use App\Models\CommissionVisit;
use App\Repositories\CommissionVisitRepositoryInterface;

class CommissionVisitRepository extends SingleKeyModelRepository implements CommissionVisitRepositoryInterface
{
    /**
     * @var CommissionVisit
     */
    protected $model;

    /**
     * CommissionVisitRepository constructor.
     *
     * @param CommissionVisit $model
     */
    public function __construct(CommissionVisit $model)
    {
        $this->model = $model;
    }

    /**
     * @return \App\Models\Base|CommissionVisit|\Illuminate\Database\Eloquent\Model
     */
    public function getBlankModel()
    {
        return new CommissionVisit();
    }
}
