<?php

namespace App\Repositories\Eloquent;

use App\Models\CorpAgreementTempLink;
use App\Repositories\CorpAgreementTempLinkRepositoryInterface;

class CorpAgreementTempLinkRepository extends SingleKeyModelRepository implements CorpAgreementTempLinkRepositoryInterface
{
    /**
     * @var CorpAgreementTempLink
     */
    protected $model;

    /**
     * CorpAgreementTempLinkRepository constructor.
     *
     * @param CorpAgreementTempLink $model
     */
    public function __construct(CorpAgreementTempLink $model)
    {
        $this->model = $model;
    }

    /**
     * @return \App\Models\Base|CorpAgreementTempLink|\Illuminate\Database\Eloquent\Model
     */
    public function getBlankModel()
    {
        return new CorpAgreementTempLink();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
        ];
    }

    /**
     * get by corp id and corp agreement id
     *
     * @param  integer $corpId
     * @param  integer $corpAgreementId
     * @return object
     */
    public function getItemByCorpIdAndCorpAgreementId($corpId, $corpAgreementId)
    {
        return $this->model->select(['id'])
            ->where('corp_id', $corpId)
            ->where('corp_agreement_id', $corpAgreementId)
            ->orderBy('id', 'desc')
            ->first();
    }

    /**
     * @param $corpId
     * @return mixed fist_mcorp
     */
    public function getTempLink($corpId)
    {
        $result = $this->model->leftJoin(
            'corp_agreement',
            'corp_agreement_temp_link.corp_agreement_id',
            '=',
            'corp_agreement.id'
        )
            ->where('corp_agreement_temp_link.corp_id', $corpId)
            ->orderBy('corp_agreement_temp_link.id', 'desc')
            ->select('corp_agreement_temp_link.*', 'corp_agreement.status')
            ->first();
        return $result;
    }

    /**
     * @param $corpId
     * @param $corpAgreementId
     * @return mixed
     */
    public function insertAgreementTempLink($corpId, $corpAgreementId)
    {
        $id = $this->model->insertGetId(
            [
            'corp_id' => $corpId,
            'corp_agreement_id' => $corpAgreementId
            ]
        );
        return $this->model->where('id', $id)->first();
    }

    /**
     * @param $corpId
     * @param $tempId
     * @return mixed
     */
    public function getFirstByCorpId($corpId, $tempId)
    {
        return $this->model->where(
            [
            ['corp_id', $corpId],
            ['id', '!=', $tempId]
            ]
        )
            ->orderBy('id', 'desc')
            ->select('id')
            ->first();
    }

    /**
     * @param array $data
     * @return array
     */
    public function updateByTempLink($data)
    {
        unset($data['status']);
        return $this->model->where('id', $data['id'])->update($data);
    }

    /**
     * @param $corpId
     * @param $corpAgreementId
     * @return mixed
     */
    public function getByCorpIdAndCorpAgreementId($corpId, $corpAgreementId)
    {
        return $this->model->where(
            [
            ['corp_id', $corpId],
            ['corp_agreement_id', $corpAgreementId]
            ]
        )
            ->orderBy('id', 'desc')->first();
    }

    /**
     * @param $corpId
     * @param $corpAgreementId
     * @return mixed
     */
    public function getFirstByCorpIdAndCorpAgreementId($corpId, $corpAgreementId)
    {
        return $this->model->where(
            [
            ['corp_id', $corpId],
            ['corp_agreement_id', $corpAgreementId]
            ]
        )
            ->orderBy('id', 'desc')->first();
    }

    /**
     * @param $corpId
     * @return \Illuminate\Support\Collection
     */
    public function getByCorpIdWith2Record($corpId)
    {
        return $this->model->where(
            [
            ['corp_id', $corpId],
            ]
        )->orderBy('id', 'desc')->limit(2)->get();
    }

    /**
     * @param $corpId
     * @return \Illuminate\Database\Eloquent\Model|null|object|static
     */
    public function getFirstIdByCorpId($corpId)
    {
        $query = $this->model->where('corp_id', '=', $corpId)
            ->orderBy('id', 'desc')
            ->first();
        return $query;
    }

    /**
     * @param $corpId
     * @param $limit
     * @return array
     */
    public function getItemByCorpIdAndLimit($corpId, $limit)
    {
        $query = $this->model->where('corp_id', '=', $corpId)
            ->orderBy('id', 'desc')
            ->limit($limit)->get()->toArray();
        return $query;
    }

    /**
     * @param $corpId
     * @return mixed
     */
    public function findLatestByCorpId($corpId)
    {
        return $this->model->where(
            [
            ['corp_id', $corpId],
            ]
        )
            ->orderBy('id', 'desc')
            ->first();
    }

    /**
     * @param $corpId
     * @param $corpAgreementId
     * @return int
     */
    public function insertAndGetIdBack($corpId, $corpAgreementId)
    {
        $id = $this->model->insertGetId(
            [
            'corp_id' => $corpId,
            'corp_agreement_id' => $corpAgreementId
            ]
        );
        return $id;
    }

    /**
     * @param $cropId
     * @return \Illuminate\Database\Eloquent\Model|null|object|static
     */
    public function getByCropIdWithRelation($cropId)
    {
        return $this->model->with(["corpAgreement", "mCorpCategoriesTemps"])
            ->where("corp_id", $cropId)->orderBy("id", "desc")->first();
    }
}
