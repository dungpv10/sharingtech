<?php

namespace App\Repositories\Eloquent;

use App\Models\GeneralSearchItem;
use App\Repositories\GeneralSearchItemRepositoryInterface;

class GeneralSearchItemRepository extends SingleKeyModelRepository implements GeneralSearchItemRepositoryInterface
{
    /**
     * @var GeneralSearchItem
     */
    protected $model;

    /**
     * GeneralSearchItemRepository constructor.
     *
     * @param GeneralSearchItem $model
     */
    public function __construct(GeneralSearchItem $model)
    {
        $this->model = $model;
    }

    /**
     * @param int $mGeneralId
     * @return bool|void
     * @throws \Exception
     */
    public function deleteById($mGeneralId)
    {
        $this->model->where('general_search_id', '=', $mGeneralId)->delete();
    }

    /**
     * @param $mGeneralId
     * @return array|mixed
     */
    public function findGeneralSearchCondition($mGeneralId)
    {
        return $this->model->where('general_search_id', '=', $mGeneralId)->get()->toarray();
    }

    /**
     * @param $datas
     * @return mixed|void
     */
    public function insertGeneralSearch($datas)
    {
        foreach ($datas as $data) {
            $this->model->insert($data);
        }
    }
}
