<?php

namespace App\Repositories;

interface ExclusionTimeRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * get list data ExclusionTime
     *
     * @return array
     */
    public function getList();

    /**
     * @param null $pattern
     * @return mixed
     */
    public function findByPattern($pattern = null);

    /**
     * @return mixed
     */
    public function getExclusionTime();

    /**
     * update exclusion_times table function
     *
     * @param  $id
     * @param  $data
     * @return mixed
     */
    public function updateExclusion($id, $data);

    /**
     * @param $genreId
     * @param $prefectureCd
     * @return mixed
     */
    public function getData($genreId, $prefectureCd);
}
