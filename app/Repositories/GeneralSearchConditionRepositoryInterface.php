<?php

namespace App\Repositories;

interface GeneralSearchConditionRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $mGeneralId
     * @return bool
     */
    public function deleteById($mGeneralId);

    /**
     * @param $mGeneralId
     * @return mixed
     */
    public function findGeneralSearchCondition($mGeneralId);

    /**
     * @param $data
     * @return mixed
     */
    public function insertGeneralSearch($data);
}
