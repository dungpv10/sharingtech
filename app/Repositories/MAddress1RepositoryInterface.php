<?php


namespace App\Repositories;

interface MAddress1RepositoryInterface
{
    /**
     * @param $address1
     * @return mixed
     */
    public function findByAddressName($address1);

    /**
     * @param $corpId
     * @return mixed
     */
    public function findByCorpIdAndPrefecturalCode($corpId);

    /**
     * @param $corpId
     * @return mixed
     */
    public function findAreaDataSetByCorpIdAndPrefecturalCode($corpId);

    /**
     * @return mixed
     */
    public function getList();
}
