<?php

namespace App\Repositories;

interface MCategoryCopyRuleRepositoryInterface
{
    /**
     * @param $orgCategoryId
     * @return mixed
     */
    public function findAllByOrgCategoryId($orgCategoryId);

    /**
     * @param $listOriginCateId
     * @return mixed
     */
    public function findAllByListOriginCategoryId($listOriginCateId);

    /**
     * @param $listOriginCateId
     * @return mixed
     */
    public function findCorpCateByOrgCateId($listOriginCateId);
}
