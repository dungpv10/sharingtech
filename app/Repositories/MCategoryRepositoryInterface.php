<?php

namespace App\Repositories;

interface MCategoryRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $data
     * @return \App\Models\Base
     */
    public function save($data);

    /**
     * @param $corpId
     * @return mixed
     */
    public function getFirstByCorpId($corpId);

    /**
     * @param $id
     * @param $genreId
     * @return mixed
     */
    public function getCount($id, $genreId);

    /**
     * @param null $genreId
     * @param bool $isAllCategory
     * @return mixed
     */
    public function getList($genreId = null, $isAllCategory = false);

    /**
     * @param null $genreId
     * @param bool $isAllCategory
     * @return mixed
     */
    public function getDropListCategory($genreId = null, $isAllCategory = false);

    /**
     * @param $id
     * @return mixed
     */
    public function getListText($id);

    /**
     * @param $id
     * @return mixed
     */
    public function getDefaultFee($id);

    /**
     * @param $id
     * @return mixed
     */
    public function getCommissionType($id);

    /**
     * Acquisition of Contract STOP category
     *
     * @param  $corpId
     * @return \Illuminate\Support\Collection
     */
    public function getStopCategoryList($corpId);

    /**
     * @param $categoryId
     * @return mixed
     */
    public function getFeeData($id);

    /**
     * @param $corpId
     * @param $tempId
     * @return mixed
     */
    public function getAllTempCategory($corpId, $tempId);

    /**
     * @param $corpId
     * @return mixed
     */
    public function getAllCategoryByCorpId($corpId);

    /**
     * @return mixed
     */
    public function findAllForAffiliation();

    /**
     * @param $listCopyCategoriesId
     * @return mixed
     */
    public function findAllByCopyCategoryIds($listCopyCategoriesId);

    /**
     * @param $categoryId
     * @return mixed
     */
    public function getFeeDataCategories($categoryId);

    /**
     * @param null $genreId
     * @param bool $isAllCategory
     * @return mixed
     */
    public function getListStHide($genreId = null, $isAllCategory = false);
}
