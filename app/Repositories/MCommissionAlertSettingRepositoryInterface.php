<?php

namespace App\Repositories;

interface MCommissionAlertSettingRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $phaseId
     * @param null    $correspondStatus
     * @return mixed
     */
    public function findByPhaseId($phaseId, $correspondStatus = null);
}
