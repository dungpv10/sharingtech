<?php

namespace App\Repositories;

interface MCorpCategoriesTempRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $data
     * @return \App\Models\Base
     */
    public function save($data);

    /**
     * @param $corpId
     * @param $tempId
     * @return mixed
     */
    public function countByCorpIdAndTempId($corpId, $tempId);

    /**
     * @param $tempId
     * @return mixed
     */
    public function countByTempId($tempId);

    /**
     * @param null $id
     * @param null $tempId
     * @return mixed
     */
    public function getMCorpCategoryGenreList($id = null, $tempId = null);

    /**
     * @param $corpId
     * @param null   $tempId
     * @param null   $latestTempLink
     * @param null   $mCorpCategoryRepo
     * @param bool   $getAll
     * @return mixed
     */
    public function findCategoryTempCopy(
        $corpId,
        $tempId = null,
        $latestTempLink = null,
        $mCorpCategoryRepo = null,
        $getAll = false
    );

    /**
     * @param $id
     * @param $tempId
     * @return mixed
     */
    public function getTempData($id, $tempId);

    /**
     * @param $saveData
     * @return mixed
     */
    public function saveAll($saveData);

    /**
     * get by corp id and temp id
     *
     * @param  integer $corpId
     * @param  integer $tempId
     * @return array object
     */
    public function getByCorpIdAndTempId($corpId, $tempId);

    /**
     * @param $corpId
     * @param $tempId
     * @param $deleteFlag
     * @param $disableFlgOfCategory
     * @return mixed
     */
    public function findAllByCorpIdAndTempIdWithFlag(
        $corpId,
        $tempId,
        $deleteFlag,
        $disableFlgOfCategory
    );

    /**
     * @return mixed
     */
    public function getCorpAgreementCategory();

    /**
     * @return mixed
     */
    public function getCsvCorpAgreementCategory();

    /**
     * @param $corpId
     * @param $categoryId
     * @param $tempId
     * @param $deleteFlag
     * @return mixed
     */
    public function findAllByCorpIdAndCateIdAndTempIdAndDelFlag($corpId, $categoryId, $tempId, $deleteFlag);

    /**
     * @param $idTemp
     * @return mixed
     */
    public function getCountByTempId($idTemp);

    /**
     * @param $data
     * @return mixed
     */
    public function saveWithData($data);

    /**
     * @param $categoryId
     * @param $tempId
     * @return mixed
     */
    public function findAllByCategoryIdAndTempId($categoryId, $tempId);

    /**
     * @param $idCate
     * @return mixed
     */
    public function getListCategoryIdById($idCate);

    /**
     * @param $listCopyCate
     * @param $idCorp
     * @param $idTemp
     * @return mixed
     */
    public function getListIdBy($listCopyCate, $idCorp, $idTemp);

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * @param $idCorp
     * @param $idGenre
     * @param $idCategory
     * @param $idTemp
     * @return mixed
     */
    public function getIdFirstBy($idCorp, $idGenre, $idCategory, $idTemp);

    /**
     * @param $corpId
     * @param $categoryId
     * @param $tempId
     * @param $deleteFlag
     * @return mixed
     */
    public function getFirstByCorpIdAndCateIdAndTempIdAndDelFlag($corpId, $categoryId, $tempId, $deleteFlag);

    /**
     * @param $id
     * @param $type
     * @return mixed
     */
    public function updateTargetAreaType($id, $type);
}
