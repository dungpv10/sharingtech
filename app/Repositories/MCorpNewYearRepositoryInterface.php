<?php

namespace App\Repositories;

interface MCorpNewYearRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @return mixed
     */
    public function deleteAll();

    /**
     * @param $corpId
     * @param $data
     * @return mixed
     */
    public function updateNewYear($corpId, $data);

    /**
     * @param $mCorpId
     * @param array   $fields
     * @return mixed
     */
    public function getItemByMCorpId($mCorpId, $fields = ['*']);
}
