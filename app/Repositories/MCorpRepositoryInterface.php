<?php

namespace App\Repositories;

use Illuminate\Support\Collection;

interface MCorpRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $data
     * @return \App\Models\Base
     */
    public function save($data);

    /**
     * @param $customerTel
     * @return mixed
     */
    public function searchAffiliationInfoAll($customerTel);

    /**
     * @param $id
     * @param $toArray
     * @return mixed
     */
    public function getFirstById($id, $toArray = false);

    /**
     * get m_corps data
     *
     * @param  $data
     * @param  $page
     * @return mixed
     */
    public function searchCorpAndPaging($data, $page);

    /**
     * @param $id
     * @return Collection
     */
    public function findByIdForAffiliation($id);

    /**
     * @param $searchKey
     * @param $conditions
     * @param $limitSearch
     * @param $count
     * @return mixed
     */
    public function searchByCorpIdOrCorpName($searchKey, $conditions, $limitSearch, $count);

    /**
     * @param $data
     * @param $limitSearch
     * @param $count
     * @return mixed
     */
    public function searchCorpAddList($data, $limitSearch, $count);

    /**
     * @param $data
     * @param $isNew
     * @param $count
     * @return mixed
     */
    public function searchCorpForPopup($data, $isNew, $count);

    /**
     * @param $id
     * @return mixed
     */
    public function getDataAffiliationById($id);

    /**
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updateCorp($id, $data);

    /**
     * @param $id
     * @return mixed
     */
    public function deleteSoftById($id);

    /**
     * @param $searchKey
     * @param $searchValue
     * @param $excludeCorpId
     * @return mixed
     */
    public function buildAdvanceSearchByIdOrName($searchKey, $searchValue, $excludeCorpId);

    /**
     * @param $searchKey
     * @param $searchValue
     * @param $excludeCorpId
     * @param int $limit
     * @return mixed
     */
    public function getAdvanceSearchByIdOrName($searchKey, $searchValue, $excludeCorpId, $limit = 50);

    /**
     * @param $searchKey
     * @param $searchValue
     * @param $excludeCorpId
     * @return mixed
     */
    public function getCountAdvanceSearchByIdOrName($searchKey, $searchValue, $excludeCorpId);

    /**
     * @param $categoryIds
     * @param $listPref
     * @param $corpIds
     * @param $type
     * @param $text
     * @return mixed
     */
    public function searchByCategoryPref($categoryIds, $listPref, $corpIds, $type, $text);

    /**
     * @param $categoryIds
     * @param $address1
     * @return mixed
     */
    public function getListByCategoryIdsAndAddress1($categoryIds, $address1);

    /**
     * @param $allCondition
     * @param string       $orderBy
     * @param string       $direction
     * @param $page
     * @param integer      $limit
     * @return mixed
     */
    public function getListCorpByConditionFromAffiliation(
        $allCondition,
        $orderBy = 'id',
        $direction = 'asc',
        $page = 1,
        $limit = 100
    );

    /**
     * @param $allCondition
     * @return mixed
     */
    public function createDataDownloadCsvAffiliation($allCondition);

    /**
     * @param $corpId
     * @return mixed
     */
    public function updateGuidelineCheckDate($corpId);

    /**
     * @param $data
     * @return mixed
     */
    public function updateById($data);

    /**
     * get official_corp_name column
     *
     * @param  $id
     * @return mixed
     */
    public function getOfficialName($id);

    /**
     * get all by id
     * @param $id
     * @return mixed
     */
    public function getAllInformationById($id);

    /**
     * Get crop unattended for report development search
     *
     * @param  $genreId
     * @return mixed
     */
    public function getUnattendedForReportDevByGenreId($genreId);

    /**
     * Get crop advance for report development search
     *
     * @param  $genreId
     * @return mixed
     */
    public function getAdvanceForReportDevByGenreId($genreId);

    /**
     * @param $genreId
     * @param $address
     * @param null    $status
     * @return mixed
     */
    public function getListForDataTableByGenreIdAndAddressAndStatus($genreId, $address, $status = null);

    /**
     * @param null $data search
     * @return array m corp
     */
    public function getListForCommissionSelect($data = null);

    /**
     * @param $affiliationId
     * @return mixed
     */
    public function findByAffiliationId($affiliationId);

    /**
     * @param array $corpIds
     * @return mixed
     */
    public function getHolidayByCorpId(array $corpIds);

    /**
     * @param $data
     * @return mixed
     */
    public function demandCorpData($data);

    /**
     * @param $id
     * @return mixed
     */
    public function isCommissionStop($id);

    /**
     * @param $corpId
     * @return mixed
     */
    public function getCorpData($corpId);

    /**
     * @param $id
     * @param array $columns
     * @param array $order
     * @return mixed
     */
    public function getDataById($id, $columns = ['*'], $order = ['column' => 'id', 'dir' => 'desc']);

    /**
     * @param $corpName
     * @return mixed
     */
    public function findByName($corpName);

    /**
     * @param $corpId
     * @return mixed
     */
    public function getHolidays($corpId);

    /**
     * @param $corpId
     * @return mixed
     */
    public function getHolidayListByCorpId($corpId);

    /**
     * @param $corpId
     * @param null   $categoryId
     * @return mixed
     */
    public function getByCorpIdAndCategoryId($corpId, $categoryId = null);

    /**
     * @param $num
     * @param null $categoryId
     * @param null $corpId
     * @return mixed
     */
    public function getCommissionChangeByCategoryIdAndCorpId($num, $categoryId = null, $corpId = null);

    /**
     * Get data to check deadline in command
     * @param $jisCd
     * @param $data
     * @return mixed
     */
    public function getDataCheckDeadlineCommand($jisCd, $data);

    /**
     * Get m_corp contactable time
     *
     * @param  $corpId
     * @return mixed
     */
    public function getContactableTime($corpId);
}
