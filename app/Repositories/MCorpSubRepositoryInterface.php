<?php

namespace App\Repositories;

/**
 * Interface MCorpSubRepositoryInterface
 *
 * @package App\Repositories
 */
interface MCorpSubRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $corpId
     * @return mixed
     */
    public function findByCorpIdForAffiliation($corpId);

    /**
     * Acquisition of company master incidental information
     *
     * @param  $id
     * @return array
     */
    public function getMCorpSubList($id);

    /**
     * @return mixed
     */
    public function holidayQuery();

    /**
     * @param $corpId
     * @return mixed
     */
    public function getMCorpSubData($corpId);

    /**
     * @param $corpId
     * @param $fields
     * @param $orders
     * @return mixed
     */
    public function getItemByMCorpId($corpId, $fields, $orders);
}
