<?php

namespace App\Repositories;

interface MCorpTargetAreaRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * get all by corp id
     *
     * @param  integer $corpId
     * @return array object
     */
    public function getAllByCorpId($corpId);

    /**
     * @param $data
     * @return \App\Models\Base
     */
    public function save($data);

    /**
     * @param $corpId
     * @return mixed
     */
    public function getByCorpId($corpId);

    /**
     * @param null $corpId
     * @return mixed
     */
    public function countByCorpId($corpId = null);

    /**
     * @param null $corpId
     * @return mixed
     */
    public function getLastModifiedByCorpId($corpId = null);

    /**
     * @param $id
     * @return mixed
     */
    public function editTargetAreaToGenre($id);

    /**
     * @param null $id
     * @return mixed
     */
    public function removeByCorpId($id = null);

    /**
     * @param null $id
     * @param null $genreId
     * @return mixed
     */
    public function editTargetAreaToCategory($id = null, $genreId = null);

    /**
     * get list by corp_id and address_code
     *
     * @param $corpId
     * @param $addressCode
     * @return mixed
     */
    public function getListByCorpIdAndAddressCode($corpId, $addressCode);

    /**
     * @param $ids
     * @return mixed
     */
    public function deleteByListId($ids);

    /**
     * @param $corpId
     * @param array $columns
     * @param array $order
     * @return mixed
     */
    public function getLastByMCorp($corpId, $columns = ['*'], $order = ['column' => 'id', 'dir' => 'desc']);

    /**
     * @param $corpId
     * @return mixed
     */
    public function getJscByCorpId($corpId);
}
