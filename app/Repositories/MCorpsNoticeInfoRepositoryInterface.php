<?php

namespace App\Repositories;

interface MCorpsNoticeInfoRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $role
     * @param $noticeId
     * @param $corp
     * @return mixed
     */
    public function markReadNotice($role, $noticeId, $corp);

    /**
     * @param $noticeId
     * @return mixed
     */
    public function findAnswerListByNoticeInfoId($noticeId);

    /**
     * @param $noticeInfo
     * @param $user
     * @param $answerValue
     * @return mixed
     */
    public function answerNotice($noticeInfo, $user, $answerValue);

    /**
     * @param $noticeId
     * @return mixed
     */
    public function isNoticeAnswered($noticeId);

    /**
     * @param $noticeId
     * @return mixed
     */
    public function getListAnswerCSV($noticeId);
}
