<?php

namespace App\Repositories;

interface MGeneralSearchRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $mGeneralId
     * @return mixed
     */
    public function findGeneralSearch($mGeneralId);

    /**
     * @param $whereConditions
     * @param $orwhereConditions
     * @return mixed
     */
    public function findGeneralSearchAuth($whereConditions, $orwhereConditions);

    /**
     * @return mixed
     */
    public function getLastInsertID();

    /**
     * @param $generalId
     * @return mixed
     */
    public function deleteGeneralSearch($generalId);

    /**
     * @param $data
     * @return mixed
     */
    public function insertGeneralSearch($data);

    /**
     * @param $sql
     * @return mixed
     */
    public function runQueryText($sql);

    /**
     * @param $data
     * @return mixed
     */
    public function updateGeneralSearch($data);
}
