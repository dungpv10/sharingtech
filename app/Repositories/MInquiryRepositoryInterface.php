<?php

namespace App\Repositories;

interface MInquiryRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param null $category
     * @return mixed
     */
    public function getListInquiryByCategory($category = null);
}
