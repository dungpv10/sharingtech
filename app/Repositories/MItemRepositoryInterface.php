<?php

namespace App\Repositories;

interface MItemRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $data
     * @return \App\Models\Base
     */
    public function save($data);

    /**
     * @param $category
     * @return mixed
     */
    public function getListByCategoryItem($category);

    /**
     * @param $category
     * @param null     $itemId
     * @return mixed
     */
    public function getFirstOldList($category, $itemId = null);

    /**
     * @param $query
     * @return mixed
     */
    public function scopeGetList($query);

    /**
     * @param $category
     * @param null     $itemId
     * @return mixed
     */
    public function getList($category, $itemId = null);

    /**
     * @param $category
     * @param $value
     * @return mixed
     */
    public function getListText($category, $value);

    /**
     * @param $categories
     * @return mixed
     */
    public function getByCategory($categories);

    /**
     * @param $categoryName
     * @return mixed
     */
    public function getMItemListByItemCategory($categoryName);

    /**
     * @param $categoryName
     * @param $date
     * @return mixed
     */
    public function getMItemList($categoryName, $date);

    /**
     * @return mixed
     */
    public function getByLongHoliday();

    /**
     * @return mixed
     */
    public function deleteByLongHoliday();
}
