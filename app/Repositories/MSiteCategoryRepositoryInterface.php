<?php

namespace App\Repositories;

interface MSiteCategoryRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param null $siteId
     * @return mixed
     */
    public function getCategoriesBySite($siteId = null);
}
