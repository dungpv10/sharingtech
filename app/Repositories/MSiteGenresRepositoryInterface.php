<?php

namespace App\Repositories;

interface MSiteGenresRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $siteId
     * @return mixed
     */
    public function getGenreBySiteStHide($siteId);

    /**
     * @param $siteId
     * @return mixed
     */
    public function getGenreBySite($siteId);

    /**
     * @param $siteId
     * @return mixed
     */
    public function getGenreRankBySiteId($siteId);
}
