<?php

namespace App\Repositories;

interface MSiteRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $siteTel
     * @return mixed
     */
    public function searchMsite($siteTel);

    /**
     * @param $data
     * @return mixed
     */
    public function findMaxLimit($data);

    /**
     * @return mixed
     */
    public function getList();

    /**
     * @param $flg
     * @return mixed
     */
    public function getCrossSiteFlg($flg);

    /**
     * @return mixed
     */
    public function getListMSitesForDropDown();

    /**
     * @param $siteId
     * @return mixed
     */
    public function getCommissionTypeDiv($siteId);

    /**
     * @param $siteName
     * @return mixed
     */
    public function getSiteByName($siteName);

    /**
     * get one site by site's tel
     * @param  int $siteTel site tel
     * @return mixed
     */
    public function getFirstSiteByTel($siteTel);
}
