<?php

namespace App\Repositories;

interface MTargetAreaRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $data
     * @return \App\Models\Base
     */
    public function save($data);

    /**
     * @param int $corpCategoryId
     * @return bool
     */
    public function deleteById($corpCategoryId);

    /**
     * @param $data
     * @return mixed
     */
    public function insertByCorpCategoryId($data);

    /**
     * @param integer|null $id
     * @param integer|null $jisCD
     * @return integer
     */
    public function getCorpCategoryTargetAreaCount($id = null, $jisCD = null);

    /**
     * count corp category target area
     *
     * @param  integer $corpCategoryId
     * @return integer
     */
    public function countCorpCategoryTargetArea($corpCategoryId);

    /**
     * @param null $id
     * @param null $jisCd
     * @return mixed
     */
    public function getCorpCategoryTargetAreaCount2($id = null, $jisCd = null);

    /**
     * @param $corpCategoryId
     * @return mixed
     */
    public function findAllByCorpCategoryId($corpCategoryId);

    /**
     * @param $corpCategoryId
     * @param $jisCd
     * @return mixed
     */
    public function findAllByCorpCategoryIdAndJisCd($corpCategoryId, $jisCd);

    /**
     * @param null $corpId
     * @param null $defaultJisCds
     * @return mixed
     */
    public function countHasJisCdsOfCorpCategory($corpId = null, $defaultJisCds = null);

    /**
     * @param $corpId
     * @return mixed
     */
    public function getTargetAreaLastModified($corpId);

    /**
     * @param $corpId
     * @return mixed
     */
    public function getCorpCategoryTargetAreaCount3($corpId);

    /**
     * @param null $id
     * @param null $jisCd
     * @return mixed
     */
    public function getCorpCategoryTargetAreaByJisCd($id = null, $jisCd = null);

    /**
     * @param $data
     * @return mixed
     */
    public function saveAll($data);

    /**
     * @param $corpCategoryId
     * @return mixed
     */
    public function deleteByCorpCategoryId($corpCategoryId);
}
