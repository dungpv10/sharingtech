<?php

namespace App\Repositories;

interface MTaxRateRepositoryInterface
{
    /**
     * @param $date
     * @return mixed
     */
    public function findByDate($date);
}
