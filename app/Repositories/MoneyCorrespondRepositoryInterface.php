<?php
namespace App\Repositories;

interface MoneyCorrespondRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $id
     * @return mixed
     */
    public function deleteMoneyRecord($id);

    /**
     * @param $corpId
     * @param $nominee
     * @param $orderBy
     * @return mixed
     */
    public function getMoneyCorrespondDataInitial($corpId, $nominee, $orderBy);
}
