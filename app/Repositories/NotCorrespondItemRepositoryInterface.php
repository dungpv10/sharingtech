<?php

namespace App\Repositories;

interface NotCorrespondItemRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $corpId
     * @return mixed
     */
    public function findBy($corpId);

    /**
     * @return mixed
     */
    public function findFirst();
}
