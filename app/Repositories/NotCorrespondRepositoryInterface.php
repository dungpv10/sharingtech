<?php

namespace App\Repositories;

interface NotCorrespondRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function updateById($data);

    /**
     * @param $ids
     * @return mixed
     */
    public function deleteMultiRecord($ids);

    /**
     * @return mixed
     */
    public function getFirstItem();

    /**
     * @param $corpId
     * @return mixed
     */
    public function findNotCorrespond($corpId);
}
