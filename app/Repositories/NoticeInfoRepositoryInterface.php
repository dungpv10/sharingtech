<?php

namespace App\Repositories;

interface NoticeInfoRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $noticeId
     * @param $mCorp
     * @return mixed
     */
    public function getNoticeInfoByAffiliation($noticeId, $mCorp);

    /**
     * @param $noticeId
     * @return mixed
     */
    public function getNoticeInfoByOtherRoles($noticeId);

    /**
     * @param $id
     * @return mixed
     */
    public function findActiveById($id);

    /**
     * @param $corpId
     * @param $corpCommissionType
     * @return mixed
     */
    public function countUnreadNoticeInfoByCorpId($corpId, $corpCommissionType);

    /**
     * @param $corpId
     * @param $corpCreated
     * @param $corpCommissionType
     * @return mixed
     */
    public function countUnansweredByCorpId($corpId, $corpCreated, $corpCommissionType);

    /**
     * @param $corpId
     * @param $corpCommissionType
     * @param $corpCreatedDate
     * @return mixed
     */
    public function countUnreadByCorpIdAndCreatedDate($corpId, $corpCommissionType, $corpCreatedDate);

    /**
     * @param $corpId
     * @return mixed
     */
    public function getNoticeInfoStatusByCorpId($corpId);

    /**
     * save notice info
     * @param  array $fields
     * @param  integer $id
     * @return \Illuminate\Database\Eloquent\Model|mixed|null|object|static
     */
    public function saveNoticeInfo($fields, $id = null);
}
