<?php

namespace App\Repositories;

interface NoticeInfoTargetRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $noticeId
     * @return mixed
     */
    public function findCorpListByNoticeInfoId($noticeId);

    /**
     * @param $user
     * @param $noticeId
     * @param $listOfCorpIds
     * @return mixed
     */
    public function updateCorpListOfNoticeInfo($user, $noticeId, $listOfCorpIds);

    /**
     * @param $noticeId
     * @return mixed
     */
    public function removeCorpListOfNoticeInfo($noticeId);
}
