<?php

namespace App\Repositories;

interface PgDescriptionRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $tableName
     * @param $columns
     * @return mixed
     */
    public function getColumnAlias($tableName, $columns);
}
