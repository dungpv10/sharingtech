<?php

namespace App\Repositories;

interface ProgAddDemandInfoRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $pCorpId
     * @return mixed
     */
    public function getDataByProgCorpId($pCorpId);

    /**
     * @param $progAddId
     * @param $data
     * @return mixed
     */
    public function updateById($progAddId, $data);

    /**
     * @param $idOrFileId
     * @param $field
     * @return mixed
     */
    public function getCSVData($idOrFileId, $field);

    /**
     * @param $ids
     * @return mixed
     */
    public function findByIds($ids);

    /**
     * @param $data
     * @return mixed
     */
    public function insertGetId($data);
}
