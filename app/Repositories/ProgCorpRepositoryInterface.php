<?php

namespace App\Repositories;

interface ProgCorpRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $id
     * @return mixed
     */
    public function findById($id);

    /**
     * @param $fileId
     * @return mixed
     */
    public function getCorpWithHolidayByFileId($fileId);

    /**
     * @param array $corpIds
     * @return mixed
     */
    public function getHolidayByCorpId(array $corpIds);

    /**
     * @param $corpIds
     * @param $importFileId
     * @return mixed
     */
    public function findByMutilCorpIdAndFileId($corpIds, $importFileId);

    /**
     * @param $data
     * @return mixed
     */
    public function insertGetIds($data);

    /**
     * @param $corpId
     * @return mixed
     */
    public function countProgCropForShowDialogBox($corpId);

    /**
     * @param $pCorpId
     * @param $data
     * @return mixed
     */
    public function updateProgressCorp($pCorpId, $data);

    /**
     * @param $pCorpId
     * @return mixed
     */
    public function getDataWithMcorpAndDemandInfoById($pCorpId);

    /**
     * @param $corpId
     * @param $fileId
     * @return mixed
     */
    public function findFirstByCorpIdAndFileId($corpId, $fileId);

    /**
     * @param $corpId
     * @param $fileId
     * @param $progressFlag
     * @return mixed
     */
    public function findProgCorpWithFlag($corpId, $fileId, $progressFlag);
}
