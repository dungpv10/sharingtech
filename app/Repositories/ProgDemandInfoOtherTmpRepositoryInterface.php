<?php

namespace App\Repositories;

interface ProgDemandInfoOtherTmpRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * delete by prog corp id
     *
     * @param  integer $progCorpId
     * @return boolean
     */
    public function deleteByProgCorpId($progCorpId);
}
