<?php

namespace App\Repositories;

interface ProgDemandInfoRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * update data
     *
     * @param  integer $id   record id
     * @param  array   $data data
     * @return boolean
     */
    public function update($id, $data);

    /**
     * @param $id
     * @return mixed
     */
    public function findById($id);

    /**
     * @param $data
     * @return mixed
     */
    public function insertTMP($data);

    /**
     * @param $arrayCommissionId
     * @param $fileId
     * @return mixed
     */
    public function delProgDemand($arrayCommissionId, $fileId);

    /**
     * @param $commissionInfo
     * @param $fileId
     * @return mixed
     */
    public function findByMulticondition($commissionInfo, $fileId);

    /**
     * @param $idOrFileId
     * @param $field
     * @return mixed
     */
    public function getCSVData($idOrFileId, $field);

    /**
     * @param $progDemandId
     * @return mixed
     */
    public function findWithCommissionById($progDemandId);

    /**
     * @param $ids
     * @return mixed
     */
    public function findByIds($ids);

    /**
     * @param $progCorpId
     * @return mixed
     */
    public function findByProgCorpId($progCorpId);
}
