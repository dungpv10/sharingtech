<?php

namespace App\Repositories;

interface ProgImportFilesRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @author thaihv
     * check exist row in database
     * @param  integer $id id of prog import file
     * @return boolean
     */
    public function findById($id);

    /**
     * @return mixed
     */
    public function getImportFileReleased();

    /**
     * @param $paginate
     * @return mixed
     */
    public function getImportFileNotDelete($paginate);

    /**
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updateDelete($id, $data);

    /**
     * @param $id
     * @return mixed
     */
    public function findNotDeleteById($id);
}
