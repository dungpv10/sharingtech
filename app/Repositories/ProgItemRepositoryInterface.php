<?php

namespace App\Repositories;

interface ProgItemRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $id
     * @return mixed
     */
    public function findById($id);
}
