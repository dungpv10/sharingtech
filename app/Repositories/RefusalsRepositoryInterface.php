<?php

namespace App\Repositories;

interface RefusalsRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $auctionId
     * @param $dataUpdate
     * @return mixed
     */
    public function updateData($auctionId, $dataUpdate);
}
