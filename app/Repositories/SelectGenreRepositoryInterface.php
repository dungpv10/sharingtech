<?php

namespace App\Repositories;

interface SelectGenreRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $genreId
     * @return mixed
     */
    public function findByGenreId($genreId);
}
