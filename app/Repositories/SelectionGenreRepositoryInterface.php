<?php

namespace App\Repositories;

interface SelectionGenreRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * Update or save selection genre
     *
     * @param  $data
     * @param  null $id
     * @return mixed
     */
    public function updateOrSave($data, $id = null);

    /**
     * @param $id
     * @param null $field
     * @return mixed
     */
    public function findBaseOnGenreId($id, $field = null);
}
