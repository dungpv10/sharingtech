<?php

namespace App\Repositories;

interface VisitTimeRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * @param $data
     * @return \App\Models\Base
     */
    public function save($data);

    /**
     * @param $demandId
     * @return mixed
     */
    public function findAllByDemandId($demandId);

    /**
     * @param $id
     * @return mixed
     */
    public function findById($id);

    /**
     * @param $data
     * @return mixed
     */
    public function saveMany($data);

    /**
     * @param $data
     * @return mixed
     */
    public function multipleUpdate($data);

    /**
     * @param $ids
     * @return mixed
     */
    public function multipleDelete($ids);

    /**
     * @param $demandId
     * @return mixed
     */
    public function findListByDemandId($demandId);

    /**
     * @param $demandId
     * @return mixed
     */
    public function findAllWithAuctionInfo($demandId);
}
