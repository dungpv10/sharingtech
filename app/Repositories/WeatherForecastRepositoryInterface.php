<?php

namespace App\Repositories;

/**
 * Interface WeatherForecastRepositoryInterface
 *
 * @package App\Repositories
 */
interface WeatherForecastRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * Get total data by date range
     *
     * @param  $fromDate
     * @param  $toDate
     * @return mixed
     */
    public function countByDateRange($fromDate, $toDate);
}
