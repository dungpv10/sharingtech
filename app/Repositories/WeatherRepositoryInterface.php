<?php

namespace App\Repositories;

/**
 * Interface WeatherRepositoryInterface
 *
 * @package App\Repositories
 */
interface WeatherRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    /**
     * Get total data by date
     *
     * @param  $date
     * @return mixed
     */
    public function countByDate($date);
}
