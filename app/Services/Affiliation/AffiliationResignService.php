<?php

namespace App\Services\Affiliation;

use App\Repositories\ApprovalRepositoryInterface;
use App\Repositories\CorpAgreementTempLinkRepositoryInterface;
use App\Repositories\CorpCategoryApplicationRepositoryInterface;
use App\Repositories\CorpCategoryGroupApplicationRepositoryInterface;
use App\Repositories\MCorpCategoriesTempRepositoryInterface;
use App\Repositories\MCorpCategoryRepositoryInterface;
use App\Repositories\MCorpRepositoryInterface;
use App\Repositories\MCorpTargetAreaRepositoryInterface;
use App\Repositories\MTargetAreaRepositoryInterface;
use App\Services\BaseService;
use DB;
use Illuminate\Http\Request;

class AffiliationResignService extends BaseService
{
    /**
     * @var MCorpRepositoryInterface
     */
    protected $mCorps;
    /**
     * @var MCorpCategoryRepositoryInterface
     */
    protected $mCorpCategories;
    /**
     * @var MCorpCategoriesTempRepositoryInterface
     */
    protected $mCorpCategoriesTemp;
    /**
     * @var MCorpTargetAreaRepositoryInterface
     */
    protected $mCorpTargetArea;
    /**
     * @var MTargetAreaRepositoryInterface
     */
    protected $mTargetArea;
    /**
     * @var CorpCategoryGroupApplicationRepositoryInterface
     */
    protected $corpCategoryGroupApp;
    /**
     * @var CorpCategoryApplicationRepositoryInterface
     */
    protected $corpCategoryApp;
    /**
     * @var CorpAgreementTempLinkRepositoryInterface
     */
    protected $corpAgreementTempLink;
    /**
     * @var ApprovalRepositoryInterface
     */
    protected $approval;
    /**
     * @var AffiliationGenreResigningService
     */
    protected $affGenreResignService;

    /**
     * AffiliationResignService constructor.
     *
     * @param MCorpRepositoryInterface                        $mCorpRepositoryInterface
     * @param MCorpCategoryRepositoryInterface                $mCorpCategoryRepositoryInterface
     * @param MCorpCategoriesTempRepositoryInterface          $mCorpCategoriesTempInterface
     * @param CorpAgreementTempLinkRepositoryInterface        $corpAgreementTempLinkInterface
     * @param AffiliationGenreResigningService                $affiliationGenreResigningService
     * @param MCorpTargetAreaRepositoryInterface              $mCorpTargetAreaInterface
     * @param MTargetAreaRepositoryInterface                  $mTargetAreaRepositoryInterface
     * @param CorpCategoryGroupApplicationRepositoryInterface $corpCategoryGroupAppInterface
     * @param CorpCategoryApplicationRepositoryInterface      $corpCategoryAppInterface
     * @param ApprovalRepositoryInterface                     $approvalInterface
     */
    public function __construct(
        MCorpRepositoryInterface $mCorpRepositoryInterface,
        MCorpCategoryRepositoryInterface $mCorpCategoryRepositoryInterface,
        MCorpCategoriesTempRepositoryInterface $mCorpCategoriesTempInterface,
        CorpAgreementTempLinkRepositoryInterface $corpAgreementTempLinkInterface,
        AffiliationGenreResigningService $affiliationGenreResigningService,
        MCorpTargetAreaRepositoryInterface $mCorpTargetAreaInterface,
        MTargetAreaRepositoryInterface $mTargetAreaRepositoryInterface,
        CorpCategoryGroupApplicationRepositoryInterface $corpCategoryGroupAppInterface,
        CorpCategoryApplicationRepositoryInterface $corpCategoryAppInterface,
        ApprovalRepositoryInterface $approvalInterface
    ) {
        $this->mCorps = $mCorpRepositoryInterface;
        $this->mCorpCategories = $mCorpCategoryRepositoryInterface;
        $this->mCorpCategoriesTemp = $mCorpCategoriesTempInterface;
        $this->corpAgreementTempLink = $corpAgreementTempLinkInterface;
        $this->affGenreResignService = $affiliationGenreResigningService;
        $this->mCorpTargetArea = $mCorpTargetAreaInterface;
        $this->mTargetArea = $mTargetAreaRepositoryInterface;
        $this->corpCategoryGroupApp = $corpCategoryGroupAppInterface;
        $this->corpCategoryApp = $corpCategoryAppInterface;
        $this->approval = $approvalInterface;
    }

    /**
     * data for render view
     *
     * @param  $idCorp
     * @return mixed
     */
    public function prepareDataForView($idCorp)
    {
        $tempId = null;
        $resultTempLink = $this->corpAgreementTempLink->getFirstIdByCorpId($idCorp);
        if (!empty($resultTempLink)) {
            $tempId = $resultTempLink['id'];
        }
        $data['bAllowShowReconfirm'] = $this->affGenreResignService->getStateCanShowReconfirm($idCorp);
        $corpData = $this->mCorps->getAllInformationById($idCorp);
        $data['infoCorp'] = $corpData;
        $data['tempId'] = $tempId;
        $cateList = $this->getGenreList($idCorp, $tempId);
        $listBasicCateA = $this->separateListArea($cateList['basic'], 1);
        $listIndividualCateA = $this->separateListArea($cateList['individual'], 1);
        $listBasicCateB = $this->separateListArea($cateList['basic'], 2);
        $listIndividualCateB = $this->separateListArea($cateList['individual'], 2);
        $data['listCateA'] = array(
            'basic' => $listBasicCateA,
            'individual' => $listIndividualCateA
        );
        $data['listCateB'] = array(
            'basic' => $listBasicCateB,
            'individual' => $listIndividualCateB
        );
        $data['lastModified'] = $this->getLastModified($listBasicCateA);
        $data['listFeeUnit'] = ['' => __('affiliation_resign.none')] + __('affiliation_resign.fee_div');
        $data['listCorpCommisionType'] = __('affiliation_resign.list_corp_commission_type');
        return $data;
    }

    /**
     * get list genre to show on view
     *
     * @param  $idCorp
     * @param  $idTemp
     * @return array
     */
    private function getGenreList($idCorp, $idTemp)
    {
        $corpTargetArea = $this->mCorpTargetArea->getByCorpId($idCorp);
        $corpTargetAreaCount = count($corpTargetArea);
        $listCategoryGenre = $this->affGenreResignService->findCategoryTempCopy($idCorp, $idTemp)->toArray();
        $registerData = array_filter(
            $listCategoryGenre,
            function ($arr) {
                if (!empty($arr['id'])) {
                    return true;
                }
                return false;
            }
        );
        $listCustomArea = [];
        $listNormalArea = [];
        foreach ($listCategoryGenre as $obj) {
            $customFlg = false;
            $mstedtFlg = false;
            if ($obj['target_area_type'] == 0) {
                $mstedtFlg = true;
                $corpCate = $this->mCorpCategories->findLastIdByCorpIdAndCategoryId($idCorp, $obj['category_id']);
                if (!empty($corpCate)) {
                    $targetAreaCount = $this->mTargetArea->getCorpCategoryTargetAreaCount($corpCate->id);
                    if ($targetAreaCount != $corpTargetAreaCount) {
                        $customFlg = true;
                    }
                    if (!empty($corpTargetArea)) {
                        foreach ($corpTargetArea as $areaObj) {
                            $areaCount = $this->mTargetArea->getCorpCategoryTargetAreaCount(
                                $corpCate->id,
                                $areaObj['jis_cd']
                            );
                            if ($areaCount <= 0) {
                                $customFlg = true;
                                break;
                            }
                        }
                    }
                }
            } else {
                if ($obj['target_area_type'] == 2) {
                    $customFlg = true;
                }
            }
            if ($customFlg == true) {
                $listCustomArea[] = $obj;
                if ($mstedtFlg && !empty($registerData)) {
                    $this->mCorpCategoriesTemp->updateTargetAreaType($obj['id'], 2);
                }
            } else {
                $listNormalArea[] = $obj;
                if ($mstedtFlg && !empty($registerData)) {
                    $this->mCorpCategoriesTemp->updateTargetAreaType($obj['id'], 1);
                }
            }
        }
        $data = array(
            'basic' => $listNormalArea,
            'individual' => $listCustomArea
        );
        return $data;
    }

    /**
     * filter list by corp_commission_type and disable_flg = false
     *
     * @param  $listArea
     * @param  $type
     * @return array
     */
    private function separateListArea($listArea, $type)
    {
        $listResult = array_filter(
            $listArea,
            function ($arr) use ($type) {
                if ($arr['corp_commission_type'] == $type && $arr['disable_flg'] == false) {
                    return true;
                }
                return false;
            }
        );
        return $listResult;
    }

    /**
     * get last modified
     *
     * @param  $listCategories
     * @return string
     */
    private function getLastModified($listCategories)
    {
        $lastModified = '';
        foreach ($listCategories as $arr) {
            if (isset($arr['modified'])
                && (strtotime($arr['modified']) > strtotime(__('affiliation_resign.time_default')))
            ) {
                $lastModified = $arr['modified'];
            }
        }
        return $lastModified;
    }

    /**
     * update change info
     *
     * @param  Request $request
     * @return array
     */
    public function updateResign(Request $request)
    {
        $corpId = $request->get('corpId');
        $sData = $request->get('listData');
        if ($sData && strlen(trim($sData)) > 0) {
            $listData = json_decode($sData, true);
            $resultValidate = $this->validateRequest($listData);
            if ($resultValidate['validate']) {
                $firstObj = reset($listData);
                if ($this->checkModifiedCategoryTemp($firstObj['id'], $firstObj['modified'])) {
                    try {
                        DB::beginTransaction();
                        $idTemp = $this->affGenreResignService->getTempIdFromCorpIdInCorpAgreementTempLink($corpId);
                        /*fail update to m_corp_categories_temp*/
                        $this->copyCorpCategoryByTempId($corpId, $idTemp);
                        //                        $this->updateCorpCategoryTemp($listData);
                        $targetData = [];
                        foreach ($listData as $arr) {
                            if (!empty($arr['application_check'])) {
                                $targetData[] = $arr;
                            }
                        }
                        if (empty($targetData) || empty($corpId)) {
                            return $this->returnAjaxMessage(false);
                        }
                        $this->addCorpCategoryApp($corpId, $targetData);
                        DB::commit();
                        return $this->returnAjaxMessage(true);
                    } catch (\Exception $ex) {
                        DB::rollBack();
                        return $this->returnAjaxMessage(false);
                    }
                }
            } else {
                return $this->returnAjaxMessage(false, $resultValidate['msg']);
            }
        } else {
            return $this->returnAjaxMessage(true);
        }
        return $this->returnAjaxMessage(false);
    }

    /**
     * validate request data update
     *
     * @param  $listData
     * @return array
     */
    private function validateRequest($listData)
    {
        $bValidate = true;
        $errMsg = '';
        if (is_array($listData) && count($listData) > 0) {
            foreach ($listData as $arr) {
                if (!empty(trim($arr['order_fee']))
                    && $arr['corp_commission_type'] != 2
                    && strlen(trim($arr['order_fee_unit'])) == 0
                ) {
                    $bValidate = false;
                    $errMsg = __('affiliation_resign.message_error_miss_unit');
                    break;
                }
                if (!empty($arr['application_check']) && empty($arr['application_reason'])) {
                    $bValidate = false;
                    $errMsg = __('affiliation_resign.message_error_miss_reason');
                    break;
                }
                if (empty(trim($arr['order_fee']))) {
                    $bValidate = false;
                    $errMsg = __('affiliation_resign.message_error_miss_commission');
                    break;
                }
                //Never come here because select only have 2 option with value
                if (empty($arr['corp_commission_type'])) {
                    $bValidate = false;
                    $errMsg = __('affiliation_resign.message_error_miss_order_from');
                    break;
                }
                //Use 4 database condition
                if (strlen(trim($arr['note'])) > 1000) {
                    $bValidate = false;
                    $errMsg = __('affiliation_resign.message_error_over_1k_characters');
                    break;
                }
            }
        }
        return ['validate' => $bValidate, 'msg' => $errMsg];
    }

    /**
     * check modified
     *
     * @param  $id
     * @param  $dateModified
     * @return boolean
     */
    private function checkModifiedCategoryTemp($id, $dateModified)
    {
        if (empty($id)) {
            return true;
        }
        $results = $this->mCorpCategoriesTemp->getById($id);
        if (isset($results['modified'])) {
            if ($dateModified == $results['modified']) {
                return true;
            }
        }
        return false;
    }

    /**
     * IMPORTANT not update info of item in m_corp_categories_temp
     * insert data to m_corp_categories_temp
     *
     * @param  $corpId
     * @param  $tempId
     * @return boolean
     */
    private function copyCorpCategoryByTempId($corpId, $tempId)
    {
        $result = $this->affGenreResignService->findCategoryTempCopy($corpId, $tempId)->toArray();
        $bRegistered = false;
        foreach ($result as $arr) {
            if (!empty($arr['id'])) {
                $bRegistered = true;
                break;
            }
        }
        if (!$bRegistered) {
            $bSaved = true;
            foreach ($result as $obj) {
                $bSaved = $this->mCorpCategoriesTemp->saveWithData($obj);
                if (!$bSaved) {
                    break;
                }
            }
            return $bSaved;
        } else {
            return true;
        }
    }

    /**
     * save message to flash for show error or success action
     *
     * @param  $bSuccess
     * @param  null     $message
     * @return array
     */
    private function returnAjaxMessage($bSuccess, $message = null)
    {
        if ($message != null) {
            if (strlen(trim($message)) > 0) {
                \Session::flash(__('affiliation_resign.KEY_SESSION_MESSAGE_RESIGN'), $message);
            }
        } else {
            if ($bSuccess) {
                \Session::flash(
                    __('affiliation_resign.KEY_SESSION_MESSAGE_RESIGN'),
                    __('affiliation_resign.message_update_complete')
                );
            } else {
                \Session::flash(
                    __('affiliation_resign.KEY_SESSION_MESSAGE_RESIGN'),
                    __('affiliation_resign.message_error_insert_fail')
                );
            }
        }
        if ($bSuccess) {
            return ['code' => 'SUCCESS'];
        } else {
            return ['code' => 'FAIL'];
        }
    }

    /**
     * add to 3 tables: corp_category_group_app, corp_category_app, approval
     *
     * @param $corpId
     * @param $data
     */
    private function addCorpCategoryApp($corpId, $data)
    {
        $time = date('Y-m-d H:i:s');
        $saveGroup['corp_id'] = $corpId;
        $saveGroup['created'] = $time;
        $saveGroup['created_user_id'] = \Auth::user()->user_id;
        $saveGroup['modified'] = $time;
        $saveGroup['modified_user_id'] = \Auth::user()->user_id;
        $newGroupId = $this->corpCategoryGroupApp->insertGetId($saveGroup);
        foreach ($data as $arr) {
            $newCorpCateAppObj = [];
            $newCorpCateAppObj['corp_id'] = $corpId;
            $newCorpCateAppObj['group_id'] = $newGroupId;
            $newCorpCateAppObj['category_id'] = $arr['category_id'];
            if ($arr['corp_commission_type'] != 2) {
                $newCorpCateAppObj['introduce_fee'] = null;
                $newCorpCateAppObj['order_fee'] = $arr['order_fee'];
                $newCorpCateAppObj['order_fee_unit'] = $arr['order_fee_unit'];
            } else {
                $newCorpCateAppObj['introduce_fee'] = $arr['order_fee'];
                $newCorpCateAppObj['order_fee'] = null;
                $newCorpCateAppObj['order_fee_unit'] = null;
            }
            $newCorpCateAppObj['created'] = $time;
            $newCorpCateAppObj['created_user_id'] = \Auth::user()->user_id;
            $newCorpCateAppObj['modified'] = $time;
            $newCorpCateAppObj['modified_user_id'] = \Auth::user()->user_id;
            $newCorpCateAppObj['genre_id'] = $arr['genre_id'];
            $newCorpCateAppObj['corp_commission_type'] = $arr['corp_commission_type'];
            $newCorpCateAppObj['note'] = $arr['note'];
            $corpCateAppId = $this->corpCategoryApp->insertGetId($newCorpCateAppObj);

            $approvalObj = array(
                'relation_application_id' => $corpCateAppId,
                'application_section' => 'CorpCategoryApplication',
                'application_reason' => $arr['application_reason'],
                'approval_datetime' => $time,
                'approval_user_id' => \Auth::user()->user_id,
                'status' => -1,
            );
            $this->approval->insert($approvalObj);
        }
    }

    /**
     * Update m_corp_categories_temp
     *
     * @param  Request $request
     * @return array
     */
    public function updateReconfirm(Request $request)
    {
        $kind = 'WEB';
        $isFax = (bool)$request->get('isFax');
        if ($isFax) {
            $kind = 'FAX';
        }
        $corpId = $request->get('corpId');
        try {
            $idTemp = $this->affGenreResignService->getTempIdFromCorpIdInCorpAgreementTempLink($corpId);
            $count = $this->mCorpCategoriesTemp->countByCorpIdAndTempId($corpId, $idTemp);
            if ($count == 0) {
                $tempData = $this->affGenreResignService->findCategoryTempCopy($corpId, $idTemp);
                if (!empty($tempData)) {
                    foreach ($tempData as $obj) {
                        $this->mCorpCategoriesTemp->saveWithData($obj);
                    }
                }
            }
            $result = $this->affGenreResignService->updateReconfirmResign($corpId, $kind);
            DB::commit();
            if ($result) {
                return $this->returnAjaxMessage(true);
            } else {
                return $this->returnAjaxMessage(false);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->returnAjaxMessage(false);
        }
    }
}
