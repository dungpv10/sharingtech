<?php

namespace App\Services;

use App\Helpers\MailHelper;
use App\Mail\Agreementponsibility;
use App\Repositories\AffiliationAreaStatRepositoryInterface;
use App\Repositories\AffiliationInfoRepositoryInterface;
use App\Repositories\AgreementAttachedFileRepositoryInterface;
use App\Repositories\AgreementRepositoryInterface;
use App\Repositories\AntisocialCheckRepositoryInterface;
use App\Repositories\AutoCommissionCorpRepositoryInterface;
use App\Repositories\CorpAgreementRepositoryInterface;
use App\Repositories\CorpAgreementTempLinkRepositoryInterface;
use App\Repositories\MCategoryRepositoryInterface;
use App\Repositories\MCorpCategoriesTempRepositoryInterface;
use App\Repositories\MCorpCategoryRepositoryInterface;
use App\Repositories\MCorpNewYearRepositoryInterface;
use App\Repositories\MCorpRepositoryInterface;
use App\Repositories\MCorpSubRepositoryInterface;
use App\Repositories\MCorpTargetAreaRepositoryInterface;
use App\Repositories\MGenresRepositoryInterface;
use App\Repositories\MPostRepositoryInterface;
use App\Repositories\MTargetAreaRepositoryInterface;
use Auth;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Lang;

class AffiliationService
{
    /**
     * @var CorpAgreementRepositoryInterface
     */
    protected $corpAgreementRepository;
    /**
     * @var MCorpRepositoryInterface
     */
    protected $mCorpRepository;
    /**
     * @var AgreementAttachedFileRepositoryInterface
     */
    protected $agrAttachedFileRepository;
    /**
     * @var AntisocialCheckRepositoryInterface
     */
    protected $antisocialCheckRepository;
    /**
     * @var MCorpCategoryRepositoryInterface
     */
    protected $mCorpCategoryRepository;
    /**
     * @var MTargetAreaRepositoryInterface
     */
    protected $mTargetAreaRepository;
    /**
     * @var AutoCommissionCorpRepositoryInterface
     */
    protected $autoCommissionCorpRepository;
    /**
     * @var CorpAgreementTempLinkRepositoryInterface
     */
    protected $corpAgrTempLinkRepository;
    /**
     * @var MCorpCategoriesTempRepositoryInterface
     */
    protected $mCorpCategoriesTempRepository;
    /**
     * @var MCategoryRepositoryInterface
     */
    protected $mCategoryRepository;
    /**
     * @var MCorpTargetAreaRepositoryInterface
     */
    protected $mCorpTargetAreaRepository;
    /**
     * @var AffiliationAreaStatRepositoryInterface
     */
    protected $affiliationAreaStatRepository;
    /**
     * @var AgreementRepositoryInterface
     */
    protected $agreementRepository;
    /**
     * @var MGenresRepositoryInterface
     */
    protected $mGenreRepository;
    /**
     * @var MCorpNewYearRepositoryInterface
     */
    private $mCorpNewYearRepository;
    /**
     * @var MCorpSubRepositoryInterface
     */
    private $mCorpSubRepository;
    /**
     * @var AffiliationInfoRepositoryInterface
     */
    private $affiliationInfoRepository;
    /**
     * @var MPostRepositoryInterface
     */
    private $mPostRepository;

    /**
     * AffiliationService constructor.
     * @param CorpAgreementRepositoryInterface $corpAgreementRepository
     * @param MCorpRepositoryInterface $mCorpRepository
     * @param AgreementAttachedFileRepositoryInterface $agrAttachedFileRepository
     * @param AntisocialCheckRepositoryInterface $antisocialCheckRepository
     * @param MCorpCategoryRepositoryInterface $mCorpCategoryRepository
     * @param MTargetAreaRepositoryInterface $mTargetAreaRepository
     * @param AutoCommissionCorpRepositoryInterface $autoCommissionCorpRepository
     * @param CorpAgreementTempLinkRepositoryInterface $corpAgrTempLinkRepository
     * @param MCorpCategoriesTempRepositoryInterface $mCorpCategoriesTempRepository
     * @param MCategoryRepositoryInterface $mCategoryRepository
     * @param MCorpTargetAreaRepositoryInterface $mCorpTargetAreaRepository
     * @param AffiliationAreaStatRepositoryInterface $affiliationAreaStatRepository
     * @param AgreementRepositoryInterface $agreementRepository
     * @param MGenresRepositoryInterface $mGenreRepository
     * @param MCorpNewYearRepositoryInterface $mCorpNewYearRepository
     * @param MCorpSubRepositoryInterface $mCorpSubRepository
     * @param MPostRepositoryInterface $mPostRepository
     * @param AffiliationInfoRepositoryInterface $affiliationInfoRepository
     */
    public function __construct(
        CorpAgreementRepositoryInterface $corpAgreementRepository,
        MCorpRepositoryInterface $mCorpRepository,
        AgreementAttachedFileRepositoryInterface $agrAttachedFileRepository,
        AntisocialCheckRepositoryInterface $antisocialCheckRepository,
        MCorpCategoryRepositoryInterface $mCorpCategoryRepository,
        MTargetAreaRepositoryInterface $mTargetAreaRepository,
        AutoCommissionCorpRepositoryInterface $autoCommissionCorpRepository,
        CorpAgreementTempLinkRepositoryInterface $corpAgrTempLinkRepository,
        MCorpCategoriesTempRepositoryInterface $mCorpCategoriesTempRepository,
        MCategoryRepositoryInterface $mCategoryRepository,
        MCorpTargetAreaRepositoryInterface $mCorpTargetAreaRepository,
        AffiliationAreaStatRepositoryInterface $affiliationAreaStatRepository,
        AgreementRepositoryInterface $agreementRepository,
        MGenresRepositoryInterface $mGenreRepository,
        MCorpNewYearRepositoryInterface $mCorpNewYearRepository,
        MCorpSubRepositoryInterface $mCorpSubRepository,
        MPostRepositoryInterface $mPostRepository,
        AffiliationInfoRepositoryInterface $affiliationInfoRepository
    ) {
        $this->corpAgreementRepository = $corpAgreementRepository;
        $this->mCorpRepository = $mCorpRepository;
        $this->agrAttachedFileRepository = $agrAttachedFileRepository;
        $this->antisocialCheckRepository = $antisocialCheckRepository;
        $this->mCorpCategoryRepository = $mCorpCategoryRepository;
        $this->mTargetAreaRepository = $mTargetAreaRepository;
        $this->autoCommissionCorpRepository = $autoCommissionCorpRepository;
        $this->corpAgrTempLinkRepository = $corpAgrTempLinkRepository;
        $this->mCorpCategoriesTempRepository = $mCorpCategoriesTempRepository;
        $this->mCategoryRepository = $mCategoryRepository;
        $this->mCorpTargetAreaRepository = $mCorpTargetAreaRepository;
        $this->affiliationAreaStatRepository = $affiliationAreaStatRepository;
        $this->agreementRepository = $agreementRepository;
        $this->mGenreRepository = $mGenreRepository;
        $this->mCorpSubRepository = $mCorpSubRepository;
        $this->mCorpNewYearRepository = $mCorpNewYearRepository;
        $this->affiliationInfoRepository = $affiliationInfoRepository;
        $this->mPostRepository = $mPostRepository;
    }

    /**
     * get agreement provisions of corp agreement
     *
     * @param  object $corpAgreement
     * @return string
     */
    public static function getAgreementProvisions($corpAgreement)
    {
        if (!$corpAgreement) {
            return null;
        }
        return empty($corpAgreement->customize_agreement)
            ? str_replace(array("\n", "\r\n"), "<br>\n", $corpAgreement->original_agreement)
            : str_replace(array("\n", "\r\n"), "<br>\n", $corpAgreement->customize_agreement);
    }

    /**
     * get url download by role and status corp agreement
     *
     * @param  string  $role
     * @param  object  $corpAgreement
     * @param  integer $corpId
     * @return string
     */
    public static function getUrlDownloadByRoleAndStatusCorpAgreement($role, $corpAgreement, $corpId)
    {
        $agreementReportDownloadUrl = null;
        if ($role != 'affiliation'
            && !empty($corpAgreement->status)
            && $corpAgreement->status == 'Complete'
        ) {
            $agreementReportDownloadUrl = config('rits.agreement_report_download') . $corpId . '&caId=' . $corpAgreement->id;
        }
        return $agreementReportDownloadUrl;
    }

    /**
     * check role
     *
     * @param  string $role
     * @param  array  $roleOption
     * @return boolean
     */
    public static function isRole($role, $roleOption)
    {
        return in_array($role, $roleOption) ? true : false;
    }

    /**
     * format date
     *
     * @param  string $date
     * @param  string $format
     * @return string
     */
    public static function dateTimeFormat($date, $format)
    {
        return Carbon::parse($date)->format($format);
    }

    /**
     * update agreement
     *
     * @param  array   $data
     * @param  integer $corpId
     * @param  integer $agreementId
     * @return view
     * @throws Exception
     */
    public function agreementUpdate($data, $corpId, $agreementId = null)
    {
        $status = false;
        DB::beginTransaction();
        try {
            $corpAgreement = $this->corpAgreementRepository->getFirstByCorpIdAndAgreementId(
                $corpId,
                $agreementId
            );
            $corpData = $this->mCorpRepository->find($corpId);
            $userId = Auth::user()['user_id'];
            $corpData = self::formatInputDataCorp($corpData, $data, $userId);
            if (!$corpAgreement) {
                $dataCorpAgreement = self::formatInputDataCorpAgreementTypeInsert($corpId, $data, $userId);
                $resultInsertCorpAgreement = $this->corpAgreementRepository->insertOrUpdateItem($dataCorpAgreement);
                if ($resultInsertCorpAgreement['status']) {
                    if (self::updateCategory($resultInsertCorpAgreement['item'])) {
                        if ($this->mCorpRepository->save($corpData)) {
                            $status = true;
                        }
                    }
                }
            } else {
                $corpAgreement = self::formatInputDataCorpAgreementTypeUpdate($corpAgreement, $data, $userId);
                if ($this->corpAgreementRepository->save($corpAgreement)
                    && self::updateCategory($corpAgreement)
                    && $this->mCorpRepository->save($corpData)
                ) {
                    $status = true;
                }
                if ($status
                    && isset($data['CorpAgreement']['acceptation'])
                    && $data['CorpAgreement']['acceptation'] == 1
                    && in_array($corpData->coordination_method, [1, 2, 6, 7])
                ) {
                    $resultSendMail = self::sendAgreementEmail($corpData);
                }
            }
            if ($status) {
                $result['message'] = Lang::get('agreement.update_is_completed');
                $result['class'] = 'box--success';
                if (isset($resultSendMail) && !$resultSendMail) {
                    $result['message'] = Lang::get('agreement.failed_to_send_approval_notification_mail');
                    $result['class'] = 'box--error';
                }
                DB::commit();
            } else {
                $result['message'] = Lang::get('agreement.update_error');
                $result['class'] = 'box--error';
                DB::rollback();
            }
            return $result;
        } catch (Exception $e) {
            DB::rollback();
            throw new Exception();
        }
    }

    /**
     * format input data corp
     *
     * @param  object $corpData
     * @param  array  $data
     * @param  $userId
     * @return object mCorp
     */
    public static function formatInputDataCorp($corpData, $data, $userId)
    {
        if (isset($data['MCorp']['commission_accept_flg'])
            && $corpData->commission_accept_flg != $data['MCorp']['commission_accept_flg']
        ) {
            $corpData->commission_accept_flg = $data['MCorp']['commission_accept_flg'];
            $corpData->commission_accept_date = date('Y-m-d H:i:s');
            $corpData->commission_accept_user_id = $userId;
        }
        return $corpData;
    }

    /**
     * format input data corp agreement type insert
     *
     * @param  integer $corpId
     * @param  array   $data
     * @param  integer $userId
     * @return array
     */
    public static function formatInputDataCorpAgreementTypeInsert($corpId, $data, $userId)
    {
        $dateNow = date('Y-m-d H:i:s');
        $dataCorpAgreement['corp_id'] = $corpId;
        $dataCorpAgreement['status'] = 'NotSigned';

        if (isset($data['CorpAgreement']['agreement_date'])) {
            $dataCorpAgreement['agreement_date'] = $data['CorpAgreement']['agreement_date'];
        }

        if (isset($data['CorpAgreement']['agreement_flag'])) {
            $dataCorpAgreement['agreement_flag'] = $data['CorpAgreement']['agreement_flag'] ? true : false;
        }

        if (isset($data['CorpAgreement']['corp_kind'])) {
            $dataCorpAgreement['corp_kind'] = $data['CorpAgreement']['corp_kind'];
        }

        if (isset($data['CorpAgreement']['hansha_check'])) {
            $dataCorpAgreement['hansha_check'] = $data['CorpAgreement']['hansha_check'];
            $dataCorpAgreement['hansha_check_user_id'] = $userId;
            $dataCorpAgreement['hansha_check_date'] = $dateNow;
        }

        if (isset($data['CorpAgreement']['transactions_law'])
            && $data['CorpAgreement']['transactions_law'] == 1
        ) {
            $dataCorpAgreement['transactions_law_user_id'] = $userId;
            $dataCorpAgreement['transactions_law_date'] = $dateNow;
        }

        if (isset($data['CorpAgreement']['acceptation'])
            && $data['CorpAgreement']['acceptation'] == 1
            && !empty($dataCorpAgreement['status'])
            && $dataCorpAgreement['status'] == "Application"
        ) {
            $dataCorpAgreement['status'] = 'Complete';
            $dataCorpAgreement['acceptation_user_id'] = $userId;
            $dataCorpAgreement['acceptation_date'] = $dateNow;
        }
        return $dataCorpAgreement;
    }

    /**
     * update cateogry
     *
     * @param  object $corpAgreement
     * @return boolean
     */
    private function updateCategory($corpAgreement)
    {
        if (empty($corpAgreement)) {
            return true;
        } elseif (!isset($corpAgreement->status)
            || $corpAgreement->status !== 'Complete'
        ) {
            return true;
        }
        $result = false;
        try {
            $corpId = $corpAgreement->corp_id;
            $corpAgreementId = $corpAgreement->id;
            $result = self::updateCorpCategoryFormCorpCategoryTemp($corpId, $corpAgreementId);
        } catch (Exception $e) {
            $result = false;
        }
        return $result;
    }

    /**
     * update corp category form corp category temp
     *
     * @param  integer $corpId
     * @param  integer $corpAgreementId
     * @return boolean
     */
    private function updateCorpCategoryFormCorpCategoryTemp($corpId = null, $corpAgreementId = null)
    {
        $resultsFlg = true;
        $tempLink = $this->corpAgrTempLinkRepository->getItemByCorpIdAndCorpAgreementId(
            $corpId,
            $corpAgreementId
        );
        if (!$tempLink) {
            return $resultsFlg;
        }
        $tempId = $tempLink->id;
        $tempData = $this->mCorpCategoriesTempRepository->getByCorpIdAndTempId($corpId, $tempId);
        $data = array();
        foreach ($tempData as $key => $val) {
            $findCount = $this->mCategoryRepository->countByCategoryIdAndGenreId($val->category_id, $val->genre_id);
            if (empty($findCount)) {
                continue;
            }
            if ($val->delete_flag) {
                $tempData[$key]->action = 'Delete';
                continue;
            }
            $arrayField = $this->mCorpCategoryRepository->getArrayFieldCategory();
            $data[] = self::getDataCategoryFromCategoryTemp($arrayField, $val);
            if (isset($val->mcc_id)) {
                $action = self::checkUpdateCategory($val);
                $action = empty($action) ? null : $action;
            } else {
                $action = 'Add';
            }
            $tempData[$key]->action = $action;
        }
        try {
            if (empty($data)) {
                return $resultsFlg;
            }
            $delArray = array();
            $resultsFlg = $this->updateCorpCategoryGenre($corpId, $data);
            if ($resultsFlg) {
                $resultsFlg = $this->mCorpCategoriesTempRepository->saveManyData($tempData);
            }
            if ($resultsFlg) {
                $resultsFlg = $this->updateTargetAreaGenre($corpId, $data, $delArray);
                if ($resultsFlg) {
                    $resultsFlg = $this->createAffiliationAreaStats($corpId);
                } else {
                    $resultsFlg = false;
                }
            } else {
                $resultsFlg = false;
            }
        } catch (Exception $e) {
            $resultsFlg = false;
        }
        return $resultsFlg;
    }

    /**
     * get data category from category temp
     *
     * @param  array  $arrayField
     * @param  object $categoryTemp
     * @return array
     */
    public static function getDataCategoryFromCategoryTemp($arrayField, $categoryTemp)
    {
        $arrayValue['id'] = isset($categoryTemp->mcc_id) ? $categoryTemp->mcc_id : '';
        foreach ($arrayField as $field) {
            $arrayValue[$field] = isset($categoryTemp->$field) ? $categoryTemp->$field : '';
        }
        return $arrayValue;
    }

    /**
     * check update category
     *
     * @param  object $mCorpCategory
     * @return string
     */
    public static function checkUpdateCategory($mCorpCategory)
    {
        $updateColumn = array();
        if (!empty($mCorpCategory->action)) {
            return $mCorpCategory->action;
        }
        if ($mCorpCategory->mcc_order_fee !== $mCorpCategory->order_fee) {
            $updateColumn[] = 'order_fee';
        }
        if ($mCorpCategory->mcc_order_fee_unit !== $mCorpCategory->order_fee_unit) {
            $updateColumn[] = 'order_fee_unit';
        }
        if ($mCorpCategory->mcc_introduce_fee !== $mCorpCategory->introduce_fee) {
            $updateColumn[] = 'introduce_fee';
        }
        if ($mCorpCategory->mcc_corp_commission_type !== $mCorpCategory->corp_commission_type) {
            $updateColumn[] = 'corp_commission_type';
        }

        $srcNote = empty($mCorpCategory->mcc_note) ? '' : $mCorpCategory->mcc_note;
        $destNote = empty($mCorpCategory->note) ? '' : $mCorpCategory->note;

        $srcSelectList = empty($mCorpCategory->mcc_select_list) ? '' : $mCorpCategory->mcc_select_list;
        $destSelectList = empty($mCorpCategory->select_list) ? '' : $mCorpCategory->select_list;

        if ($srcNote !== $destNote) {
            $updateColumn[] = 'note';
        }
        if ($srcSelectList !== $destSelectList) {
            $updateColumn[] = 'select_list';
        }

        if (count($updateColumn) > 0) {
            $retStr = implode(',', $updateColumn);
            if ($retStr !== '') {
                $retStr = 'Update:' . $retStr;
            }
        } else {
            $retStr = null;
        }

        return $retStr;
    }

    /**
     * update category genre
     *
     * @param  integer $id
     * @param  array   $data
     * @return boolean
     */
    private function updateCorpCategoryGenre($id, $data)
    {
        $resultsFlg = false;
        $count = 0;
        $delMccIdArray = $this->mCorpCategoryRepository->getListIdByCorpId($id);
        foreach ($data as $v) {
            if (!isset($v['category_id'])) {
                continue;
            }
            $saveData [] = $v;
            foreach ($delMccIdArray as $key => $value) {
                if ($value == $v['id']) {
                    unset($delMccIdArray[$key]);
                    break;
                }
            }
            $resultsFlg = self::checkEditCorpCategory($data, $v['category_id'], $count);
            if (!$resultsFlg) {
                return false;
            }
            $count++;
        }
        $this->mCorpCategoryRepository->deleteListItemByArrayId($delMccIdArray);
        if (!empty($saveData)) {
            if ($resultsFlg) {
                if ($this->mCorpCategoryRepository->updateManyItemWithArray($saveData)) {
                    return true;
                }
            }
            return false;
        } else {
            return true;
        }
    }

    /**
     * check edit corp category
     *
     * @param  array   $data
     * @param  integer $categoryId
     * @param  integer $count
     * @return boolean
     */
    public static function checkEditCorpCategory($data, $categoryId, $count)
    {
        $resultsFlg = true;
        for ($i = 0; $i < $count; $i++) {
            if (!empty($data [$i] ['category_id'])) {
                if ($data [$i] ['category_id'] == $categoryId) {
                    $resultsFlg = false;
                    break;
                }
            }
        }
        return $resultsFlg;
    }

    /**
     * update target area genre
     *
     * @param  integer $id
     * @param  array   $data
     * @return boolean
     */
    private function updateTargetAreaGenre($id, $data)
    {
        $delMccIdArray = $this->mCorpCategoryRepository->getListIdByCorpId($id);
        $saveData = array();
        foreach ($data as $v) {
            if (!isset($v['category_id'])) {
                continue;
            }
            foreach ($delMccIdArray as $key => $value) {
                if ($value == $v['id']) {
                    unset($delMccIdArray[$key]);
                    break;
                }
            }
        }
        $this->mTargetAreaRepository->deleteListItemByArrayCorpCategoryId($delMccIdArray);
        $corpAreas = $this->mCorpTargetAreaRepository->getAllByCorpId($id);
        $idList = $this->mCorpCategoryRepository->getListForIdByCorpIdAndGenreId($id);
        foreach ($idList as $key => $val) {
            if ($val['corp_category_id']) {
                continue;
            }
            foreach ($corpAreas as $area) {
                $setData = array();
                $setData['corp_category_id'] = $val->id;
                $setData['jis_cd'] = $area->jis_cd;
                $saveData[] = $setData;
            }
            $listMCorpCategory = $this->mCorpCategoryRepository->getAllByCorpIdAndGenreId(
                $id,
                $val->genre_id
            );
            foreach ($listMCorpCategory as $mCorpCategory) {
                if ($mCorpCategory->target_area_type > 0) {
                    $this->mCorpCategoryRepository->updateCorpCategoryTargetAreaType(
                        $mCorpCategory->id,
                        $mCorpCategory->target_area_type
                    );
                    break;
                }
            }
        }
        if (count($saveData)) {
            if (!$this->mTargetAreaRepository->insert($saveData)) {
                return false;
            }
        }
        return true;
    }

    /**
     * create affiliation area stats
     *
     * @param  integer $id
     * @return boolean
     */
    private function createAffiliationAreaStats($id)
    {
        $listMCC = $this->mCorpCategoryRepository->getListByCorpIdAndAffiliationStatus($id);
        $saveData = [];
        foreach ($listMCC as $mcc) {
            for ($i = 1; $i <= 47; $i++) {
                $data = $this->affiliationAreaStatRepository->findByCorpIdAndGenerIdAndPrefecture(
                    $mcc->id,
                    $mcc->genre_id,
                    $i
                );
                if (count($data) === 0) {
                    $dataAAS = array();
                    $dataAAS['corp_id'] = $mcc->id;
                    $dataAAS['genre_id'] = $mcc->genre_id;
                    $dataAAS['prefecture'] = $i;
                    $dataAAS['commission_count_category'] = 0;
                    $dataAAS['orders_count_category'] = 0;
                    $dataAAS['created'] = date("Y/m/d H:i:s", time());
                    $dataAAS['created_user_id'] = Auth::user()['user_id'];
                    $dataAAS['commission_unit_price_rank'] = 'z';
                    $saveData[] = $dataAAS;
                }
            }
        }
        if (count($saveData)) {
            if (!$this->affiliationAreaStatRepository->insert($saveData)) {
                return false;
            }
        }
        return true;
    }

    /**
     * format input data corp agreement type update
     *
     * @param  object  $corpAgreement
     * @param  array   $data
     * @param  integer $userId
     * @return object
     */
    public static function formatInputDataCorpAgreementTypeUpdate($corpAgreement, $data, $userId)
    {
        $dateNow = date('Y-m-d H:i:s');
        if (isset($data['CorpAgreement']['agreement_date'])) {
            $corpAgreement->agreement_date = $data['CorpAgreement']['agreement_date'];
        }

        if (isset($data['CorpAgreement']['agreement_flag'])) {
            $corpAgreement->agreement_flag = $data['CorpAgreement']['agreement_flag'] ? true : false;
        }

        if (isset($data['CorpAgreement']['corp_kind'])) {
            $corpAgreement->corp_kind = $data['CorpAgreement']['corp_kind'];
        }

        if (isset($data['CorpAgreement']['hansha_check'])) {
            if (!isset($corpAgreement->hansha_check)
                || (isset($corpAgreement->hansha_check)
                && $corpAgreement->hansha_check                != $data['CorpAgreement']['hansha_check'])
            ) {
                $corpAgreement->hansha_check = $data['CorpAgreement']['hansha_check'];
                $corpAgreement->hansha_check_user_id = $userId;
                $corpAgreement->hansha_check_date = $dateNow;
            }
        }

        if (isset($data['CorpAgreement']['transactions_law'])
            && $data['CorpAgreement']['transactions_law'] == 1
        ) {
            if (!isset($corpAgreement->transactions_law_date)) {
                $corpAgreement->transactions_law_user_id = $userId;
                $corpAgreement->transactions_law_date = $dateNow;
            }
        } else {
            $corpAgreement->transactions_law_user_id = null;
            $corpAgreement->transactions_law_date = null;
        }

        if (isset($data['CorpAgreement']['acceptation'])
            && $data['CorpAgreement']['acceptation'] == 1
        ) {
            if (!isset($corpAgreement->acceptation_date)) {
                $corpAgreement->acceptation_user_id = $userId;
                $corpAgreement->acceptation_date = $dateNow;
            }

            if (!empty($corpAgreement->status) && $corpAgreement->status == "Application") {
                $corpAgreement->status = 'Complete';
            }
            if (!empty($corpAgreement->kind) && $corpAgreement->kind == 'FAX') {
                $corpAgreement->status = 'Complete';
            }
        } else {
            if (!empty($data['agreement_remand_flg']) && $data['agreement_remand_flg']) {
                $corpAgreement->status = 'Reconfirmation';
            }
            $corpAgreement->acceptation_user_id = null;
            $corpAgreement->acceptation_date = null;
        }

        return $corpAgreement;
    }

    /**
     * @param $corpData
     * @return bool
     */
    public static function sendAgreementEmail($corpData)
    {
        try {
            $addrArr = array();
            $tmpAddrs = array();
            if (!empty($corpData->mailaddress_pc)) {
                $tmpAddrs = explode(";", $corpData->mailaddress_pc);
                foreach ($tmpAddrs as $address) {
                    $addrArr[] = $address;
                }
            }
            $tmpAddrs = array();
            if (!empty($corpData->mailaddress_mobile)) {
                $tmpAddrs = explode(";", $corpData->mailaddress_mobile);
                foreach ($tmpAddrs as $address) {
                    $addrArr[] = $address;
                }
            }
            if (count($addrArr)) {
                $toAddr = $addrArr;
            } else {
                return true;
            }
            $corpName = $corpData->official_corp_name;
            $fromST = config('rits.agreement_alert_mail_setting.from_address');
            $subjectST = config('rits.agreement_alert_mail_setting.title');
            $dataST = [
                'subject' => $subjectST,
                'from' => $fromST,
                'to' => $toAddr,
            ];
            MailHelper::sendMail($dataST['to'], new Agreementponsibility($dataST, $corpName));
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * update reconfirmation
     *
     * @param  integer $corpId
     * @return view
     * @throws Exception
     */
    public function updateReconfirmation($corpId)
    {
        DB::beginTransaction();
        try {
            $corpAgreementCnt = $this->corpAgreementRepository->getCountByCorpIdAndStatus($corpId);
            $corpAgreement = $this->corpAgreementRepository->getFirstByCorpIdAndAgreementId($corpId, null, true);
            $agreement = $this->agreementRepository->findLastItem();
            if ((empty($corpAgreementCnt) || $corpAgreementCnt == 0)
                && $corpAgreement && $agreement
            ) {
                $dataCorpAgreement['corp_kind'] = $corpAgreement->corp_kind;
                $dataCorpAgreement['agreement_history_id'] = $agreement->last_history_id;
                $dataCorpAgreement['ticket_no'] = $agreement->ticket_no;
                $dataCorpAgreement['status'] = 'Reconfirmation';
                $dataCorpAgreement['corp_id'] = $corpId;
                $resultInsertCorpAgreement = $this->corpAgreementRepository->insertOrUpdateItem($dataCorpAgreement);
                if (!$resultInsertCorpAgreement['status']) {
                    throw new Exception();
                }
            }
            if (!empty($resultInsertCorpAgreement['item'])) {
                $dataCorpAgreementTempLink['corp_id'] = $corpId;
                $dataCorpAgreementTempLink['corp_agreement_id'] = $resultInsertCorpAgreement['item']->id;
                if (!$this->corpAgrTempLinkRepository->insertOrUpdateItem($dataCorpAgreementTempLink)['status']) {
                    throw new Exception();
                }
            }
            DB::commit();

            $agreementId = !empty($resultInsertCorpAgreement['item']->id) ? $resultInsertCorpAgreement['item']->id : '';
            $result['message'] = Lang::get('agreement.update_is_completed');
            $result['class'] = 'box--success';
            $result['agreement_id'] = $agreementId;
            return $result;
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception();
        }
    }

    /**
     * agreement upload file
     *
     * @param  object  $request
     * @param  integer $corpId
     * @param  integer $corpAgreementId
     * @return view
     * @throws Exception
     */
    public function agreementUploadFile($request, $corpId, $corpAgreementId = null)
    {
        DB::beginTransaction();
        try {
            $result['class'] = 'box--error';
            if ($request->has('upload_file_path')) {
                if (!$request->has('upload_file_path')) {
                    throw new Exception(Lang::get('agreement.file_does_not_exist'));
                }
                $files = $request->file('upload_file_path');
                $dataUnLink = [];
                foreach ($files as $key => $file) {
                    $fileName = $file->getClientOriginalName();
                    $fileExtension = $file->getClientOriginalExtension();
                    $fileTmpName = $file->getRealPath();
                    $fileType = $file->getMimeType();
                    $prefix = storage_path('upload/');
                    $saveDir = $prefix . 'agreement' . '/' . $corpId . '/';
                    if (!self::checkPermissionFileAndFolder($fileTmpName, $saveDir)) {
                        $result['message'] = Lang::get('agreement.error_while_processing');
                        return $result;
                    }
                    $dataAttachedFile = self::formatInputDataAttachedFile(
                        $corpId,
                        $corpAgreementId,
                        $fileName,
                        $fileType
                    );
                    $resultInsert = $this->agrAttachedFileRepository->insertOrUpdateItem($dataAttachedFile);
                    if (!$resultInsert['status']) {
                        throw new Exception('error_while_processing');
                    }
                    $attachedFile = $resultInsert['item'];
                    $fileStorage = $attachedFile->id . '.' . $fileExtension;
                    $file->move($saveDir, $fileStorage);
                    $dataUpdateFile['path'] = $saveDir . $fileStorage;
                    $resultInsert = $this->agrAttachedFileRepository->insertOrUpdateItem(
                        $dataUpdateFile,
                        $attachedFile
                    );
                    $dataUnLink[] = $dataUpdateFile['path'];
                    if (!$resultInsert['status']) {
                        foreach ($dataUnLink as $key => $link) {
                            unlink($link);
                        }
                        $result['message'] = Lang::get('agreement.error_while_processing');
                        return $result;
                    }
                }
                $result['message'] = Lang::get('agreement.upload_file_success');
                $result['class'] = 'box--success';
            } else {
                if ($request->has('file_id')) {
                    $fileId = $request->input('file_id');
                    $file = $this->agrAttachedFileRepository->findByCorpIdAndId($corpId, $fileId);
                    if (!$file) {
                        $result['message'] = Lang::get('agreement.file_does_not_exist');
                        return $result;
                    }
                    $filePath = $file->path;
                    $fileCreateDate = strtotime($file->create_date);
                    $agreement = $this->corpAgreementRepository->findByCorpIdAndStatus(
                        $corpId,
                        ['Complete', 'Application']
                    );
                    if (!$agreement) {
                        $result['message'] = Lang::get('agreement.error_while_processing');
                        return $result;
                    }
                    $agreementUpdateDate = empty($agreement->update_date) ? 0 : strtotime($agreement->acceptation_date);
                    if ($fileCreateDate < $agreementUpdateDate) {
                        $result['message'] = Lang::get('agreement.do_not_delete_file');
                        return $result;
                    }
                    if (!$file->delete()) {
                        $result['message'] = Lang::get('agreement.file_does_not_exist');
                        return $result;
                    }
                    if (!is_file($filePath)) {
                        DB::commit();
                        $result['message'] = Lang::get('agreement.file_does_not_exist');
                        return $result;
                    }
                    if (!unlink($filePath)) {
                        $result['message'] = Lang::get('agreement.file_does_not_exist');
                        return $result;
                    }
                    $result['message'] = Lang::get('agreement.delete_file_success');
                    $result['class'] = 'box--success';
                }
            }
            DB::commit();
            return $result;
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception();
        }
    }

    /**
     * check permission file and folder
     *
     * @param  string $fileName
     * @param  string $folderName
     * @return boolean
     */
    public static function checkPermissionFileAndFolder($fileName = null, $folderName = null)
    {
        $result = true;
        if ($fileName) {
            if (!is_file($fileName)) {
                $result = false;
            }
            if (!is_readable($fileName)) {
                $result = false;
            }
        }
        if ($folderName) {
            if (!is_dir($folderName)) {
                if (!mkdir($folderName, 0755, true)) {
                    $result = false;
                }
            }
        }
        return $result;
    }

    /**
     * format input data attached file
     *
     * @param  integer $corpId
     * @param  integer $corpAgreementId
     * @param  string  $fileName
     * @param  string  $fileType
     * @return array
     */
    public static function formatInputDataAttachedFile($corpId, $corpAgreementId, $fileName, $fileType)
    {
        $userId = Auth::user()['id'];
        $timeNow = date('Y-m-d h:i:s');
        $dataAttachedFile['corp_id'] = $corpId;
        $dataAttachedFile['corp_agreement_id'] = $corpAgreementId;
        $dataAttachedFile['kind'] = 'Cert';
        $dataAttachedFile['path'] = '';
        $dataAttachedFile['name'] = $fileName;
        $dataAttachedFile['content_type'] = $fileType;
        $dataAttachedFile['version_no'] = 1;
        $dataAttachedFile['create_date'] = $timeNow;
        $dataAttachedFile['create_user_id'] = $userId;
        $dataAttachedFile['update_date'] = $timeNow;
        $dataAttachedFile['update_user_id'] = $userId;

        return $dataAttachedFile;
    }

    /**
     * check file exists
     *
     * @param  $corpId
     * @param  object $file
     * @return boolean
     */
    public static function checkFileExists($corpId, $file)
    {
        $array = explode('.', $file->name);
        $fileExtension = array_pop($array);
        $pathFile = '/agreement/' . $corpId . '/' . $file->id . '.' . $fileExtension;
        if (Storage::exists($pathFile)) {
            return true;
        }
        return false;
    }
}
