<?php


namespace App\Services;

use App\Models\License;
use App\Repositories\AgreementAdminLicenseRepositoryInterface;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class AgreementAdminLicenseService
{
    /**
     * @var AgreementAdminLicenseRepositoryInterface
     */
    protected $agreementLicenseRepository;


    /**
     * AgreementAdminLicenseService constructor.
     *
     * @param AgreementAdminLicenseRepositoryInterface $agreementLicenseRepository
     */
    public function __construct(
        AgreementAdminLicenseRepositoryInterface $agreementLicenseRepository
    ) {
        $this->agreementLicenseRepository = $agreementLicenseRepository;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function getAgreementLicenseData(Request $request)
    {
        $query = $this->agreementLicenseRepository->getAllLicense();

        if ($request->input('order.0.column') == null || $request->input('order.0.column') == '') {
            $query = $query->orderBy('id', 'asc');
        }

        // search all test box
        if ($request->input('search.value') != null && $request->input('search.value') != '') {
            $searchValue = strtolower($request->input('search.value'));
            $sql = "(CAST(id as TEXT) like ?) OR
                    LOWER(name) like ? OR
                    LOWER(CASE certificate_required_flag WHEN true THEN '" . License::HAVE_TO . "' ELSE '' END) like ?";
            $query = $query->havingRaw($sql, ["%{$searchValue}%", "%{$searchValue}%", "%{$searchValue}%"]);
        }

        // search textboxs in table's headers
        if ($request->input('columns.0.search.value') != null && $request->input('columns.0.search.value') != '') {
            $idValue = $request->input('columns.0.search.value');
            $sql = "CAST(id as TEXT) like ?";
            $query = $query->havingRaw($sql, ["%{$idValue}%"]);
        }
        if ($request->input('columns.1.search.value') != null && $request->input('columns.1.search.value') != '') {
            $nameValue = strtolower($request->input('columns.1.search.value'));
            $sql = "LOWER(name) like ?";
            $query = $query->havingRaw($sql, ["%{$nameValue}%"]);
        }
        if ($request->input('columns.2.search.value') != null && $request->input('columns.2.search.value') != '') {
            $requiredValue = strtolower($request->input('columns.2.search.value'));
            $sql = "LOWER(CASE certificate_required_flag WHEN true THEN '" . License::HAVE_TO . "' ELSE '' END) like ?";
            $query = $query->havingRaw($sql, ["%{$requiredValue}%"]);
        }

        $datatableQuery = DataTables::of($query)
            ->filterColumn(
                'id',
                function ($query) {
                }
            )
            ->filterColumn(
                'name',
                function ($query) {
                }
            )
            ->filterColumn(
                'certificate_required_flag_converted',
                function ($query) {
                }
            )
            ->addColumn(
                'detail_url',
                function ($query) {
                    return action('Agreement\AgreementAdminLicenseController@getLicenseDetail', ['id' => $query->id]);
                }
            )
            ->addColumn(
                'delete_url',
                function ($query) {
                    return action('Agreement\AgreementAdminLicenseController@deleteLicense', ['id' => $query->id]);
                }
            );

        return $datatableQuery->make(true);
    }
}
