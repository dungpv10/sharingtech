<?php

namespace App\Services;

use App\Helpers\MailHelper;
use App\Helpers\Util;
use App\Models\AffiliationAreaStat;
use App\Models\AgreementAttachedFile;
use App\Models\CorpAgreement;
use App\Models\MCorp;
use App\Models\MCorpCategoriesTemp;
use App\Models\MCorpCategory;
use App\Models\MCorpTargetArea;
use App\Repositories\AffiliationAreaStatRepositoryInterface;
use App\Repositories\AffiliationInfoRepositoryInterface;
use App\Repositories\AgreementAttachedFileRepositoryInterface;
use App\Repositories\AgreementCustomizeRepositoryInterface;
use App\Repositories\AgreementEditHistoryRepositoryInterface;
use App\Repositories\AgreementRepositoryInterface;
use App\Repositories\CorpAgreementRepositoryInterface;
use App\Repositories\CorpAgreementTempLinkRepositoryInterface;
use App\Repositories\MAddress1RepositoryInterface;
use App\Repositories\MCorpCategoriesTempRepositoryInterface;
use App\Repositories\MCorpCategoryRepositoryInterface;
use App\Repositories\MCorpRepositoryInterface;
use App\Repositories\MCorpSubRepositoryInterface;
use App\Repositories\MCorpTargetAreaRepositoryInterface;
use App\Repositories\MPostRepositoryInterface;
use App\Repositories\MTargetAreaRepositoryInterface;
use App\Services\Logic\AgreementSystemLogic;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\MessageBag;
use Mpdf\Tag\Strong;

class AgreementSystemService
{
    /**
     * @var CorpAgreementRepositoryInterface
     */
    protected $corpAgreementRepository;
    /**
     * @var AgreementRepositoryInterface
     */
    protected $agreementRepository;
    /**
     * @var AgreementEditHistoryRepositoryInterface
     */
    protected $agreementEditHistoryRepository;
    /**
     * @var AgreementCustomizeRepositoryInterface
     */
    protected $agreementCustomizeRepository;
    /**
     * @var AgreementSystemLogic
     */
    protected $agreementSystemLogic;
    /**
     * @var CorpAgreementTempLinkRepositoryInterface
     */
    protected $tempLinkRepository;
    /**
     * @var MCorpCategoriesTempRepositoryInterface
     */
    protected $mCorpCategoriesTempRepository;
    /**
     * @var MCorpRepositoryInterface
     */
    protected $mCorpRepository;
    /**
     * @var AffiliationInfoRepositoryInterface
     */
    protected $affiliationInfoRepository;
    /**
     * @var MCorpSubRepositoryInterface
     */
    protected $mCorpSubRepository;
    /**
     * @var MAddress1RepositoryInterface
     */
    protected $mAddress1Repository;
    /**
     * @var MPostRepositoryInterface
     */
    protected $mPostRepository;
    /**
     * @var AgreementAttachedFileRepositoryInterface
     */
    protected $attachedFileRepository;
    /**
     * @var MCorpCategoryRepositoryInterface
     */
    protected $mCorpCategoryRepository;
    /**
     * @var AgreementService
     */
    protected $agreementService;
    /**
     * @var MCorpTargetAreaRepositoryInterface
     */
    protected $mCorpTargetAreaRepository;
    /**
     * @var MTargetAreaRepositoryInterface
     */
    protected $mTargetAreaRepository;
    /**
     * @var AffiliationAreaStatRepositoryInterface
     */
    protected $affiliationAreaStatRepository;

    /**
     * AgreementSystemService constructor.
     * @param AgreementRepositoryInterface $agreementRepository
     * @param CorpAgreementRepositoryInterface $corpAgreementRepository
     * @param AgreementEditHistoryRepositoryInterface $agreementEditHistoryRepository
     * @param AgreementCustomizeRepositoryInterface $agreementCustomizeRepository
     * @param CorpAgreementTempLinkRepositoryInterface $tempLinkRepository
     * @param AgreementSystemLogic $agreementSystemLogic
     * @param MCorpCategoriesTempRepositoryInterface $mCorpCategoriesTempRepository
     * @param MCorpRepositoryInterface $mCorpRepository
     * @param AffiliationInfoRepositoryInterface $affiliationInfoRepository
     * @param MCorpSubRepositoryInterface $mCorpSubRepository
     * @param MAddress1RepositoryInterface $mAddress1Repository
     * @param MPostRepositoryInterface $mPostRepository
     * @param AgreementAttachedFileRepositoryInterface $attachedFileRepository
     * @param MCorpCategoryRepositoryInterface $mCorpCategoryRepository
     * @param AgreementService $agreementService
     * @param MCorpTargetAreaRepositoryInterface $mCorpTargetAreaRepository
     * @param MTargetAreaRepositoryInterface $mTargetAreaRepository
     * @param AffiliationAreaStatRepositoryInterface $affiliationAreaStatRepository
     */
    public function __construct(
        AgreementRepositoryInterface $agreementRepository,
        CorpAgreementRepositoryInterface $corpAgreementRepository,
        AgreementEditHistoryRepositoryInterface $agreementEditHistoryRepository,
        AgreementCustomizeRepositoryInterface $agreementCustomizeRepository,
        CorpAgreementTempLinkRepositoryInterface $tempLinkRepository,
        AgreementSystemLogic $agreementSystemLogic,
        MCorpCategoriesTempRepositoryInterface $mCorpCategoriesTempRepository,
        MCorpRepositoryInterface $mCorpRepository,
        AffiliationInfoRepositoryInterface $affiliationInfoRepository,
        MCorpSubRepositoryInterface $mCorpSubRepository,
        MAddress1RepositoryInterface $mAddress1Repository,
        MPostRepositoryInterface $mPostRepository,
        AgreementAttachedFileRepositoryInterface $attachedFileRepository,
        MCorpCategoryRepositoryInterface $mCorpCategoryRepository,
        AgreementService $agreementService,
        MCorpTargetAreaRepositoryInterface $mCorpTargetAreaRepository,
        MTargetAreaRepositoryInterface $mTargetAreaRepository,
        AffiliationAreaStatRepositoryInterface $affiliationAreaStatRepository
    ) {
        $this->agreementRepository = $agreementRepository;
        $this->corpAgreementRepository = $corpAgreementRepository;
        $this->agreementEditHistoryRepository = $agreementEditHistoryRepository;
        $this->agreementCustomizeRepository = $agreementCustomizeRepository;
        $this->tempLinkRepository = $tempLinkRepository;
        $this->agreementSystemLogic = $agreementSystemLogic;
        $this->mCorpCategoriesTempRepository = $mCorpCategoriesTempRepository;
        $this->mCorpRepository = $mCorpRepository;
        $this->affiliationInfoRepository = $affiliationInfoRepository;
        $this->mCorpSubRepository = $mCorpSubRepository;
        $this->mAddress1Repository = $mAddress1Repository;
        $this->mPostRepository = $mPostRepository;
        $this->attachedFileRepository = $attachedFileRepository;
        $this->mCorpCategoryRepository = $mCorpCategoryRepository;
        $this->agreementService = $agreementService;
        $this->mCorpTargetAreaRepository = $mCorpTargetAreaRepository;
        $this->mTargetAreaRepository = $mTargetAreaRepository;
        $this->affiliationAreaStatRepository = $affiliationAreaStatRepository;
    }

    /**
     * @param $user
     * @throws \Exception
     */
    public function step0Process($user)
    {
        $corpAgreement = $this->checkFirstCorpAgreementNotComplete($user);
        $agreement = $this->agreementRepository->findCurrentVersion();
        if (is_null($corpAgreement)) {
            $corpAgreement = $this->agreementSystemLogic->initDataCorpAgreement($user);
        } else {
            if ($corpAgreement->agreement_id == null || $corpAgreement->agreement_id == 0) {
                $corpAgreement->agreement_id = $agreement->agreement_id;
                $corpAgreement->update_date = Carbon::now()->toDateTimeString();
                $corpAgreement->update_user_id = $user->id;
                $this->corpAgreementRepository->save($corpAgreement);
            }
        }

        $corpAgreementTempLink = $this->agreementSystemLogic->initCorpAgreementTempLink($corpAgreement, $user);

        $mCorpCategoriesTempList = $this->mCorpCategoriesTempRepository->findAllByCorpIdAndTempIdWithFlag($corpAgreement->corp_id, $corpAgreementTempLink->id);

        if (checkIsNullOrEmptyCollection($mCorpCategoriesTempList)) {
            $this->agreementSystemLogic->initCorpCategoryTemp($user, $user->affiliation_id, $corpAgreementTempLink->id);
        }
    }

    /**
     * @param $user
     */
    public function step1Process($user)
    {
        $corpAgreement = $this->checkFirstCorpAgreementNotComplete($user);
        if (!is_null($corpAgreement)) {
            if ($corpAgreement->status == CorpAgreement::STEP0
                || $corpAgreement->status == CorpAgreement::RECONFIRMATION
                || $corpAgreement->status == CorpAgreement::RESIGNING
            ) {
                $corpAgreement->status = CorpAgreement::STEP1;
                $corpAgreement->update_date = Carbon::now()->toDateTimeString();
                $corpAgreement->update_user_id = $user->id;
                $this->corpAgreementRepository->save($corpAgreement);
            }
        }
    }

    /**
     * @param $corpId
     * @return mixed
     */
    public function findCustomizedAgreementByCorpId($corpId)
    {
        return $this->agreementSystemLogic->findCustomizedAgreementByCorpId($corpId);
    }


    /**
     * @param $user
     * @return null|object
     */
    private function checkFirstCorpAgreementNotComplete($user)
    {
        $corpAgreement = $this->corpAgreementRepository->getFirstByCorpIdAndAgreementId($user->affiliation_id, null, true);
        if (!is_null($corpAgreement) && $corpAgreement->status !== CorpAgreement::COMPLETE) {
            return $corpAgreement;
        } else {
            return null;
        }
    }

    /**
     * @param $responsibility
     * @return mixed
     */
    public function getStep2($responsibility)
    {
        $prefectureDiv = \Config::get('datacustom.prefecture_div');
        $coordinationMethodList = MCorp::COORDINATION_METHOD_LIST;
        $responsibilityMei = '';
        if (stripos($responsibility, " ") > 0) {
            $idx = stripos($responsibility, " ");
            $responsibilitySei = substr($responsibility, 0, $idx);
            $responsibilityMei = substr($responsibility, $idx + 1);
        } else {
            $responsibilitySei = $responsibility;
        }
        $data['responsibilitySei'] = $responsibilitySei;
        $data['responsibilityMei'] = $responsibilityMei;
        $data['prefectureDiv'] = $prefectureDiv;
        $data['coordinationMethodList'] = $coordinationMethodList;
        return $data;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function convertRequestData($data)
    {
        if (!array_key_exists('support_language_en', $data['mCorp'])) {
            $data['mCorp']['support_language_en'] = '0';
        }
        if (!array_key_exists('support_language_zh', $data['mCorp'])) {
            $data['mCorp']['support_language_zh'] = '0';
        }
        if (!array_key_exists('mobile_mail_none', $data['mCorp'])) {
            $data['mCorp']['mobile_mail_none'] = '0';
        }
        if (!array_key_exists('support24hour', $data['mCorp'])) {
            $data['mCorp']['support24hour'] = '0';
        }
        if (!array_key_exists('available_time_other', $data['mCorp'])) {
            $data['mCorp']['available_time_other'] = '0';
        }
        if (!array_key_exists('contactable_support24hour', $data['mCorp'])) {
            $data['mCorp']['contactable_support24hour'] = '0';
        }
        if (!array_key_exists('contactable_time_other', $data['mCorp'])) {
            $data['mCorp']['contactable_time_other'] = '0';
        }
        $data['mCorp']['responsibility'] = $data['mCorp']['responsibility_sei'] . ' ' . $data['mCorp']['responsibility_mei'];
        unset($data['mCorp']['responsibility_sei']);
        unset($data['mCorp']['responsibility_mei']);
        $data['affiliationInfo']['corp_id'] = $data['mCorp']['id'];
        return $data;
    }

    /**
     * @param $data
     * @return bool
     * @throws \Exception
     */
    public function updateData($data)
    {
        DB::beginTransaction();
        try {
            $this->mCorpRepository->updateById($data['mCorp']);
            $this->affiliationInfoRepository->updateByCorpId($data['affiliationInfo']);
            $this->updateMCorpSub($data['mCorp']['id'], $data['holidays']);
            DB::commit();
            return true;
        } catch (\Exception $exception) {
            DB::rollback();
            throw $exception;
            return false;
        }
    }

    /**
     * @param $corpId
     * @param $holidays
     * @throws \Exception
     */
    private function updateMCorpSub($corpId, $holidays)
    {
        DB::beginTransaction();
        try {
            if (!empty($holidays) && !empty($corpId)) {
                $conditions = [
                    ['m_corp_subs.corp_id', '=', $corpId],
                    ['m_corp_subs.item_category', '=', __('common.holiday')]
                ];
                $this->mCorpSubRepository->deleteByCondition($conditions);
                foreach (array_values($holidays) as $key => $holiday) {
                    $corpSubList[$key]['corp_id'] = $corpId;
                    $corpSubList[$key]['item_category'] = __('common.holiday');
                    $corpSubList[$key]['item_id'] = $holiday;
                    $corpSubList[$key]['created_user_id'] = Auth::user()->user_id;
                    $corpSubList[$key]['created'] = Carbon::now()->toDateTimeString();
                    $corpSubList[$key]['modified_user_id'] = Auth::user()->user_id;
                    $corpSubList[$key]['modified'] = Carbon::now()->toDateTimeString();
                }
                $this->mCorpSubRepository->insert($corpSubList);
            }
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollback();
            throw $exception;
        }
    }

    /**
     * @param $user
     * @param $companyKind
     */
    public function step2Process($user, $companyKind)
    {
        $corpAgreement = $this->checkFirstCorpAgreementNotComplete($user);
        if (!is_null($corpAgreement)) {
            if ($corpAgreement->status == CorpAgreement::STEP1) {
                $corpAgreement->status = CorpAgreement::STEP2;
            }
            $corpAgreement->corp_kind = $companyKind;
            $corpAgreement->update_date = Carbon::now()->toDateTimeString();
            $corpAgreement->update_user_id = $user->id;
            $this->corpAgreementRepository->save($corpAgreement);
        }
    }

    /**
     * @param $corpId
     * @return mixed
     */
    public function getPrefList($corpId)
    {
        $prefList = $this->mAddress1Repository->findByCorpIdAndPrefecturalCode($corpId);
        foreach ($prefList as $address) {
            $dataPostCount = 0;
            $registPostCount = 0;
            $postList = $this->mPostRepository->findByCorpIdAndPrefecturalCode($corpId, $address->address1_cd);
            foreach ($postList as $post) {
                $dataPostCount++;
                if ($post['corp_id'] != null) {
                    $registPostCount++;
                }
            }
            $status = 2;
            if ($registPostCount == 0) {
                $status = 1;
            } elseif ($dataPostCount == $registPostCount) {
                $status = 3;
            }
            $address->status = $status;
        }
        return $prefList;
    }

    /**
     * @param $user
     */
    public function step3Process($user)
    {
        $corpAgreement = $this->checkFirstCorpAgreementNotComplete($user);
        if (!is_null($corpAgreement)) {
            if ($corpAgreement->status == CorpAgreement::STEP2) {
                $corpAgreement->status = CorpAgreement::STEP3;
                $corpAgreement->update_date = Carbon::now()->toDateTimeString();
                $corpAgreement->update_user_id = $user->id;
                $this->corpAgreementRepository->save($corpAgreement);
            }
        }
    }

    /**
     * @param $user
     */
    public function step4Process($user)
    {
        $corpAgreement = $this->checkFirstCorpAgreementNotComplete($user);
        if (!is_null($corpAgreement)) {
            if ($corpAgreement->status == CorpAgreement::STEP3) {
                $corpAgreement->status = CorpAgreement::STEP4;
                $corpAgreement->update_date = Carbon::now()->toDateTimeString();
                $corpAgreement->update_user_id = $user->id;
                $this->corpAgreementRepository->save($corpAgreement);
            }
        }
    }

    /**
     * @param $user
     */
    public function step5Process($user)
    {
        $corpAgreement = $this->checkFirstCorpAgreementNotComplete($user);
        if (!is_null($corpAgreement)) {
            if ($corpAgreement->status == CorpAgreement::STEP4) {
                $corpAgreement->status = CorpAgreement::STEP5;
                $corpAgreement->update_date = Carbon::now()->toDateTimeString();
                $corpAgreement->update_user_id = $user->id;
                $this->corpAgreementRepository->save($corpAgreement);
            }
        }
    }


    /**
     * @param $user
     */
    public function stepConfirmProcess($user)
    {
        $corpAgreement = $this->checkFirstCorpAgreementNotComplete($user);
        if (!is_null($corpAgreement) && $corpAgreement->status == CorpAgreement::STEP5) {
            $mCorp = $this->mCorpRepository->find($corpAgreement->corp_id);
            $corpAgreement = $this->updateCorpAgreementInStepConfirm($mCorp, $corpAgreement, $user);
            $this->updateMCorpInStepConfirm($mCorp, $user);
            $this->sendMailInStepConfirm($corpAgreement);
        }
    }

    /**
     * @param $mCorp
     * @param $corpAgreement
     * @param $user
     * @return \App\Models\Base
     */
    private function updateCorpAgreementInStepConfirm($mCorp, $corpAgreement, $user)
    {
        if ($mCorp->commission_accept_flg == 2 || $mCorp->commission_accept_flg == 3) {
            $corpAgreement->status = CorpAgreement::COMPLETE;
            $corpAgreement->acceptation_date = Carbon::now()->toDateTimeString();
            $corpAgreement->agreement_flag = true;
            $corpAgreement->acceptation_user_id = "SYSTEM";
            $this->commissionAcceptProcess($corpAgreement);
        } else {
            $corpAgreement->status = CorpAgreement::APPLICATION;
        }
        $corpAgreement->update_date = Carbon::now()->toDateTimeString();
        $corpAgreement->update_user_id = $user->id;
        $corpAgreement->original_agreement = $this->agreementService->getOriginalAgreement();
        $corpAgreement->customize_agreement = $this->agreementService->getCustomizeAgreement($mCorp->id);
        $corpAgreement->accept_check = true;
        $corpAgreement = $this->corpAgreementRepository->save($corpAgreement);
        return $corpAgreement;
    }

    /**
     * update m_corps
     *
     * @param $mCorp
     * @param $user
     */
    private function updateMCorpInStepConfirm($mCorp, $user)
    {
        if ($mCorp->commission_accept_flg == 2 || $mCorp->commission_accept_flg == 3) {
            $mCorp->commission_accept_flg = 1;
            $mCorp->commission_accept_date = Carbon::now()->toDateTimeString();
            $mCorp->commission_accept_user_id = "SYSTEM";
            $mCorp->modified = $mCorp->commission_accept_date;
            $mCorp->modified_user_id = $user->id;
            $this->mCorpRepository->save($mCorp);
        }
    }

    /**
     * send email
     *
     * @param $corpAgreement
     */
    private function sendMailInStepConfirm($corpAgreement)
    {
        if ($corpAgreement->status == CorpAgreement::APPLICATION) {
            try {
                $corpId = $corpAgreement->corp_id;
                $mailSubject = sprintf('《%d》加盟店からの契約申請がありました。', $corpId);
                $url = config('datacustom.php_affiliation_agreement_url');
                $mailContents = sprintf('契約約款確認URL: %s/%d', $url, $corpId);
                $mailTo = config('datacustom.email_address_confirm_agreement.kameiten');
                $mailFrom = config('datacustom.email_address_confirm_agreement.mailback');
                MailHelper::sendRawMail($mailContents, $mailSubject, $mailFrom, $mailTo, null);
            } catch (\Exception $e) {
                Log::error($e);
            }
        }
    }

    /**
     * @param $corpAgreement
     */
    private function commissionAcceptProcess($corpAgreement)
    {
        $corpId = $corpAgreement->corp_id;
        $tempLink = $this->tempLinkRepository->findLatestByCorpId($corpId);
        $mCorpCategoryList = $this->mCorpCategoryRepository->findAllByCorpId($corpId);
        $mCorpCategoriesTempList = $this->mCorpCategoriesTempRepository->findAllByCorpIdAndTempIdWithFlag($corpId, $tempLink->id);
        $corpCategoryIdDeleteList = [];
        $updateMCorpCategoryList = [];
        $this->deleteMCorpCategoryWhenCommissionAccept($corpCategoryIdDeleteList, $mCorpCategoryList, $mCorpCategoriesTempList);
        $this->insertOrUpdateMCorpCategoryWhenCommissionAccept($updateMCorpCategoryList, $mCorpCategoriesTempList);
        $this->deleteMCorpTargetAreasWhenCommissionAccept($corpCategoryIdDeleteList);
        $this->insertOrUpdateMCorpTargetAreasWhenCommissionAccept($corpId, $updateMCorpCategoryList);
        $this->updateAffiliationAreaStats($updateMCorpCategoryList);
    }

    /**
     * @param $corpCategoryIdDeleteList
     * @param $mCorpCategoryList
     * @param $mCorpCategoriesTempList
     */
    private function deleteMCorpCategoryWhenCommissionAccept(&$corpCategoryIdDeleteList, $mCorpCategoryList, &$mCorpCategoriesTempList)
    {
        foreach ($mCorpCategoryList as $mCorpCategory) {
            $isDelete = true;
            foreach ($mCorpCategoriesTempList as $mCorpCategoryTemp) {
                if ($mCorpCategoryTemp->delete_flag) {
                    continue;
                }
                if ($mCorpCategory->category_id == $mCorpCategoryTemp->category_id) {
                    $isDelete = false;
                    $mCorpCategoryTemp->m_corp_category_id = $mCorpCategory->m_corp_category_id;
                    break;
                }
            }
            if ($isDelete) {
                array_push($corpCategoryIdDeleteList, $mCorpCategory->m_corp_category_id);
            }
        }
        $this->mCorpCategoryRepository->deleteListItemByArrayId($corpCategoryIdDeleteList);
    }

    /**
     * insert or update m_corp_category
     *
     * @param $updateMCorpCategoryList
     * @param $mCorpCategoriesTempList
     */
    private function insertOrUpdateMCorpCategoryWhenCommissionAccept(&$updateMCorpCategoryList, $mCorpCategoriesTempList)
    {
        foreach ($mCorpCategoriesTempList as $mCorpCategoryTemp) {
            if ($mCorpCategoryTemp->delete_flag) {
                $mCorpCategoryTemp->action = "Delete";
                $this->saveMCorpCategoriesTemp($mCorpCategoryTemp);
            } else {
                $mCorpCategory = $this->createMCorpCategory($mCorpCategoryTemp);
                $this->mCorpCategoryRepository->save($mCorpCategory);
                array_push($updateMCorpCategoryList, $mCorpCategory);
                $this->saveMCorpCategoriesTemp($mCorpCategoryTemp);
            }
        }
    }

    /**
     * save m_corp_categories_temp
     *
     * @param $mCorpCategoryTemp
     */
    private function saveMCorpCategoriesTemp($mCorpCategoryTemp)
    {
        $result = $this->mCorpCategoriesTempRepository->find($mCorpCategoryTemp->m_corp_categories_temp_id);
        $result->action = $mCorpCategoryTemp->action;
        $this->mCorpCategoriesTempRepository->save($result);
    }

    /**
     * @param $mCorpCategoryTemp
     * @return \App\Models\Base|MCorpCategory|null
     */
    private function createMCorpCategory(&$mCorpCategoryTemp)
    {
        $mCorpCategory = null;
        if ($mCorpCategoryTemp->m_corp_category_id != null) {
            $mCorpCategory = $this->mCorpCategoryRepository->find($mCorpCategoryTemp->m_corp_category_id);
            $mCorpCategoryTemp->action = $this->getUpdateColumn($mCorpCategory, $mCorpCategoryTemp);
        }
        if ($mCorpCategory == null) {
            $mCorpCategory = new MCorpCategory();
            $mCorpCategory->created = Carbon::now()->toDateTimeString();
            $mCorpCategory->created_user_id = Auth::user()->user_id;
            $mCorpCategoryTemp->action = "Add";
        }
        $mCorpCategory->corp_id = $mCorpCategoryTemp->corp_id;
        $mCorpCategory->genre_id = $mCorpCategoryTemp->genre_id;
        $mCorpCategory->category_id = $mCorpCategoryTemp->category_id;
        $mCorpCategory->order_fee = $mCorpCategoryTemp->order_fee;
        $mCorpCategory->order_fee_unit = $mCorpCategoryTemp->order_fee_unit;
        $mCorpCategory->introduce_fee = $mCorpCategoryTemp->introduce_fee;
        $mCorpCategory->note = $mCorpCategoryTemp->note;
        $mCorpCategory->select_list = $mCorpCategoryTemp->select_list;
        $mCorpCategory->select_genre_category = $mCorpCategoryTemp->select_genre_category;
        $mCorpCategory->target_area_type = $mCorpCategoryTemp->target_area_type;
        $mCorpCategory->corp_commission_type = $mCorpCategoryTemp->corp_commission_type;
        $mCorpCategory->modified = Carbon::now()->toDateTimeString();
        $mCorpCategory->modified_user_id = Auth::user()->user_id;

        return $mCorpCategory;
    }

    /**
     * @param $mCorpCategory
     * @param $mCorpCategoryTemp
     * @return null|string
     */
    private function getUpdateColumn($mCorpCategory, $mCorpCategoryTemp)
    {
        $result = "";
        if ($mCorpCategory->order_fee != $mCorpCategoryTemp->order_fee) {
            $result .= (checkIsNullOrEmptyStr($result) ? "" : ",") . "order_fee";
        }
        if ($mCorpCategory->order_fee_unit != $mCorpCategoryTemp->order_fee_unit) {
            $result .= (checkIsNullOrEmptyStr($result) ? "" : ",") . "order_fee_unit";
        }
        if ($mCorpCategory->introduce_fee != $mCorpCategoryTemp->introduce_fee) {
            $result .= (checkIsNullOrEmptyStr($result) ? "" : ",") . "introduce_fee";
        }
        if (!checkIsNullOrEmptyStr($mCorpCategory->note) && !checkIsNullOrEmptyStr($mCorpCategoryTemp->note)
            && $mCorpCategory->note != $mCorpCategoryTemp->note
        ) {
            $result .= (checkIsNullOrEmptyStr($result) ? "" : ",") . "note";
        }
        if (!checkIsNullOrEmptyStr($mCorpCategory->select_list) && !checkIsNullOrEmptyStr($mCorpCategoryTemp->select_list)
            && $mCorpCategory->select_list != $mCorpCategoryTemp->select_list
        ) {
            $result .= (checkIsNullOrEmptyStr($result) ? "" : ",") . "select_list";
        }
        if ($mCorpCategory->corp_commission_type != $mCorpCategoryTemp->corp_commission_type) {
            $result .= (checkIsNullOrEmptyStr($result) ? "" : ",") . "corp_commission_type";
        }

        return checkIsNullOrEmptyStr($result) ? null : $result;
    }

    /**
     * @param $corpCategoryIdDeleteList
     */
    private function deleteMCorpTargetAreasWhenCommissionAccept($corpCategoryIdDeleteList)
    {
        foreach ($corpCategoryIdDeleteList as $corpCategoryId) {
            $this->mTargetAreaRepository->deleteByCorpCategoryId($corpCategoryId);
        }
    }

    /**
     * @param $corpId
     * @param $updateMCorpCategoryList
     */
    private function insertOrUpdateMCorpTargetAreasWhenCommissionAccept($corpId, $updateMCorpCategoryList)
    {
        $mCorpTargetAreaList = $this->mCorpTargetAreaRepository->getAllByCorpId($corpId);
        foreach ($updateMCorpCategoryList as $mCorpCategory) {
            $mTargetAreaList = $this->mTargetAreaRepository->findAllByCorpCategoryId($mCorpCategory->id);
            if (!checkIsNullOrEmptyCollection($mTargetAreaList)) {
                foreach ($mCorpTargetAreaList as $mCorpTargetArea) {
                    $mCorpTargetAreas = new MCorpTargetArea();
                    $mCorpTargetAreas->corp_id = $corpId;
                    $mCorpTargetAreas->jis_cd = $mCorpTargetArea->jis_cd;
                    $mCorpTargetAreas->created = Carbon::now()->toDateTimeString();
                    $mCorpTargetAreas->modified = $mCorpTargetAreas->created;
                    $mCorpTargetAreas->created_user_id = Auth::user()->user_id;
                    $mCorpTargetAreas->modified_user_id = $mCorpTargetAreas->created_user_id;

                    $this->mCorpTargetAreaRepository->save($mCorpTargetAreas);
                }
            }
        }
    }

    /**
     * @param $updateMCorpCategoryList
     */
    private function updateAffiliationAreaStats($updateMCorpCategoryList)
    {
        foreach ($updateMCorpCategoryList as $mCorpCategory) {
            for ($prefectureId = 1; $prefectureId < 48; $prefectureId++) {
                $affiliationAreaStatList = $this->affiliationAreaStatRepository->findByCorpIdAndGenerIdAndPrefecture(
                    $mCorpCategory->corp_id,
                    $mCorpCategory->genre_id,
                    $prefectureId
                );
                if (is_null($affiliationAreaStatList)) {
                    $affiliationAreaStat = new AffiliationAreaStat();
                    $affiliationAreaStat->corp_id = $mCorpCategory->corp_id;
                    $affiliationAreaStat->genre_id = $mCorpCategory->genre_id;
                    $affiliationAreaStat->prefecture = $prefectureId;
                    $affiliationAreaStat->commission_count_category = 0;
                    $affiliationAreaStat->orders_count_category = 0;
                    $affiliationAreaStat->modified_user_id = Auth::user()->user_id;
                    $affiliationAreaStat->modified = Carbon::now()->toDateTimeString();
                    $affiliationAreaStat->created_user_id = $affiliationAreaStat->modified_user_id;
                    $affiliationAreaStat->created = $affiliationAreaStat->modified;

                    $this->affiliationAreaStatRepository->save($affiliationAreaStat);
                }
            }
        }
    }

    /**
     * @param $corpId
     * @return object
     */
    public function getFileList($corpId)
    {
        return $this->attachedFileRepository->getAllAgreementAttachedFileByCorpIdAndKind($corpId, AgreementAttachedFile::CERT);
    }

    /**
     * @param $corpId
     * @param $corpAgreementId
     * @param $fileUpload
     * @return MessageBag
     */
    public function uploadFile($corpId, $corpAgreementId, $fileUpload)
    {
        $agreementAttachFile = null;
        try {
            $agreementAttachFile = $this->createAgreementAttachFileObject($corpId, $corpAgreementId, $fileUpload);
            $agreementAttachFile = $this->attachedFileRepository->save($agreementAttachFile);

            $fullPath = $this->moveFileInFolder($fileUpload, $corpId, $agreementAttachFile->id);
            $agreementAttachFile->path = $fullPath;
            $this->attachedFileRepository->save($agreementAttachFile);
        } catch (\Exception $exception) {
            Log::error($exception);
            $this->attachedFileRepository->delete($agreementAttachFile);
            $errors = new MessageBag();
            $errors->add('upload_file_error', Lang::get('agreement_system.upload_file_error'));
            return $errors;
        }
    }

    /**
     * @param $corpId
     * @param $agreementAttachedFileId
     * @return MessageBag
     */
    public function deleteFile($corpId, $agreementAttachedFileId)
    {
        $errors = new MessageBag();
        try {
            if ($agreementAttachedFileId == null) {
                $errors->add('errors_selected', Lang::get('agreement_system.errors_selected'));
            } else {
                $agreementAttachedFile = $this->attachedFileRepository->find($agreementAttachedFileId);
                if ($agreementAttachedFile != null) {
                    $corpAgreementCompleteLatest = $this->corpAgreementRepository->findByCorpIdAndStatusCompleteAndNotNullAcceptationDate($corpId);
                    if ($corpAgreementCompleteLatest == null
                        || (strtotime($agreementAttachedFile->update_date) > strtotime($corpAgreementCompleteLatest->acceptation_date))) {
                        $this->attachedFileRepository->delete($agreementAttachedFile);
                        try {
                            Storage::delete($this->getPathFileInStorage($agreementAttachedFile));
                        } catch (\Exception $exception) {
                            Log::error($exception);
                            $errors->add('not_found_file', Lang::get('agreement_system.not_found_file'));
                        }
                    } else {
                        $errors->add('errors_file_delete', Lang::get('agreement_system.errors_file_delete'));
                    }
                }
            }
        } catch (\Exception $exception) {
            Log::error($exception);
            $errors->add('errors_system_error', Lang::get('agreement_system.errors_system_error'));
        }
        return $errors;
    }

    /**
     * @param $agreementAttachedFile
     * @return string
     */
    private function getPathFileInStorage($agreementAttachedFile)
    {
        $elements = explode(".", $agreementAttachedFile->name);
        $postfix = strtolower($elements[1]);

        $rootPath = config('datacustom.file_path_agreement');
        $corpPath = $rootPath . "/" . $agreementAttachedFile->corp_id . "/";
        $pathFile = $agreementAttachedFile->id . "." . $postfix;
        return $corpPath . $pathFile;
    }


    /**
     * @param $file
     * @param $corpId
     * @param $agreementAttachedFileId
     * @return string
     */
    private function moveFileInFolder($file, $corpId, $agreementAttachedFileId)
    {
        $fileName = $file->getClientOriginalName();
        $elements = explode(".", $fileName);
        $postfix = strtolower($elements[1]);

        $rootPath = config('datacustom.file_path_agreement');
        $corpPath = $rootPath . "/" . $corpId;
        $fileNameCreate = $agreementAttachedFileId . "." . $postfix;

        $pathFile = Storage::putFileAs($corpPath, $file, $fileNameCreate);
        $fullPath = storage_path('upload') . "/" . $pathFile;

        return $fullPath;
    }

    /**
     * @param $corpId
     * @param $corpAgreementId
     * @param $fileUpload
     * @return AgreementAttachedFile
     */
    private function createAgreementAttachFileObject($corpId, $corpAgreementId, $fileUpload)
    {
        $name = $fileUpload->getClientOriginalName();
        $contentType = $fileUpload->getClientMimeType();
        $agreementAttachFile = new AgreementAttachedFile();
        $agreementAttachFile->corp_id = $corpId;
        $agreementAttachFile->corp_agreement_id = $corpAgreementId;
        $agreementAttachFile->kind = AgreementAttachedFile::CERT;
        $agreementAttachFile->delete_flag = false;
        $agreementAttachFile->create_date = Carbon::now()->toDateTimeString();
        $agreementAttachFile->update_date = $agreementAttachFile->create_date;
        $agreementAttachFile->create_user_id = Auth::user()->id;
        $agreementAttachFile->update_user_id = $agreementAttachFile->create_user_id;
        $agreementAttachFile->name = $name;
        $agreementAttachFile->content_type = $contentType;
        return $agreementAttachFile;
    }

    /**
     * @param $agreementFileId
     * @return string
     */
    public function getThumbnail2($agreementFileId)
    {
        $agreementAttachFile = $this->attachedFileRepository->find($agreementFileId);
        $pathFile = $this->getPathFileInStorage($agreementAttachFile);
        $fileAttach['content'] = Storage::get($pathFile);
        $fileAttach['contentType'] = $agreementAttachFile->content_type;
        return $fileAttach;
    }
}
