<?php

namespace App\Services\Ajax;

class AjaxService
{

    /**
     * @param null $address1
     * @param null $address2
     * @return array
     */
    public function parseData($address1 = null, $address2 = null)
    {
        $data = [];
        if ($address1) {
            $data['address1'] = $this->getPrefectureJp($address1);
        }
        if ($address2) {
            $data['repAddress2'] = str_replace('ﾂ', 'ツ', str_replace('ﾉ', 'ノ', str_replace('ヶ', 'ケ', $address1)));
            $data['repAddress22'] = str_replace('ツ', 'ﾂ', str_replace('ノ', 'ﾉ', str_replace('ケ', 'ヶ', $data['repAddress2'])));
        }

        return $data;
    }

    /**
     * @param $address
     * @return \Illuminate\Config\Repository|mixed
     */
    private function getPrefectureJp($address)
    {
        return config('datacustom.prefecture_div.' . $address);
    }

    /**
     * get ProcessType in AutoCommissionCorp
     * @param $commissionCorps
     * @param $targetCommissionCorp
     * @param $value
     * @return mixed
     */
    public function checkTargetCommissionCorp($commissionCorps, &$targetCommissionCorp, $value)
    {
        if ($targetCommissionCorp == null) {
            foreach ($commissionCorps as $key => $commissionCorp) {
                if ($commissionCorp->id == $value->corp_id) {
                    $targetCommissionCorp = $commissionCorp;
                    break;
                }
            }
        }
    }
}
