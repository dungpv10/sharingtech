<?php

namespace App\Services;

use App\Models\AuctionAgreementItem;
use App\Models\AuctionAgreementLink;
use App\Models\DemandInfo;
use App\Models\MTaxRate;
use App\Repositories\AccumulatedInformationsRepositoryInterface;
use App\Repositories\AffiliationAreaStatRepositoryInterface;
use App\Repositories\BillRepositoryInterface;
use App\Repositories\DemandInfoRepositoryInterface;
use App\Repositories\MCorpCategoryRepositoryInterface;
use App\Repositories\MCorpRepositoryInterface;
use App\Repositories\MPostRepositoryInterface;
use App\Repositories\RefusalsRepositoryInterface;
use Auth;
use Illuminate\Support\Facades\Log;
use Lang;
use App\Repositories\AuctionInfoRepositoryInterface;
use App\Repositories\AutoCallRepositoryInterface;
use App\Repositories\CommissionInfoRepositoryInterface;
use App\Repositories\MCategoryRepositoryInterface;
use App\Repositories\MSiteRepositoryInterface;
use App\Repositories\MItemRepositoryInterface;
use App\Repositories\MGenresRepositoryInterface;
use App\Services\Credit\CreditService;

use Exception;
use \DateTime;
use DB;

class AuctionService
{
    /**
     * @var DemandInfo
     */
    protected $demandInfo;
    /**
     * @var CommissionInfoRepositoryInterface
     */
    protected $commissionInfoRepository;

    /**
     * @var MSiteRepositoryInterface
     */
    protected $mSiteRepository;

    /**
     * @var MItemRepositoryInterface
     */
    protected $mItemRepository;

    /**
     * @var AuctionInfoRepositoryInterface
     */
    protected $auctionInfoRepository;

    /**
     * @var RefusalsRepositoryInterface
     */
    protected $refusalRepository;

    /**
     * @var AccumulatedInformationsRepositoryInterface
     */
    protected $accumulatedInfoRepo;
    /**
     * @var MCorpRepositoryInterface
     */
    protected $mCorpRepository;

    /**
     * @var MPostRepositoryInterface
     */
    protected $mPostRepository;

    /**
     * @var CreditService
     */
    protected $creditService;

    /**
     * @var DemandInfoService
     */
    protected $demandInfoService;

    /**
     * @var AutoCallRepositoryInterface
     */
    protected $autoCallItemRepo;

    /**
     * @var MGenresRepositoryInterface
     */
    protected $genreRepo;

    /**
     * @var MCategoryRepositoryInterface
     */
    protected $mCategoryRepo;
    /**
     * @var DemandInfoRepositoryInterface
     */
    protected $demandInfoRepository;
    /**
     * @var AuctionAgreementLink
     */
    protected $mAuctionAgreementLink;
    /**
     * @var MCorpCategoryRepositoryInterface
     */
    protected $mCorpCategoryRepository;
    /**
     * @var AffiliationAreaStatRepositoryInterface
     */
    protected $affiliationAreaStatRepo;
    /**
     * @var BillRepositoryInterface
     */
    protected $billInfoRepository;

    /**
     * AuctionService constructor.
     * @param MCorpCategoryRepositoryInterface $mCorpCategoryRepository
     * @param CommissionInfoRepositoryInterface $commissionInfoRepository
     * @param MSiteRepositoryInterface $mSiteRepository
     * @param MItemRepositoryInterface $mItemRepository
     * @param AuctionInfoRepositoryInterface $auctionInfoRepository
     * @param RefusalsRepositoryInterface $refusalsRepository
     * @param AccumulatedInformationsRepositoryInterface $accumulatedInfoRepo
     * @param MCorpRepositoryInterface $mCorpRepository
     * @param MPostRepositoryInterface $mPostRepository
     * @param AutoCallRepositoryInterface $autoCallItemRepo
     * @param MGenresRepositoryInterface $genreRepo
     * @param MCategoryRepositoryInterface $mCategoryRepo
     * @param CreditService $creditService
     * @param AuctionAgreementLink $mAuctionAgreementLink
     * @param DemandInfoRepositoryInterface $demandInfoRepository
     * @param AffiliationAreaStatRepositoryInterface $affiliationAreaStatRepo
     * @param DemandInfo $demandInfo
     * @param BillRepositoryInterface $billInfoRepository
     */
    public function __construct(
        MCorpCategoryRepositoryInterface $mCorpCategoryRepository,
        CommissionInfoRepositoryInterface $commissionInfoRepository,
        MSiteRepositoryInterface $mSiteRepository,
        MItemRepositoryInterface $mItemRepository,
        AuctionInfoRepositoryInterface $auctionInfoRepository,
        RefusalsRepositoryInterface $refusalsRepository,
        AccumulatedInformationsRepositoryInterface $accumulatedInfoRepo,
        MCorpRepositoryInterface $mCorpRepository,
        MPostRepositoryInterface $mPostRepository,
        AutoCallRepositoryInterface $autoCallItemRepo,
        MGenresRepositoryInterface $genreRepo,
        MCategoryRepositoryInterface $mCategoryRepo,
        CreditService $creditService,
        AuctionAgreementLink $mAuctionAgreementLink,
        DemandInfoRepositoryInterface $demandInfoRepository,
        AffiliationAreaStatRepositoryInterface $affiliationAreaStatRepo,
        DemandInfo $demandInfo,
        BillRepositoryInterface $billInfoRepository
    ) {
        $this->commissionInfoRepository = $commissionInfoRepository;
        $this->mSiteRepository = $mSiteRepository;
        $this->mItemRepository = $mItemRepository;
        $this->auctionInfoRepository = $auctionInfoRepository;
        $this->refusalRepository = $refusalsRepository;
        $this->accumulatedInfoRepo = $accumulatedInfoRepo;
        $this->mCorpRepository = $mCorpRepository;
        $this->mPostRepository = $mPostRepository;
        $this->creditService = $creditService;
        $this->autoCallItemRepo = $autoCallItemRepo;
        $this->genreRepo = $genreRepo;
        $this->mCategoryRepo = $mCategoryRepo;
        $this->demandInfoRepository = $demandInfoRepository;
        $this->mAuctionAgreementLink = $mAuctionAgreementLink;
        $this->mCorpCategoryRepository = $mCorpCategoryRepository;
        $this->affiliationAreaStatRepo = $affiliationAreaStatRepo;
        $this->demandInfo = $demandInfo;
        $this->billInfoRepository = $billInfoRepository;
    }

    /**
     * check role
     *
     * @param  string $role
     * @param  array $roleOption
     * @return boolean
     */
    public static function isRole($role, $roleOption)
    {
        return in_array($role, $roleOption) ? true : false;
    }

    /**
     * ge div value
     *
     * @param  string $code
     * @param  string $text
     * @return string
     */
    public static function getDivValue($code, $text)
    {
        $data = array_flip(config('rits.'.$code));

        return @$data[$text];
    }

    /**
     * format option serach
     *
     * @param  array $input
     * @return array
     */
    public static function formatOptionSearchUp($input)
    {
        $sort = isset($input['sort']) ? $input['sort'] : null;
        $orderType = isset($input['order']) ? $input['order'] : 'asc';
        if ($sort && strpos($sort, 'demand_infos') !== false) {
            if ($sort == 'demand_infos.visit_time_min') {
                $order = ['visit_time.visit_time_min' => $orderType];
            } else {
                $order = [$sort => $orderType];
            }
        } else {
            $order = ['demand_infos.auction_deadline_time' => $orderType];
        }

        return $order;
    }

    /**
     * @param $input
     * @return array
     */
    public static function formatOptionSearch($input)
    {
        $sort = isset($input['sort']) ? $input['sort'] : null;
        $orderType = isset($input['order']) ? $input['order'] : 'asc';
        return self::getOrderInformation($sort, $orderType);
    }

    /**
     * get order information
     * @param  string $sort
     * @param  string $orderType
     * @return array
     */
    protected static function getOrderInformation($sort, $orderType)
    {
        switch ($sort) {
            case 'demand_id':
                $order = ['auction_infos.demand_id' => $orderType];
                break;
            case 'visit_time':
                $order = ['visit_times.visit_time' => $orderType];
                break;
            case 'contact_desired_time':
                $order = ['demand_infos.contact_desired_time' => $orderType];
                break;
            case 'genre_name':
                $order = ['m_genres.genre_name' => $orderType];
                break;
            case 'customer_name':
                $order = ['demand_infos.customer_name' => $orderType];
                break;
            case 'tel1':
                $order = ['demand_infos.tel1' => $orderType];
                break;
            case 'address':
                $order = [
                    'demand_infos.address1' => $orderType,
                    'demand_infos.address2' => $orderType,
                    'demand_infos.address3' => $orderType,
                ];
                break;
            default:
                $order = ['visit_times.visit_time' => $orderType];
                break;
        }

        return $order;
    }

    /**
     * Get data for calendar event
     *
     * @param $auctionAlreadyData
     * @return array
     */
    public static function getCalendarEventData($auctionAlreadyData)
    {
        $countAuctionAlreadyData = count($auctionAlreadyData);
        if (!$countAuctionAlreadyData) {
            return [];
        }
        $ordersCorrespondenceStr = __('auction.Orders correspondence string');
        foreach ($auctionAlreadyData as $auction) {
            $dataArray = self::formatDataAuctionAlready($auction);
            if (isset($auction->order_respond_datetime)) {
                $dataArray['display_date'] = $auction->order_respond_datetime;
                $dataArray['dialog_display_date'] = $ordersCorrespondenceStr . dateTimeWeek($auction->order_respond_datetime);
            }

            $dataArray['commission_id'] = $auction->id;
            $dataArray['demand_id'] = $auction->demand_id;
            $dataArray['customer_name'] = $auction->customer_name;
            $dataArray['site_name'] = $auction->site_name;

            if (isset($dataArray['dialog_display_date']) && strstr($dataArray['dialog_display_date'], '〜')) {
                $fromDate = mb_substr($dataArray['dialog_display_date'], 0, 25);
                $dataArray['sort_date'] = (isset($fromDate) ? substr($fromDate, -5) : '');
            } else {
                $dataArray['sort_date'] = (isset($dataArray['dialog_display_date']) ? substr($dataArray['dialog_display_date'], -5) : '');
            }

            $displayDateSplitArr = preg_split('/[\s]+/', $dataArray['display_date'], -1, PREG_SPLIT_NO_EMPTY);

            if (empty($displayDateSplitArr[0])) {
                $displayDateSplitArr = [];
                $displayDateSplitArr[0] = 'dumm';
            }

            $key = $displayDateSplitArr[0];
            if (isset($results[$key])) {
                array_push($results[$key], $dataArray);
            } else {
                $results[$key] = [$dataArray];
            }
        }

        return self::formatCalendarEventData($results);
    }

    /**
     * format data auction already
     * @param  object $auction
     * @return array
     */
    public static function formatDataAuctionAlready($auction)
    {
        $visitHopeStr = __('auction.Visit hope string');
        $telHopeStr = __('auction.Tel hope string');
        $dataArray['display_date'] = null;
        if ($auction->is_contact_time_range_flg == 0 &&
            isset($auction->contact_desired_time)) {
            if (isset($auction->visit_desired_time)) {
                $dataArray['display_date'] = $auction->visit_desired_time;
                $dataArray['dialog_display_date'] = $visitHopeStr . dateTimeWeek($auction->visit_desired_time);
            } else {
                $dataArray['display_date'] = $auction->contact_desired_time;
                $dataArray['dialog_display_date'] = $visitHopeStr . dateTimeWeek($auction->contact_desired_time);
            }
        } elseif ($auction->is_contact_time_range_flg == 1 &&
            isset($auction->contact_desired_time_from) &&
            isset($auction->contact_desired_time_to)) {
            if (isset($auction->visit_desired_time)) {
                $dataArray['display_date'] = $auction->visit_desired_time;
                $dataArray['dialog_display_date'] = $visitHopeStr . dateTimeWeek($auction->visit_desired_time);
            } else {
                $dataArray['display_date'] = $auction->contact_desired_time_from;
                $dataArray['dialog_display_date'] = $telHopeStr . dateTimeWeek($auction->contact_desired_time_from) . "〜" . dateTimeWeek($auction->contact_desired_time_to);
            }
        } elseif ($auction->is_visit_time_range_flg == 0 &&
            isset($auction->visit_time)) {
            $dataArray['display_date'] = $auction->visit_time;
            $dataArray['dialog_display_date'] = $visitHopeStr . dateTimeWeek($auction->visit_time);
        } elseif ($auction->is_visit_time_range_flg == 1 &&
            isset($auction->visit_time_from)) {
            if (isset($auction->visit_desired_time)) {
                $dataArray['display_date'] = $auction->visit_desired_time;
                $dataArray['dialog_display_date'] = $visitHopeStr . dateTimeWeek($auction->visit_desired_time);
            } else {
                $dataArray['display_date'] = $auction->visit_adjust_time;
                $dataArray['dialog_display_date'] = $visitHopeStr . dateTimeWeek($auction->visit_adjust_time);
            }
        }
        return $dataArray;
    }
    /**
     * format calendar event data
     * @param  array $results
     * @return array
     */
    public static function formatCalendarEventData($results)
    {
        $sortResult = [];

        foreach ($results as $resultKey => $valueArr) {
            if (count($valueArr) == 0) {
                $sortResult[$resultKey] = $valueArr;
                continue;
            }

            $keySortDate = [];
            $keyDemandId = [];
            foreach ($valueArr as $key => $value) {
                $keySortDate[$key] = $value['sort_date'];
                $keyDemandId[$key] = $value['demand_id'];
            }
            array_multisort($keySortDate, SORT_ASC, $keyDemandId, SORT_ASC, $valueArr);

            $sortResult[$resultKey] = $valueArr;
        }

        return $sortResult;
    }

    /**
     * Get data sort
     * @param  array $data
     * @return array
     */
    public static function getDataSort($data)
    {
        $dataSort['sort'] = null;
        $dataSort['order'] = null;
        if (!empty($data['sort'])) {
            $dataSort['sort'] = $data['sort'];
        }
        if (!empty($data['order'])) {
            $dataSort['order'] = $data['order'];
        }

        return $dataSort;
    }

    /**
     * get order by sort item
     *
     * @param  string $sort
     * @param  string $order
     * @param  string $sortValue
     * @return array|string
     */
    public static function getInforOrderSort($sort, $order, $sortValue)
    {
        $orderDisplay = 'desc';
        $icon = '';
        if ($sort == $sortValue) {
            $icon = trans('common.asc');
            if ($order == 'desc') {
                $orderDisplay = 'asc';
                $icon = trans('common.desc');
            }
        }

        return [
            'order_display' => $orderDisplay,
            'icon' => $icon,
        ];
    }

    /**
     * get status detach auction btn
     *
     * @param $data
     * @return int
     */
    public static function detectAuctionBtn($data)
    {
        $user = Auth::user();
        if ($user['auth'] != 'affiliation') {
            return 0;
        }

        $commissionInfoIds = DB::table('commission_infos')->where('demand_id', $data->id)->where('commit_flg', 1)->pluck('corp_id')->toArray();
        if (in_array($user['affiliation_id'], $commissionInfoIds)) {
            return 1;
        }

        $mSite = DB::table('m_sites')->select('manual_selection_limit', 'auction_selection_limit')->where('id', $data->site_id)->first();
        if (!$mSite) {
            $max = 0;
        }
        if ($data->selection_system == 2 || $data->selection_system == 3) {
            $max = $mSite->auction_selection_limit;
        } else {
            $max = $mSite->manual_selection_limit;
        }
        if (count($commissionInfoIds) >= $max) {
            return 2;
        }

        if (strtotime($data->auction_deadline_time) <= strtotime(date('Y-m-d H:i:s'))) {
            return 3;
        }

        if ($data->auction == 1) {
            return 4;
        }

        return 5;
    }

    /**
     * find last commission`
     *
     * @param  array $data
     * @param  string $field
     * @return object
     */
    public static function findLastCommission($data, $field = null)
    {
        $commissionInfo = DB::table('commission_infos')->where('demand_id', $data->id)->where('commit_flg', 1)->orderBy('created', 'desc')->first();

        if ($field) {
            if (isset($commissionInfo->$field)) {
                return $commissionInfo->$field;
            }
        }

        return $commissionInfo;
    }

    /**
     * get config jp text
     *
     * @param  string $code
     * @param  string $key
     * @param  boolean $priority
     * @return array|string
     */
    public static function getDivTextJP($code, $key, $priority = false)
    {
        try {
            if (!$priority) {
                if ($key < 10) {
                    $key = '0'.(int) $key;
                }
                if (config('app.locale') == 'jp') {
                    return config('jpstate.kanji')[$key];
                } else {
                    return config('jpstate.romaji')[$key];
                }
            } else {
                return __('auction.'.config('rits.'.$code)[$key]);
            }
        } catch (Exception $e) {
            return '';
        }
    }

    /**
     * format currency
     *
     * @param  string $amount
     * @return integer
     */
    public static function yenFormat2($amount)
    {
        if (is_numeric($amount)) {
            return number_format($amount).__('common.yen');
        } else {
            return '0'.__('common.yen');
        }
    }

    /**
     * masking address3
     *
     * @param  string $address3
     * @return string
     */
    public static function maskingAddress3($address3)
    {
        if (is_null($address3)) {
            return '';
        }

        return mb_substr($address3, 0, 3, "UTF-8").'*******';
    }

    /**
     * Get drop text
     *
     * @param  string $category
     * @param  integer $itemId
     * @return string
     */
    public static function getDropText($category, $itemId)
    {
        $item = DB::table('m_items')->select('item_name')->where('item_category', $category)->where('item_id', $itemId)->orderBy('sort_order', 'desc')->first();
        if (!$item) {
            return null;
        }

        return $item->item_name;
    }

    /**
     * Get commission data
     *
     * @param $auctionId
     * @return \Illuminate\Database\Eloquent\Model|null|object|static
     */
    public function getCommissionData($auctionId)
    {
        // get maximum number
        $limit = $this->findCommitLimit($auctionId);

        // get number of bids
        $currentNumber = $this->auctionInfoRepository->countByIdAndCommissionCommitFlag($auctionId);

        // When it reaches the upper limit number, it gets the data fixed at the end
        if ($currentNumber >= $limit) {
            return $this->auctionInfoRepository->getByIdAndCommissionCommitFlag($auctionId);
        } else {
            return null;
        }
    }
    /**
     * @param $auctionId
     * @return $lastCommission or null
     */
    public function getCommissionDataSupport($auctionId)
    {
        $auction = $this->auctionInfoRepository->getById($auctionId);
        $demand  = $this->demandInfoRepository->getDemandById($auction->demand_id);
        $max = $this->mSiteRepository->findMaxLimit($demand);

        $currentNum = $this->auctionInfoRepository->countByIdAndCommissionCommitFlag($auctionId);

        if ($currentNum >= $max) {
            $lastCommission = $this->auctionInfoRepository->getByIdAndCommissionCommitFlag($auctionId);
            return $lastCommission;
        }
        return null;
    }

    /**
     * @return AuctionAgreementItem
     */
    public function getItemAuctionAgreement()
    {
        return AuctionAgreementItem::find(1)->item;
    }
    /**
     * @param $auctionProvisions
     * @return null|string|string[]
     */
    public function formatAuctionPolicy($auctionProvisions)
    {
        return preg_replace("/\\r\\n|\\r|\\n/", '<br>', $auctionProvisions);
    }

    /**
     * @param $idCorp
     * @return bool
     */
    public function isPopupStopFlag($idCorp)
    {
        $mCorp = $this->mCorpRepository->findById($idCorp);

        if (empty($mCorp)) {
            return false;
        }

        $popupStopFlag = $mCorp[0]['popup_stop_flg'];
        if (!$popupStopFlag) {
            return false;
        }

        return true;
    }

    /**
     * @param $auctionId
     * @return mixed
     */
    protected function findCommitLimit($auctionId)
    {
        $data = $this->auctionInfoRepository->getFirstById($auctionId);
        $limitCommit = $this->mSiteRepository->findMaxLimit($data);

        return $limitCommit;
    }



    /**
     * @param $data
     * @return boolean|mixed
     * @throws Exception
     */
    public function editRefusal($data)
    {
        $resultsFlg = false;
        DB::beginTransaction();

        try {
            $resultsFlg = $this->refusalRepository->updateData($data['auctionInfo']['id'], $data['refusal']);
            if ($resultsFlg) {
                $resultsFlg = $this->auctionInfoRepository->updateFlag($data['auctionInfo']);
            }

            if ($resultsFlg) {
                DB::commit();
            } else {
                DB::rollback();
            }
        } catch (Exception $e) {
            DB::rollback();
            $resultsFlg = false;
        }

        return $resultsFlg;
    }

    /**
     * @param $demandId
     * @param $corpId
     */
    public function updateAccumulatedInfoRegistDate($demandId, $corpId)
    {
        try {
            DB::beginTransaction();
            $listAcc = $this->accumulatedInfoRepo->getAllInfos($demandId);
            if (!empty($listAcc)) {
                foreach ($listAcc as $acc) {
                    if ($acc->corp_id == $corpId) {
                        $acc->bid_regist_date = date('Y-m-d H:i');
                        $acc->modified_user_id = $corpId;
                    } else {
                        $acc->refusal_date = date('Y-m-d H:i');
                        $acc->modified_user_id = 'SYSTEM';
                    }
                    $acc->save();
                }
                DB::commit();
            }
        } catch (Exception $exception) {
            DB::rollback();
            Log::info($exception);
        }
    }

    /**
     * Update accumulated info with refusal date
     *
     * @param $demandId
     * @param $corpId
     */
    public function updateAccumulatedInfoRefusalDate($demandId, $corpId)
    {
        if (empty($demandId) || empty($corpId)) {
            return;
        }

        try {
            $accumulatedInformation = $this->accumulatedInfoRepo->getInfos($corpId, $demandId);

            if ($accumulatedInformation) {
                $accumulatedInformation->refusal_date = date('Y-m-d H:i');
                $accumulatedInformation->modified_user_id = $corpId;
                $accumulatedInformation->save();
            }
        } catch (Exception $e) {
            logger('AccumulatedInformation fail: '.$e->getMessage());
        }

        return;
    }

    /**
     * Check empty commission fee
     *
     * @param $infoCommission
     * @return bool
     */
    public function checkEmptyCommissionFeeRate($infoCommission)
    {
        if (isset($infoCommission['commit_flg'])) {
            if (!empty($infoCommission['commit_flg'])) {
                if (empty($infoCommission['commission_fee_rate'])) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @param $infoCommission
     * @return bool
     */
    public function checkCommissionIntroduce($infoCommission)
    {
        if ($infoCommission['commission_type'] == getDivValue('commission_type', 'normal_commission')) {
            if ($infoCommission['commission_status'] == getDivValue('construction_status', 'introduction')) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param $infoCommission
     * @return bool
     */
    public function checkCommissionStatusComplete($infoCommission)
    {
        if ($infoCommission ['commission_status'] != getDivValue('construction_status', 'construction') && $infoCommission ['commission_status'] != getDivValue('construction_status', 'introduction') && $infoCommission ['commission_status'] != getDivValue('construction_status', 'order_fail')) {
            if (!empty($infoCommission ['complete_date']) || !empty($infoCommission ['construction_price_tax_exclude'])) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param $infoCommission
     * @return bool
     */
    public function checkCommissionStatusOrderFail($infoCommission)
    {
        if ($infoCommission ['commission_status'] != getDivValue('construction_status', 'order_fail')) {
            if (!empty($infoCommission ['order_fail_date'])) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param $corpId
     */
    public function updatePopupStopFlg($corpId)
    {
        $mCorp = $this->mCorpRepository->getFirstById($corpId);

        if (isset($mCorp)) {
            $mCorp->popup_stop_flg = 1;
            $mCorp->save();
        }
    }

    /**
     * @param $data
     * @return bool
     */
    public function checkNumberAuctionInfos($data)
    {
        if (!empty($data['demandInfo']['do_auction'])) {
            //Retrieve deal ID
            $demandId = (array_key_exists('id', $data['demandInfo'])) ? $data['demandInfo']['id'] : null;
            //Acquire transmission time per rank
            $rankTime = [];
            if ($data['demandInfo']['method'] == 'visit') {
                $rankTime = getPushSendTimeOfVisitTime($data['demandInfo']['auction_start_time'], $data['demandInfo']['auction_deadline_time'], $data['demandInfo']['genre_id'], $data['demandInfo']['address1']);
            } else {
                $rankTime = getPushSendTimeOfContactDesiredTime($data['demandInfo']['auction_start_time'], $data['demandInfo']['auction_deadline_time'], $data['demandInfo']['genre_id'], $data['demandInfo']['address1']);
            }

            // Search characters in municipalities for upper and lower case letters (m_posts.address 2)
            $dataSearch = [
                'address1' => $data['demandInfo']['address1'],
                'address2' => $data['demandInfo']['address2'],
            ];
            $jisCd = $this->mPostRepository->getTargetArea($dataSearch);
            if (empty($jisCd)) {
                // For areas that do not exist, return to manual selection.
                return false;
            }

            $corpData = [
                'genre_id' => $data['demandInfo']['genre_id'],
                'site_id' => $data['demandInfo']['site_id'],
                'category_id' => $data['demandInfo']['category_id'],
                'address1' => $data['demandInfo']['address1'],
                'jis_cd' => $jisCd,
            ];
            $results = $this->mCorpRepository->demandCorpData($corpData);
            if ($results->count() == 0) {
                // If there are 0 merchants that match genre and area, return to manual selection.
                return false;
            } else {
                $beforeList = [];
                $beforeData = $this->auctionInfoRepository->getListByDemandId($demandId);
                foreach ($beforeData as $key => $val) {
                    $beforeList[$val->corp_id] = $val->id;
                }
                $idx = 0;
                $auctionData = [];
                foreach ($results as $key => $val) {
                    //Check if accumulation of credit unit price of transaction data has reached the limit
                    $resultCredit = $this->creditService->checkCredit($val->id, $data['demandInfo']['genre_id'], false, false);
                    if ($resultCredit == config('constant.CREDIT_DANGER')) {
                        //If the credit limit is exceeded, subsequent processing is not performed
                        continue;
                    }
                    // In case of an unapproved franchisee, no further processing is done
                    if ($data['demandInfo']['site_id'] != config('rits.CREDIT_EXCLUSION_SITE_ID')) {
                        $agrementLicense = false;
                        if (is_null($data['demandInfo']['category_id'])) {
                            $agrementLicense = true;
                        } else {
                            $agrementLicense = $this->mCorpRepository->isCommissionStop($val->id);
                        }
                        if ($agrementLicense === false) {
                            continue;
                        }
                    }

                    // If lost_flg, del_flg registered in Commission_infos is "1", it will not be subject to bidding
                    $targetCommissionData = $this->commissionInfoRepository->getFirstByDemandIdAndCorpId($demandId, $val->id);
                    // A franchise with lost_flg and del_flg of "0" in the retrieved pickup data is targeted for bidding
                    if (!$targetCommissionData || ($targetCommissionData->lost_flg == 0) && ($targetCommissionData->del_flg == 0)) {
                        //When the category-based method of setting is set, use it for judgment
                        //Store member shipping method in variable
                        $auctionStatusFlg = '';
                        //If there is an agency method by genre (not 0) overwrite the variable.
                        if ($val->mCorpCategory->first() && $val->mCorpCategory->first()->auction_status != 0) {
                            $auctionStatusFlg = $val->mCorpCategory->first()->auction_status;
                        }

                        //Change judgment formula
                        if (empty($auctionStatusFlg) || $auctionStatusFlg == getDivValue('auction_delivery_status', 'delivery') || $auctionStatusFlg == getDivValue('auction_delivery_status', 'deny')) {
                            if (!empty($val->affiliationAreaStats->first()->commission_unit_price_rank)) {
                                $rank = mb_strtolower($val->affiliationAreaStats->first()->commission_unit_price_rank);
                            } else {
                                $rank = 'z';
                            }
                            if (!empty($rankTime[$rank])) {
                                $idx++;
                            }
                        }
                    } else { // lost_flg,del_flg check
                        if (isset($beforeList[$val->id])) {
                            // At the time of re-bidding, the member shop with lost_flg and del_flg being 1 declines that it has declined, and sets the declaration flag to 1
                            $auctionId = isset($beforeList[$val->id]) ? $beforeList[$val->id] : null;
                            $auctionData = ['refusal_flg' => 1];
                            //To write
                            $this->auctionInfoRepository->saveAuction($auctionId, $auctionData);
                        }
                    }
                }
                if ($idx == 0) {
                    // If there is no member store that matches the unit price rank, return to manual selection.
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @param $demandId
     * @param $data
     * @return array|bool
     */
    public function getAuctionInfoForAutoCommission($demandId, $data)
    {
        // Obtain the date and time of the visit, the desired date and time of contact
        $visitTimeList = [];

        if (!empty($data['visitTime'])) {
            foreach ($data['visitTime'] as $val) {
                if ($val['is_visit_time_range_flg'] == 0 && strlen($val['visit_time']) > 0) {
                    $visitTimeList[] = $val['visit_time'];
                }
                if ($val['is_visit_time_range_flg'] == 1 && strlen($val['visit_time_from']) > 0) {
                    $visitTimeList[] = $val['visit_time_from'];
                }
            }
        }

        if (!empty($visitTimeList)) {
            // When to use the visit date and time
            $preferredDate = getMinVisitTime($visitTimeList);
            $data['demandInfo']['method'] = 'visit';
        } else {
            // When using the contact date and time desired
            if ($data['demandInfo']['is_contact_time_range_flg'] == 0) {
                $preferredDate = $data['demandInfo']['contact_desired_time'];
            }
            if ($data['demandInfo']['is_contact_time_range_flg'] == 1) {
                $preferredDate = $data['demandInfo']['contact_desired_time_from'];
            }
            $data['demandInfo']['method'] = 'tel';
        }

        // To acquire mail transmission time
        // Temporarily set DemandInfo.auction_start_time, DemandInfo.auction_deadline_time
        if (empty($data['demandInfo']['auction_start_time'])) {
            $data['demandInfo']['auction_start_time'] = date('Y-m-d H:i:s');
        }
        if (empty($data['demandInfo']['auction_deadline_time'])) {
            $data['demandInfo']['auction_deadline_time'] = '';
        }
        $priority = $data['demandInfo']['priority'];
        // When the priority is not set yet
        $judgeResult = $this->buildJudeResult($data['demandInfo'], $preferredDate);
        if ($judgeResult['result_flg'] == 1) {
            //Auction start date and time
            if (!empty($judgeResult['result_date'])) {
                $data['demandInfo']['auction_start_time'] = $judgeResult['result_date'];
            }
            //When the priority is changed, the determination for each priority is performed again
            $judgeResult = judgeAuction($data['demandInfo']['auction_start_time'], $preferredDate, $data['demandInfo']['genre_id'], $data['demandInfo']['address1'], $data['demandInfo']['auction_deadline_time'], $priority);
        }
        $autoCommissions = $this->updateAuctionInfos($demandId, $data, true);
        // Perform sort
        if (is_array($autoCommissions)) {
            uasort($autoCommissions, function ($first, $second) {
                if ($first['push_time'] != $second['push_time']) {
                    // AuctionInfo.push_time asc
                    return $first['push_time'] < $second['push_time'] ? -1 : 1;
                } elseif ($first['AffiliationAreaStat']['commission_unit_price_category'] != $second['AffiliationAreaStat']['commission_unit_price_category']) {
                    // AffiliationAreaStat.commission_unit_price_category IS NULL
                    if (empty($first['AffiliationAreaStat']['commission_unit_price_category']) && !empty($second['AffiliationAreaStat']['commission_unit_price_category'])) {
                        return 1;
                    } elseif (!empty($first['AffiliationAreaStat']['commission_unit_price_category']) && empty($second['AffiliationAreaStat']['commission_unit_price_category'])) {
                        return -1;
                    } else { // AffiliationAreaStat.commission_unit_price_category desc
                        return $first['AffiliationAreaStat']['commission_unit_price_category'] > $second['AffiliationAreaStat']['commission_unit_price_category'] ? -1 : 1;
                    }
                } elseif ($first['AffiliationAreaStat']['commission_count_category'] != $second['AffiliationAreaStat']['commission_count_category']) {
                    // AffiliationAreaStat.commission_count_category desc
                    return $first['AffiliationAreaStat']['commission_count_category'] > $second['AffiliationAreaStat']['commission_count_category'] ? -1 : 1;
                } else {
                    return 0;
                }
            });
        }

        return $autoCommissions;
    }

    /**
     * @param $demandId
     * @param $data
     * @param bool $autoSelection
     * @return array|bool
     */
    public function updateAuctionInfos($demandId, $data, $autoSelection = false)
    {
        //In case of automatic consignment, auction is not selected
        Log::debug('____ start update auction info _______');
        if ((!empty($data['demandInfo']['do_auction']) || $autoSelection) && $data['demandInfo']['do_auto_selection_category'] == 0) {
            // Acquire transmission time per rank
            $rankTime = [];
            if ($data['demandInfo']['method'] == 'visit') {
                $rankTime = getPushSendTimeOfVisitTime($data['demandInfo']['auction_start_time'], $data['demandInfo']['auction_deadline_time'], $data['demandInfo']['genre_id'], $data['demandInfo']['address1']);
            } else {
                $rankTime = getPushSendTimeOfContactDesiredTime($data['demandInfo']['auction_start_time'], $data['demandInfo']['auction_deadline_time'], $data['demandInfo']['genre_id'], $data['demandInfo']['address1']);
            }
            // Acquire time to auto call
            $autoCall = $this->getAutoCallInterval($data['demandInfo']['priority']);
            // Acquisition of auto call flag by genre
            $autoCallFlag = $this->getAutoCallFlagMGenre($data['demandInfo']['genre_id']);
            // Acquisition of list of companies corresponding to the case
            // Search characters in municipalities for upper and lower case letters (m_posts.address 2)
            $mPostJscd = $this->mPostRepository->getTargetArea([
                'address1' => $data ['demandInfo'] ['address1'],
                'address2' => $data['demandInfo']['address2'],
            ]);

            if (empty($mPostJscd)) {
                Log::debug('____ empty mPostJscd  _______');
                //In the case of nonexistent areas, we can not auction, so we will exit here.
                // We have already made 0 as a target so that manual selection is made by separate processing.
                return true;
            }
            $corpData = [
                'genre_id' => $data['demandInfo']['genre_id'],
                'site_id' => $data['demandInfo']['site_id'],
                'category_id' => $data['demandInfo']['category_id'],
                'address1' => $data['demandInfo']['address1'],
                'jis_cd' => $mPostJscd,
            ];
            $results = $this->mCorpRepository->demandCorpData($corpData, true);
            if ($results->count() > 0) {
                $beforeList = [];
                $beforeData = $this->auctionInfoRepository->getListByDemandId($demandId);
                foreach ($beforeData as $key => $val) {
                    $beforeList[$val->corp_id] = $val->id;
                }
                $idx = 0;
                $auctionData = [];
                $autoAuctionData = [];
                foreach ($results as $key => $val) {
                    // If there is an agency method by genre (not 0) overwrite the variable.
                    if ($val->m_cate_auction_status != 0) {
                        $auctionStatusFlg = $val->m_cate_auction_status;
                    } else {
                        $auctionStatusFlg = $val->auction_status;
                    }
                    //Check if accumulation of credit unit price of transaction data has reached the limit
                    $resultCredit = $this->creditService->checkCredit($val->id, $data['demandInfo']['genre_id'], false, false);
                    if (in_array(config('constant.CREDIT_DANGER'), $resultCredit)) {
                        //If the credit limit is exceeded, subsequent processing is not performed
                        continue;
                    }
                    if ($data['demandInfo']['site_id'] != config('rits.CREDIT_EXCLUSION_SITE_ID')) {
                        $agrementLicense = false;
                        if (is_null($data['demandInfo']['category_id'])) {
                            $agrementLicense = true;
                        } else {
                            $agrementLicense = $this->mCorpRepository->isCommissionStop($val->id);
                        }
                        if ($agrementLicense === false) {
                            continue;
                        }
                    }

                    // Acquire default commission rate and default commission rate unit
                    if (empty($val->order_fee) || is_null($val->order_fee_unit)) {
                        // If it can not be acquired from m_corp_categories, it is obtained from m_categories
                        $mcCategoryData = $this->mCategoryRepo->getFeeDataMCategories($data['demandInfo']['category_id']);
                        if (empty($mcCategoryData)) {
                            // If it can not be acquired from m_categories, it is left blank.
                            $mcCategoryData = ['category_default_fee' => '', 'category_default_fee_unit' => ''];
                        }
                        $val->order_fee = $mcCategoryData['category_default_fee'];
                        $val->order_fee_unit = $mcCategoryData['category_default_fee_unit'];
                    }
                    // If lost_flg, del_flg registered in Commission_infos is "1", it will not be subject to bidding
                    $targetCommissionData = $this->commissionInfoRepository->getFirstByDemandIdAndCorpId($demandId, $val->id);
                    // A franchise with lost_flg and del_flg of "0" in the retrieved pickup data is targeted for bidding
                    if (is_null($targetCommissionData) || (($targetCommissionData->lost_flg == 0) && ($targetCommissionData->del_flg == 0))) {
                        if (!empty($val->commission_unit_price_rank)) {
                            $rank = mb_strtolower($val->commission_unit_price_rank);
                            if (isset($rankTime[$rank])) {
                                $as = false;
                                if (empty($auctionStatusFlg) || $auctionStatusFlg == getDivValue('auction_delivery_status', 'delivery') || $auctionStatusFlg == getDivValue('auction_delivery_status', 'deny')) {
                                    $as = true;
                                }
                                if (!empty($rankTime[$rank]) && $as) {
                                    //ORANGE-250 CHG E
                                    $auctionData[$idx]['id'] = isset($beforeList[$val->id]) ? $beforeList[$val->id]: '' ;
                                    $auctionData[$idx]['demand_id'] = $demandId;
                                    $auctionData[$idx]['corp_id'] = $val->id;
                                    $auctionData[$idx]['push_time'] = $rankTime[$rank];
                                    $auctionData[$idx]['push_flg'] = 0;
                                    $auctionData[$idx]['before_push_flg'] = 0;
                                    $auctionData[$idx]['display_flg'] = 0;
                                    $auctionData[$idx]['refusal_flg'] = 0;
                                    $auctionData[$idx]['rank'] = $rank; // For extraction per rank
                                    if ($autoCall != null && $this->isAutoCallable($autoCallFlag, $val->auto_call_flag)) {
                                        $auctionData[$idx]['auto_call_time'] = date('Y-m-d H:i', strtotime($rankTime[$rank].'+'.$autoCall.'minute'));
                                        $auctionData[$idx]['auto_call_flg'] = 0;
                                    } else {
                                        $auctionData[$idx]['auto_call_time'] = null;
                                        $auctionData[$idx]['auto_call_flg'] = null;
                                    }
                                    $idx++;
                                }
                                //Create data for automatic selection
                                if (!empty($rankTime[$rank]) && (empty($auctionStatusFlg) || $auctionStatusFlg == getDivValue('auction_delivery_status', 'delivery') || $auctionStatusFlg == getDivValue('auction_delivery_status', 'ng'))) {
                                    $autoAuctionData[] = [
                                        'id' => isset($beforeList[$val->id]) ? $beforeList[$val->id] : '',
                                        'demand_id' => $demandId,
                                        'corp_id' => $val->id,
                                        'push_time' => $rankTime[$rank],
                                        'push_flg' => 0,
                                        'before_push_flg' => 0,
                                        'display_flg' => 0,
                                        'refusal_flg' => 0,
                                        'rank' => $rank,
                                        'affiliationAreaStat' => [
                                            'commission_unit_price_category' => $val->commission_unit_price_category,
                                            'commission_count_category' => $val->commission_count_category,
                                            'commission_unit_price_rank' => $val->commission_unit_price_rank,
                                        ],
                                        'mCorpCategory' => [
                                            'order_fee' => $val->order_fee,
                                            'order_fee_unit' => $val->order_fee_unit,
                                            'introduce_fee' => $val->introduce_fee,
                                            'corp_commission_type' => $val->corp_commission_type,
                                        ],
                                        'mCorp' => [
                                            'id' => $val->id,
                                        ],
                                    ];
                                }
                            }
                        } else {
                            // If the rank is blank, it shall be handled as the first round of auction and shall be subject to auction.
                            $rank = 'z';
                            if (isset($rankTime[$rank])) {
                                $as = false;
                                if (empty($auctionStatusFlg) || $auctionStatusFlg == getDivValue('auction_delivery_status', 'delivery') || $auctionStatusFlg == getDivValue('auction_delivery_status', 'deny')) {
                                    $as = true;
                                }
                                if (!empty($rankTime[$rank]) && $as) {
                                    $auctionData[$idx]['id'] = isset($beforeList[$val->id]) ? $beforeList[$val->id]: '' ;
                                    $auctionData[$idx]['demand_id'] = $demandId;
                                    $auctionData[$idx]['corp_id'] = $val->id;
                                    $auctionData[$idx]['push_time'] = $rankTime[$rank];
                                    $auctionData[$idx]['push_flg'] = 0;
                                    $auctionData[$idx]['before_push_flg'] = 0;
                                    $auctionData[$idx]['display_flg'] = 0;
                                    $auctionData[$idx]['refusal_flg'] = 0;
                                    $auctionData[$idx]['rank'] = $rank; // ランク毎抽出用

                                    if ($autoCall !== null && $this->isAutoCallable($autoCallFlag, $val->auto_call_flag)) {
                                        $auctionData[$idx]['auto_call_time'] = date('Y-m-d H:i', strtotime($rankTime[$rank].'+'.$autoCall.'minute'));
                                        $auctionData[$idx]['auto_call_flg'] = 0;
                                    } else {
                                        $auctionData[$idx]['auto_call_time'] = null;
                                        $auctionData[$idx]['auto_call_flg'] = null;
                                    }
                                    $idx++;
                                }
                                // Create data for automatic selection
                                if (!empty($rankTime[$rank]) && (empty($auctionStatusFlg) || $auctionStatusFlg == getDivValue('auction_delivery_status', 'delivery') || $auctionStatusFlg == getDivValue('auction_delivery_status', 'ng'))) {
                                    $autoAuctionData[] = [
                                        'id' => isset($beforeList[$val->id]) ? $beforeList[$val->id] : '',
                                        'demand_id' => $demandId,
                                        'corp_id' => $val->id,
                                        'push_time' => $rankTime[$rank],
                                        'push_flg' => 0,
                                        'before_push_flg' => 0,
                                        'display_flg' => 0,
                                        'refusal_flg' => 0,
                                        'rank' => $rank,
                                        'affiliationAreaStat' => [
                                            'commission_unit_price_category' => $val->commission_unit_price_category,
                                            'commission_count_category' => $val->commission_count_category,
                                            'commission_unit_price_rank' => $val->commission_unit_price_rank,
                                        ],
                                        'mCorpCategory' => [
                                            'order_fee' => $val->order_fee,
                                            'order_fee_unit' => $val->order_fee_unit,
                                            'introduce_fee' => $val->introduce_fee,
                                            'corp_commission_type' => $val->corp_commission_type,
                                        ],
                                        'mCorp' => [
                                            'id' => $val->id,
                                        ],
                                    ];
                                }
                            }
                        }
                    } else { // lost_flg,del_flg check
                        if (isset($beforeList[$val->id])) {
                            // At the time of re-bidding, the member shop with lost_flg and del_flg being 1 declines that it has declined, and sets the declaration flag to 1
                            $auctionId = isset($beforeList[$val->id]) ? $beforeList[$val->id] : '';
                            if (!$autoSelection) {
                                // 書き込む
                                Log::debug('____ call  $this->auctionInfoRepository->saveAuction()  _______');
                                $this->auctionInfoRepository->saveAuction($auctionId, ['refusal_flg' => 1]);
                            }
                        }
                    }
                }
                if (!$autoSelection) {
                    // In case of bidding ceremony selection (manual or automatic)
                    if (0 < count($auctionData)) {
                        // $auctionData['demandInfo']['genre_id'] = $data['demandInfo']['genre_id'];
                        // $auctionData['demandInfo']['prefecture'] = $data['demandInfo']['address1'];
                        Log::debug('____ call  $this->carryAuctionPushTime()  _______');
                        $this->carryAuctionPushTime($auctionData, $rankTime, $autoCall);
                        Log::debug('____ call  $this->auctionInfoRepository->saveAuctions()  _______');
                        $this->auctionInfoRepository->saveAuctions($auctionData);
                        return $auctionData;
                    }
                } else {
                    // In case of automatic selection
                    if (0 < count($autoAuctionData)) {
                        // $autoAuctionData['demandInfo']['genre_id'] = $data['demandInfo']['genre_id'];
                        // $autoAuctionData['demandInfo']['prefecture'] = $data['demandInfo']['address1'];
                        Log::debug('____ call  $this->carryAuctionPushTime()  _______');
                        $this->carryAuctionPushTime($autoAuctionData, $rankTime, $autoCall);
                    }

                    return $autoAuctionData;
                }
            }
        }

        return true;
    }

    /**
     * Acquire transmission interval until auto call
     *
     * @param  integer $priority
     * @return mixed
     */
    public function getAutoCallInterval($priority)
    {
        $autoCall = $this->autoCallItemRepo->getItem();
        if ($priority == getDivValue('priority', 'asap')) {
            $time = $autoCall->asap;
        } elseif ($priority == getDivValue('priority', 'immediately')) {
            $time = $autoCall->immediately;
        } else {
            $time = $autoCall->normal;
        }

        return $time;
    }

    /**
     * @param $genreId
     * @return int
     */
    public function getAutoCallFlagMGenre($genreId)
    {

        $result = $this->genreRepo->findById($genreId);

        return !empty($result) ? $result->auto_call_flag : 1;
    }

    /**
     * @param int $genre
     * @param int $corp
     * @return bool
     */
    public function isAutoCallable($genre = 1, $corp = 1)
    {
        if ($genre != 1) {
            // If auto call flag by genre is auto call disabled, auto call excluded
            return false;
        } elseif ($corp != 1) {
            // If the auto call flag by company is auto call disabled, it is not subject to auto call
            return false;
        }

        return true;
    }

    /**
     * @param $auctionData
     * @param $rankTime
     * @param $autoCallInterval
     * @return array
     */
    public function carryAuctionPushTime($auctionData, $rankTime, $autoCallInterval)
    {
        if (empty($auctionData)) {
            return [];
        }

        $rankTimeSort = [
            [
                'rank' => 'a',
                'rank_time' => $rankTime['a'],
            ],
            [
                'rank' => 'b',
                'rank_time' => $rankTime['b'],
            ],
            [
                'rank' => 'c',
                'rank_time' => $rankTime['c'],
            ],
            [
                'rank' => 'd',
                'rank_time' => $rankTime['d'],
            ],
            [
                'rank' => 'z',
                'rank_time' => $rankTime['z'],
            ],
        ];
        // Remove the rank whose rank_time is not set yet
        $rankTimeSort = array_filter($rankTimeSort, function ($vr) {
            return !empty($vr['rank_time']);
        });

        uasort($rankTimeSort, function ($first, $second) {
            if ($first['rank_time'] == $second['rank_time']) {
                return ($first['rank'] < $second['rank']) ? -1 : 1;
            }
            return ($first['rank_time'] < $second['rank_time']) ? -1 : 1;
        });
        foreach ($rankTimeSort as $rank) {
            $ranks = array_filter($auctionData, function ($data) use ($rank) {
                return $data['rank'] == $rank['rank'];
            });
            if (empty($ranks)) {
                // There are no merchants that fall under the rank
                if (empty($highestEmptyRank)) {
                    $highestEmptyRank = $rank['rank'];
                }
            } else {
                // If there is no member store of the highest rank, carry forward advance (= push_time advance)
                if (!empty($highestEmptyRank)) {
                    foreach ($auctionData as $key => $val) {
                        if ($val['rank'] == $rank['rank']) {
                            $auctionData[$key]['push_time'] = $rankTime[$highestEmptyRank];
                            if (!empty($auctionData[$key]['auto_call_time'])) {
                                $auctionData[$key]['auto_call_time'] = date('Y-m-d H:i', strtotime($rankTime[$highestEmptyRank].'+'.$autoCallInterval.'minute'));
                            }
                        }
                    }
                }
                break;
            }
        }
    }


    /**
     * @param $data
     * @return bool
     */
    public function editSupport($data)
    {
        try {
            $this->auctionInfoRepository->updateAuctionInfo($data);

            $demandData = $this->demandInfoRepository->getDemandById($data['demand_id'])->toarray();

            $resultCategoryData = $this->mCorpCategoryRepository->findByCorpIdAndGenreIdAndCategoryId(
                $data['corp_id'],
                $demandData['genre_id'],
                $demandData['category_id']
            );

            $categoryData = isset($resultCategoryData) ? $resultCategoryData->toarray() : [];
            if (count($categoryData) == 0) {
                return false;
            }

            $createInfoCommission = $this->getInfoCommissionToCreate($categoryData, $data, $demandData);

            if (!$this->checkEmptyCommissionFeeRate($createInfoCommission)
                || !$this->checkCommissionIntroduce($createInfoCommission)
                || !$this->checkCommissionStatusComplete($createInfoCommission)
                || !$this->checkCommissionStatusOrderFail($createInfoCommission)) {
                return false;
            }
            DB::beginTransaction();
            //Edit commission
            $newCommission = $this->commissionInfoRepository->save($createInfoCommission)->toarray();
            //Edit demand info
            $this->updateDemandInfo($data['demand_id']);
            //Create bill info
            $auctionFee = $this->auctionInfoRepository->getAuctionFee($data['id']);
            $this->createBillInfo($newCommission, $data, $auctionFee);
            //Create auction agreement link
            if ($auctionFee > 0) {
                $this->createAuctionAgreementLink($newCommission, $data, $auctionFee);
            }
            DB::commit();
            return true;
        } catch (\Exception $exception) {
            DB::rollback();
            return false;
        }
    }
    /**
     * @param $categoryData
     * @param $data
     * @param $demandData
     * @return array
     */
    protected function getInfoCommissionToCreate($categoryData, $data, $demandData)
    {
        $edit = [];

        if ($categoryData['corp_commission_type'] != 2) {
            $commissionType   = getDivValue('commission_type', 'normal_commission');
            $commissionStatus = getDivValue('construction_status', 'progression');
        } else {
            $commissionType   = getDivValue('commission_type', 'package_estimate');
            $commissionStatus = getDivValue('construction_status', 'introduction');
            $categoryData['order_fee']      = $categoryData['introduce_fee'];
            $categoryData['order_fee_unit'] = 0;

            $edit['confirmd_fee_rate']   = 100;
            $edit['commission_fee_rate'] = 100;
            $edit['complete_date']       = date('Y/m/d');
        }

        if (empty($categoryData['order_fee']) || is_null($categoryData['order_fee_unit'])) {
            $mcCategoryData = $this->mCategoryRepo->getFeeData($demandData['category_id']);

            if (empty($mcCategoryData)) {
                $mcCategoryData = array('category_default_fee' => '', 'category_default_fee_unit' => '');
            } else {
                $mcCategoryData = $mcCategoryData->toarray();
            }
            $categoryData['order_fee']      = $mcCategoryData['category_default_fee'];
            $categoryData['order_fee_unit'] = $mcCategoryData['category_default_fee_unit'];
            $categoryData['note'] = '';
        }

        $edit['demand_id']                     = $data['demand_id'];
        $edit['corp_id']                       = $data['corp_id'];
        $edit['commit_flg']                    = 1;
        $edit['commission_type']               = $commissionType;
        $edit['commission_status']             = $commissionStatus;
        $edit['unit_price_calc_exclude']       = 0;
        $edit['commission_note_send_datetime'] = date('Y/m/d H:i', time());
        $edit['commission_visit_time_id']      = isset($data['visit_time_id']) ? $data['visit_time_id'] : 0;

        if ($categoryData['order_fee_unit'] == 0) {
            $edit['corp_fee'] = $categoryData['order_fee'];
        } elseif ($categoryData['order_fee_unit'] == 1) {
            $edit['commission_fee_rate'] = $categoryData['order_fee'];
        }
        $edit['business_trip_amount'] = !empty($data['business_trip_amount']) ? $data['business_trip_amount'] : 0 ;

        $affiliationAreaData = $this->affiliationAreaStatRepo->findByCorpIdAndGenerIdAndPrefecture($data['corp_id'], $demandData['genre_id'], $demandData['address1']);
        $affiliationAreaData = $this->changeTypeCollectionModel($affiliationAreaData);

        $edit['select_commission_unit_price_rank'] = $this->getSelectCommissionUnitPriceRank($affiliationAreaData['commission_unit_price_rank']);
        $edit['select_commission_unit_price'] = $this->getSelectCommissionUnitPrice($affiliationAreaData['commission_unit_price_category']);
        $edit['order_fee_unit'] = $categoryData['order_fee_unit'];

        return $edit;
    }
    /**
     * @param $data
     * @return null
     */
    protected function changeTypeCollectionModel($data)
    {
        if (!empty($data)) {
            return $data->toarray();
        }
        return null;
    }
    /**
     * @param $data
     * @return string
     */
    protected function getSelectCommissionUnitPriceRank($data)
    {
        return !empty($data) ? $data : '-' ;
    }

    /**
     * @param $data
     * @return int
     */
    protected function getSelectCommissionUnitPrice($data)
    {
        return !empty($data) ? $data : 0 ;
    }

    /**
     * @param $demandId
     */
    protected function updateDemandInfo($demandId)
    {
        $this->demandInfo->where('id', $demandId)->update([
            'push_stop_flg'  => 1,
            'demand_status'  => getDivValue('demand_status', 'information_sent'),
            'modified' => date(config('constant.FullDateTimeFormat'), time()),
            'modified_user_id' => Auth::user()->user_id
        ]);
    }

    /**
     * @param $newCommission
     * @param null $data
     * @param null $auctionFee
     */
    protected function createBillInfo($newCommission, $data = null, $auctionFee = null)
    {
        if ($newCommission['commission_type'] == getDivValue('commission_type', 'package_estimate')) {
            $billInfo = $this->getBillInfoToCreate($newCommission, true);
            $this->billInfoRepository->insertData($billInfo);
        }
        if ($auctionFee> 0) {
            $billInfo = $this->getBillInfoToCreate($newCommission, false, $data, $auctionFee);
            $this->billInfoRepository->insertData($billInfo);
        }
    }


    /**
     * @param $newCommission
     * @param $hasTax
     * @param null $data
     * @param null $auctionFee
     * @return array
     */
    protected function getBillInfoToCreate($newCommission, $hasTax, $data = null, $auctionFee = null)
    {
        $dataSave = [];
        $mTax = $this->getTaxRate();
        $taxRate = count($mTax)>0 ? floatval($mTax['tax_rate']) : 0;

        if ($hasTax) {
            $dataSave['commission_id']       = $newCommission['id'];
            $dataSave['demand_id']           = $newCommission['demand_id'];
            $dataSave['bill_status']         = getDivValue('bill_status', 'not_issue');
            $dataSave['comfirmed_fee_rate']  = 100;
            $dataSave['fee_target_price']    = isset($newCommission['corp_fee'])? $newCommission['corp_fee'] : 0;
            $dataSave['fee_tax_exclude']     = isset($newCommission['corp_fee'])? $newCommission['corp_fee'] : 0;
            $dataSave['tax']                 = floor($dataSave['fee_tax_exclude'] * $taxRate);
            $dataSave['total_bill_price']    = $dataSave['fee_tax_exclude'] + $dataSave['tax'];
            $dataSave['fee_payment_price']   = 0;
            $dataSave['fee_payment_balance'] = $dataSave['fee_tax_exclude'] + $dataSave['tax'];
            return $dataSave;
        }

        $dataSave['commission_id']       = $newCommission['id'];
        $dataSave['demand_id']           = $data['demand_id'];
        $dataSave['bill_status']         = 1;
        $dataSave['comfirmed_fee_rate']  = 100;
        $dataSave['fee_target_price']    = $auctionFee;
        $dataSave['fee_tax_exclude']     = $auctionFee;
        $dataSave['tax']                 = intval($dataSave['fee_tax_exclude'] * $taxRate);
        $dataSave['total_bill_price']    = $dataSave['fee_tax_exclude'] + $dataSave['tax'];
        $dataSave['fee_payment_price']   = 0;
        $dataSave['fee_payment_balance'] = $dataSave['total_bill_price'];
        $dataSave['auction_id']          = $data['id'];
        return $dataSave;
    }

    /**
     * @return array
     */
    protected function getTaxRate()
    {
        return MTaxRate::where('start_date', '<=', date('Y-m-d H:i:s'))
            ->orWhere([
                ['end_date' , '=', ''],
                ['end_date', '>=', date('Y-m-d H:i:s')]
            ])->first()->toarray();
    }

    /**
     * @param $newCommission
     * @param $data
     * @param $auctionFee
     */
    protected function createAuctionAgreementLink($newCommission, $data, $auctionFee)
    {
        $userLoginId = Auth::user()->user_id;
        $auctionLink = $this->mAuctionAgreementLink;

        $auctionLink->auction_id = $data['id'];
        $auctionLink->corp_id = $data['corp_id'];
        $auctionLink->auction_agreement_id = 1;
        $auctionLink->demand_id = $data['demand_id'];
        $auctionLink->commission_id = $newCommission['id'];
        $auctionLink->auction_fee = $auctionFee;
        $auctionLink->agreement_check = $data['agreement_check'];
        $auctionLink->responders = $data['responders'];
        $auctionLink->modified = date(config('constant.FullDateTimeFormat'), time());
        $auctionLink->created = date(config('constant.FullDateTimeFormat'), time());
        $auctionLink->created_user_id = $userLoginId;
        $auctionLink->modified_user_id = $userLoginId;
        $auctionLink->save();
    }

    /**
     * @author  thaihv
     * @param $demandInfo
     * @param $preferredDate
     * @return array
     */
    public function buildJudeResult(&$demandInfo, $preferredDate)
    {
        $judgeResult = array(
            "result_flg"  => 0,// 0 = No change, 1 = Changed (It is subject to exclusion time in normal case)
            "result_date" => null// start date
        );
        if (empty($demandInfo['priority'])) {
            $judgeResult = judgeAuction($demandInfo['auction_start_time'], $preferredDate, $demandInfo['genre_id'], $demandInfo['address1'], $demandInfo['auction_deadline_time'], $demandInfo['priority']);
        } // When the priority is urgent
        elseif ($demandInfo['priority'] == getDivValue('priority', 'asap')) {
            $judgeResult = judgeAsap($demandInfo['auction_start_time'], $demandInfo['genre_id'], $demandInfo['address1'], $demandInfo['auction_deadline_time'], $demandInfo['priority']);
        } // When the priority is urgent
        elseif ($demandInfo['priority'] == getDivValue('priority', 'immediately')) {
            $judgeResult = judgeImmediately($demandInfo['auction_start_time'], $preferredDate, $demandInfo['genre_id'], $demandInfo['address1'], $demandInfo['auction_deadline_time'], $demandInfo['priority']);
        } // When the priority is normal
        else {
            $judgeResult = judgeNormal($demandInfo['auction_start_time'], $demandInfo['genre_id'], $demandInfo['address1'], $demandInfo['auction_deadline_time'], $demandInfo['priority']);
        }

        return $judgeResult;
    }

    /**
     * get by id and commission commit flag
     * @param  integer $auctionId
     * @return string
     */
    public function getByIdAndCommissionCommitFlag($auctionId)
    {
        return $this->auctionInfoRepository->getByIdAndCommissionCommitFlag($auctionId);
    }
}
