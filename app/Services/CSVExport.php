<?php

namespace App\Services;

class CSVExport
{
    /**
     * @var string
     */
    public $delimiter = ',';
    /**
     * @var string
     */
    public $enclosure = '"';
    /**
     * @var string
     */
    public $filename = 'Export.csv';
    /**
     * @var array
     */
    public $line = array();
    /**
     * @var array
     */
    public $fields = array();
    /**
     * @var mixed
     */
    public $buffer;
    /**
     * @var string
     */
    public $exHeader = '';

    /**
     * @param $fieldList
     * @param $fileName
     * @param $dataList
     */
    public function download($fieldList, $fileName, $dataList)
    {
        $this->csvHelper();

        $this->setFilename($fileName);

        $this->setFields($fieldList);

        $this->addExHeader($this->exHeader);

        $this->addGrid($dataList);

        echo $this->render(true, 'SJIS-win');

        die();
    }

    /**
     *
     */
    public function csvHelper()
    {
        $this->clear();
    }

    /**
     *
     */
    public function clear()
    {
        $this->line = array();
        $this->buffer = fopen('php://temp/maxmemory:' . (5 * 1024 * 1024), 'r+');
    }

    /**
     * @param $value
     */
    public function addField($value)
    {
        $this->line[] = $value;
    }

    /**
     *
     */
    public function endRow()
    {
        $this->addRow($this->line);
        $this->line = array();
    }

    /**
     * @param $row
     */
    public function addRow($row)
    {
        $this->encfputscv($this->buffer, $row);
    }

    /**
     * @param $rowList
     * @param bool    $renderFields
     */
    public function addGrid($rowList, $renderFields = true)
    {
        if ($renderFields) {
            $this->encfputscv($this->buffer, array_values($this->fields));
        }
        foreach ($rowList as $row) {
            $resultRow = array();
            foreach ($this->fields as $column => $title) {
                if (array_key_exists($column, $row)) {
                    if (preg_match('/_div$/', $column)) {
                        $target_data = getDivTextJP($column, $row[$column]);
                        array_push($resultRow, $target_data);
                    } else {
                        array_push($resultRow, $row[$column]);
                    }
                }
            }
            $this->encfputscv($this->buffer, $resultRow);
        }
    }

    /**
     * @param $values
     */
    public function addExHeader($values)
    {
        if (is_array($values)) {
            reset($values);
            if (key($values) !== 0) {
                $this->encfputscv($this->buffer, array_keys($values));
            }
            $this->encfputscv($this->buffer, array_values($values));
        }
    }

    /**
     *
     */
    public function renderHeaders()
    {
        header("Content-type:text/csv");
        header("Content-disposition:attachment;filename=" . $this->filename);
    }

    /**
     * @param $fields
     */
    public function setFields($fields)
    {
        $this->fields = $fields;
    }

    /**
     * @param $filename
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
        if (strtolower(substr($this->filename, -4)) != '.csv') {
            $this->filename .= '.csv';
        }
    }

    /**
     * @param bool   $outputHeaders
     * @param null   $toEncoding
     * @param string $fromEncoding
     * @return bool|null|string|string[]
     */
    public function render($outputHeaders = true, $toEncoding = null, $fromEncoding = "auto")
    {
        if ($outputHeaders) {
            if (is_string($outputHeaders)) {
                $this->setFilename($outputHeaders);
            }
            $this->renderHeaders();
        }
        rewind($this->buffer);
        $output = stream_get_contents($this->buffer);
        if ($toEncoding) {
            $output = mb_convert_encoding($output, $toEncoding, $fromEncoding);
        }
        return $output;
    }

    /**
     * @param $fp
     * @param $row
     * @param string $delimiter
     * @param string $enclosure
     * @param string $eol
     * @return bool|int
     */
    public function encfputscv($fp, $row, $delimiter = ',', $enclosure = '"', $eol = "\r\n")
    {
        $tmp = array();

        foreach ($row as $v) {
            $v = str_replace('"', '""', $v);
            $tmp[] = $enclosure . $v . $enclosure;
        }
        $str = implode($delimiter, $tmp) . $eol;
        return fwrite($fp, $str);
    }
}
