<?php

namespace App\Services\Command;

use App\Models\AutoCallItem;
use App\Repositories\AuctionInfoRepositoryInterface;
use App\Repositories\CommissionInfoRepositoryInterface;
use App\Repositories\DemandInfoRepositoryInterface;
use App\Repositories\MCategoryRepositoryInterface;
use App\Repositories\MCorpRepositoryInterface;
use App\Repositories\MGenresRepositoryInterface;
use App\Repositories\MPostRepositoryInterface;
use App\Repositories\VisitTimeRepositoryInterface;
use App\Services\AuctionService;
use App\Services\Credit\CreditService;
use Illuminate\Support\Facades\Hash;

class CheckDeadlinePastAuctionService
{
    /**
     * @var CommissionInfoRepositoryInterface
     */
    protected $commissionInfoRepository;

    /**
     * @var MCategoryRepositoryInterface
     */
    protected $mCategoryRepository;

    /**
     * @var MCorpRepositoryInterface
     */
    protected $mCorpRepository;

    /**
     * @var DemandInfoRepositoryInterface
     */
    protected $demandInfoRepository;

    /**
     * @var VisitTimeRepositoryInterface
     */
    protected $visitTimeRepository;

    /**
     * @var AuctionInfoRepositoryInterface
     */
    protected $auctionInfoRepository;

    /**
     * @var MGenresRepositoryInterface
     */
    protected $mGenreRepository;

    /**
     * @var MPostRepositoryInterface
     */
    protected $mPostRepository;

    /**
     * @var CreditService
     */
    protected $creditService;

    /**
     * @var AuctionService
     */
    protected $auctionService;

    /**
     * Default user
     * @var string
     */
    private $user = 'system';

    /**
     * CheckDeadlinePastAuctionService constructor.
     * @param CommissionInfoRepositoryInterface $commissionInfoRepository
     * @param MCategoryRepositoryInterface $mCategoryRepository
     * @param MCorpRepositoryInterface $mCorpRepository
     * @param DemandInfoRepositoryInterface $demandInfoRepository
     * @param VisitTimeRepositoryInterface $visitTimeRepository
     * @param AuctionInfoRepositoryInterface $auctionInfoRepository
     * @param MGenresRepositoryInterface $mGenreRepository
     * @param MPostRepositoryInterface $mPostRepository
     * @param CreditService $creditService
     * @param AuctionService $auctionService
     */
    public function __construct(
        CommissionInfoRepositoryInterface $commissionInfoRepository,
        MCategoryRepositoryInterface $mCategoryRepository,
        MCorpRepositoryInterface $mCorpRepository,
        DemandInfoRepositoryInterface $demandInfoRepository,
        VisitTimeRepositoryInterface $visitTimeRepository,
        AuctionInfoRepositoryInterface $auctionInfoRepository,
        MGenresRepositoryInterface $mGenreRepository,
        MPostRepositoryInterface $mPostRepository,
        CreditService $creditService,
        AuctionService $auctionService
    ) {
        $this->commissionInfoRepository = $commissionInfoRepository;
        $this->mCategoryRepository = $mCategoryRepository;
        $this->mCorpRepository = $mCorpRepository;
        $this->demandInfoRepository = $demandInfoRepository;
        $this->visitTimeRepository = $visitTimeRepository;
        $this->auctionInfoRepository = $auctionInfoRepository;
        $this->mGenreRepository = $mGenreRepository;
        $this->mPostRepository = $mPostRepository;
        $this->creditService = $creditService;
        $this->auctionService = $auctionService;
    }

    /**
     * Get auction commission list
     *
     * @param $data
     * @return array
     */
    public function getAuctionCommissionList($data)
    {
        $commissionInfos = [];

        // Bidding flow history
        $correspondingContents = '';
        // Automatic selection history
        $correspondingContents2 = '';

        // Acquire registered auction information
        $auctionInfos = $this->auctionInfoRepository->getAuctionInfoByDemandIdForCheckDeadline($data->id);

        // Acquire registered registration information
        $registedCommission = $this->commissionInfoRepository->findByDemandId($data->id);

        foreach ($auctionInfos as $k => $v) {
            if ($data->selection_system == getDivValue('selection_type', 'auction_selection') && $v->refusal_flg != 1) {
                continue;
            }

            // Selection method automatic OR manual
            if (!empty($data->selection_system) && $data->selection_system == getDivValue(
                'selection_type',
                'auction_selection'
            )) {
                $correspondingContents .= '入札流れ 手動選定 ';
            } else {
                $correspondingContents .= '入札流れ 自動選定 ';
            }

            // Add company name
            $correspondingContents .= '[' . $v->corp_name . '] ';

            // Reason for declining
            if ($v->refusal_flg == 1) {
                $correspondingContents .= "＜辞退＞\n";
            } else {
                $correspondingContents .= "\n";
            }
            if (!empty($v->corresponds_time1) || !empty($v->corresponds_time2) || !empty($v->corresponds_time3)) {
                $correspondingContents .= '  ■対応時間が合わず ';
                if (!empty($v->corresponds_time1)) {
                    $correspondingContents .= dateTimeFormat($v->corresponds_time1);
                }
                if (!empty($v->corresponds_time2)) {
                    $correspondingContents .= dateTimeFormat($v->corresponds_time2);
                }
                if (!empty($v->corresponds_time3)) {
                    $correspondingContents .= dateTimeFormat($v->corresponds_time3);
                }
                $correspondingContents .= "\n";
            }
            if (!empty($v->cost_from) || !empty($v->cost_to)) {
                $correspondingContents .= '  ■価格が合わず ' . yenFormat2($v->cost_from) . '～' . yenFormat2($v->cost_to);
                $correspondingContents .= "\n";
            }
            if (!empty($v->estimable_time_from)) {
                $correspondingContents .= '  ■見積もり日程が対応不可 ' . dateTimeFormat($v->estimable_time_from);
                $correspondingContents .= "\n";
            }
            if (!empty($v->contactable_time_from)) {
                $correspondingContents .= '  ■連絡希望日時が対応不可 ' . dateTimeFormat($v->contactable_time_from);
                $correspondingContents .= "\n";
            }
            if (!empty($v->other_contens)) {
                $correspondingContents .= '  ■その他対応不可理由 ' . $v->other_contens;
                $correspondingContents .= "\n";
            }

            if (!empty($registedCommission)) {
                $hasCommissionDb = $registedCommission->filter(function ($item) use ($v) {
                    return $item->corp_id == $v->corp_id;
                });
            }

            if (empty($hasCommissionDb) && $v->refusal_flg == 1) {
                if ($v->corp_commission_type != 2) {
                    // Conclusion base
                    $orderFee = $v->order_fee;
                    $orderFeeUnit = $v->order_fee_unit;
                    $commissionType = getDivValue('commission_type', 'normal_commission');
                    $commissionStatus = getDivValue('construction_status', 'progression');
                } else {
                    // Introduction base
                    $orderFee = $v->introduce_fee;
                    $orderFeeUnit = 0;
                    $commissionType = getDivValue('commission_type', 'package_estimate');
                    $commissionStatus = getDivValue('construction_status', 'introduction');
                }

                $commissionInfos['commissionInfo'][] = [
                    'demand_id' => $data->id,
                    'corp_id' => $v->corp_id,
                    'commit_flg' => 0,
                    'commission_type' => $commissionType,
                    'lost_flg' => $v->refusal_flg,
                    'commission_status' => $commissionStatus,
                    'unit_price_exclude' => 0,
                    'corp_fee' => $orderFeeUnit == 0 ? $orderFee : '',
                    'commission_fee_rate' => $v->corp_commission_type != 2 ? ($orderFeeUnit == 1 ? $orderFee : '') : 100,
                    'business_trip_amount' => !empty($v->business_trip_amount) ? $v->business_trip_amount : 0,
                    'select_commission_unit_price_rank' => !empty($v->commission_unit_price_rank) ? $v->commission_unit_price_rank : '',
                    'select_commission_unit_price' => !empty($v->commission_unit_price_category) ? $v->commission_unit_price_category : '',
                    'order_fee_unit' => $orderFeeUnit,
                    'modified_user_id' => $this->user,
                    'modified' => date('Y-m-d H:i:s'),
                    'created_user_id' => $this->user,
                    'created' => date('Y-m-d H:i:s'),

                    // For sorting
                    'sort_push_time' => strtotime($v->push_time),
                    'sort_commission_unit_price_category' => $v->commission_unit_price_category,
                    'sort_commission_count_category' => $v->commission_count_category,
                ];
            }
        }

        if (!empty($data->selection_system) && $data->selection_system == getDivValue('selection_type', 'automatic_auction_selection')) {
            // Acquire auction information for automatic selection
            $demandInfo = $this->getDemand($data->id);
            $autoAuctionInfos = $this->auctionService->getAuctionInfoForAutoCommission($demandInfo['demandInfo']['id'], $demandInfo);
            $defaultFee = $this->mCategoryRepository->getDefaultFee($demandInfo['demandInfo']['category_id']);
            foreach ($autoAuctionInfos as $v) {
                // Do not register already registered suppliers
                if (!empty($commissionInfos)) {
                    $hasCommission = array_filter($commissionInfos['commissionInfo'], function ($item) use ($v) {
                        return $item['corp_id'] == $v['mCorp']['id'];
                    });
                }

                // If you have already registered with DB, do not register
                if (!empty($registedCommission)) {
                    $hasCommissionDb = $registedCommission->filter(function ($item) use ($v) {
                        return $item['corp_id'] == $v['mCorp']['id'];
                    });
                }

                if (empty($hasCommission) && empty($hasCommissionDb)) {
                    if ($v['mCorpCategory']['corp_commission_type'] != 2) {
                        // Conclusion base
                        $orderFee = $v['mCorpCategory']['order_fee'];
                        $orderFeeUnit = $v['mCorpCategory']['order_fee_unit'];
                        $commissionType = getDivValue('commission_type', 'normal_commission');
                        $commissionStatus = getDivValue('construction_status', 'progression');
                    } else {
                        // Introduction base
                        $orderFee = $v['mCorpCategory']['introduce_fee'];
                        $orderFeeUnit = 0;
                        $commissionType = getDivValue('commission_type', 'package_estimate');
                        $commissionStatus = getDivValue('construction_status', 'introduction');
                    }

                    $orderFee = !empty($orderFee) ? $orderFee : $defaultFee['category_default_fee'];
                    $orderFeeUnit = (!empty($v['mCorpCategory']['order_fee']) || !empty($v['mCorpCategory']['introduce_fee'])) ? $orderFeeUnit : $defaultFee['category_default_fee_unit'];

                    $commissionInfos['commissionInfo'][] = array(
                        'demand_id' => $data->id,
                        'corp_id' => $v['mCorp']['id'],
                        'commit_flg' => 0,
                        'commission_type' => $commissionType,
                        'lost_flg' => 0,
                        'commission_status' => $commissionStatus,
                        'unit_price_exclude' => 0,
                        'corp_fee' => $orderFeeUnit == 0 ? $orderFee : null,
                        'commission_fee_rate' => $orderFeeUnit == 0 ? null : $orderFee,
                        'business_trip_amount' => !empty($demandInfo['demandInfo']['business_trip_amount']) ? $demandInfo['demandInfo']['business_trip_amount'] : 0,
                        'select_commission_unit_price_rank' => $v['affiliationAreaStat']['commission_unit_price_rank'],
                        'select_commission_unit_price' => $v['affiliationAreaStat']['commission_unit_price_rank'],
                        'order_fee_unit' => $orderFeeUnit,
                        'modified_user_id' => $this->user,
                        'modified' => date('Y-m-d H:i:s'),
                        'created_user_id' => $this->user,
                        'created' => date('Y-m-d H:i:s'),
                        // For sorting
                        'sort_push_time' => strtotime($v['push_time']),
                        'sort_commission_unit_price_category' => $v['affiliationAreaStat']['commission_unit_price_category'],
                        'sort_commission_count_category' => $v['affiliationAreaStat']['commission_count_category'],
                    );
                }

                $correspondingContents2 .= '自動選定 ';
                $corp = $this->mCorpRepository->find($v['mCorp']['id']);
                $correspondingContents2 .= '[' . $corp->corp_name . "]\n";
            }
        }

        // Perform sorting together with bid declaration and automatic selection
        if (!empty($commissionInfos)) {
            uasort($commissionInfos, function ($a, $b) {
                if ($a['sort_push_time'] != $b['sort_push_time']) {
                    return $a['sort_push_time'] < $b['sort_push_time'] ? -1 : 1;
                } else {
                    if ($a['sort_commission_unit_price_category'] != $b['sort_commission_unit_price_category']) {
                        if (empty($a['sort_commission_unit_price_category']) && !empty($b['sort_commission_unit_price_category'])) {
                            return 1;
                        } else {
                            if (!empty($a['sort_commission_unit_price_category']) && empty($b['sort_commission_unit_price_category'])) {
                                return -1;
                            } else {
                                return $a['sort_commission_unit_price_category'] > $b['sort_commission_unit_price_category'] ? -1 : 1;
                            }
                        }
                    } else {
                        if ($a['sort_commission_count_category'] != $b['sort_commission_count_category']) {
                            return $a['sort_commission_count_category'] > $b['sort_commission_count_category'] ? -1 : 1;
                        } else {
                            return 0;
                        }
                    }
                }
            });
        }

        if (!empty($correspondingContents)) {
            $commissionInfos['corresponding_contens'][0] = $correspondingContents;
        }

        if (!empty($correspondingContents2)) {
            $commissionInfos['corresponding_contens'][1] = $correspondingContents2;
        }

        return $commissionInfos;
    }

    /**
     * @param $demandId
     * @return \App\Models\Base|null
     */
    private function getDemand($demandId)
    {
        // Acquisition of item data
        $results['demandInfo'] = $this->demandInfoRepository->find($demandId)->toArray();
        $results['demandInfo']['recursive'] = 2;

        // Acquiring visit date and time
        $data = $this->visitTimeRepository->findAllWithAuctionInfo($demandId);
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $results['visitTime'][$key]['id'] = $value->id;
                $results['visitTime'][$key]['visit_time'] = $value->visit_time;
                $results['visitTime'][$key]['is_visit_time_range_flg'] = $value->is_visit_time_range_flg;
                $results['visitTime'][$key]['visit_time_from'] = $value->visit_time_from;
                $results['visitTime'][$key]['visit_time_to'] = $value->visit_time_to;
                $results['visitTime'][$key]['visit_adjust_time'] = $value->visit_adjust_time;
                $results['visitTime'][$key]['visit_time_before'] = ($value->is_visit_time_range_flg == 0) ? $value->visit_time : $value->visit_time_from;
                $results['visitTime'][$key]['commit_flg'] = !empty($value->auction_info_id) ? 1 : 0;
            }
        }

        return $results;
    }
}
