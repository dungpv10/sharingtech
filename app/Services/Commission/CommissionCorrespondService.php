<?php

namespace App\Services\Commission;

use App\Services\BaseService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;

class CommissionCorrespondService extends BaseService
{
    /**
     * Validate before regist
     * @param array $data
     * @return array
     */
    public function validate($data)
    {
        $result = ['check' => true];
        $message = [
            'required' => '必須入力です。',
            'date' => '日時形式で入力してください。',
            'max' => ':max文字以内で設定してください。'
        ];
        $validate = Validator::make($data, [
            'commission_id' => 'required',
            'correspond_datetime' => 'date',
            'responders' => 'max:20',
            'corresponding_contens' => 'max:1000'
        ], $message);

        $result['validate'] = $validate;
        $allValid = [
            $this->checkInputResponders($data),
            $this->checkInputContens($data)
        ];

        if ($validate->fails()) {
            $result['check'] = false;
        }

        if (in_array(false, $allValid)) {
            $result['check'] = false;
        }

        return $result;
    }

    /**
     * @param array $data
     * @return boolean
     */
    private function checkInputContens($data)
    {
        if (empty($data['corresponding_contens'])) {
            if (!empty($data['responders']) || !empty($data['rits_responders'])) {
                session()->flash('corresponding_contens_error', trans('commission.corresponding_contens_require'));
                Log::debug('false checkInputContens');
                return false;
            }
        }

        return true;
    }

    /**
     * @param array $data
     * @return boolean
     */
    private function checkInputResponders($data)
    {
        if (empty($data['responders'])
            && empty($data['rits_responders'])
            && !empty($data['corresponding_contens'])) {
            session()->flash('rits_responders_error', trans('commission.rits_responders_require'));
            Log::debug('false checkInputResponders');
            return false;
        }

        return true;
    }
}
