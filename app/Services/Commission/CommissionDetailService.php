<?php
namespace App\Services\Commission;

use App\Repositories\CommissionInfoRepositoryInterface;
use App\Repositories\MGenresRepositoryInterface;
use App\Repositories\BillRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Exception;
use App\Repositories\DemandInfoRepositoryInterface;
use App\Repositories\CommissionCorrespondsRepositoryInterface;
use App\Services\BaseService;
use App\Repositories\MTaxRateRepositoryInterface;
use App\Repositories\DemandInquiryAnsRepositoryInterface;
use App\Repositories\DemandAttachedFileRepositoryInterface;
use App\Repositories\MCommissionAlertSettingRepositoryInterface;
use App\Repositories\MTimeRepositoryInterface;
use App\Repositories\MSiteRepositoryInterface;
use App\Repositories\VisitTimeRepositoryInterface;
use App\Repositories\MUserRepositoryInterface;
use Illuminate\Support\Facades\Config;

class CommissionDetailService extends BaseService
{
    /**
     * @var CommissionCorrespondsRepositoryInterface
     */
    private $commissionCorrespondsRepository;

    /**
     *
     * @var CommissionInfoRepositoryInterface
     */
    private $commissionInfoRepository;

    /**
     *
     * @var MGenresRepositoryInterface
     */
    private $mGenresRepository;

    /**
     *
     * @var BillRepositoryInterface
     */
    private $billRepository;

    /**
     * @var DemandInfoRepositoryInterface
     */
    private $demandInfoRepository;

    /**
     * @var MTaxRateRepositoryInterface
     */
    private $mTaxRateRepository;
    /**
     * @var DemandInquiryAnsRepositoryInterface
     */
    private $demandInquiryAnsRepository;
    /**
     * @var DemandAttachedFileRepositoryInterface
     */
    private $demandAttachedFileRepository;
    /**
     * @var MCommissionAlertSettingRepositoryInterface
     */
    private $mCommissionAlertSettingRepository;
    /**
     * @var MTimeRepositoryInterface
     */
    private $mTimeRepository;
    /**
     * @var MSiteRepositoryInterface
     */
    private $mSiteRepository;
    /**
     * @var VisitTimeRepositoryInterface
     */
    private $visitTimeRepository;
    /**
     * @var MUserRepositoryInterface
     */
    private $mUserRepository;

    /**
     * CommissionDetailService constructor.
     *
     * @param CommissionCorrespondsRepositoryInterface   $commissionCorrespondsRepository
     * @param CommissionInfoRepositoryInterface          $commissionInfoRepository
     * @param MGenresRepositoryInterface                 $mGenresRepository
     * @param BillRepositoryInterface                    $billRepository
     * @param DemandInfoRepositoryInterface              $demandInfoRepository
     * @param MTaxRateRepositoryInterface                $mTaxRateRepository
     * @param DemandInquiryAnsRepositoryInterface        $demandInquiryAnsRepository
     * @param DemandAttachedFileRepositoryInterface      $demandAttachedFileRepository
     * @param MCommissionAlertSettingRepositoryInterface $mCommissionAlertSettingRepository
     * @param MTimeRepositoryInterface                   $mTimeRepository
     * @param MSiteRepositoryInterface                   $mSiteRepository
     * @param VisitTimeRepositoryInterface               $visitTimeRepository
     * @param MUserRepositoryInterface                   $mUserRepository
     */
    public function __construct(
        CommissionCorrespondsRepositoryInterface $commissionCorrespondsRepository,
        CommissionInfoRepositoryInterface $commissionInfoRepository,
        MGenresRepositoryInterface $mGenresRepository,
        BillRepositoryInterface $billRepository,
        DemandInfoRepositoryInterface $demandInfoRepository,
        MTaxRateRepositoryInterface $mTaxRateRepository,
        DemandInquiryAnsRepositoryInterface $demandInquiryAnsRepository,
        DemandAttachedFileRepositoryInterface $demandAttachedFileRepository,
        MCommissionAlertSettingRepositoryInterface $mCommissionAlertSettingRepository,
        MTimeRepositoryInterface $mTimeRepository,
        MSiteRepositoryInterface $mSiteRepository,
        VisitTimeRepositoryInterface $visitTimeRepository,
        MUserRepositoryInterface $mUserRepository
    ) {
            $this->commissionCorrespondsRepository = $commissionCorrespondsRepository;
            $this->commissionInfoRepository = $commissionInfoRepository;
            $this->mGenresRepository = $mGenresRepository;
            $this->billRepository = $billRepository;
            $this->demandInfoRepository = $demandInfoRepository;
            $this->mTaxRateRepository = $mTaxRateRepository;
            $this->demandInquiryAnsRepository = $demandInquiryAnsRepository;
            $this->demandAttachedFileRepository = $demandAttachedFileRepository;
            $this->mCommissionAlertSettingRepository = $mCommissionAlertSettingRepository;
            $this->mTimeRepository = $mTimeRepository;
            $this->mSiteRepository = $mSiteRepository;
            $this->visitTimeRepository = $visitTimeRepository;
            $this->mUserRepository = $mUserRepository;
    }

    /**
     * Update commission if exist data
     *
     * @param  integer $id
     * @return boolean
     * @throws Exception
     */
    public function updateCommissionData($id = null)
    {
        if (Auth::user()->auth == 'affiliation') {
            // get data commission
            $commissionData = $this->commissionInfoRepository->find($id);

            // only execute when exist commission data
            if (! empty($commissionData) && $commissionData['CommissionInfo__app_notread'] == 1) {
                $commissionData['CommissionInfo__id'] = $id;
                $commissionData['CommissionInfo__app_notread'] = 0;
                $commissionData['CommissionInfo__modified'] = false;

                DB::beginTransaction();

                if ($this->commissionInfoRepository->save($commissionData)) {
                    DB::commit();
                } else {
                    DB::rollback();

                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @param $id
     * @return array
     */
    public function getCommissionData($id)
    {
        $result = $this->commissionInfoRepository->getCommissionInfoById($id);

        if (! empty($result['AuctionInfo__id'])) {
            $result = $this->getAuctionCommission($result['AuctionInfo__id'], $result);
        }

        return $result;
    }

    /**
     * @param null $date
     * @return float|int|string
     */
    public function getTaxRates($date = null)
    {
        $result = '';

        if (!empty($date)) {
            $row = $this->mTaxRateRepository->findByDate($date);
            $result = $row->tax_rate * 100;
        }

        return $result;
    }

    /**
     * @param null $date
     * @return array
     */
    public function getMTaxRates($date = null)
    {
        $result = [
            'tax_rate_val' => '',
            'tax_rate' => ''

        ];

        if (empty($date)) {
            $date = date('Y-m-d');
        }

        if (!empty($date)) {
            $row = $this->mTaxRateRepository->findByDate($date);

            if ($row) {
                $result['tax_rate_val'] = $row->tax_rate;
                $result['tax_rate'] = $row->tax_rate * 100;
            }
        }

        return $result;
    }

    /**
     * @param null $id
     * @return mixed
     */
    public function getCommissionCorresponds($id = null)
    {
        $commissionCorresponds = $this->commissionCorrespondsRepository->findById($id);

        return $commissionCorresponds;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getDemandAttachedFiles($id)
    {
        $demandAttachedFiles = $this->demandAttachedFileRepository->findByDemandId($id);

        return $demandAttachedFiles;
    }

    /**
     * Get detail demand attach file by id
     * @param integer $id
     * @return mixed
     */
    public function getDetailDemandAttachFile($id)
    {
        $demandAttachFile = $this->demandAttachedFileRepository->getFileDownload($id);

        return $demandAttachFile;
    }

    /**
     * Get default JBR info
     * @param $data
     * @return mixed
     */
    public function getDefaultJbrInfo($data)
    {
        // Set initial value of JBR quotation status
        if (empty($data['DemandInfo__jbr_estimate_status'])) {
            switch ($data['DemandInfo__genre_id']) {
                case Config::get('jbr_glass_genre_id'):
                    $data['DemandInfo__jbr_estimate_status'] = getDivValue('estimate_status', 'mikaisyu');
                    break;
                case Config::get('jbr_moving_genre_id'):
                    $data['DemandInfo__jbr_estimate_status'] = '';
                    break;
                default:
                    $data['DemandInfo__jbr_estimate_status'] = '';
                    break;
            }
        }

        // Set initial value of receipt status
        if (empty($data['DemandInfo__jbr_receipt_status'])) {
            switch ($data['DemandInfo__genre_id']) {
                case Config::get('jbr_glass_genre_id'):
                    $data['DemandInfo__jbr_receipt_status'] = getDivValue('receipt_status', 'mikaisyu');
                    break;
                case Config::get('jbr_moving_genre_id'):
                    $data['DemandInfo__jbr_receipt_status'] = '';
                    break;
                default:
                    $data['DemandInfo__jbr_receipt_status'] = getDivValue('receipt_status', 'mikaisyu');
                    break;
            }
        }

        return $data;
    }

    /**
     * @param null    $correspondStatus
     * @param $phaseId
     * @return mixed
     */
    public function mCommissionAlertSettings($correspondStatus = null, $phaseId = null)
    {
        $alertSetting = $this->mCommissionAlertSettingRepository->findByPhaseId($phaseId, $correspondStatus);

        return $alertSetting;
    }

    /**
     * @param $itemCategory
     * @return array
     */
    public function getDisclosureData($itemCategory)
    {
        $list = $this->mTimeRepository->getByItemCategory($itemCategory);

        return $list;
    }

    /**
     * @param $id
     * @return \App\Models\Base|null
     */
    public function getMsiteById($id)
    {
        $result = $this->mSiteRepository->find($id);

        return $result;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getVisitTime($id)
    {
        $result = $this->visitTimeRepository->findById($id);

        return $result;
    }

    /**
     * @return array
     */
    public function getMUsers()
    {
        $rows = $this->mUserRepository->getListUserNotAffiliation();
        $results = [];

        foreach ($rows as $row) {
            $results[$row['id']] = $row['user_name'];
        }

        return $results;
    }

    /**
     * @param $id
     * @param $modified
     * @return bool
     */
    public function checkModifiedCommission($id, $modified)
    {
        $result = $this->commissionInfoRepository->find($id);

        if ($modified == $result->modified) {
            return true;
        }

        session()->flash('error', __('commission_detail.modified_not_check'));

        return false;
    }

    /**
     * @param null  $id
     * @param array $data
     * @return bool
     */
    public function editCommission($id = null, $data = [])
    {
        $data['CommissionInfo__id'] = $id;
        $auth = Auth::user()->auth;

        if ($auth != 'affiliation') {
            $oldData = $this->getCommissionData($id);
            $oldStatus = $oldData['CommissionInfo__commission_status'];
            $newStatus = $data['CommissionInfo']['commission_status'];

            if ($oldStatus != getDivValue('construction_status', 'progression') && $newStatus == getDivValue('construction_status', 'progression')) {
                $data['CommissionInfo__reported_flg'] = 0;
            }
        }

        if (empty($data['CommissionInfo']['first_commission'])) {
            $data['CommissionInfo']['first_commission'] = 0;
        }
        if (empty($data['CommissionInfo']['unit_price_calc_exclude'])) {
            $data['CommissionInfo']['unit_price_calc_exclude'] = 0;
        }
        if (empty($data['CommissionInfo']['commission_order_fail_reason'])) {
            $data['CommissionInfo']['commission_order_fail_reason'] = 0;
        }

        $data['CommissionInfo']['complete_date'] = isset($data['CommissionInfo']['complete_date']) ? str_replace("-", "/", $data['CommissionInfo']['complete_date']) : '';
        $data['CommissionInfo']['order_fail_date'] = isset($data['CommissionInfo']['order_fail_date']) ? str_replace("-", "/", $data['CommissionInfo']['order_fail_date']) : '';

        if ($data['hidden_last_updated'] == 1) {
            $data['CommissionInfo']['commission_status_last_updated'] = date("Y-m-d G:i:s");
        }

        // 更新
        if ($this->commissionInfoRepository->save($data['CommissionInfo'])) {
            return true;
        }

        return false;
    }

    /**
     * @param array $data
     * @return bool
     */
    public function editDemand($data = array())
    {
        unset($data['DemandInfo']['demand_status']);

        $data['DemandInfo']['order_date'] = str_replace('-', '/', $data['DemandInfo']['order_date']);

        // 更新
        if ($this->demandInfoRepository->save($data['DemandInfo'])) {
            return true;
        }

        return false;
    }

    /**
     * @param null  $commissionId
     * @param array $data
     * @return bool
     */
    public function registHistory($commissionId = null, $data = array())
    {
        $data['commission_id'] = $commissionId;

        if (!empty($data['responders']) || !empty($data['rits_responders']) || !empty($data['corresponding_contens'])) {
            if ($this->commissionCorrespondsRepository->save($data)) {
                return true;
            }

            return false;
        }

        return true;
    }

    /**
     * @param null $id
     * @param null $data
     * @return string
     */
    public function getCorrespond($id = null, $data = null)
    {
        $correspond = '';

        if (!empty($data['CommissionInfo'])) {
            $columns = $this->getColumn();
            $oldData = $this->commissionInfoRepository->find($id)->toArray();

            foreach ($data['CommissionInfo'] as $newKey => $newValue) {
                foreach ($oldData as $oldKey => $oldValue) {
                    if ($newKey == 'modified' || $newKey == 'created' || $newKey == 'modifiedby' || $newKey == 'createdby') {
                        break;
                    }

                    if ($newKey == $oldKey) {
                        //日付フォーマット
                        if ($newKey == 'commission_note_send_datetime' || $newKey == 'tel_commission_datetime') {
                            if (!empty($newValue)) {
                                $newValue = str_replace('/', '-', $newValue);
                                $newValue = $newValue.':00';
                            }
                        } elseif ($newKey == 'commission_status_last_updated') {
                            break;
                        }

                        if ($newValue != $oldValue) {
                            $comment = '';
                            $col = '';
                            $newText = '';
                            $oldText = '';

                            foreach ($columns as $column) {
                                if ($column['column_name'] == $newKey) {
                                    $comment = $column['column_comment'];
                                    $col = $column['column_name'];

                                    $newText = $this->getValueCommissionInfo($column['column_name'], $newValue);
                                    $oldText = $this->getValueCommissionInfo($column['column_name'], $oldValue);

                                    break;
                                }
                            }
                            $new = $newValue;
                            $old = $oldValue;

                            if (!empty($newText)) {
                                $new = $newText;
                            }
                            if (!empty($oldText)) {
                                $old = $oldText;
                            }

                            $correspond .= $comment.' : '.$old.' → '.$new."\n";
                        }

                        break;
                    }
                }
            }
        }

        //案件管理
        if (!empty($data['DemandInfo'])) {
            $columns = $this->getColumn('demand_infos');
            $oldData = $this->demandInfoRepository->find($data['DemandInfo']['id'])->toArray();

            foreach ($data['DemandInfo'] as $newKey => $newValue) {
                foreach ($oldData as $oldKey => $oldValue) {
                    if ($newKey == 'modified' || $newKey == 'created' || $newKey == 'modifiedby' || $newKey == 'createdby' || $newKey == 'demand_status') {
                        break;
                    }

                    if ($newKey == $oldKey) {
                        if ($newValue != $oldValue) {
                            $comment = '';
                            $col = '';
                            $newText = '';
                            $oldText = '';

                            foreach ($columns as $column) {
                                if ($column['column_name'] == $newKey) {
                                    $comment = $column['column_comment'];
                                    $col = $column['column_name'];

                                    $newText = $this->getValueDemandInfo($column['column_name'], $newValue);
                                    $oldText = $this->getValueDemandInfo($column['column_name'], $oldValue);

                                    break;
                                }
                            }

                            $new = $newValue;
                            $old = $oldValue;

                            if (! empty($newText)) {
                                $new = $newText;
                            }
                            if (! empty($oldText)) {
                                $old = $oldText;
                            }

                            $correspond .= $comment . ' : ' . $old . ' → ' . $new . "\n";
                        }

                        break;
                    }
                }
            }
        }

        return $correspond;
    }

    /**
     * @param null  $id
     * @param array $data
     * @return bool
     */
    public function registBillInfo($id = null, $data = [])
    {
        $setData = $data['BillInfo'];

        if ($data['CommissionInfo']['commission_status'] == getDivValue('construction_status', 'introduction') && $data['CommissionInfo']['introduction_free'] == 1) {
            $setData['fee_target_price'] = 0;
            $setData['fee_tax_exclude'] = 0;
        }

        $setData['demand_id'] = $data['CommissionInfo']['demand_id'];
        $setData['commission_id'] = $id;

        if (isset($data['CommissionInfo']['deduction_tax_include'])) {
            $setData['deduction_tax_include'] = $data['CommissionInfo']['deduction_tax_include'];
        }

        if (isset($data['CommissionInfo']['deduction_tax_exclude'])) {
            $setData['deduction_tax_exclude'] = $data['CommissionInfo']['deduction_tax_exclude'];
        }

        if (isset($data['CommissionInfo']['irregular_fee_rate'])) {
            $setData['irregular_fee_rate'] = $data['CommissionInfo']['irregular_fee_rate'];
        }

        if (isset($data['CommissionInfo']['irregular_fee'])) {
            $setData['irregular_fee'] = $data['CommissionInfo']['irregular_fee'];
        }

        if (isset($data['CommissionInfo']['confirmd_fee_rate'])) {
            $setData['comfirmed_fee_rate'] = $data['CommissionInfo']['confirmd_fee_rate'];
        }

        $setData['tax'] = $setData['fee_tax_exclude'] * ($data['MTaxRate']['tax_rate'] / 100);

        if (isset($data['insurance_price'])) {
            $setData['insurance_price'] = $data['insurance_price'];
        } else {
            $setData['insurance_price'] = 0;
        }

        $setData['total_bill_price'] = $setData['fee_tax_exclude'] + $setData['tax'] + $setData['insurance_price'];

        if (empty($data['BillInfo']['id'])) {
            $setData['bill_status'] = 1;
            $setData['fee_payment_price'] = 0;
            $setData['fee_payment_balance'] = $setData['total_bill_price'];
        } else {
            if (empty($setData['fee_payment_price'])) {
                $setData['fee_payment_price'] = 0;
            }
            $setData['fee_payment_balance'] = $setData['total_bill_price'] - $setData['fee_payment_price'];
        }

        if ($this->billRepository->insertData($setData)) {
            return true;
        }

        return false;
    }

    /**
     * @param null $id
     * @param null $demandId
     * @return bool
     */
    public function setOtherCommission($id = null, $demandId = null)
    {
        $result = false;
        $rows = $this->commissionInfoRepository->getByDemandId($demandId, $id);

        if (count($rows) == 0) {
            $result = true;
        } else {
            foreach ($rows as $row) {
                $saveData = [
                    'id' => $row['id'],
                    'demand_id' => $demandId,
                    'unit_price_calc_exclude' => 1
                ];

                $this->commissionInfoRepository->save($saveData);
                $result = true;
            }
        }

        return $result;
    }

    /**
     * @param $data
     * @return bool
     */
    public function updateUploadFileName($data)
    {
        try {
            $demandInfo = [
                'id' => $data['DemandInfo']['id']
            ];

            if (isset($data['DemandInfo']['jbr_estimate']['name'])) {
                $demandInfo['upload_estimate_file_name'] = $data['DemandInfo']['jbr_estimate']['name'];
            }

            if (isset($data['DemandInfo']['jbr_receipt']['name'])) {
                $demandInfo['upload_receipt_file_name'] = $data['DemandInfo']['jbr_receipt']['name'];
            }

            if (count($demandInfo) > 1) {
                $this->demandInfoRepository->save($demandInfo);
            }
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @param $id
     * @param $data
     * @return bool
     */
    public function registCorrespond($id, $data)
    {
        $correspond = $this->getCorrespond($id, $data);
        $result = true;

        if (! empty($correspond)) {
            $dataCorrespond['corresponding_contens'] = $correspond;
            $dataCorrespond['responders'] = '自動登録[]';
            $dataCorrespond['rits_responders'] = null;
            $dataCorrespond['commission_id'] = $id;
            $dataCorrespond['created_user_id'] = 'system';
            $dataCorrespond['modified_user_id'] = 'system';
            $dataCorrespond['correspond_datetime'] = date('Y-m-d H:i:s');

            if (! $this->commissionCorrespondsRepository->save($dataCorrespond)) {
                $result = false;
            }
        }

        return $result;
    }

    /**
     * @param $commissionId
     * @param $commissionStatus
     * @param $completeDate
     * @param $constructionPriceTaxExculude
     * @return array
     */
    public function calcBillPrice($commissionId, $commissionStatus, $completeDate, $constructionPriceTaxExculude)
    {
        $data = $this->commissionInfoRepository->findCommissionInfo($commissionId);
        $data['CommissionInfo__commission_status'] = $commissionStatus;
        $data['CommissionInfo__complete_date'] = $completeDate;
        $data['CommissionInfo__construction_price_tax_exclude'] = $constructionPriceTaxExculude;

        $data = $this->calculateBillPrice($data);

        $result = [
            'MTaxRate' => ['tax_rate' => $data['MTaxRate__tax_rate']],
            'CommissionInfo' => [
                'construction_price_tax_exclude' => $data['CommissionInfo__construction_price_tax_exclude'],
                'construction_price_tax_include' => $data['CommissionInfo__construction_price_tax_include'],
                'corp_fee' => $data['CommissionInfo__corp_fee'],
                'deduction_tax_exclude' => $data['CommissionInfo__deduction_tax_exclude'],
                'deduction_tax_include' => $data['CommissionInfo__deduction_tax_include'],
                'confirmd_fee_rate' => $data['CommissionInfo__confirmd_fee_rate'],
            ],
            'BillInfo' => [
                'fee_target_price' => $data['BillInfo__fee_target_price'],
                'fee_tax_exclude' => $data['BillInfo__fee_tax_exclude'],
                'tax' => $data['BillInfo__tax'],
                'insurance_price' => $data['BillInfo__insurance_price'],
                'total_bill_price' => $data['BillInfo__total_bill_price']
            ],
        ];

        return $result;
    }

    /**
     * @param $auctionId
     * @param $data
     * @return array
     */
    private function getAuctionCommission($auctionId, $data)
    {
        if (empty($auctionId)) {
            return $data;
        }

        $auction = $this->billRepository->findByAuctionId($auctionId);
        $data += (array) $auction;

        return $data;
    }

    /**
     * @param string $tableName
     * @return array
     */
    private function getColumn($tableName = 'commission_infos')
    {
        $rows = [];

        if ($tableName == 'commission_infos') {
            $rows = $this->commissionInfoRepository->getAllFields();
        } elseif ($tableName == 'demand_infos') {
            $rows = $this->demandInfoRepository->getAllFields();
        }

        foreach ($rows as $row) {
            $result[] = (array) $row;
        }

        return $result;
    }

    /**
     * @param null $col
     * @param null $val
     * @return mixed|string
     */
    private function getValueCommissionInfo($col = null, $val = null)
    {
        if (empty($col) || ! isset($val) || $val == '') {
            return '';
        }

        $rtn = [];

        if ($col == 'irregular_reason') {
            $rtn = getDropList('イレギュラー理由');
        } elseif ($col == 're_commission_exclusion_status') {
            $rtn = array(
                '0' => '',
                '1' => '成功',
                '2' => '失敗'
            );
        } elseif ($col == 'reform_upsell_ic') {
            $rtn = array(
                '1' => '申請',
                '2' => '認証',
                '3' => '非認証'
            );
        } elseif ($col == 'commission_type') {
            $rows = DB::table('m_commission_types')
                        ->orderBy('id', 'ASC')
                        ->get();

            foreach ($rows as $row) {
                $rtn[$row->id] = $row->commission_type_name;
            }
        } elseif ($col == 'commission_status') {
            $rtn = getDropList('commission_status');
        } elseif ($col == 'commission_order_fail_reason') {
            $rtn = getDropList('commission_order_fail_reason');
        } elseif ($col == 'progress_reported' || $col == 'unit_price_calc_exclude' || $col == 'first_commission' || $col == 'introduction_free' || $col == 'ac_commission_exclusion_flg') {
            $rtn = array(
                '0' => 'チェック無',
                '1' => 'チェック有'
            );
        } elseif ($col == 'tel_support' || $col == 'visit_support' || $col == 'order_support') {
            $rtn = array(
                '0' => '対応中',
                '1' => '非対応'
            );
        } elseif ($col == 'order_fee_unit') {
            $rtn = array(
                '0' => '円',
                '1' => '%'
            );
        } elseif ($col == 'appointers' || $col == 'tel_commission_person' || $col == 'commission_note_sender') {
            $rtn = $this->getMUsers();
        }

        return isset($rtn[$val]) ? $rtn[$val] : '';
    }

    /**
     * @param null $col
     * @param null $val
     * @return mixed|string
     */
    private function getValueDemandInfo($col = null, $val = null)
    {
        if (empty($col) || ! isset($val) || $val == '') {
            return '';
        }

        $rtn = [];

        if ($col == 'construction_class') {
            $rtn = getDropList('建物種別');
        } elseif ($col == 'demand_status') {
            $rtn = getDropList('demand_status');
        } elseif ($col == 'order_fail_reason') {
            $rtn = getDropList('order_fail_reason');
        } elseif ($col == 'jbr_work_contents') {
            $rtn = getDropList('jbr_work_contents');
        } elseif ($col == 'jbr_category') {
            $rtn = getDropList('jbr_category');
        } elseif ($col == 'jbr_estimate_status') {
            $rtn = getDropList('jbr_estimate_status');
        } elseif ($col == 'jbr_receipt_status') {
            $rtn = getDropList('jbr_receipt_status');
        } elseif ($col == 'pet_tombstone_demand') {
            $rtn = getDropList('pet_tombstone_demand');
        } elseif ($col == 'sms_demand') {
            $rtn = getDropList('sms_demand');
        } elseif ($col == 'special_measures') {
            $rtn = getDropList('案件特別施策');
        } elseif ($col == 'acceptance_status') {
            $rtn = getDropList('受付ステータス');
        } elseif ($col == 'priority') {
            $rtn = array(
                '0' => '-',
                '1' => '大至急',
                '2' => '至急',
                '3' => '通常'
            );
        } elseif ($col == 'riro_kureka') {
            $rtn = array(
                '0' => 'チェック無',
                '1' => 'チェック有'
            );
        }

        return isset($rtn[$val]) ? $rtn[$val] : '';
    }

    /**
     * @param $data
     * @return mixed
     */
    private function calculateBillPrice($data)
    {
        $taxRate = $this->getMTaxRates($data['CommissionInfo__complete_date']);
        $data['MTaxRate__tax_rate'] = $taxRate['tax_rate'];

        if ($data['CommissionInfo__commission_status'] != getDivValue('construction_status', 'introduction')) {
            $construction_price_tax_exclude = $data['CommissionInfo__construction_price_tax_exclude'];

            if (empty($construction_price_tax_exclude)) {
                $construction_price_tax_exclude = 0;
            }

            if (empty($data['CommissionInfo__business_trip_amount'])) {
                $data['CommissionInfo__business_trip_amount'] = 0;
            }

            if (empty($data['CommissionInfo__deduction_tax_include'])) {
                $data['CommissionInfo__deduction_tax_include'] = 0;
            }

            if ($taxRate['tax_rate_val'] != '') {
                if (!empty($data['CommissionInfo__construction_price_tax_exclude'])) {
                    $data['CommissionInfo__construction_price_tax_include'] = round($construction_price_tax_exclude * (1 + $taxRate['tax_rate_val']));
                } else {
                    $data['CommissionInfo__construction_price_tax_include'] = $data['CommissionInfo__construction_price_tax_exclude'];
                }

                if (!empty($data['CommissionInfo__deduction_tax_include'])) {
                    $data['CommissionInfo__deduction_tax_exclude'] = round($data['CommissionInfo__deduction_tax_include'] / (1 + $taxRate['tax_rate_val']));
                } else {
                    $data['CommissionInfo__deduction_tax_exclude'] = 0;
                }
            } else {
                $data['CommissionInfo__construction_price_tax_include'] = $data['CommissionInfo__construction_price_tax_exclude'];
                $data['CommissionInfo__deduction_tax_exclude'] = $data['CommissionInfo__deduction_tax_include'];
            }

            if (empty($data['CommissionInfo__deduction_tax_exclude'])) {
                $data['CommissionInfo__deduction_tax_exclude'] = 0;
            }

            if ($construction_price_tax_exclude != 0) {
                $data['BillInfo__fee_target_price']
                = $construction_price_tax_exclude - $data['CommissionInfo__deduction_tax_exclude'];
            } else {
                $data['BillInfo__fee_target_price'] = 0;
            }

            if ($data['MGenre__insurant_flg'] == 1 && $data['AffiliationInfo__liability_insurance'] == 2) {
                    // 保険料 = 施工金額(税抜) × 0.01
                    $data['BillInfo__insurance_price'] = round($construction_price_tax_exclude * 0.01);
            } else {
                $data['BillInfo__insurance_price'] = 0;
            }
        }

        if (is_null($data['CommissionInfo__order_fee_unit'])) {
            if (is_null($data['MCorpCategory__order_fee_unit'])) {
                $default_category = $this->MCategory->getDefault_fee($data['DemandInfo__category_id']);
                $data['CommissionInfo__order_fee_unit'] = $default_category['category_default_fee_unit'];
            } else {
                $data['CommissionInfo__order_fee_unit'] = $data['MCorpCategory__order_fee_unit'];
            }
        }

        if ($data['CommissionInfo__order_fee_unit'] != 0 && $data['CommissionInfo__commission_status'] != getDivValue('construction_status', 'introduction')) {
            if (!empty($data['CommissionInfo__irregular_fee_rate'])) {
                $data['CommissionInfo__confirmd_fee_rate'] = $data['CommissionInfo__irregular_fee_rate'];
            } else {
                if (empty($data['CommissionInfo__confirmd_fee_rate'])) {
                    $data['CommissionInfo__confirmd_fee_rate'] = $data['CommissionInfo__commission_fee_rate'];
                }
            }

            if (!empty($data['CommissionInfo__irregular_fee'])) {
                $data['BillInfo__fee_tax_exclude'] = $data['CommissionInfo__irregular_fee'];
            } else {
                $data['BillInfo__fee_tax_exclude'] = round($data['BillInfo__fee_target_price'] * $data['CommissionInfo__confirmd_fee_rate'] * 0.01);
            }
            if (!empty($data['BillInfo__fee_tax_exclude'])) {
                $data['CommissionInfo__corp_fee'] = $data['BillInfo__fee_tax_exclude'];
            }
        } else {
            if (!empty($data['CommissionInfo__irregular_fee'])) {
                $data['BillInfo__fee_tax_exclude'] = $data['CommissionInfo__irregular_fee'];
            } else {
                $data['BillInfo__fee_tax_exclude'] = $data['CommissionInfo__corp_fee'];
            }

            if ($data['CommissionInfo__commission_status'] == getDivValue('construction_status', 'introduction')) {
                $data['BillInfo__fee_target_price'] = $data['BillInfo__fee_tax_exclude'];

                if ($data['CommissionInfo__introduction_free'] == 1) {
                    $data['BillInfo__fee_tax_exclude'] = 0;
                }
            }
        }

        if (!empty($taxRate['tax_rate_val'])) {
            $data['BillInfo__tax'] = round($data['BillInfo__fee_tax_exclude'] * $taxRate['tax_rate_val']);
        } else {
            $data['BillInfo__tax'] = 0;
        }

        $fee_tax_exclude = !empty($data['BillInfo__fee_tax_exclude']) ? $data['BillInfo__fee_tax_exclude'] : 0;
        $data['BillInfo__total_bill_price'] = $fee_tax_exclude + $data['BillInfo__tax'] + $data['BillInfo__insurance_price'];

        return $data;
    }
}
