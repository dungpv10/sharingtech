<?php

namespace App\Services\Commission;

use App\Repositories\MPostRepositoryInterface;
use App\Services\BaseService;

class CommissionSelectService extends BaseService
{
    /**
     * @var MPostRepositoryInterface
     */
    protected $mPostRepository;

    /**
     * CommissionSelectService constructor.
     *
     * @param \App\Repositories\MPostRepositoryInterface $mPostRepository
     */
    public function __construct(MPostRepositoryInterface $mPostRepository)
    {
        $this->mPostRepository = $mPostRepository;
    }

    /**
     * Check value of key address2 and add key jis_ci
     * Use in CommissionSelectController
     *
     * @author Nguyen.DoNhu <Nguyen.DoNhu@Nashtechglobal.com>
     * @param  array $data
     * @return array
     */
    public function validateDataAndAddParams($data)
    {
        if (isset($data['address2'])) {
            $dateAddress2 = $data['address2'];
            $dateAddress2 = preg_replace('/^[ 　]+/u', '', $dateAddress2);
            $dateAddress2 = preg_replace('/[ 　]+$/u', '', $dateAddress2);
            $data['address2'] = $dateAddress2;
        }

        if (!isset($data['search'])) {
            $data['jis_cd'] = '';
            if (!empty($data['address1']) && $data['address1'] != '99') {
                $data['jis_cd'] = $this->mPostRepository->getTargetArea($data);
            }
        }

        return $data;
    }

    /**
     * Update value of key commission_unit_price_rank_
     *
     * @author Nguyen.DoNhu <Nguyen.DoNhu@Nashtechglobal.com>
     * @param  array   $obj
     * @param  integer $position
     */
    private function changeCommissionUnitPrice(&$obj, $position)
    {
        if (empty($obj['targer_commission_unit_price'])) {
            $obj['commission_unit_price_rank_' . $position] = 'a';
        } elseif (empty($obj['commission_unit_price_category'])) {
            $obj['commission_unit_price_rank_' . $position] = 'd';
        } else {
            $rankF = $obj['commission_unit_price_category'] / $obj['targer_commission_unit_price'] * 100;
            if ($rankF >= 100) {
                $obj['commission_unit_price_rank_' . $position] = 'a';
            } elseif ($rankF >= 80) {
                $obj['commission_unit_price_rank_' . $position] = 'b';
            } elseif ($rankF >= 65) {
                $obj['commission_unit_price_rank_' . $position] = 'c';
            } else {
                $obj['commission_unit_price_rank_' . $position] = 'd';
            }
        }
    }

    /**
     * Return new corp by update value of key commission_unit_price_rank_
     *
     * @author Nguyen.DoNhu <Nguyen.DoNhu@Nashtechglobal.com>
     * @param  array $corpList
     * @return array
     */
    public function getNewCorpList($corpList)
    {
        $temp = [];
        for ($i = 0; $i < count($corpList); $i++) {
            $obj = $corpList[$i];
            $this->changeCommissionUnitPrice($obj, 1);
            $this->changeCommissionUnitPrice($obj, 2);
            $temp[] = $obj;
        }
        return $temp;
    }
}
