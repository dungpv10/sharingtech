<?php

namespace App\Services;

use App\Models\MCorpNewYear;
use App\Repositories\BillRepositoryInterface;
use App\Repositories\CommissionInfoRepositoryInterface;
use App\Repositories\Eloquent\CommissionCorrespondsRepository;
use App\Repositories\MCategoryRepositoryInterface;
use App\Repositories\MCorpCategoryRepositoryInterface;
use App\Repositories\MTaxRateRepositoryInterface;
use App\Repositories\MCorpRepositoryInterface;
use App\Repositories\MUserRepositoryInterface;
use App\Repositories\MSiteRepositoryInterface;
use App\Repositories\VisitTimeRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Services\Aws\AwsUtilService;
use App\Models\MUser;
use Illuminate\Support\Facades\Validator;

class CommissionService
{
    /**
     * @var CommissionInfoRepositoryInterface
     */
    protected $commissionInfoRepo;

    /**
     * @var BillRepositoryInterface
     */
    protected $billRepo;

    /**
     * @var MTaxRateRepositoryInterface
     */
    protected $mTaxRateRepo;

    /**
     * @var MCategoryRepositoryInterface
     */
    protected $mCategoryRepo;

    /**
     * @var MCorpCategoryRepositoryInterface
     */
    protected $mCorpCategoryRepo;

    /**
     * @var CommissionCorrespondsRepository
     */
    protected $commissionCorRepo;

    /**
     * @var MCorpRepositoryInterface
     */
    protected $mCorpRepo;

    /**
     * @var VisitTimeRepositoryInterface
     */
    protected $visitTimeRepo;

    /**
     * @var MUserRepositoryInterface
     */
    protected $mUserRepo;

    /**
     * @var AwsUtilService
     */
    protected $snsService;

    /**
     * @var MSiteRepositoryInterface
     */
    protected $mSiteRepo;

    /**
     * CommissionService constructor.
     * @param CommissionInfoRepositoryInterface $commissionInfoRepo
     * @param BillRepositoryInterface $billRepo
     * @param MTaxRateRepositoryInterface $mTaxRateRepo
     * @param MCategoryRepositoryInterface $mCategoryRepo
     * @param MCorpCategoryRepositoryInterface $mCorpCategoryRepo
     * @param CommissionCorrespondsRepository $commissionCorrespondsRepo
     * @param MCorpRepositoryInterface $mCorpRepo
     * @param VisitTimeRepositoryInterface $visitTimeRepo
     * @param MUserRepositoryInterface $mUserRepo
     * @param AwsUtilService $snsService
     * @param MSiteRepositoryInterface $mSiteRepo
     */
    public function __construct(
        CommissionInfoRepositoryInterface $commissionInfoRepo,
        BillRepositoryInterface $billRepo,
        MTaxRateRepositoryInterface $mTaxRateRepo,
        MCategoryRepositoryInterface $mCategoryRepo,
        MCorpCategoryRepositoryInterface $mCorpCategoryRepo,
        CommissionCorrespondsRepository $commissionCorrespondsRepo,
        MCorpRepositoryInterface $mCorpRepo,
        VisitTimeRepositoryInterface $visitTimeRepo,
        MUserRepositoryInterface $mUserRepo,
        AwsUtilService $snsService,
        MSiteRepositoryInterface $mSiteRepo
    ) {
        $this->commissionInfoRepo = $commissionInfoRepo;
        $this->billRepo = $billRepo;
        $this->mTaxRateRepo = $mTaxRateRepo;
        $this->mCategoryRepo = $mCategoryRepo;
        $this->commissionCorRepo = $commissionCorrespondsRepo;
        $this->mCorpRepo = $mCorpRepo;
        $this->mCorpCategoryRepo = $mCorpCategoryRepo;
        $this->visitTimeRepo = $visitTimeRepo;
        $this->snsService = $snsService;
        $this->mSiteRepo = $mSiteRepo;
        $this->mUserRepo = $mUserRepo;
        $this->mUser = new MUser();
    }

    /**
     * Update approvals.status
     *
     * @param  $approvalId
     * @param  $commissionApp
     * @param  $actionName
     * @param  $user
     * @return bool
     * @throws \Exception
     */
    public function commissionApproval($approvalId, $commissionApp, $actionName, $user)
    {
        DB::beginTransaction();

        /* Update approval */
        if ($actionName == "rejected") {
            $this->approvalRepository->getBlankModel()->where("id", $approvalId)->update(["status" => 2]);
        } else {
            if ($actionName == "approval") {
                $this->approvalRepository->getBlankModel()->where("id", $approvalId)->update(["status" => 1]);
            }
        }

        $commissionData = $this->setCommission($commissionApp->commission_id);
        unset($commissionData['commit_flg']);
        $modified = $commissionData['modified'];
        unset($commissionData['modified']);
        /* if flag deduction tax include is TRUE */
        if ($commissionApp->chg_deduction_tax_include) {
            $commissionData['deduction_tax_include'] = $commissionApp->deduction_tax_include;
        }
        /* if flag irregular fee rate is TRUE */
        if ($commissionApp->chg_irregular_fee_rate) {
            $commissionData['irregular_fee_rate'] = $commissionApp->irregular_fee_rate;
        }
        /* if flag irregular rate is TRUE */
        if ($commissionApp->chg_irregular_fee) {
            $commissionData['irregular_fee'] = $commissionApp->irregular_fee;
        }
        /* if flag introduction free is TRUE */
        if ($commissionApp->chg_introduction_free) {
            $commissionData['introduction_free'] = $commissionApp->introduction_free;
        }
        /* if flag irregular reason is TRUE */
        if ($commissionApp->chg_irregular_fee_rate || $commissionApp->chg_irregular_fee) {
            $commissionData['irregular_reason'] = $commissionApp->irregular_reason;
        }
        if ($commissionApp->chg_ac_commission_exclusion_flg) {
            $commissionData['ac_commission_exclusion_flg'] = $commissionApp->ac_commission_exclusion_flg;
        }
        if ($commissionApp->chg_introduction_not) {
            $commissionData['introduction_not'] = $commissionApp->introduction_not;
        }
        /* Calculator price in bill info */
        $commissionData = $this->priceCalc($commissionData);

        $resultsFlg = true;
        if ($this->checkModifiedCommission($commissionApp->commission_id, $modified)) {
            if (!empty($commissionData['commission_note_send_datetime'])) {
                $commissionData['commission_note_send_datetime'] = substr($commissionData['commission_note_send_datetime'], 0, strlen($commissionData['commission_note_send_datetime']) - 3);
            }
            if (!empty($commissionData['tel_commission_datetime'])) {
                $commissionData['tel_commission_datetime'] = substr($commissionData['tel_commission_datetime'], 0, strlen($commissionData['tel_commission_datetime']) - 3);
            }

            $correspond = $this->getCorrespond($commissionApp->commission_id, $commissionData);

            if ($actionName == 'approval') {
                $resultsFlg = $this->editCommission($commissionApp->commission_id, $commissionData, $user, 0);

                if ($resultsFlg) {
                    if ($commissionData['commission_status'] == getDivValue('construction_status', 'construction') || $commissionData['commission_status'] == getDivValue('construction_status', 'introduction')) {
                        $resultsFlg = $this->registBillInfo($commissionApp->commission_id, $commissionData);
                    }

                    if ($resultsFlg && !empty($correspond)) {
                        $cd = [];
                        $cd['corresponding_contens'] = $correspond;
                        $cd['responders'] = trans("commission.service.automatic.registration").'['.$user->user_name.']';
                        $cd['rits_responders'] = null;
                        $cd['commission_id'] = $commissionApp->commission_id;
                        $cd['created_user_id'] = 'system';
                        $cd['modified_user_id'] = 'system';
                        $cd['correspond_datetime'] = date('Y-m-d H:i:s');

                        $this->commissionCorRepo->getBlankModel()->insert($cd);
                    }
                }
            }
        }

        if ($resultsFlg) {
            DB::commit();
        } else {
            DB::rollback();
        }

        return $resultsFlg;
    }

    /**
     * Get commission_info by $commissionId
     *
     * @author Nguyen.DoNhu <Nguyen.DoNhu@Nashtechglobal.com>
     * @param  integer $commissionId
     * @return mixed
     */
    private function setCommission($commissionId = null)
    {
        $result = $this->commissionInfoRepo->getCommissionInfoByIdForApproval($commissionId);
        if (!empty($result['demandInfo']['auctionInfo']['id'])) {
            $result = $this->setAuctionCommission($result['demandInfo']['auctionInfo']['id'], $result);
        }
        $result = $this->setSupport($commissionId, $result);

        return $result;
    }

    /**
     * Find and add bill_info in $data if $auctionId not empty
     *
     * @param  $auctionId
     * @param  $data
     * @return mixed
     */
    private function setAuctionCommission($auctionId, $data)
    {
        if (empty($auctionId)) {
            return $data;
        }

        $billInfo = $this->billRepo->findBy("auction_id", $auctionId);
        $data['bill_info'] = $billInfo->toArray();

        return $data;
    }

    /**
     * Update item in $data
     *
     * @author Nguyen.DoNhu <Nguyen.DoNhu@Nashtechglobal.com>
     * @param  integer $commissionId
     * @param  array   $data
     * @return mixed
     */
    private function setSupport($commissionId, $data)
    {
        $commissionOrder = $this->commissionOrderRepo->findBy("commission_id", $commissionId);
        if ($commissionOrder) {
            $data['commission_order_support'] = $commissionOrder->toArray();
        }

        $commissionTel = $this->commissionTelRepo->findBy("commission_id", $commissionId);
        if ($commissionOrder) {
            $data['commission_tel_support'] = $commissionTel->toArray();
        }

        $commissionVisit = $this->commissionVisitRepo->findBy("commission_id", $commissionId);
        if ($commissionOrder) {
            $data['commission_visit_support'] = $commissionVisit->toArray();
        }

        return $data;
    }

    /**
     * Calculator price tax
     *
     * @author Nguyen.DoNhu <Nguyen.DoNhu@Nashtechglobal.com>
     * @param  array $data
     * @return array
     */
    private function priceCalc($data = null)
    {
        /* Get tax rate */
        $taxRate = $this->setMTaxRates($data['complete_date']);

        /* Add key m_tax_rate */
        $data['m_tax_rate'] = [];
        /* Add ['m_tax_rate']['tax_rate'] = $taxRate['tax_rate'] */
        $data['m_tax_rate']['tax_rate'] = $taxRate['tax_rate'];
        /* Add key insurance_price */
        $data['insurance_price'] = $data['bill_info']['insurance_price'];

        $calcData = $this->calculateBillPrice($data);

        $data['corp_fee'] = $calcData['corp_fee'];
        $data['construction_price_tax_exclude'] = $calcData['construction_price_tax_exclude'];
        $data['construction_price_tax_include'] = $calcData['construction_price_tax_include'];
        $data['deduction_tax_exclude'] = $calcData['deduction_tax_exclude'];
        $data['deduction_tax_include'] = $calcData['deduction_tax_include'];
        $data['confirmd_fee_rate'] = $calcData['confirmd_fee_rate'];

        $data['bill_info']['fee_target_price'] = $calcData['bill_info']['fee_target_price'];
        $data['bill_info']['fee_tax_exclude'] = $calcData['bill_info']['fee_tax_exclude'];
        $data['bill_info']['total_bill_price'] = $calcData['bill_info']['total_bill_price'];
        $data['bill_info']['tax'] = $calcData['bill_info']['tax'];
        $data['bill_info']['insurance_price'] = $calcData['bill_info']['insurance_price'];

        return $data;
    }

    /**
     * Get tax rate in table m_tax_rates by date
     * return array[tax_rate, tax_rate_val] with tax_rate_val is real value of tax_rate
     * and tax_rate = tax_rate*100
     *
     * @param  string $date
     * @return array
     */
    private function setMTaxRates($date = null)
    {
        if (!empty($date)) {
            $result = $this->mTaxRateRepo->findByDate($date);
            if ($result) {
                $result = $result->toArray();
                $result['tax_rate_val'] = $result['tax_rate'];
                $result['tax_rate'] *= 100;

                return $result;
            }
        } else {
            return ['tax_rate' => '', 'tax_rate_val' => ''];
        }
    }

    /**
     * Update item in $data by get tax rate
     *
     * @param  array $data
     * @return mixed
     */
    private function calculateBillPrice($data)
    {
        /* Get tax rate */
        $taxRate = $this->setMTaxRates($data['complete_date']);
        $data['m_tax_rate']['tax_rate'] = $taxRate['tax_rate'];
        if ($data['commission_status'] != getDivValue('construction_status', 'introduction')) {
            $constructionPriceTaxExclude = $data['construction_price_tax_exclude'];
            if (empty($constructionPriceTaxExclude)) {
                $constructionPriceTaxExclude = 0;
            }
            if (empty($data['business_trip_amount'])) {
                $data['business_trip_amount'] = 0;
            }
            if (empty($data['deduction_tax_include'])) {
                $data['deduction_tax_include'] = 0;
            }
            /* Update key construction_price_tax_include and deduction_tax_exclude*/
            if (array_key_exists('tax_rate_val', $taxRate) && $taxRate['tax_rate_val'] != '') {
                if (!empty($data['construction_price_tax_exclude'])) {
                    $data['construction_price_tax_include'] = round($constructionPriceTaxExclude * (1 + $taxRate['tax_rate_val']));
                } else {
                    $data['construction_price_tax_include'] = $data['construction_price_tax_exclude'];
                }
                if (!empty($data['deduction_tax_include'])) {
                    $data['deduction_tax_exclude'] = round($data['deduction_tax_include'] / (1 + $taxRate['tax_rate_val']));
                } else {
                    $data['deduction_tax_exclude'] = 0;
                }
            } else {
                $data['construction_price_tax_include'] = $data['construction_price_tax_exclude'];
                $data['deduction_tax_exclude'] = $data['deduction_tax_include'];
            }
            if (empty($data['deduction_tax_exclude'])) {
                $data['deduction_tax_exclude'] = 0;
            }

            /* Update $data['bill_info']['fee_target_price'] */
            if ($constructionPriceTaxExclude != 0) {
                $data['bill_info']['fee_target_price'] = $constructionPriceTaxExclude - $data['deduction_tax_exclude'];
            } else {
                $data['bill_info']['fee_target_price'] = 0;
            }
            /* Update $data['bill_info']['insurance_price'] */
            if ($data['demand_info']['m_genres']['insurant_flg'] == 1 && $data['affiliation_info']['liability_insurance'] == 2) {
                $data['bill_info']['insurance_price'] = round($constructionPriceTaxExclude * 0.01);
            } else {
                $data['bill_info']['insurance_price'] = 0;
            }
        }

        /* Update $data['order_fee_unit'] if null */
        if (!isset($data['order_fee_unit']) || is_null($data['order_fee_unit'])) {
            if (!isset($data['m_corp_category']['order_fee_unit']) || is_null($data['m_corp_category']['order_fee_unit'])) {
                $defaultCategory = $this->mCategoryRepo->find($data['demand_info']['category_id']);
                if ($defaultCategory) {
                    $data['order_fee_unit'] = $defaultCategory->category_default_fee_unit;
                }
            } else {
                $data['order_fee_unit'] = $data['m_corp_category']['order_fee_unit'];
            }
        }

        if ($data['order_fee_unit'] != 0 && $data['commission_status'] != getDivValue('construction_status', 'introduction')) {
            /* Update $data['confirmd_fee_rate'] */
            if (!empty($data['irregular_fee_rate'])) {
                $data['confirmd_fee_rate'] = $data['irregular_fee_rate'];
            } else {
                if (empty($data['confirmd_fee_rate'])) {
                    $data['confirmd_fee_rate'] = $data['commission_fee_rate'];
                }
            }
            /* Update $data['bill_info']['fee_tax_exclude'] */
            if (!empty($data['irregular_fee'])) {
                $data['bill_info']['fee_tax_exclude'] = $data['irregular_fee'];
            } else {
                $data['bill_info']['fee_tax_exclude'] = round($data['bill_info']['fee_target_price'] * $data['confirmd_fee_rate'] * 0.01);
            }
            /* Update $data['corp_fee'] */
            if (!empty($data['bill_info']['fee_tax_exclude'])) {
                $data['corp_fee'] = $data['bill_info']['fee_tax_exclude'];
            }
        } else {
            /* Update $data['bill_info']['fee_tax_exclude'] */
            if (!empty($data['irregular_fee'])) {
                $data['bill_info']['fee_tax_exclude'] = $data['irregular_fee'];
            } else {
                $data['bill_info']['fee_tax_exclude'] = $data['corp_fee'];
            }
            /* If $data['commission_status'] == 5 */
            if ($data['commission_status'] == getDivValue('construction_status', 'introduction')) {
                $data['bill_info']['fee_target_price'] = $data['bill_info']['fee_tax_exclude'];
                if ($data['introduction_free'] == 1) {
                    $data['bill_info']['fee_tax_exclude'] = 0;
                }
            }
        }

        /* Update $data['bill_info']['tax'] */
        if (!empty($taxRate['tax_rate_val'])) {
            $data['bill_info']['tax'] = round($data['bill_info']['fee_tax_exclude'] * $taxRate['tax_rate_val']);
        } else {
            $data['bill_info']['tax'] = 0;
        }

        /* Update $data['bill_info']['total_bill_price'] */
        $feeTaxExclude = !empty($data['bill_info']['fee_tax_exclude']) ? $data['bill_info']['fee_tax_exclude'] : 0;
        $data['bill_info']['total_bill_price'] = $feeTaxExclude + $data['bill_info']['tax'] + $data['bill_info']['insurance_price'];

        return $data;
    }

    /**
     * Check time modified commission_info
     *
     * @param  integer $id
     * @param  string  $modified
     * @return boolean
     */
    private function checkModifiedCommission($id, $modified)
    {
        $result = $this->commissionInfoRepo->find($id);
        if ($result && $modified == $result->modified) {
            return true;
        }

        return false;
    }

    /**
     * @param null $id
     * @param null $data
     * @return string
     */
    private function getCorrespond($id = null, $data = null)
    {
        $correspond = '';

        if (!empty($data)) {
            $columns = $this->commissionInfoRepo->getAllFields();
            $commissionInfo = $this->commissionInfoRepo->find($id)->toArray();

            foreach ($data as $newKey => $newValue) {
                foreach ($commissionInfo as $oldKey => $oldValue) {
                    if ($newKey == $oldKey) {
                        if ($newKey == 'commission_note_send_datetime' || $newKey == 'tel_commission_datetime') {
                            if (!empty($newValue)) {
                                $newValue = str_replace('/', '-', $newValue);
                                $newValue = $newValue.':00';
                            }
                        } elseif ($newKey == 'commission_status_last_updated') {
                            break;
                        }

                        if ($newValue != $oldValue) {
                            $comment = '';
                            $newText = '';
                            $oldText = '';

                            foreach ($columns as $column) {
                                if ($column->column_name == $newKey) {
                                    $comment = $column->column_comment;
                                    $newText = $this->getValueByTableCommissionInfo($column->column_name, $newValue);
                                    $oldText = $this->getValueByTableCommissionInfo($column->column_name, $oldValue);
                                    break;
                                }
                            }
                            $new = $newValue;
                            $old = $oldValue;
                            if (!empty($newText)) {
                                $new = $newText;
                            }
                            if (!empty($oldText)) {
                                $old = $oldText;
                            }
                            $correspond .= $comment.' : '.$old.' → '.$new."\n";
                        }
                        break;
                    }
                }
            }
        }

        $correspond .= $this->getCorrespondByDemand($data);

        return $correspond;
    }

    /**
     * @param null $data
     * @return string
     */
    private function getCorrespondByDemand($data = null)
    {
        $correspond = "";
        if (!empty($data['demand_info'])) {
            $columns = $this->demandInfoRepo->getAllFields();
            $di = $this->demandInfoRepo->find($data['demand_info']['id'])->toArray();

            foreach ($data['demand_info'] as $newKey => $newValue) {
                foreach ($di as $oldKey => $oldValue) {
                    if ($newKey == $oldKey) {
                        if ($newValue != $oldValue) {
                            $comment = '';
                            $newText = '';
                            $oldText = '';

                            foreach ($columns as $column) {
                                if ($column->column_name == $newKey) {
                                    $comment = $column[0]['column_comment'];
                                    $newText = $this->getValueByTableDemandInfo($column->column_name, $newValue);
                                    $oldText = $this->getValueByTableDemandInfo($column->column_name, $oldValue);
                                    break;
                                }
                            }
                            $new = $newValue;
                            $old = $oldValue;
                            if (!empty($newText)) {
                                $new = $newText;
                            }
                            if (!empty($oldText)) {
                                $old = $oldText;
                            }
                            $correspond .= $comment.' : '.$old.' → '.$new."\n";
                        }
                        break;
                    }
                }
            }
        }

        return $correspond;
    }

    /**
     * @param null $col
     * @param null $val
     * @return mixed|string
     */
    private function getValueByTableCommissionInfo($col = null, $val = null)
    {
        if (empty($col) || !isset($val) || $val == '') {
            return '';
        }
        if ($col == 'irregular_reason') {
            $rtn = getDropList(trans('イレギュラー理由', true));

            return $rtn[$val];
        } elseif ($col == 're_commission_exclusion_status') {
            $rtn = self::ARRAY_A;

            return $rtn[$val];
        } elseif ($col == 'reform_upsell_ic') {
            $rtn = self::ARRAY_B;

            return $rtn[$val];
        } elseif ($col == 'commission_type') {
            $rtn = $this->mCommissionTypeRepo->getList();

            return $rtn[$val];
        } elseif ($col == 'commission_status') {
            $rtn = getDropList(trans('commission_status', true));

            return $rtn[$val];
        } elseif ($col == 'commission_order_fail_reason') {
            $rtn = getDropList(trans('commission_order_fail_reason', true));

            return $rtn[$val];
        } elseif ($col == 'progress_reported' || $col == 'unit_price_calc_exclude' || $col == 'first_commission' || $col == 'introduction_free' || $col == 'ac_commission_exclusion_flg') {
            $rtn = self::ARRAY_C;

            return $rtn[$val];
        } elseif ($col == 'tel_support' || $col == 'visit_support' || $col == 'order_support') {
            $rtn = self::ARRAY_D;

            return $rtn[$val];
        } elseif ($col == 'order_fee_unit') {
            $rtn = self::ARRAY_E;

            return $rtn[$val];
        } elseif ($col == 'appointers' || $col == 'tel_commission_person' || $col == 'commission_note_sender') {
            $rtn = $this->mUser->dropDownUser();

            return $rtn[$val];
        }

        return '';
    }

    /**
     * @param null $col
     * @param null $val
     * @return string
     */
    private function getValueByTableDemandInfo($col = null, $val = null)
    {
        if (empty($col) || !isset($val) || $val == '') {
            return '';
        }
        if ($col == 'construction_class') {
            $rtn = getDropList('建物種別');

            return $rtn[$val];
        } elseif ($col == 'demand_status') {
            $rtn = getDropList(trans('demand_status', true));

            return $rtn[$val];
        } elseif ($col == 'order_fail_reason') {
            $rtn = getDropList(trans('order_fail_reason', true));

            return $rtn[$val];
        } elseif ($col == 'jbr_work_contents') {
            $rtn = getDropList(trans('jbr_work_contents', true));

            return $rtn[$val];
        } elseif ($col == 'jbr_category') {
            $rtn = getDropList(trans('jbr_category', true));

            return $rtn[$val];
        } elseif ($col == 'jbr_estimate_status') {
            $rtn = getDropList(trans('jbr_estimate_status', true));

            return $rtn[$val];
        } elseif ($col == 'jbr_receipt_status') {
            $rtn = getDropList(trans('jbr_receipt_status', true));

            return $rtn[$val];
        } elseif ($col == 'pet_tombstone_demand') {
            $rtn = getDropList(trans('pet_tombstone_demand', true));

            return $rtn[$val];
        } elseif ($col == 'sms_demand') {
            $rtn = getDropList(trans('sms_demand', true));

            return $rtn[$val];
        } elseif ($col == 'special_measures') {
            $rtn = getDropList(trans('案件特別施策', true));

            return $rtn[$val];
        } elseif ($col == 'acceptance_status') {
            $rtn = getDropList(trans('受付ステータス', true));

            return $rtn[$val];
        } elseif ($col == 'priority') {
            $rtn = ARRAY_F;

            return $rtn[$val];
        } elseif ($col == 'riro_kureka') {
            $rtn = ARRAY_G;

            return $rtn[$val];
        }

        return '';
    }

    /**
     * @param null  $id
     * @param array $data
     * @return bool
     */
    private function registBillInfo($id = null, $data = [])
    {

        $setData = $data['bill_info'];
        if ($data['commission_status'] == getDivValue('construction_status', 'introduction') && $data['introduction_free'] == 1) {
            $setData['fee_target_price'] = 0;
            $setData['fee_tax_exclude'] = 0;
        }
        $setData['demand_id'] = $data['demand_id'];
        $setData['commission_id'] = $id;
        if (isset($data['deduction_tax_include'])) {
            $setData['deduction_tax_include'] = $data['deduction_tax_include'];
        }
        if (isset($data['deduction_tax_exclude'])) {
            $setData['deduction_tax_exclude'] = $data['deduction_tax_exclude'];
        }
        if (isset($data['irregular_fee_rate'])) {
            $setData['irregular_fee_rate'] = $data['irregular_fee_rate'];
        }
        if (isset($data['irregular_fee'])) {
            $setData['irregular_fee'] = $data['irregular_fee'];
        }
        if (isset($data['confirmd_fee_rate'])) {
            $setData['comfirmed_fee_rate'] = $data['confirmd_fee_rate'];
        }
        $setData['tax'] = $setData['fee_tax_exclude'] * ($data['m_tax_rate']['tax_rate'] / 100);
        if (isset($data['insurance_price'])) {
            $setData['insurance_price'] = $data['insurance_price'];
        } else {
            $setData['insurance_price'] = 0;
        }
        $setData['total_bill_price'] = $setData['fee_tax_exclude'] + $setData['tax'] + $setData['insurance_price'];
        if (empty($data['bill_info']['id'])) {
            $setData['bill_status'] = 1;
            $setData['fee_payment_price'] = 0;
            $setData['fee_payment_balance'] = $setData['total_bill_price'];
        } else {
            if (empty($setData['fee_payment_price'])) {
                $setData['fee_payment_price'] = 0;
            }
            $setData['fee_payment_balance'] = $setData['total_bill_price'] - $setData['fee_payment_price'];
        }
        $setData['deduction_tax_exclude'] = intval($setData['deduction_tax_exclude']);
        $setData['fee_target_price'] = intval($setData['fee_target_price']);
        $setData['fee_tax_exclude'] = intval($setData['fee_tax_exclude']);
        $setData['tax'] = intval($setData['tax']);
        $setData['total_bill_price'] = intval($setData['total_bill_price']);
        $setData['fee_payment_balance'] = intval($setData['fee_payment_balance']);

        return $this->billRepo->getBlankModel()->where("id", $setData['id'])->update($setData);
    }

    /**
     * @param null  $id
     * @param array $data
     * @param $user
     * @param int   $hiddenLastUpdate
     * @return bool
     */
    private function editCommission($id = null, $data = [], $user = null, $hiddenLastUpdate = 0)
    {
        $data['id'] = $id;

        if ($user->auth != "affiliation") {
            $oldData = $this->commissionInfoRepo->find($id)->toArray();
            $oldStatus = $oldData['commission_status'];
            $newStatus = $data['commission_status'];

            if ($oldStatus != getDivValue('construction_status', 'progression') && $newStatus == getDivValue('construction_status', 'progression')) {
                $data['reported_flg'] = 0;
            }
        }

        if (empty($data['first_commission'])) {
            $data['first_commission'] = 0;
        }
        if (empty($data['unit_price_calc_exclude'])) {
            $data['unit_price_calc_exclude'] = 0;
        }
        if (empty($data['commission_order_fail_reason'])) {
            $data['commission_order_fail_reason'] = 0;
        }

        $data['complete_date'] = str_replace("-", "/", $data['complete_date']);
        $data['order_fail_date'] = str_replace("-", "/", $data['order_fail_date']);

        if ($hiddenLastUpdate == 1) {
            $data['commission_status_last_updated'] = date("Y-m-d G:i:s");
        }

        unset($data['demand_info']);
        unset($data['bill_info']);
        unset($data['affiliation_info']);
        unset($data['m_corp']);
        unset($data['m_tax_rate']);
        unset($data['insurance_price']);
        unset($data['m_corp_category']);

        if ($this->commissionInfoRepo->getBlankModel()->where("id", $id)->update($data)) {
            return true;
        };

        return false;
    }

    /**
     * @param array $data
     * @param $check
     * @return \Illuminate\Support\Collection
     */
    public function getCorpList($data = array(), $check = null)
    {
        $flashError = false;
        $limit = 100;
        /* Error checking */
        empty($data['category_id']) ? $data['category_id'] = 0 : '';
        $newYear = new MCorpNewYear;
        $newYearColumns = \Schema::getColumnListing($newYear->getTable());
        /* TODO map unitprice */
        $fieldsHoliday = '(SELECT ARRAY_TO_STRING(ARRAY( SELECT item_name FROM m_items INNER JOIN m_corp_subs ON m_corp_subs.item_category = m_items.item_category AND m_corp_subs.item_id = m_items.item_id WHERE m_corp_subs.item_category = \'休業日\' AND m_corp_subs.corp_id = "MCorp"."id" ORDER BY m_items.sort_order ASC ),\'｜\') as "MCorp__holiday" )';
        $fieldsCommissionUnitPrice = '(SELECT m_genres.targer_commission_unit_price FROM m_genres WHERE m_genres.id = "MCorpCategory"."genre_id") AS "targer_commission_unit_price"';

        $fields = '(SELECT COUNT(0) FROM commission_infos WHERE corp_id = "MCorp".id AND commission_status = 1 ) AS in_progress';
        $fields .= ',(SELECT COUNT(0) FROM commission_infos WHERE corp_id = "MCorp".id AND commission_status = 2 ) AS in_order';
        $fields .= ',(SELECT COUNT(0) FROM commission_infos WHERE corp_id = "MCorp".id AND commission_status = 3 ) AS complete';
        $fields .= ',(SELECT COUNT(0) FROM commission_infos WHERE corp_id = "MCorp".id AND commission_status = 4 ) AS failed';
        $fields .= ',' . implode(',', array_map(
            function ($value) {
                    return '"MCorpNewYear".' . '"' . $value . '"';
            },
            $newYearColumns
        ));
        $conditions = [
            ['MCorp.affiliation_status', '=', 1],
            ['MCorp.del_flg', '=', 0]
        ];

        $targetCheckFlg = false;
        $joinsType = 'left';
        if (empty($data['target_check'])) {
            if (!empty($data['category_id']) && !empty($data['jis_cd'])) {
                $targetCheckFlg = true;
                $joinsType = 'inner';
            } else {
                if ($flashError) {
                    session()->flash(__('demand.errorCommissionSelect'));
                }
                return array();
            }
        }
        $qBuilder = \DB::table('m_corps AS MCorp')->join('affiliation_infos AS AffiliationInfo', 'MCorp.id', '=', 'AffiliationInfo.corp_id')
            ->join('m_items AS MItem', function ($join) {
                $join->on('MItem.item_id', '=', 'MCorp.coordination_method');
                $join->where('MItem.item_category', '=', '顧客情報連絡手段');
            })
            ->join('m_corp_categories AS MCorpCategory', function ($join) use ($data) {
                $join->on('MCorpCategory.corp_id', '=', 'MCorp.id');
                $join->where('MCorpCategory.category_id', '=', $data['category_id']);
            }, null, null, $joinsType)
            ->leftJoin('affiliation_stats AS AffiliationStat', function ($join) use ($data) {
                $join->on('AffiliationStat.corp_id', '=', 'MCorp.id');
                $join->on('AffiliationStat.genre_id', '=', 'MCorpCategory.genre_id');
            })
            ->leftJoin('affiliation_subs AS AffiliationSubs', function ($join) use ($data) {
                $join->on('AffiliationSubs.affiliation_id', '=', 'AffiliationInfo.id');
                $join->where('AffiliationSubs.item_id', '=', $data['category_id']);
            })
            ->leftJoin('m_corp_new_years AS MCorpNewYear', 'MCorpNewYear.corp_id', '=', 'MCorp.id')
            ->select(
                'MCorp.id AS MCorp_id',
                'MCorp.corp_name AS MCorp_corp_name',
                'MCorp.commission_dial AS MCorp_commission_dial',
                'MCorp.coordination_method AS MCorp_coordination_method',
                'MCorp.mailaddress_pc AS MCorp_mailaddress_pc',
                'MCorp.fax AS MCorp_fax',
                'MCorp.note AS MCorp_note',
                'MCorp.support24hour AS MCorp_support24hour',
                'MCorp.available_time_from AS MCorp_available_time_from',
                'MCorp.available_time_to AS MCorp_available_time_to',
                'MCorp.available_time AS MCorp_available_time',
                'MCorp.contactable_support24hour AS MCorp_contactable_support24hour',
                'MCorp.contactable_time_from AS MCorp_contactable_time_from',
                'MCorp.contactable_time_to AS MCorp_contactable_time_to',
                'MCorp.contactable_time AS MCorp_contactable_time',
                'MCorp.address1 AS MCorp_address1',
                'MCorp.address2 AS MCorp_address2',
                'MCorp.address3 AS MCorp_address3',
                'AffiliationInfo.fee AS AffiliationInfo_fee',
                'AffiliationInfo.commission_unit_price AS AffiliationInfo_commission_unit_price',
                'AffiliationInfo.attention AS AffiliationInfo_attention',
                'AffiliationInfo.commission_count AS AffiliationInfo_commission_count',
                'AffiliationInfo.sf_construction_count AS AffiliationInfo_sf_construction_count',
                'AffiliationInfo.attention AS AffiliationInfo_attention',
                'MCorpCategory.order_fee AS MCorpCategory_order_fee',
                'MCorpCategory.order_fee_unit AS MCorpCategory_order_fee_unit',
                'MCorpCategory.note AS MCorpCategory_note',
                'MCorpCategory.select_list AS MCorpCategory_select_list',
                'MCorpCategory.introduce_fee AS MCorpCategory_introduce_fee',
                'MCorpCategory.corp_commission_type AS MCorpCategory_corp_commission_type',
                'AffiliationStat.commission_unit_price_category AS AffiliationStat_commission_unit_price_category',
                'AffiliationStat.commission_count_category AS AffiliationStat_commission_count_category',
                'AffiliationStat.orders_count_category AS AffiliationStat_orders_count_category'
            );


        if ($targetCheckFlg) {
            if (empty($check)) {
                $limit = 1500;
                $conditions[] = ['AffiliationAreaStat.commission_count_category', '>=', 5];
            } else {
                $limit = 2000;
                $conditions[] = ['AffiliationAreaStat.commission_count_category', '<', 5];
            }
            $prefecture = (int)substr($data ['jis_cd'], 0, 2);

            $qBuilder->addSelect(
                'AffiliationAreaStat.commission_unit_price_category AS AffiliationAreaStat_commission_unit_price_category',
                'AffiliationAreaStat.commission_count_category AS AffiliationAreaStat_commission_count_category',
                'AffiliationAreaStat.commission_unit_price_rank AS AffiliationAreaStat_commission_unit_price_rank',
                'MCorpCategory.category_id AS MCorpCategory_category_id',
                'MCorpCategory.auction_status AS MCorpCategory_auction_status'
            );
            $qBuilder->join('m_target_areas AS MTargetArea', function ($join) use ($data) {
                $join->on('MTargetArea.corp_category_id', '=', 'MCorpCategory.id');
                $join->where('MTargetArea.jis_cd', $data ['jis_cd']);
            });
            $qBuilder->join('affiliation_area_stats AS AffiliationAreaStat', function ($join) use ($prefecture) {
                $join->on('AffiliationAreaStat.corp_id', '=', 'MCorp.id');
                $join->on('AffiliationAreaStat.genre_id', '=', 'MCorpCategory.genre_id');
                $join->where('AffiliationAreaStat.prefecture', $prefecture);
            })
                ->orderByRaw('"AffiliationAreaStat"."commission_unit_price_category" IS NULL ASC')
                ->orderByDesc('AffiliationAreaStat.commission_unit_price_category')
                ->orderByDesc('AffiliationAreaStat.commission_count_category');

            $qBuilder->where(function ($q) {
                $q->orWhereNull('MCorp.auction_status')
                    ->orWhere(function ($q) {
                        $q->where('MCorp.auction_status', '!=', 3)
                            ->where('MCorpCategory.auction_status', '!=', 3);
                    })
                    ->orWhere(function ($q) {
                        $q->where('MCorp.auction_status', 1)
                            ->where('MCorpCategory.auction_status', '!=', 3);
                    })
                    ->orWhere(function ($q) {
                        $q->where('MCorp.auction_status', 3)
                            ->where('MCorpCategory.auction_status', 1);
                    })
                    ->orWhere(function ($q) {
                        $q->where('MCorp.auction_status', 3)
                            ->where('MCorpCategory.auction_status', 2);
                    });
            });
        } else {
            $qBuilder->orderByRaw('"AffiliationInfo"."commission_unit_price" IS NULL ASC')
                ->orderByDesc('AffiliationInfo.commission_unit_price')
                ->orderByDesc('AffiliationInfo.commission_count');
        }

        $qBuilder->addSelect(DB::raw($fields))
            ->addSelect(DB::raw($fieldsCommissionUnitPrice))
            ->addSelect(DB::raw($fieldsHoliday))
            ->where($conditions)
            ->whereNull('AffiliationSubs.affiliation_id')
            ->whereNull('AffiliationSubs.item_id')
            ->whereNotIn('MCorp.commission_accept_flg', [0, 3])
            ->whereRaw('coalesce("MCorp"."corp_commission_status", 0) not in (1,2,4,5)');

        return $qBuilder->limit($limit)->get();
    }

    /**
     * @param $data
     * @return bool
     * @throws \Exception
     */
    public function updateCommission($data)
    {
        $errorNo = [];
        /* If supplier information is not entered, nothing is done */
        Log::Debug('___ Start update commission ________');
        if (!array_key_exists('commissionInfo', $data)) {
            Log::Debug('___ empty commissionInfo ________');
            return true;
        }
        /* If there is no transaction type = "contract base" it does nothing */
        $commissionInfo = array_filter($data['commissionInfo'], function ($item) {
            return $item['commission_type'] != 1;
        });
        if (empty($commissionInfo)) {
            Log::Debug('___ empty commission_type =1 ________');
            return true;
        }
        /* Retrieve deal ID */
        $demandId = (array_key_exists('id', $data['demandInfo'])) ? $data['demandInfo']['id'] : null;
        $billInfoData = $this->billRepo->getByDemandIdAuctionId($demandId);
        /* If the visit date is not entered, I do nothing */
        if (array_key_exists('visitTime', $data)) {
            /* Acquire the registered visit_times to store the ID of the desired visit date in commission_infos */
            $visittimeData = $this->visitTimeRepo->findAllByDemandId($demandId, true);
        }
        /* Registration of destination information */
        $commissionData = array();
        $cnt = 0;
        $insert = false;
        $saveSelectFlg = true;

        $commissionInsertData = [];
        $commissionUpdateData = [];
        $dateInsert = date('Y-m-d H:i:s');
        foreach ($commissionInfo as $key => $val) {
            $cnt++;
            /* Judge whether registration is necessary or not based on the presence / absence of supplier company ID*/
            if (!empty($val['corp_id'])) {
                /* Confirm existence of registered data with case ID and company ID
                 Failure flag*/
                $lostFlg = false;
                $corp = $this->mCorpRepo->getFirstById($val['corp_id']);
                /*If the company name is for lost information, create an order status with a missing order*/
                if ($corp->corp_name == config('rits.lost_corp_name')) {
                    $lostFlg = true;
                }
                /* It checks whether the fax number of the company master and PC mail are set
                TODO: refactor*/
                if ($data['send_commission_info'] == 1) {
                    $coordinationMethod = $corp->coordination_method;
                    if ($coordinationMethod == getDivValue('coordination_method', 'mail_fax')) {
                        if (empty($corp->fax) && empty($corp->mailaddress_pc)) {
                            array_push($errorNo, $cnt);
                        }
                    } elseif ($coordinationMethod == getDivValue('coordination_method', 'mail')) {
                        if (empty($corp->mailaddress_pc)) {
                            array_push($errorNo, $cnt);
                        }
                    } elseif ($coordinationMethod == getDivValue('coordination_method', 'fax')) {
                        if (empty($corp->fax)) {
                            array_push($errorNo, $cnt);
                        }
                    }
                }
                /* Update or registration judgment*/

                if (!empty($val['id'])) {
                    /* When updating*/
                    $tempData = [
                        'id' => $val['id'],
                        'corp_id' => $val['corp_id'],
                        'commit_flg' => $val['commit_flg'],
                        'appointers' => $val['appointers'],
                        'modified' => $dateInsert,
                        'modified_user_id' => auth()->user()->user_id,
                        'corp_claim_flg' => empty($val['corp_claim_flg']) ? null : (int)$val['corp_claim_flg'],
                        'commission_note_send_datetime' => !empty($val['commission_note_send_datetime']) ? $val['commission_note_send_datetime'] : null,
                        'commission_note_sender' => $val['commission_note_sender'],
                        'first_commission' => empty($val['first_commission']) ? 0 : (int)$val['first_commission'],
                        'unit_price_calc_exclude' => $val['unit_price_calc_exclude'],
                        'lost_flg' => !empty($val['lost_flg']) ? (int)$val['lost_flg'] : 0,
                        'del_flg' => !empty($val['del_flg']) ? (int)$val['del_flg'] : 0,
                        'select_commission_unit_price_rank' => $val['select_commission_unit_price_rank'],
                        'select_commission_unit_price' => empty($val['select_commission_unit_price']) ? 0 : (int)$val['select_commission_unit_price'],
                    ];
                    if ($visittimeData) {
                        $tempData['commission_visit_time_id'] = $visittimeData->id;
                    }
                    if (isset($val['auto_select_flg'])) {
                        $tempData['auto_select_flg'] = $val['auto_select_flg'];
                    }
                    if (isset($val['corp_fee'])) {
                        $tempData['corp_fee'] = $val['corp_fee'];
                    } elseif (isset($val['commission_fee_rate']) && !empty($val['commission_fee_rate'])) {
                        $tempData['commission_fee_rate'] = $val['commission_fee_rate'];
                    }
                    if ($val['commit_flg'] == 1 && $data['not-send'] == 1) {
                        $tempData['send_mail_fax_othersend'] = 1;
                    }

                    if ($lostFlg) {
                        $tempData['commission_status'] = getDivValue('construction_status', 'order_fail');
                    }
                    $commissionUpdateData[] = $tempData;
                } else {
                    $tempInsert = [];
                    $insert = true;
                    /* In case of new registration*/
                    $tempInsert['modified'] = $dateInsert;
                    $tempInsert['created'] = $dateInsert;
                    $tempInsert['modified_user_id'] = auth()->user()->user_id;
                    $tempInsert['created_user_id'] = auth()->user()->user_id;
                    $tempInsert['demand_id'] = $data['demandInfo']['id'];
                    $tempInsert['corp_id'] = $val['corp_id'];
                    $tempInsert['commit_flg'] = $val['commit_flg'];
                    $tempInsert['commission_type'] = getDivValue('commission_type', 'normal_commission');  // Order type: 0 (ordinary agency)
                    $tempInsert['appointers'] = $val['appointers'];
                    $tempInsert['corp_claim_flg'] = empty($val['corp_claim_flg']) ? null : (int)$val['corp_claim_flg'];
                    $tempInsert['commission_note_send_datetime'] = !empty($val['commission_note_send_datetime']) ? $val['commission_note_send_datetime'] : null;
                    $tempInsert['commission_note_sender'] = $val['commission_note_sender'];
                    $tempInsert['first_commission'] = empty($val['first_commission']) ? 0 : (int)$val['first_commission'];
                    $tempInsert['unit_price_calc_exclude'] = $val['unit_price_calc_exclude'];
                    $tempInsert['commission_status'] = getDivValue('construction_status', 'progression'); // Contract status
                    $tempInsert['unit_price_calc_exclude'] = 0;// Unit price per transaction: 0
                    $tempInsert['lost_flg'] = !empty($val['lost_flg']) ? (int)$val['lost_flg'] : 0;
                    $tempInsert['del_flg'] = !empty($val['del_flg']) ? (int)$val['del_flg'] : 0;
                    $tempInsert['select_commission_unit_price_rank'] = $val['select_commission_unit_price_rank'];
                    $tempInsert['select_commission_unit_price'] = empty($val['select_commission_unit_price']) ? 0 : (int)$val['select_commission_unit_price'];
                    if ($visittimeData) {
                        $tempInsert['commission_visit_time_id'] = $visittimeData['visitTime']['id'];
                    }
                    if (isset($val['auto_select_flg'])) {
                        $tempInsert['auto_select_flg'] = $val['auto_select_flg'];
                    }
                    if (isset($val['corp_fee'])) {
                        $tempInsert['corp_fee'] = $val['corp_fee'];
                    } elseif (isset($val['commission_fee_rate']) && !empty($val['commission_fee_rate'])) {
                        $tempInsert['commission_fee_rate'] = $val['commission_fee_rate'];
                    }
                    $tempInsert['order_fee_unit'] = empty($val['order_fee_unit']) ? null : (int)$val['order_fee_unit'];

                    /*When sending an aggregate table separately*/
                    if ($val['commit_flg'] == 1 && $data['not-send'] == 1) {
                        $tempInsert['send_mail_fax_othersend'] = 1;
                    }

                    if ($lostFlg) {
                        $tempInsert['commission_status'] = getDivValue('construction_status', 'order_fail');
                    }

                    $commissionInsertData[] = $tempInsert;
                }
            }
        }
        /* Automatic Selection Update of select_flg*/
        if (!empty($data['updata_auto_select'])) {
            $this->AutoSelectSetting->saveSelectFlg($data['demandInfo']['genre_id'], $data['demandInfo']['address1']);
        }
        if (count($errorNo) > 0) {
            Log::Debug('___ commission error no: ________');
            return $errorNo;
        }
        $this->commissionInfoRepo->insertCommission($commissionInsertData);
        $this->commissionInfoRepo->multipleUpdate($commissionUpdateData);

        return [];
    }

    /**
     * @param $data
     * @return bool
     * @throws \Exception
     */
    public function updateIntroduce($data)
    {
        Log::debug("________1@call __update_introduce()");
        if (!isset($data['commissionInfo'])) { // TODO: REMOVE
            return true;
        }
        /* If there is no transaction type = "referral base", I do nothing*/
        $introduceInfo = array_filter(
            $data['commissionInfo'],
            function ($v) {
                return $v['commission_type'] == 1;
            }
        );
        if (empty($introduceInfo)) {
            return true;
        }
        /* Newly registered company ID list (for billing information registration)*/
        $inserted = array();
        /* Registered company ID list (deletion of billing data)*/
        $insertedDel = array();
        /*Retrieve deal ID*/
        $demandId = (array_key_exists('id', $data['demandInfo'])) ? $data['demandInfo']['id'] : null;
        /* Registration of introduction destination information*/
        $introduceData = array();
        foreach ($introduceInfo as $key => $val) {
            /* Confirm existence of registered data with case ID and company ID*/
            $currentData = $this->commissionInfoRepo->findByDemandIdCorpAndType($demandId, $val['corp_id'], getDivValue('commission_type', 'package_estimate'));
            /* Update or registration judgment*/
            if ($currentData) {
                Log::debug("2@call 更新");
                /* When updating*/
                $introduceData[$key]['commission_note_send_datetime'] = $val['commission_note_send_datetime'];// Delivery date and time of introduction form
                $introduceData[$key]['commission_note_sender'] = $val['commission_note_sender'];
                /* Introduction slip sender*/
                $introduceData[$key]['id'] = $currentData['commissionInfo']['id'];
                /* delete flag*/
                $introduceData[$key]['del_flg'] = (int)$val['del_flg'];
                /*Can not introduce*/
                $introduceData[$key]['introduction_not'] = $val['introduction_not'];
                $introduceData[$key]['lost_flg'] = (int)$val['lost_flg'];
                $introduceData[$key]['commit_flg'] = empty($val['commit_flg']) ? 0 : (int)$val['commit_flg'];
                $introduceData[$key]['appointers'] = $val['appointers'];
                $introduceData[$key]['corp_claim_flg'] = empty($val['corp_claim_flg']) ? null : (int)$val['corp_claim_flg'];
                $introduceData[$key]['first_commission'] = empty($val['first_commission']) ? 0 : $val['first_commission'];
                $introduceData[$key]['unit_price_calc_exclude'] = $val['unit_price_calc_exclude'];
                if (!empty($val['commit_flg'])) {
                    $introduceData[$key]['confirmd_fee_rate'] = 100;
                    $introduceData[$key]['complete_date'] = date('Y/m/d');
                }
                if (!empty($val['introduction_not']) || empty($val['commit_flg'])) {
                    array_push($insertedDel, $currentData['commissionInfo']['id']);
                } else {
                    $count = $this->billRepository->countByDemandIdAndCommissionId($demandId, $currentData ['commissionInfo']['id']);
                    if ($count == 0) {
                        array_push($inserted, $val['corp_id']);
                    }
                }
            } else {
                Log::debug("_________2@call 新規");
                /* In case of new registration*/
                $introduceData[$key]['demand_id'] = $demandId;// Opportunity ID
                $introduceData[$key]['corp_id'] = $val['corp_id'];//Merchant ID
                if (isset($val['commission_note_send_datetime'])) {
                    $introduceData[$key]['commission_note_send_datetime'] = $val['commission_note_send_datetime']; // Delivery date and time of introduction form
                }
                if (isset($val['commission_note_sender'])) {
                    /* Introduction slip sender*/
                    $introduceData[$key]['commission_note_sender'] = $val['commission_note_sender'];
                }
                /*Intermediary type: 1 (collective estimate)*/
                $introduceData[$key]['commission_type'] = getDivValue('commission_type', 'package_estimate');
                /* Confirmation flag: 0 (undetermined)*/
                $introduceData[$key]['commit_flg'] = $val['commit_flg'];
                // Selector: Login User
                $introduceData[$key]['appointers'] = $val['appointers'];
                /* First Check Check: 0*/
                $introduceData[$key]['first_commission'] = empty($val['first_commission']) ? 0 : $val['first_commission'];
                /* Intermediary fee: Compatible genre master by company. Referral fee*/
                $defaultFee = $this->mCategoryRepo->getDefaultFee($data['demandInfo']['category_id']);
                $introduceData[$key]['corp_fee'] = $this->mCorpCategoryRepo->getIntroduceFee($val['corp_id'], $data['demandInfo']['category_id'], $defaultFee);
                $introduceData[$key]['corp_claim_flg'] = $val['corp_claim_flg'];
                /* Interpretation commission rate: 100*/
                $introduceData[$key]['commission_fee_rate'] = 100;
                /*Agency situation: 5 (introduced)*/
                $introduceData[$key]['commission_status'] = getDivValue('construction_status', 'introduction');
                /* Unit price per transaction: 0*/
                $introduceData[$key]['unit_price_calc_exclude'] = 0;
                /* Before interruption flag*/
                $introduceData[$key]['lost_flg'] = (int)$val['lost_flg'];
                /* Delete flag*/
                $introduceData[$key]['del_flg'] = (int)$val['del_flg'];
                if (isset($val['introduction_not'])) {
                    /* Can not introduce*/
                    $introduceData[$key]['introduction_not'] = $val['introduction_not'];
                }
                if (empty($val['introduction_not']) && empty($val['del_flg']) && !empty($val['commit_flg'])) {
                    array_push($inserted, $val['corp_id']);
                }

                if (!empty($val['commit_flg'])) {
                    $introduceData[$key]['confirmd_fee_rate'] = 100;
                    $introduceData[$key]['complete_date'] = date('Y/m/d');
                }
                $introduceData[$key]['order_fee_unit'] = empty($val['order_fee_unit']) ? null : (int)$val['order_fee_unit'];
            }

            /* When individual transmission is performed*/
            if (in_array($data['demandInfo']['demand_status'], array(4, 5)) && $data['not-send'] == 1) {
                $introduceData[$key]['send_mail_fax_othersend'] = 1;
            }
        }

        /* Refresh introduction destination information returned.*/
        if (!empty($introduceData)) {
            Log::debug("_________3@call CommissionInfo->saveAll");
            if (!$this->commissionInfoRepo->multipleUpdate($introduceData)) {
                throw new \Exception;
            }
        }
        /* Lilo · Creca deal*/
        if (empty($data['DemandInfo']['riro_kureka'])) {
            if (!empty($inserted)) {
                Log::debug("4@call __update_bill");
                if (!$this->commissionUpdateBill($demandId, $data['demandInfo']['category_id'], $inserted)) {
                    Log::debug("4@エラー __update_bill");
                    throw new \Exception;
                }
            }
            if (!empty($insertedDel)) {
                Log::debug("4@call __delete_bill");
                if (!$this->billRepo->deleteByDemandIdAndIds($demandId, $insertedDel)) {
                    Log::debug("4@エラー __delete_bill");
                    throw new \Exception;
                }
            }
        }
        return true;
    }


    /**
     * @param $demandId
     * @param $categoryId
     * @param $insData
     * @return mixed
     */
    public function commissionUpdateBill($demandId, $categoryId, $insData)
    {
        /* Acquisition of consumption tax rate*/
        $taxRate = $this->mTaxRateRepo->findByDate(date('Y/m/d'));
        $defaultFee = $this->mCategoryRepo->getDefaultFee($categoryId);
        /* Create billing information*/
        $saveData = array();
        foreach ($insData as $key => $val) {
            /*Acquiring agency information*/
            $currentData = $this->commissionInfoRepo->findByDemandIdCorpAndType($demandId, $val, getDivValue('commission_type', 'package_estimate'));
            /* Acquisition of referral fee*/
            $introduceFee = $this->mCorpCategoryRepo->getIntroduceFee($val, $categoryId, $defaultFee);
            /* Assignment ID*/
            $saveData[$key]['commission_id'] = $currentData['commissionInfo']['id'];
            /* Opportunity ID*/
            $saveData[$key]['demand_id'] = $demandId;
            /* Billing status: 1 (not issued)*/
            $saveData[$key]['bill_status'] = getDivValue('bill_status', 'not_issue');
            /* Commitment commission rate: 100*/
            $saveData[$key]['comfirmed_fee_rate'] = 100;
            /* Commissionable amount: Compatible genre master by company. Referral fee*/
            $saveData[$key]['fee_target_price'] = $introduceFee;
            /* Commission fee: (Final commission rate / 100) * Commissionable amount*/
            $saveData[$key]['fee_tax_exclude'] = ($saveData[$key]['comfirmed_fee_rate'] / 100) * $introduceFee;
            /* Consumption tax: commission * consumption tax rate*/
            $saveData[$key]['tax'] = floor($saveData[$key]['fee_tax_exclude'] * $taxRate);
            /* Total charge amount: commission + consumption tax*/
            $saveData[$key]['total_bill_price'] = $saveData[$key]['fee_tax_exclude'] + $saveData[$key]['tax'];
            /* Commission payment amount*/
            $saveData[$key]['fee_payment_price'] = 0;
            /* Total charge amount: commission + consumption tax*/
            $saveData[$key]['fee_payment_balance'] = $saveData[$key]['fee_tax_exclude'] + $saveData[$key]['tax'];
        }

        return $this->billRepo->insert($saveData);
    }

    /**
     * @param $demandId
     */
    public function sendNotify($demandId)
    {
        /* Get record of commission_infos from demand_id*/
        $commissionInfosDatas = $this->commissionInfoRepo->getListByDemandId($demandId, true);
        /*Send when the push notification flag is 0 and when the company's disbursement method is 6 or 7*/
        $arrIdPush = [];
        foreach ($commissionInfosDatas as $commissionInfosData) {
            if ($commissionInfosData->app_push_flg == 0 && ($commissionInfosData->mCorp->coordination_method == 6 || $commissionInfosData->mCorp->coordination_method == 7)) {
                $corpId = $commissionInfosData->corp_id;
                /*Acquire the user ID associated with the member store ID*/
                $users = $this->mUserRepo->getUserByAffiliationId($corpId);
                foreach ($users as $user) {
                    /*Send notification*/
                    $pushMessage = '新しい案件があります。';
                    /* $this->snsService->publish($user->user_id, $pushMessage);*/
                    Log::info('____SNS___: push notify ____________');
                }
                /*After sending app_push_flg is set to 1*/
                $arrIdPush[] = $commissionInfosData->id;
            }
        }

        if (!empty($arrIdPush)) {
            $this->commissionInfoRepo->updateAppPushFlg($arrIdPush);
        }
    }

    /**
     * @param $autoCommissions
     * @param $defaultFee
     * @param $commissionInfos
     */
    public function buildCommissionData($autoCommissions, $defaultFee, $commissionInfos)
    {
        foreach ($autoCommissions as $val) {
            if ($val['mCorpCategory']['corp_commission_type'] != 2) {
                /* Conclusion base*/
                $orderFee = $val['mCorpCategory']['order_fee'];
                $orderFeeUnit = $val['mCorpCategory']['order_fee_unit'];
                $commissionStatus = getDivValue('construction_status', 'progression');
                $commissionType = getDivValue('commission_type', 'normal_commission');
            } else {
                //Introduction base
                $orderFee = $val['mCorpCategory']['introduce_fee'];
                $orderFeeUnit = 0;
                $commissionStatus = getDivValue('construction_status', 'introduction');
                $commissionType = getDivValue('commission_type', 'package_estimate');
            }

            $orderFee = !empty($orderFee) ? $orderFee : $defaultFee->category_default_fee;
            $orderFeeUnit = !empty($orderFee) ? $orderFeeUnit : $defaultFee->category_default_fee_unit;

            /* If an automatically selected supplier has already been registered, do not register*/
            $hasCommissions = array_filter(
                $commissionInfos,
                function ($v) use ($val) {
                    return $v['corp_id'] == $val['id'];
                }
            );
            if (!empty($hasCommissions)) {
                continue;
            }

            $commissionInfos[] = array(
                'corp_id' => $val['id'], // corp ID
                'first_commission' => 0, // Initial check check
                'unit_price_calc_exclude' => 0, // Not covered by contract price
                'commit_flg' => 0, // Confirm
                'lost_flg' => 0, //Prior to ordering
                'corp_fee' => $orderFeeUnit == 0 ? $orderFee : null, // Brokerage fee
                'commission_fee_rate' => $orderFeeUnit == 0 ? null : $orderFee, // Commission rate at commission
                'select_commission_unit_price_rank' => $val['affiliationAreaStat']['commission_unit_price_rank'], // Unit price rank
                'select_commission_unit_price' => $val['affiliationAreaStat']['commission_unit_price_category'], // Unit price per contract
                'order_fee_unit' => $orderFeeUnit,

                'appointers' => auth()->id(), // Selector
                'corp_claim_flg' => null, //Agency complaint
                'commission_note_send_datetime' => null, //Date and time of agency sent
                'commission_note_sender' => null, // Mail order sender
                'del_flg' => 0, // Delete
                'created_user_id' => 'AutomaticAuction',
                'modified_user_id' => 'AutomaticAuction',
                'commission_status' => $commissionStatus,
                'commission_type' => $commissionType,
            );

            $isSelect = true;
        }
    }


    /**
     * @param $data
     * @return bool
     */
    public function updateCommissionSendMailFax($data)
    {
        /* If supplier information is not entered, nothing is done*/
        if (!isset($data['commissionInfo']) && !isset($data['introduceInfo'])) {
            return true;
        }

        /* Retrieve deal ID*/

        $commissionInfoData = $this->commissionInfoRepo->getAllCommissionByDemandId($data['demandInfo']['id']);

        $commissionData = array();
        foreach ($commissionInfoData as $key => $val) {
            if (!empty($val['commissionInfo']['id'])
                && $val['commissionInfo']['commit_flg'] == 1
                && $val['commissionInfo']['introduction_not'] != 1
                && $val['commissionInfo']['lost_flg'] != 1
                && array_key_exists('commissionInfo', $data)) {
                $corpInfo = $this->mCorpRepo->getFirstById($val['commissionInfo']['corp_id']);
                /* When the customer information contacting means of the member store information is "Mail + FAX", "Mail", "FAX"*/
                if (!empty($corpInfo->coordination_method)
                    && (($corpInfo->coordination_method == getDivValue('coordination_method', 'mail_fax'))
                        || ($corpInfo->coordination_method == getDivValue('coordination_method', 'mail'))
                        || ($corpInfo->coordination_method == getDivValue('coordination_method', 'fax'))
                        || ($corpInfo->coordination_method == getDivValue('coordination_method', 'mail_app'))
                        || ($corpInfo->coordination_method == getDivValue('coordination_method', 'mail_fax_app'))
                    )) {
                    $commissionData[$key]['id'] = $val['commissionInfo']['id'];
                    $commissionData[$key]['send_mail_fax'] = 1;
                    $commissionData[$key]['send_mail_fax_datetime'] = date('Y/m/d H:i:s');
                    $commissionData[$key]['modified'] = date('Y/m/d H:i:s');
                    $commissionData[$key]['modified_user_id'] = auth()->user->user_id;
                    $commissionData[$key]['send_mail_fax_othersend'] = 0;
                }
            }
        }

        if (!empty($commissionData)) {
            Log::debug('_______ start udpate commission after send mail _____');
            $this->commissionInfoRepo->multipleUpdate($commissionData);
            Log::debug('_______ end udpate commission after send mail _____');
        }
    }

    /**
     * @param $data
     * @param $status
     * @return bool
     */
    public function validate($data, $status)
    {
        $validate = Validator::make(
            $data,
            [
            'appointers' => 'max:20',
            'first_commission' => 'numeric',
            'corp_fee' => 'numeric', //checkEmptyCorpFee
            'waste_collect_oath' => 'numeric',
            'attention' => 'max:1000',
            'official_name' => 'max:200',
            'tel_commission_person' => 'max:20',
            'commission_note_sender' => 'max:20',
            'commission_dial' => 'numeric',
            'commission_status' => 'numeric', //checkCommissionIntroduce, checkCommissionStatusComplete, checkCommissionStatusOrderFail
            'commission_note_send_datetime' => 'date',
            'commission_fee_rate' => 'numeric', //checkEmptyCommissionFeeRate
            'construction_price_tax_exclude' => 'numeric', //checkCommissionOrderFailConstructionPrice, checkConstructionPrice
            'estimate_price_tax_exclude' => 'max:10',
            'construction_price_tax_include' => 'numeric',
            'deduction_tax_include' => 'numeric',
            'deduction_tax_exclude' => 'numeric',
            'confirmd_fee_rate' => 'numeric',
            'unit_price_calc_exclude' => 'numeric',
            'report_note' => 'max:1000',
            'irregular_fee_rate' => 'numeric',
            'irregular_fee' => 'numeric',
            'reported_flg' => 'numeric',
            'follow_date' => 'date',
            'business_trip_amount' => 'numeric',
            ]
        );
        if ($validate->passes() && $this->validateDemandStatus($status, $data)
            && $this->checkEmptyCorpFee($data)
            && $this->checkCommissionStatusComplete($data)
            && $this->checkEmptyCommissionFeeRate($data)
            && $this->checkCommissionIntroduce($data)
            && $this->checkCommissionStatusOrderFail($data)
            && $this->checkCommissionOrderFailReason($data)
            && $this->checkCommissionOrderFailConstructionPrice($data)
            && $this->checkConstructionPrice($data)
            && $this->checkCompleteDate($data)
            && $this->checkFutureCompleteDate($data)
            && $this->checkCompleteDateFormat($data)
            && $this->checkOrderFailDate($data)
            && $this->checkFutureOrderFailDate($data)
            && $this->checkOrderFailDateFormat($data)
            && $this->checkConstructionPriceTaxInclude($data)
            && $this->checkFalsity($data)
        ) {
            return true;
        }

        return false;
    }

    /**
     * @param $data
     * @param $status
     * @return array
     */
    public function validateRegist($data)
    {
        $result = ['check' => true];
        $message = [
            'required' => '必須入力です。',
            'date' => '日時形式で入力してください。',
            'max' => ':max文字以内で設定してください。',
            'numeric' => '半角数字で入力してください。'
        ];
        $validate = Validator::make($data, [
            'appointers' => 'max:20',
            'first_commission' => !empty($data['first_commission']) ? 'numeric' : '',
            'corp_fee' => !empty($data['corp_fee']) ? 'numeric' : '',
            'attention' => 'max:1000',
            'commission_dial' => !empty($data['commission_dial']) ? 'numeric' : '',
            'tel_commission_datetime' => !empty($data['tel_commission_datetime']) ? 'date' : '',
            'tel_commission_person' => 'max:20',
            'commission_fee_rate' => !empty($data['commission_fee_rate']) ? 'numeric' : '',
            'commission_note_sender' => !empty($data['commission_note_sender']) ? 'numeric' : '',
            'commission_note_send_datetime' => !empty($data['commission_note_send_datetime']) ? 'date' : '',
            'commission_status' => !empty($data['commission_status']) ? 'numeric' : '',
            'construction_price_tax_exclude' => !empty($data['construction_price_tax_exclude']) ? 'numeric' : '',
            'complete_date' => 'max:10',
            'order_fail_date' => 'max:10',
            'estimate_price_tax_exclude' => 'max:10',
            'construction_price_tax_include' => !empty($data['construction_price_tax_include']) ? 'numeric' : '',
            'deduction_tax_include' => !empty($data['deduction_tax_include']) ? 'numeric' : '',
            'deduction_tax_exclude' => !empty($data['deduction_tax_exclude']) ? 'numeric' : '',
            'confirmd_fee_rate' => !empty($data['confirmd_fee_rate']) ? 'numeric' : '',
            'unit_price_calc_exclude' => !empty($data['unit_price_calc_exclude']) ? 'numeric' : '',
            'report_note' => 'max:1000',
            'irregular_fee_rate' => !empty($data['irregular_fee_rate']) ? 'numeric' : '',
            'irregular_fee' => !empty($data['irregular_fee']) ? 'numeric' : '',
            'reported_flg' => !empty($data['reported_flg']) ? 'numeric' : '',
            'follow_date' => !empty($data['follow_date']) ? 'date' : '',
            'business_trip_amount' => !empty($data['business_trip_amount']) ? 'numeric' : '',
            'commission_note_send_datetime' => !empty($data['commission_note_send_datetime']) ? 'date' : '',
        ], $message);

        $result['validate'] = $validate;
        $allValid = [
            $this->checkEmptyCorpFee($data),
            $this->checkEmptyCommissionFeeRate($data),
            $this->checkCommissionIntroduce($data),
            $this->checkCommissionStatusComplete($data),
            $this->checkCommissionStatusOrderFail($data),
            $this->checkCommissionOrderFailReason($data),
            $this->checkCommissionOrderFailConstructionPrice($data),
            $this->checkConstructionPrice($data),
            $this->checkCompleteDate($data),
            $this->checkFutureCompleteDate($data),
            $this->checkCompleteDateFormat($data),
            $this->checkOrderFailDate($data),
            $this->checkFutureOrderFailDate($data),
            $this->checkOrderFailDateFormat($data),
            $this->checkConstructionPriceTaxInclude($data),
            $this->checkFalsity($data)
        ];

        if ($validate->fails()) {
            $result['check'] = false;
        }

        if (in_array(false, $allValid)) {
            $result['check'] = false;
        }

        return $result;
    }

    /**
     * @param $status
     * @param $data
     * @return bool
     */
    public function validateDemandStatus($status, $data)
    {
        foreach ($data as $d) {
            if ($status == getDivValue('demand_status', 'telephone_already')) {    // 案件状況が【進行中】電話取次済の場合
                if (empty($data['tel_commission_datetime']) && $d['commit_flg'] == 1) {
                    Log::debug('false validateDemandStatus');
                    return false;
                }
                return true;
            } elseif ($status == getDivValue('demand_status', 'information_sent')) { // 案件状況が【進行中】情報送信済の場合
                if (empty($d['commission_note_send_datetime']) && $d['commit_flg'] == 1) {
                    Log::debug('false validateDemandStatus');
                    return false;
                }
                return true;
            }
        }
        return true;
    }

    /**
     * @param $data
     * @return bool
     */
    public function checkEmptyCorpFee($data)
    {
        if (!empty($data['commit_flg']) && empty($data['corp_fee'])) {
            session()->flash('commission_errors.corp_fee', __('commission_detail.validation.error_empty_corp_fee'));
            Log::debug('false checkEmptyCorpFee');
            return false;
        }

        return true;
    }

    /**
     * @param $data
     * @return bool
     */
    public function checkEmptyCommissionFeeRate($data)
    {
        if (!empty($data['commit_flg']) && empty($data['commission_fee_rate'])) {
            session()->flash('commission_errors.commission_fee_rate', __('commission_detail.validation.error_empty_commission_fee_rate'));
            Log::debug('false checkEmptyCommissionFeeRate');
            return false;
        }

        return true;
    }

    /**
     * @param $data
     * @return bool
     */
    public function checkCommissionIntroduce($data)
    {
        if (!isset($data['commission_type'])) {
            return true;
        }

        if ($data['commission_type'] == getDivValue('commission_type', 'normal_commission')
            && $data['commission_status'] == getDivValue('construction_status', 'introduction')) {
            session()->flash('commission_errors.commission_status', __('commission_detail.validation.error_commission_status_introcude'));
            Log::debug('false checkCommissionIntroduce');
            return false;
        }

        return true;
    }

    /**
     * @param $data
     * @return bool
     */
    public function checkCommissionStatusComplete($data)
    {
        if (!isset($data['commission_status'])) {
            return true;
        }

        if ($data['commission_status'] != getDivValue('construction_status', 'construction') &&
            $data['commission_status'] != getDivValue('construction_status', 'introduction') &&
            $data['commission_status'] != getDivValue('construction_status', 'order_fail') &&
            (!empty($data['complete_date']) || !empty($data['construction_price_tax_exclude']))) {
            session()->flash('commission_errors.commission_status', __('commission_detail.validation.error_commission_status_complete'));
            Log::debug('false checkCommissionStatusComplete');
            return false;
        }

        return true;
    }

    /**
     * @param $data
     * @return bool
     */
    public function checkCommissionStatusOrderFail($data)
    {
        if (!isset($data['commission_status'])) {
            return true;
        }

        if ($data['commission_status'] != getDivValue('construction_status', 'order_fail')) {
            if (!empty($data['order_fail_date'])) {
                session()->flash('commission_errors.commission_status', __('commission_detail.validation.error_commission_status_order_fail'));
                Log::debug('false checkCommissionStatusOrderFail');
                return false;
            }
        }

        return true;
    }

    /**
     * @param $data
     * @return bool
     */
    public function checkCommissionOrderFailReason($data)
    {
        if (!isset($data['commission_status'])) {
            return true;
        }

        if (empty($data['commission_order_fail_reason'])) {
            if ($data['commission_status'] == getDivValue('construction_status', 'order_fail')) {
                session()->flash('commission_errors.commission_order_fail_reason', __('commission_detail.validation.not_empty_order_fail_date'));
                Log::debug('false checkCommissionOrderFailReason');
                return false;
            }
        }

        return true;
    }

    /**
     * @param $data
     * @return bool
     */
    public function checkCommissionOrderFailConstructionPrice($data)
    {
        if (!isset($data['commission_status'])) {
            return true;
        }

        if (!empty($data['construction_price_tax_exclude'])) {
            if ($data['commission_status'] == getDivValue('construction_status', 'order_fail')) {
                session()->flash('commission_errors.construction_price_tax_exclude', __('commission_detail.validation.empty_construction_price'));
                Log::debug('false checkCommissionOrderFailConstructionPrice');
                return false;
            }
        }

        return true;
    }

    /**
     * @param $data
     * @return bool
     */
    public function checkConstructionPrice($data)
    {
        if (!isset($data['commission_status'])) {
            return true;
        }

        if (key_exists('construction_price_tax_exclude', $data) && strlen($data['construction_price_tax_exclude']) == 0) {
            if ($data['commission_status'] == getDivValue('construction_status', 'construction')) {
                session()->flash('commission_errors.construction_price_tax_exclude', __('commission_detail.validation.not_empty_construction_price'));
                Log::debug('false checkConstructionPrice');
                return false;
            }
        }

        return true;
    }

    /**
     * @param $data
     * @return bool
     */
    public function checkCompleteDate($data)
    {
        if (!isset($data['commission_status'])) {
            return true;
        }

        if (empty($data['complete_date'])) {
            if ($data['commission_status'] == getDivValue('construction_status', 'construction')) {
                session()->flash('commission_errors.complete_date', __('commission_detail.validation.not_empty_complete_date'));
                Log::debug('false checkCompleteDate');
                return false;
            }
        }

        return true;
    }

    /**
     * @param $data
     * @return bool
     */
    public function checkFutureCompleteDate($data)
    {
        if (!isset($data['complete_date'])) {
            return true;
        }

        $user = auth()->user()->auth;

        if (!empty($data['complete_date']) && $user != 'accounting' && $user != 'system') {
            if (strtotime($data['complete_date']) > strtotime(date('Y-m-d'))) {
                session()->flash('commission_errors.complete_date', __('commission_detail.validation.error_future_date'));
                Log::debug('false checkFutureCompleteDate');
                return false;
            }
        }

        return true;
    }

    /**
     * @param $data
     * @return bool
     */
    public function checkCompleteDateFormat($data)
    {
        if (!isset($data['complete_date']) || $this->checkDateFormat($data['complete_date'])) {
            return true;
        }

        session()->flash('commission_errors.complete_date', __('commission_detail.validation.not_date_format'));

        return false;
    }

    /**
     * @param $data
     * @return bool
     */
    public function checkOrderFailDateFormat($data)
    {
        if (!isset($data['order_fail_date']) || $this->checkDateFormat($data['order_fail_date'])) {
            return true;
        }

        session()->flash('commission_errors.order_fail_date', __('commission_detail.validation.not_date_format'));

        return false;
    }

    /**
     * @param $data
     * @return bool
     */
    public function checkOrderFailDate($data)
    {
        if (!isset($data['commission_status'])) {
            return true;
        }

        if (empty($data['order_fail_date'])) {
            if ($data['commission_status'] == getDivValue('construction_status', 'order_fail')) {
                session()->flash('commission_errors.order_fail_date', __('commission_detail.validation.not_empty_order_fail_date'));
                Log::debug('false checkOrderFailDate');
                return false;
            }
        }

        return true;
    }

    /**
     * @param $data
     * @return bool
     */
    public function checkFutureOrderFailDate($data)
    {
        if (!isset($data['order_fail_date'])) {
            return true;
        }

        if (!empty($data['order_fail_date'])) {
            if (strtotime($data['order_fail_date']) > strtotime(date('Y-m-d'))) {
                session()->flash('commission_errors.order_fail_date', __('commission_detail.validation.error_future_date'));
                Log::debug('false checkFutureOrderFailDate');
                return false;
            }
        }

        return true;
    }

    /**
     * @param $data
     * @return bool
     */
    public function checkConstructionPriceTaxInclude($data)
    {
        if (!isset($data['commission_status'])) {
            return true;
        }

        if (key_exists('construction_price_tax_include', $data) && strlen($data['construction_price_tax_include']) == 0) {
            if ($data['commission_status'] == getDivValue('construction_status', 'construction')) {
                session()->flash('commission_errors.construction_price_tax_include', __('commission_detail.validation.not_empty_construction_price'));
                Log::debug('false checkConstructionPriceTaxInclude');
                return false;
            }
        }

        return true;
    }

    /**
     * @param $data
     * @return bool
     */
    public function checkFalsity($data)
    {
        if (isset($data['reported_flg']) && empty($data['reported_flg'])) {
            session()->flash('commission_errors.reported_flg', __('commission_detail.validation.falsity_not_check'));
            Log::debug('false reported_flg');
            return false;
        }

        return true;
    }

    /**
     * @param $data
     * @return bool
     */
    public function validateCheckLimitCommitFlg($data)
    {
        $maxNum = $this->mSiteRepo->findMaxLimit((object)$data['demandInfo']);
        $commitFlgCount = 0;

        foreach ($data['commissionInfo'] as $item) {
            if (!empty($item['corp_id']) && $item['commit_flg'] == "1") {
                $commitFlgCount++;
            }
        }

        return $maxNum >= $commitFlgCount;
    }

    /**
     * @param $dateValue
     * @return bool
     */
    private function checkDateFormat($dateValue)
    {
        $datePart = explode('/', $dateValue);
        switch (true) {
            case count($datePart) < 3: // 「/」で分割した個数が3より少ない
                return false;
                break;
            case strlen($datePart[0]) < 4: // 年が4桁未満
                return false;
                break;
            case strlen($datePart[1]) < 2: // 月が4桁未満
                return false;
                break;
            case strlen($datePart[2]) < 2: // 日が4桁未満
                return false;
                break;
        }

        return true;
    }

    /**
     * @param $data
     * @return bool
     */
    public function checkCommissionFlgCount($data)
    {
        $commissionFlgCount = 0;
        if (isset($data['send_commission_info'])) {
            foreach ($data['commissionInfo'] as $commission) {
                if (!empty($commission['corp_id']) && isset($commission['commit_flg']) && $commission['commit_flg']) {
                    $commissionFlgCount = $commissionFlgCount + 1;
                }
            }
            if ($commissionFlgCount == 0) {
                return false;
            }
        }
        return true;
    }
}
