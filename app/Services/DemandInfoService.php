<?php

namespace App\Services;

use App\Helpers\MailHelper;
use App\Helpers\Sanitize;
use App\Models\DemandInfo;
use App\Models\MSite;
use App\Repositories\AuctionGenreRepositoryInterface;
use App\Repositories\AuctionInfoRepositoryInterface;
use App\Repositories\CommissionInfoRepositoryInterface;
use App\Repositories\DemandInfoRepositoryInterface;
use App\Repositories\DemandInquiryAnsRepositoryInterface;
use App\Repositories\Eloquent\MItemRepository;
use App\Repositories\MCategoryRepositoryInterface;
use App\Repositories\MCorpRepositoryInterface;
use App\Repositories\MGenresRepositoryInterface;
use App\Repositories\MSiteRepositoryInterface;
use App\Repositories\MUserRepositoryInterface;
use App\Repositories\SelectGenrePrefectureRepositoryInterface;
use App\Repositories\SelectGenreRepositoryInterface;
use App\Repositories\VisitTimeRepositoryInterface;
use App\Repositories\MPostRepositoryInterface;
use App\Repositories\AutoCommissionCorpRepositoryInterface;
use Illuminate\Support\Facades\Log;
use PhpOffice\PhpWord\PhpWord;
use App\Services\Credit\CreditService;
use App\Services\CommissionService;
use App\Services\AuctionService;

class DemandInfoService
{
    /**
     * @var DemandInfoRepositoryInterface
     */
    public $demandRepo;

    /**
     * @var MSiteRepositoryInterface
     */
    public $mSiteRepo;

    /**
     * @var MGenresRepositoryInterface
     */
    public $mGenreRepo;

    /**
     * @var MCategoryRepositoryInterface
     */
    public $mCategoryRepo;

    /**
     * @var MUserRepositoryInterface
     */
    public $mUserRepo;

    /**
     * @var MCorpRepositoryInterface
     */
    public $mCorpRepo;

    /**
     * @var AuctionGenreRepositoryInterface
     */
    public $auctionGenreRepo;

    /**
     * number of record each page
     *
     * @var integer
     */
    public $paginate = 500;

    /**
     *
     */
    const NON_NOTIFICATION = '非通知';

    const USER = "SYSTEM";

    /**
     * @var CommissionInfoRepositoryInterface
     */
    public $commissionRepo;

    /**
     * @var mixed
     */
    public $mUser;

    /**
     * @var SelectGenrePrefectureRepositoryInterface
     */
    protected $selectGenrePrefectureRepository;

    /**
     * @var SelectGenreRepositoryInterface
     */
    protected $selectGenreRepository;

    /**
     * @var DemandInquiryAnsRepositoryInterface
     */
    protected $demandInquiryAnswerRepo;

    /**
     * @var VisitTimeRepositoryInterface
     */
    protected $visitTimeRepo;
    /**
     * @var MPostRepositoryInterface
     */
    protected $mPostRepo;
    /**
     * @var credit service
     */
    protected $creditService;
    /**
     * @var commission service
     */
    protected $commissionService;
    /**
     * @var AuctionService service
     */
    protected $auctionService;
    /**
     * @var selectGenrePrefectureRepo service
     */
    public $selectGenrePrefectureRepo;
    /**
     * @var AuctionInfoRepositoryInterface
     */
    protected $auctionInfoRepo;

    /**
     * @var AccumulatedInformationsRepositoryInterface
     */
    protected $accumulatedInfoRepo;

    /**
     * constructor
     * @param  DemandInfoRepositoryInterface $demandRepo repository
     * @param MSiteRepositoryInterface $mSiteRepo
     * @param MGenresRepositoryInterface $mGenreRepo
     * @param MCategoryRepositoryInterface $mCategoryRepo
     * @param MCorpRepositoryInterface $mCorpRepo
     * @param MUserRepositoryInterface $mUserRepo
     * @param VisitTimeRepositoryInterface $visitTimeRepo
     * @param SelectGenreRepositoryInterface $selectGenreRepository
     * @param DemandInquiryAnsRepositoryInterface $demandInquiryAnswerRepo
     * @param CommissionInfoRepositoryInterface $commissionRepo
     * @param SelectGenrePrefectureRepositoryInterface $selectGenrePrefectureRepository
     * @param AuctionGenreRepositoryInterface $auctionGenreRepository
     * @param MPostRepositoryInterface $mPostRepo
     * @param AutoCommissionCorpRepositoryInterface $autoCommissionCorpRepo
     * @param CreditService $creditService
     * @param CommissionService $commissionService
     * @param AuctionService $auctionService
     */
    public function __construct(
        DemandInfoRepositoryInterface $demandRepo,
        MSiteRepositoryInterface $mSiteRepo,
        MGenresRepositoryInterface $mGenreRepo,
        MCategoryRepositoryInterface $mCategoryRepo,
        MCorpRepositoryInterface $mCorpRepo,
        MUserRepositoryInterface $mUserRepo,
        VisitTimeRepositoryInterface $visitTimeRepo,
        SelectGenreRepositoryInterface $selectGenreRepository,
        DemandInquiryAnsRepositoryInterface $demandInquiryAnswerRepo,
        CommissionInfoRepositoryInterface $commissionRepo,
        SelectGenrePrefectureRepositoryInterface $selectGenrePrefectureRepository,
        AuctionGenreRepositoryInterface $auctionGenreRepository,
        MPostRepositoryInterface $mPostRepo,
        AutoCommissionCorpRepositoryInterface $autoCommissionCorpRepo,
        CreditService $creditService,
        CommissionService $commissionService,
        AuctionService $auctionService,
        SelectGenrePrefectureRepositoryInterface $selectGenrePrefectureRepo
    ) {
        $this->auctionGenreRepo = $auctionGenreRepository;
        $this->demandRepo = $demandRepo;
        $this->mUserRepo = $mUserRepo;
        $this->mGenreRepo = $mGenreRepo;
        $this->mCategoryRepo = $mCategoryRepo;
        $this->mSiteRepo = $mSiteRepo;
        $this->mCorpRepo = $mCorpRepo;
        $this->selectGenreRepository = $selectGenreRepository;
        $this->demandInquiryAnswerRepo = $demandInquiryAnswerRepo;
        $this->selectGenrePrefectureRepository = $selectGenrePrefectureRepository;
        $this->visitTimeRepo = $visitTimeRepo;
        $this->mCorpRepo = $mCorpRepo;
        $this->commissionRepo = $commissionRepo;
        $this->mPostRepo = $mPostRepo;
        $this->creditService = $creditService;
        $this->commissionService = $commissionService;
        $this->autoCommissionCorpRepo = $autoCommissionCorpRepo;
        $this->auctionService = $auctionService;
        $this->selectGenrePrefectureRepo = $selectGenrePrefectureRepository;
    }

    /**
     * update demand info
     *
     * @param  integer $dInfoId demand_info id
     * @param  array   $data    data for update
     * @return void result update false of true
     */
    public function update($dInfoId, $data)
    {
    }

    /**
     * @param $dataList
     * @return mixed
     */
    public function convertDataCsv($dataList)
    {
        $demandStatusList = getDropList(trans('demandlist.demand_status'));
        $orderFailReasonList = getDropList(trans('demandlist.order_fail_reason'));
        $siteList = $this->mSiteRepo->getList();
        $genreList = $this->mGenreRepo->getList();
        $categoryList = $this->mCategoryRepo->getList();
        $petTombstoneDemandList = getDropList(trans('demandlist.pet_tombstone_demand'));
        $smsDemandList = getDropList(trans('demandlist.sms_demand'));
        $jbrWorkContentsList = getDropList(trans('demandlist.jbr_work_contents'));
        $jbrCategoryList = getDropList(trans('demandlist.jbr_category'));
        $userList = $this->mUserRepo->dropDownUser();
        $jbrEstimateStatusList = getDropList(trans('demandlist.jbr_estimate_status'));
        $jbrReceiptStatusList = getDropList(trans('demandlist.jbr_receipt_status'));
        $sendMailFaxList = ['' => '', '0' => '', '1' => '送信済み'];
        $acceptanceStatusList = getDropList(trans('demandlist.acceptance_status'));
        $commissionStatusList = getDropList(trans('demandlist.commission_status'));
        $commissionOrderFailReasonList = getDropList(trans('demandlist.commission_order_fail_reason'));
        $selectionSystemList = getDivList('datacustom.selection_type');
        $data = $dataList;

        $changeArray = array(0 => 'mail_demand', 1 => 'nighttime_takeover', 2 => 'low_accuracy', 3 => 'remand',
            4 => 'immediately', 5 => 'corp_change', 6 => 'sms_reorder');

        foreach ($dataList as $key => $val) {
            foreach ($changeArray as $v) {
                if ($val->$v == 0) {
                    $data[$key]->$v = trans('demandlist.batu');
                } else {
                    $data[$key]->$v = trans('demandlist.maru');
                }
            }
            $data[$key]->demand_status = !empty($val->demand_status)
                ? $demandStatusList[$val->demand_status] : '';
            $data[$key]->order_fail_reason = !empty($val->order_fail_reason)
                ? $orderFailReasonList[$val->order_fail_reason] : '';
            $data[$key]->site_name = !empty($val->site_name) ? $val->site_name : '';
            $data[$key]->genre_name = !empty($val->genre_name) ? $val->genre_name : '';
            $data[$key]->category_name = !empty($val->category_name) ? $val->category_name : '';
            $data[$key]->cross_sell_source_site =
                !empty($val->cross_sell_source_site)? $siteList[$val->cross_sell_source_site] : '';
            $data[$key]->cross_sell_source_genre =
                !empty($val->cross_sell_source_genre) ? $genreList[$val->cross_sell_source_genre] : '';
            $data[$key]->cross_sell_source_category =
                !empty($val->cross_sell_source_category) ? $categoryList[$val->cross_sell_source_category] : '';
            $data[$key]->pet_tombstone_demand =
                !empty($val->pet_tombstone_demand) ? $petTombstoneDemandList[$val->pet_tombstone_demand] : '';
            $data[$key]->sms_demand = !empty($val->sms_demand) ? $smsDemandList[$val->sms_demand] : '';
            $data[$key]->receptionist = !empty($val->receptionist) ? $userList[$val->receptionist] : '';
            $data[$key]->address1 = !empty($val->address1) ? getDivTextJP('prefecture_div', $val->address1) : '';
            $data[$key]->jbr_work_contents =
                !empty($val->jbr_work_contents) ? $jbrWorkContentsList[$val->jbr_work_contents] : '';
            $data[$key]->jbr_category = !empty($val->jbr_category) ? $jbrCategoryList[$val->jbr_category] : '';
            $data[$key]->jbr_estimate_status =
                !empty($val->jbr_estimate_status) ? $jbrEstimateStatusList[$val->jbr_estimate_status] : '';
            $data[$key]->jbr_receipt_status =
                !empty($val->jbr_receipt_status) ? $jbrReceiptStatusList[$val->jbr_receipt_status] : '';
            $data[$key]->contact_desired_time = !empty($val->contact_desired_time) ? $val->contact_desired_time : '';
            $data[$key]->acceptance_status =
                !empty($val->acceptance_status) ? $acceptanceStatusList[$val->acceptance_status] : '';
            $data[$key]->nitoryu_flg = !empty($val->nitoryu_flg) ? trans('demandlist.maru') : trans('demandlist.batu');
            $data[$key]->send_mail_fax = !empty($val->send_mail_fax) ? $sendMailFaxList[$val->send_mail_fax] : '';
            $data[$key]->commit_flg = !empty($val->commit_flg) ? trans('demandlist.maru') : trans('demandlist.batu');
            $data[$key]->commission_type =
                !empty($val->commission_type) ? trans('demandlist.bulk_quote') : trans('demandlist.normal_commission');
            $data[$key]->appointers = !empty($val->appointers) ? $userList[$val->appointers] : '';
            $data[$key]->first_commission =
                !empty($val->first_commission) ? trans('demandlist.maru') : trans('demandlist.batu');
            $data[$key]->tel_commission_person =
                !empty($val->tel_commission_person) ? $userList[$val->tel_commission_person] : '';
            $data[$key]->commission_note_sender =
                !empty($val->commission_note_sender) ? $userList[$val->commission_note_sender] : '';
            $data[$key]->commission_status =
                !empty($val->commission_status) ? $commissionStatusList[$val->commission_status] : '';
            $data[$key]->commission_order_fail_reason = !empty($val->commission_order_fail_reason)
                ? $commissionOrderFailReasonList[$val->commission_order_fail_reason] : '';
            $data[$key]->selection_system =
                !is_null($val->selection_system) ? $selectionSystemList[$val->selection_system] : '';
        }

        return json_decode(json_encode($data), true);
    }

    /**
     * @param $request
     * @param $data
     * @param $sessionKeyForAffiliationSearch
     * @param $sessionKeyForDemandSearch
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function checkAffiliationResults(
        $request,
        $data,
        $sessionKeyForAffiliationSearch,
        $sessionKeyForDemandSearch
    ) {
        if (empty($data["customer_tel"])) {
            return redirect()->route('demand_cti', [0, $data['site_tel']]);
        }
        if ($data["customer_tel"] == self::NON_NOTIFICATION) {
            return redirect()->route('demand_cti', [self::NON_NOTIFICATION, $data['site_tel']]);
        }
        $affiliationResults = $this->mCorpRepo->searchAffiliationInfoAll($data['customer_tel']);
        if (count($affiliationResults) == 1) {
            return redirect()->route('affiliation.detail.edit', [$affiliationResults[0]['id']]);
        } else {
            $affiliationSearch = [
                "tel1" => $data['customer_tel'],
                "support24hour" => "",
                "data[affiliation_status]" => "",
            ];
            $request->session()->put($sessionKeyForAffiliationSearch, $affiliationSearch);

            return redirect()->route('affiliation.search');
        }
        $siteTel = $data["site_tel"];
        unset($data["site_tel"]);
        $results = $this->searchDemandInfo($data);
        $data["site_tel"] = $siteTel;
        if (empty($results)) {
            return redirect()->route('demand_cti', [$data['customer_tel'], $data['site_tel']]);
        }
        $results = $this->demandRepo->getDemandInfo($data);
        if (empty($data['site_tel']) || empty($results)) {
            return $this->checkRedirectBySiteTel($sessionKeyForDemandSearch, $data);
        } else {
            if (count($results) == 1) {
                return redirect()->route('demand.detail', $results[0]["id"]);
            } else {
                $request->session()->put($sessionKeyForDemandSearch, $data);

                return redirect()->route('demandlist.search');
            }
        }
    }

    /**
     * check redirect by site tel
     *
     * @param  string $sessionKeyForDemandSearch
     * @param  array  $data
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    private function checkRedirectBySiteTel($sessionKeyForDemandSearch, &$data)
    {
        $results = $this->mSiteRepo->searchMsite($data['site_tel']);
        if (0 != count($results)) {
            return redirect()->route('demand_cti', [$data['customer_tel'], $data['site_tel']]);
        }

        $searchCustmerTelResults = $this->searchCustmerTel($data['customer_tel']);
        if (0 < count($searchCustmerTelResults)) {
            $data['site_tel'] = "";
            $this->request->session()->put($sessionKeyForDemandSearch, $data);
            return redirect("/demand_list/search");
        }
    }

    /**
     * @author Dung.PhamVan@nashtechglobal.com
     * @return array|boolean|\Illuminate\Config\Repository|mixed
     */
    public function translatePrefecture()
    {

        return $this->translate(config('rits.prefecture_div'), 'rits_config');
    }

    /**
     * @param DemandInfo|null $demand
     * @return array|\Illuminate\Config\Repository|mixed
     */
    public function getSelectionSystemList(DemandInfo $demand = null)
    {
        if (!$demand) {
            return $this->translateSelectionSystem();
        }
        $selection = $this->getSelectionSystem($demand);
        return !empty($selection) ? $selection : $this->translateSelectionSystem();
    }

    /**
     * @return array|\Illuminate\Config\Repository|mixed
     */
    public function translateSelectionSystem()
    {

        return $this->translate(config('rits.selection_type'), 'auto_commission_corp', false);
    }

    /**
     * @param DemandInfo|null $demand
     * @return array
     */
    public function getSelectionSystem(DemandInfo $demand = null)
    {
//        $defaultSelectionSystem = $this->getDefaultSelectionSystem();

        $selectionSystem = $this->getSelectType($demand);
//        dd($selectionSystem);
        $translateSystem = $this->translateSelectionSystem();

        return $demand != null ? $selectionSystem : $translateSystem;

//        foreach ($translateSystem as $key => $trans) {
//            if (in_array($key, $arr)) {
//                $data[$key] = $trans;
//            }
//        }
//        return $data;
    }

    /**
     * @param $type
     * @return false|int|string
     */
    public function getSelectionSystemByType($type)
    {
        return array_search($type, config('rits.selection_type'));
    }

    /**
     * @return false|int|string
     */
    public function getDefaultSelectionSystem()
    {
        return $this->getSelectionSystemByType('ManualSelection');
    }

    /**
     * @param DemandInfo|null $demand
     * @return false|int|string
     */
    private function getSelectType(DemandInfo $demand = null)
    {
        $genreId = $demand->genre_id;
        $address1 = $demand->address1;

        $selectionSystem = '';

        if (!empty($address1)) {
            $genrePrefecture = $this->selectGenrePrefectureRepo->getByGenreIdAndPrefectureCd([
                'genre_id' => $genreId,
                'address1' => $address1
            ]);

            if ($genrePrefecture) {
                $selectionSystem = $genrePrefecture->selection_type;
            }
        }

        if ($selectionSystem === '') {
            $selectGenre = $this->selectGenreRepository->findByGenreId($genreId);

            if ($selectGenre) {
                $selectionSystem = $selectGenre->select_type;
            }
        }

        $defaultSelection = getDivValue('selection_type', 'manual_selection');
        $option = [$selectionSystem => __('auto_commission_corp.' . config('rits.selection_type')[(int)$selectionSystem])];

        return array_replace(
            [
                $defaultSelection => __('auto_commission_corp.' . config('rits.selection_type')['0'])
            ], $option);

//        $defaultSelectType = $this->getDefaultSelectionSystem();
//
//        if (!$demand || !$demand->address1) {
//            return $defaultSelectType;
//        }
//
//        $genrePrefecture = $this->selectGenrePrefectureRepository->getByGenreIdAndPrefectureCd(
//            ['genre_id' => $demand->genre_id, 'address1' => $demand->address1]
//        );
//
//        if ($genrePrefecture && $genrePrefecture->selection_type) {
//            return $genrePrefecture->selection_type;
//        }
//
//        $genre = $this->selectGenreRepository->findByGenreId($demand->genre_id);
//        return $genre ? $genre->select_type : $defaultSelectType;
    }

    /**
     * @param $arrTranslate
     * @param $langFile
     * @param bool         $defaultOption
     * @return array|\Illuminate\Config\Repository|mixed
     */
    public function translate($arrTranslate, $langFile, $defaultOption = true)
    {
        $langArr = array_map(
            function ($div) use ($langFile) {
                return __($langFile.'.'.$div);
            },
            $arrTranslate
        );

        return !$defaultOption ? $langArr : config('constant.defaultOption') + $langArr;
    }

    /**
     * @return array|\Illuminate\Config\Repository|mixed
     */
    public function getPriorityTranslate()
    {
        return $this->translate(config('rits.priority'), 'auction');
    }

    /**
     * @param $data
     * @return array
     */
    public function updateDemand($data)
    {
        // Register deal information
        $saveData = $data;
        $saveData['created_user_id'] = auth()->user()->user_id;
        $saveData['modified_user_id'] = auth()->user()->user_id;
        // If you change the status of the case while it is in progress
        if ($saveData['demand_status'] == 4 || $saveData['demand_status'] == 5
            && $saveData['do_auto_selection_category'] == 0 && !empty($saveData['id'])
        ) {
            if ($limitoverTime = $this->demandRepo->getLimitoverTime($saveData['id'])) {
                $saveData['commission_limitover_time'] = $limitoverTime;
            }
        }
        if (($saveData['selection_system'] == getDivValue('selection_type', 'automatic_auction_selection')
            && $data['selection_system_before'] === '')
            || !empty($data['do_auto_selection'])
        ) {
            $saveData['modified_user_id'] = 'AutomaticAuction';
            $saveData['created_user_id'] = 'AutomaticAuction';
        }
        //It indicates that you designated a member shop by area × category
        if ($data['do_auto_selection_category'] == 1) {
            $saveData['modified_user_id'] = 'AutoCommissionCorp';
            $saveData['created_user_id'] = 'AutoCommissionCorp';
        }
        Log::debug('___Start insert demand_infos___');
        $newDemandData = $this->demandRepo->updateOrCreate($saveData);
        Log::debug('___End insert demand_infos___');
        return array_merge($saveData, $newDemandData);
    }

    /**
     * [buildAutoCommissionCorpData description]
     *
     * @author thaihv
     * @param  array $data request data
     * @param  array $commissionCorps commissioncorp data
     * @param  array $newCommissionCorps new commissioncorp data
     * @param  array $autoCommissonCorp auto commission corp data
     * @param $defaultFee
     * @param $autoCommissionSelectionLimit
     * @return array  auto commissioncorp data, allreadycommission, auto commisionselectlimit
     */
    public function buildAutoCommissionCorpData(
        $data,
        $commissionCorps,
        $newCommissionCorps,
        $autoCommissonCorp,
        $defaultFee,
        $autoCommissionSelectionLimit
    ) {
        $autoCommissionCorpData = array();
        $alreadyCommissions = false;
        $autoCommissionSelectionLimitCount = 0;
        foreach ($autoCommissonCorp as $autoCorp) {
            if (isset($data['commissionInfo'])) {
                foreach ($data['commissionInfo'] as $commission) {
                    if ($autoCorp->corp_id == $commission['corp_id']) {
                        $alreadyCommissions = true;
                        break;
                    }
                }
            }
            if ($alreadyCommissions) {
                continue;
            }
            $targetCommissionCorp = null;
            // Search for selection information matching category ID registered in category x prefecture
            foreach ($newCommissionCorps as $newCommissionCorp) {
                if ($newCommissionCorp->MCorp_id == $autoCorp->corp_id) {
                    $targetCommissionCorp = $newCommissionCorp;
                    break;
                }
            }

            if ($targetCommissionCorp === null) {
                foreach ($commissionCorps as $commissionCorp) {
                    if ($commissionCorp->MCorp_id == $autoCorp->corp_id) {
                        $targetCommissionCorp = $commissionCorp;
                        break;
                    }
                }
            }
            // wtf use for?
            if ($targetCommissionCorp === null) {
                continue;
            }
            //Credit check
            $resultCredit = $this->creditService->checkCredit(
                $targetCommissionCorp->MCorp_id,
                $data['demandInfo']['genre_id'],
                false,
                true
            );
            if ($resultCredit == config('constant.CREDIT_DANGER')) {
                continue;
            }

            $commitFlg = 0;

            // Add processing type to finalization condition
            if ($targetCommissionCorp->MCorpCategory_corp_commission_type != 2) { //  what the 2
                // Conclusion base
                $orderFee = $targetCommissionCorp->MCorpCategory_order_fee;
                $orderFeeUnit = $targetCommissionCorp->MCorpCategory_order_fee_unit;
                $commissionStatus = getDivValue('construction_status', 'progression');
                $lostFlg = 0;
                $introductionNot = null;
                $commissionType = getDivValue('commission_type', 'normal_commission');
            } else {
                // Introduction base
                $orderFee = $targetCommissionCorp->MCorpCategory_introduce_fee;
                $orderFeeUnit = 0;
                $commissionStatus = getDivValue('construction_status', 'introduction');
                /* In the case of Null since it does not match the condition of mail information acquisition,
                it is modified
                */
                $lostFlg = 0;
                $introductionNot = 0;
                $commissionType = getDivValue('commission_type', 'package_estimate');
            }
            if ($autoCorp->process_type == "2"  // 1: Automatic selection 2: Automatic transfer
                || $autoCommissionSelectionLimitCount > 0
            ) {
                if ($autoCommissionSelectionLimit > $autoCommissionSelectionLimitCount) {
                    $commitFlg = 1;
                } else {
                    $lostFlg = 1;
                }
            }
            //fee
            $orderFee = !empty($orderFee) ? $orderFee : $defaultFee->category_default_fee;
            $orderFeeUnit = !empty($orderFee) ? $orderFeeUnit : $defaultFee->category_default_fee_unit;
            $autoCommissionCorpData[] = [
                'mCorp' => [
                    'fax' => $targetCommissionCorp->MCorp_fax,
                    'mailaddress_pc' => $targetCommissionCorp->MCorp_mailaddress_pc,
                    'coordination_method'  => $targetCommissionCorp->MCorp_coordination_method,
                    'contactable_time' => $targetCommissionCorp->MCorp_contactable_time_from
                        . ' - ' . $targetCommissionCorp->MCorp_contactable_time_to,
                    'holiday' => $targetCommissionCorp->MCorp__holiday,
                    'corp_name' => $targetCommissionCorp->MCorp_corp_name,
                    'commission_dial' => $targetCommissionCorp->MCorp_commission_dial,
                ],
                'corp_fee' => $orderFeeUnit == 0 ? $orderFee : null, // Brokerage fee
                'commission_fee_rate' => $orderFeeUnit == 0 ? null : $orderFee, // Commission rate at commission
                'order_fee_unit' => $orderFeeUnit,
                'commission_status' => $commissionStatus,
                'lost_flg' => $lostFlg, // Prior to ordering
                'introduction_not' => $introductionNot, //Can not introduce
                'commission_type' => $commissionType,
                'corp_id' => $targetCommissionCorp->MCorp_id,
                'del_flg' => 0, //Delete
                'appointers' => auth()->id(), // Selector
                'first_commission' => 0,
                'unit_price_calc_exclude' => 0,
                'corp_claim_flg' => 0, //Agency complaint
                'commit_flg' => $commitFlg, // Confirm
                'commission_note_sender' => $commitFlg === 1 ? auth()->id() : null, // Mail order sender
                //Date and time of agency sent
                'commission_note_send_datetime' => $commitFlg === 1 ? date('Y/m/d H:i:s') : null,
                'select_commission_unit_price_rank' =>
                    (isset($targetCommissionCorp->AffiliationAreaStat_commission_unit_price_rank)
                        && !empty($targetCommissionCorp->AffiliationAreaStat_commission_unit_price_rank)
                    ) ? $targetCommissionCorp->AffiliationAreaStat_commission_unit_price_rank : null,
                'select_commission_unit_price' =>
                    (isset($targetCommissionCorp->AffiliationAreaStat_commission_unit_price_category)
                        && !empty($targetCommissionCorp->AffiliationAreaStat_commission_unit_price_category)
                    ) ? $targetCommissionCorp->AffiliationAreaStat_commission_unit_price_category : null,
                'created_user_id' => 'AutoCommissionCorp',
                'modified_user_id' => 'AutoCommissionCorp',
                'send_mail_fax' => 1,
                'send_mail_fax_othersend' => 0,
            ];

            //It is not necessary to limit the count, but just in case
            //The count for the fixed upper limit number is incremented by +1
            if ($commitFlg === 1) {
                $autoCommissionSelectionLimitCount++;
            }
        }

        return [
            'autoCommissionCorpData' => $autoCommissionCorpData,
            'alreadyCommissions' => $alreadyCommissions,
            'autoCommissionSelectionLimitCount' => $autoCommissionSelectionLimitCount,
        ];
    }

    /**
     * @param $selectionSystem
     * @param $demandInfo
     * @param $autoComSelectionLimitCount
     * @param $data
     * @return array
     */
    public function buildRestoreAtError($selectionSystem, $demandInfo, $autoComSelectionLimitCount, $data)
    {
        $restoreAtError = [];
        $restoreAtError['demandInfo']['do_auto_selection_category'] = $demandInfo['do_auto_selection_category'];
        $restoreAtError['commissionInfo'] = $data['commissionInfo'];

        if ($autoComSelectionLimitCount > 0) {
            //Since there is an automatic destination, flag the mail transmission
            $restoreAtError['send_commission_info'] = $data['send_commission_info'];
            //In order to conduct automatic transactions, update the status of cases
            $restoreAtError['demandInfo']['demand_status'] = $demandInfo['demand_status'];
        }

        if ($demandInfo['selection_system'] == $selectionSystem) {
            $restoreAtError['demandInfo']['selection_system'] = $demandInfo['selection_system'];
        }

        return $restoreAtError;
    }

    /**
     * @param $informationSent
     * @param $agencyBefore
     * @param $manualSelection
     * @param $selectionSystem
     * @param $demandInfo
     * @param $autoCommissionSelectionLimitCount
     * @return mixed
     */
    public function updateDemandInfoDataByCorp(
        $informationSent,
        $agencyBefore,
        $manualSelection,
        $selectionSystem,
        $demandInfo,
        $autoCommissionSelectionLimitCount
    ) {
        //Store items to be restored when an error occurs

        if ($autoCommissionSelectionLimitCount > 0) {
            //Since there is an automatic destination, flag the mail transmission
            //In order to conduct automatic transactions, update the status of cases
            $demandInfo['demand_status'] = $informationSent;
        } else {
            $demandInfo['demand_status'] = $agencyBefore;
        }

        $demandInfo['do_auto_selection_category'] = 1;

        if ($demandInfo['selection_system'] == $selectionSystem) {
            $demandInfo['selection_system'] = $manualSelection;
        }

        return $demandInfo;
    }

    /**
     * @param $demandInfo
     * @return mixed
     */
    public function updateDemandInfoDataByQuickOrder($demandInfo)
    {
        $demandInfo['site_id'] = !empty($demandInfo['site_id']) ? $demandInfo['site_id'] : 647;
        $demandInfo['genre_id'] = !empty($demandInfo['genre_id']) ? $demandInfo['genre_id'] : 673;
        $demandInfo['category_id'] = !empty($demandInfo['category_id']) ? $demandInfo['category_id']: 470;
        $demandInfo['customer_name'] = !empty($demandInfo['customer_name']) ? $demandInfo['customer_name'] : '不明';
        $demandInfo['customer_tel'] = (empty($demandInfo['customer_tel']) || $demandInfo['customer_tel'] == '非通知') ?
            '9999999999' : trim($demandInfo['customer_tel']);
        $demandInfo['tel1'] = !empty($demandInfo['tel1']) ? $demandInfo['tel1'] : '9999999999';
        $demandInfo['address1'] = !empty($demandInfo['address1']) ? $demandInfo['address1'] : '99';
        $address2 = '不明';
        if (!empty($demandInfo['address2'])) {
            if (is_numeric($demandInfo['address2'])) {
                $address2 = getDivTextJP('prefecture_div', $demandInfo['address2']);
            } else {
                $address2 = $demandInfo['address2'];
            }
        }
        $demandInfo['address2'] = $address2;
        $demandInfo['construction_class'] = !empty($demandInfo['construction_class']) ?
            $demandInfo['construction_class'] : 7;
        $demandInfo['is_contact_time_range_flg'] = 0;
        $demandInfo['contact_desired_time'] = date('Y/m/d H:i');

        switch ($demandInfo['quick_order_fail_reason']) {
            case 2:
                $demandInfo['order_fail_reason'] = null;
                $demandInfo['acceptance_status'] = 3;
                $demandInfo['demand_status'] = 9;
                break;
            case 3:
                $demandInfo['order_fail_reason'] = 35;
                $demandInfo['acceptance_status'] = 3;
                $demandInfo['demand_status'] = 6;
                break;
            case 4:
                $demandInfo['order_fail_reason'] = 37;
                $demandInfo['acceptance_status'] = 3;
                $demandInfo['demand_status'] = 6;
                break;
            case 5:
                $demandInfo['order_fail_reason'] = 36;
                $demandInfo['acceptance_status'] = 3;
                $demandInfo['demand_status'] = 6;
                break;

            default:
                $demandInfo['order_fail_reason'] = 35;
                $demandInfo['acceptance_status'] = 2;
                $demandInfo['demand_status'] = 6;
                break;
        }

        if ($demandInfo['demand_status'] == 6) {
            $demandInfo['order_fail_date'] = date('Y/m/d');
        }

        return $demandInfo;
    }

    /**
     * @param $categoryId
     * @param $commissionInfoData
     * @return mixed
     */
    public function updateCommissionData($categoryId, $commissionInfoData)
    {
        $commissionTypeCategory = $this->mCategoryRepo->getCommissionType($categoryId);
        $maxCommission = config('rits.demand_max_commission'); // max commission for demand 30
        for ($cnt = 0; $cnt < $maxCommission; $cnt++) {
            if (isset($commissionInfoData[$cnt]) && $commissionInfoData[$cnt]["corp_id"] == 3539) {
                if ($commissionTypeCategory != 2) {
                    // Interaction type = contract-based basis
                    $commissionInfoData[$cnt]['commission_fee_rate'] = 999;
                }

                break;
            } elseif (isset($commissionInfoData[$cnt]) && (int)$commissionInfoData[$cnt]["corp_id"] == 0) {
                $commissionInfoData[$cnt]["corp_id"] = 3539;
                $commissionInfoData[$cnt]["commit_flg"] = 1;
                $commissionInfoData[$cnt]["mCorp"]["corp_name"] = "【SF用】取次前失注用(質問のみ等)";

                if ($commissionTypeCategory != 2) {
                    // Interaction type = contract-based basis
                    $commissionInfoData[$cnt]['commission_fee_rate'] = 999;
                    $commissionInfoData[$cnt]['corp_commission_type'] = 1;
                } else {
                    // Interaction type = contract-based basis
                    $commissionInfoData[$cnt]['corp_commission_type'] = 2;
                }

                break;
            }
        }

        return $commissionInfoData;
    }

    /**
     * @author  thaihv
     * @param $demandInfo
     * @param $preferredDate
     * @return array
     */
    public function buildJudeResult(&$demandInfo, $preferredDate)
    {
        $judgeResult = [
            "result_flg" => 0,// 0 = No change, 1 = Changed (It is subject to exclusion time in normal case)
            "result_date" => null// start date
        ];
        if (empty($demandInfo['priority'])) {
            $judgeResult = judgeAuction(
                $demandInfo['auction_start_time'],
                $preferredDate,
                $demandInfo['genre_id'],
                $demandInfo['address1'],
                $demandInfo['auction_deadline_time'],
                $demandInfo['priority']
            );
        } // When the priority is urgent
        elseif ($demandInfo['priority'] == getDivValue('priority', 'asap')) {
            $judgeResult = judgeAsap(
                $demandInfo['auction_start_time'],
                $preferredDate,
                $demandInfo['genre_id'],
                $demandInfo['address1'],
                $demandInfo['auction_deadline_time'],
                $demandInfo['priority']
            );
        } // When the priority is urgent
        elseif ($demandInfo['priority'] == getDivValue('priority', 'immediately')) {
            $judgeResult = judgeImmediately(
                $demandInfo['auction_start_time'],
                $preferredDate,
                $demandInfo['genre_id'],
                $demandInfo['address1'],
                $demandInfo['auction_deadline_time'],
                $demandInfo['priority']
            );
        } // When the priority is normal
        else {
            $judgeResult = judgeNormal(
                $demandInfo['auction_start_time'],
                $demandInfo['genre_id'],
                $demandInfo['address1'],
                $demandInfo['auction_deadline_time'],
                $demandInfo['priority']
            );
        }

        return $judgeResult;
    }

    /**
     * @author thaihv
     * @param $demandInfo
     * @param $auctionFlg
     * @param $auctionNoneFlg
     * @return mixed
     */
    public function updateDemandInfoDataByFlg($demandInfo, $auctionFlg, $auctionNoneFlg)
    {
        if ((!$auctionFlg) || (!$auctionNoneFlg)) {
            // Selection method
            $demandInfo['selection_system'] = getDivValue('selection_type', 'manual_selection');
            // Proposal status
            $demandInfo['demand_status'] = getDivValue('demand_status', 'no_selection');
            // Auction start date and time
            $demandInfo['auction_start_time'] = '';
            //Auction start date and time
            $demandInfo['auction_deadline_time'] = '';
            // Empty the auction execution flag so that auction_infos is not generated
            $demandInfo['do_auction'] = '';
        } else {
            // Auction flow case flag
            $demandInfo['auction'] = 0;
            // Auction mail STOP flag
            $demandInfo['push_stop_flg'] = 0;
            // Proposal status
            $demandInfo['demand_status'] = getDivValue('demand_status', 'agency_before');
        }

        return $demandInfo;
    }

    /**
     * @param $corpData
     * @return array
     */
    public function getMailAndFaxByCorpData($corpData)
    {
        Log::debug('___ start get mail and fax _____');
        $mailList = [];
        $faxList = [];
        // Supplier
        foreach ($corpData as $val) {
            if (empty($val['corp_id']) && !empty($val['del_flg']) && !empty($val['lost_flg']) && $val['commit_flg'] != 1) {
                continue;
            }
            /* Specify it as the mail / fax destination only when there is no check in either
            * deletion or pre-order missed order
            * Target affiliated stores with final decision
            */
            $corpInfo = $this->mCorpRepo->getFirstById($val['corp_id']);
            if (!$corpInfo) {
                continue;
            }
            switch ($corpInfo->coordination_method) {
                // 2015.09.08 n.kai ADD start ORANGE-816
                case getDivValue('coordination_method', 'mail_app'):
                    $mailList[] = $corpInfo;
                    // Not installing application notification
                    break;
                case getDivValue('coordination_method', 'mail_fax_app'):
                    $mailList[] = $corpInfo;
                    $faxList[] = $corpInfo;
                    // Not installing application notification
                    break;
                case getDivValue('coordination_method', 'mail_fax'):
                    $mailList[] = $corpInfo;
                    $faxList[] = $corpInfo;
                    break;
                case getDivValue('coordination_method', 'mail'):
                    $mailList[] = $corpInfo;
                    break;
                case getDivValue('coordination_method', 'fax'):
                    $faxList[] = $corpInfo;
                    break;
                default:
                    break;
            }
        }

        return ['mailList' => $mailList, 'faxList' => $faxList];
    }

    /**
     * @param $demandId
     * @param $corpId
     * @return array|null
     */
    public function faxCommission($demandId, $corpId)
    {
        $commissionData = $this->commissionRepo->getWordData($demandId, $corpId);
        if (!empty($commissionData)) {
            $inquiries = $commissionData->demandInfo->inquiries;
            $countIn = $inquiries->count() - 1;
            $inquiryData = '';
            foreach ($inquiries as $key => $inq) {
                $inquiryData .= $inq->mInquiry->inquiry_name.'：'.$inq->answer_note;
                if ($countIn != $key) {
                    $inquiryData .= ', ';
                }
            }

            return $this->makeWordFile($commissionData, $inquiryData);
        }

        return null;
    }

    /**
     * @param $commissionData
     * @param string         $inquiryData
     * @param bool           $isMailFile
     * @return array
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function makeWordFile($commissionData, $inquiryData = '', $isMailFile = true)
    {
        $PHPWord = new PhpWord();
        ;
        $demandInfo = $commissionData->demandInfo;
        if ($demandInfo->mSite->jbr_flg == 1 && $demandInfo->jbr_work_contents == config('datacustom.jbr_glass_category')) {
            $template = config('datacustom.commission_template_jbrglass');
        } elseif ($demandInfo->mSite->jbr_flg == 1) {
            $template = config('datacustom.commission_template_jbr');
        } else {
            if (isset($commissionData->commission_type) && $commissionData->commission_type == 1) {
                $template = config('datacustom.commission_template_introduce');
            } else {
                $template = config('datacustom.commission_template');
            }
        }

        $document = $PHPWord->loadTemplate($template);
        /* Conversion source character
        *(If WORD can not be opened due to a character, add characters before conversion and converted)
        */
        $org = ["“","”","−"];
        //Translated character
        $new = ["\"", "\"", "-"];
        //Data setting
        $document->setValue('corp_name', Sanitize::html($commissionData->mCorp->official_corp_name));
        $document->setValue('confirmd_fee_rate', $commissionData->commission_fee_rate);
        $document->setValue('demand_id', $demandInfo->id);
        $document->setValue('site_name', Sanitize::html($demandInfo->mSite->site_name));
        $document->setValue('note', Sanitize::html($demandInfo->mSite->note));
        $document->setValue('customer_name', Sanitize::html(str_replace($org, $new, $demandInfo->customer_name)));

        $customerAddress = getDivTextJP('prefecture_div', $demandInfo->address1)
            . $demandInfo->address2
            . $demandInfo->address3
            . $demandInfo->address4
            . $demandInfo->building
            . $demandInfo->room;
        $customerAddress = str_replace($org, $new, $customerAddress);
        $document->setValue('address', Sanitize::html($customerAddress));
        $document->setValue('construction_class', getDropText('建物種別', $demandInfo->construction_class));

        $document->setValue('tel1', $demandInfo->tel1);
        $document->setValue('tel2', $demandInfo->tel2);
        $document->setValue(
            'contents',
            str_replace(
                "\n",
                "<w:br/>",
                Sanitize::html(str_replace($org, $new, $demandInfo->contents))
            )
        );
        $document->setValue('contents1', $inquiryData);
        $document->setValue('receptionist', Sanitize::html($demandInfo->mUser->user_name));
        $document->setValue('commission_id', $commissionData->id);

        $document->setValue('jbr_order_no', Sanitize::html($demandInfo->jbr_order_no));
        $document->setValue(
            'jbr_work_contents',
            Sanitize::html(getDropText('[JBR様]作業内容', $demandInfo->jbr_work_contents))
        );

        $filePath = config('datacustom.commission_tmp_dir')
                    . sprintf('commission_%s_%s.docx', $demandInfo->id, $commissionData->id);
        $document->saveAs($filePath);
        if ($isMailFile) {
            $fileName = mb_encode_mimeheader(
                mb_convert_encoding(
                    sprintf(
                        '%s_%s_%s.docx',
                        __('commission.commission_print_name'),
                        $commissionData->mCorp->official_corp_name,
                        $demandInfo->id
                    ),
                    'ISO-2022-JP',
                    'UTF-8'
                )
            );
        } else {
            $fileName = mb_convert_encoding(
                sprintf(
                    '%s_%s_%s.docx',
                    __('commission.commission_print_name'),
                    $commissionData->mCorp->official_corp_name,
                    $demandInfo->id
                ),
                'SJIS-win',
                'UTF-8'
            );
        }

        return ['fileName' => $fileName, 'filePath' => $filePath];
    }

    /**
     * @param $demandInfo
     * @param $faxList
     * @param $mailInfo
     * @return bool
     */
    public function sendFax($demandInfo, $faxList, $mailInfo)
    {
        if (empty($mailInfo)) {
            return false;
        }
        if (empty($faxList)) {
            return true;
        }
        Log::debug('___start send fax___');
        $faxContents = sprintf(
            getDivText('mail_setting', 'contents'),
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
        );
        // TO address
        $toAddress = env('DEMAND_TO_ADDRESS', getDivText('fax_setting', 'to_address'));
        $subject = getDivText('fax_setting', 'title');
        $result = true;
        foreach ($faxList as $key => $val) {
            if (!empty($val->fax)) {
                $headers = ["Content-Type" => "multipart/mixed; boundary=\"__PHPRECIPE__\"\r\n\r\n"];
                $body = "--__PHPRECIPE__\r\n"."Content-Type: text/plain; charset=\"ISO-2022-JP\"\r\n"."\r\n"."%s\r\n"."--__PHPRECIPE__\r\n";
                // Create and attach an intermediary form
                 $fileRes = $this->faxCommission($demandInfo['id'], $val->id);
                //$fileRes = $this->faxCommission(1511750, $val->id);
                $fileName = $fileRes['fileName'];
                $filePath = $fileRes['filePath'];
                // Attachment
                $handle = fopen($filePath, 'rb');

                $attachFile = fread($handle, filesize($filePath));
                fclose($handle);
                $attachEncode = base64_encode($attachFile);
                $attacheBody = "Content-Type: text/plain; name=\"" . $fileName . "\"\r\n"
                        . "Content-Transfer-Encoding: base64\r\n"
                        . "Content-Disposition: attachment; filename=\"" . $fileName . "\"\r\n"
                                . "\r\n"
                                        . chunk_split($attachEncode) . "\r\n"
                                                . "--__PHPRECIPE__--\r\n";

                // Text
                $mailContents = sprintf(getDivText('fax_setting', 'contents'), $val->fax, $val->corp_name);
                $body = sprintf($body, $mailContents).$attacheBody;
                $from = env('DEMAND_FROM_ADDRESS', getDivText('mail_setting', 'from_address'));
                try {
                    MailHelper::sendMailWithHeader($from, $toAddress, $subject, $body, $headers);
                    Log::debug('___end send fax___');
                } catch (\Exception $e) {
                    Log::error($e->getMessage());
                    $result = false;
                }
            }
        }

        return $result;
    }

    /**
     * @param $demandInfo
     * @param $mailList
     * @param $mailInfo
     * @return bool
     */
    public function sendMail($demandInfo, $mailList, $mailInfo)
    {
        if (empty($mailInfo)) {
            return false;
        }
        if (empty($mailList)) {
            return true;
        }
        Log::debug('___start send mail___');
        $demandFileInfo = !$mailInfo->demandAttachedFiles->first() ? '添付資料あり（取次管理から確認して下さい。）' : '';
        if ($mailInfo->mSite->jbr_flg == 1 && $demandInfo['jbr_work_contents'] == config('datacustom.jbr_glass_category')) {
            $from = getDivText('mail_setting', 'from_address');
            $subject = sprintf(getDivText('jbr_glass_mail_setting', 'title'), $demandInfo['id']);
            $body = sprintf(getDivText('jbr_glass_mail_setting', 'contents'), $demandInfo['jbr_order_no'], route('login'), $demandInfo['id'], $mailInfo->mSite->site_name, $mailInfo->mCategory->category_name, $mailInfo->customer_name, "〒".$mailInfo->postcode, getDivTextJP('prefecture_div', $mailInfo->address1).$mailInfo->address2.$mailInfo->address3, getDropText('建物種別', $mailInfo->construction_class), $mailInfo->tel1, $mailInfo->tel2, $mailInfo->customer_mailaddress, $mailInfo->contents, $demandFileInfo);
        } elseif ($mailInfo->mSite->jbr_flg == 1) {
            $from = getDivText('mail_setting', 'from_address');
            $subject = sprintf(getDivText('jbr_mail_setting', 'title'), $demandInfo['id']);
            $body = sprintf(getDivText('jbr_glass_mail_setting', 'contents'), $demandInfo['jbr_order_no'], route('login'), $demandInfo['id'], $mailInfo->mSite->site_name, $mailInfo->mCategory->category_name, $mailInfo->customer_name, "〒".$mailInfo->postcode, getDivTextJP('prefecture_div', $mailInfo->address1).$mailInfo->address2.$mailInfo->address3, getDropText('建物種別', $mailInfo->construction_class), $mailInfo->tel1, $mailInfo->tel2, $mailInfo->customer_mailaddress, $mailInfo->contents, $demandFileInfo);
        } elseif ($mailInfo->commissionInfoMail->first()->commission_type == 1) {
            $from = getDivText('package_estimate_mail_setting', 'from_address');
            $subject = sprintf(getDivText('package_estimate_mail_setting', 'title'), $demandInfo['id']);
            $body = sprintf(getDivText('package_estimate_mail_setting', 'contents'), $demandInfo['id'], $mailInfo->receive_datetime, $mailInfo->mUser->user_name, $mailInfo->mSite->site_name, $mailInfo->mSite->site_url, $mailInfo->mSite->note, $mailInfo->mCategory->category_name, $mailInfo->customer_name, "〒".$mailInfo->postcode, getDivTextJP('prefecture_div', $mailInfo->address1).$mailInfo->address2.$mailInfo->address3, getDropText('建物種別', $mailInfo->construction_class), $mailInfo->tel1, $mailInfo->tel2, $mailInfo->customer_mailaddress, $mailInfo->mSite->site_name, $mailInfo->contents, $demandFileInfo, route('commission.detail', ['id' => $mailInfo->commissionInfoMail->first()->id]));
        } else {
            $from = getDivText('normal_commission_mail_setting', 'from_address');
            $subject = sprintf(getDivText('normal_commission_mail_setting', 'title'), $demandInfo['id']);
            $body = sprintf(getDivText('normal_commission_mail_setting', 'contents'), $demandInfo['id'], $mailInfo->receive_datetime, $mailInfo->mUser->user_name, $mailInfo->mSite->site_name, $mailInfo->mSite->site_url, $mailInfo->mSite->note, $mailInfo->mCategory->category_name, $mailInfo->customer_name, "〒".$mailInfo->postcode, getDivTextJP('prefecture_div', $mailInfo->address1).$mailInfo->address2.$mailInfo->address3, getDropText('建物種別', $mailInfo->construction_class), $mailInfo->customer_corp_name, $mailInfo->tel1, $mailInfo->tel2, $mailInfo->customer_mailaddress, $mailInfo->mSite->site_name, $mailInfo->contents, $demandFileInfo, route('commission.detail', ['id' => $mailInfo->commissionInfoMail->first()->id]));
        }
        $bcc = env('DEMAND_BCC', getDivText('bcc_mail', 'to_address'));
        if (env('DEMAND_FROM_ADDRESS')) { // TODO: remove
            $from = env('DEMAND_FROM_ADDRESS');
        }
        $result = true;
        foreach ($mailList as $val) {
            $toAddressList = [];
            if (!empty($val->mailaddress_pc)) {
                $toAddressList = explode(';', $val->mailaddress_pc);
            }
            if (!empty($val->mailaddress_mobile)) {
                $toAddressListM = explode(';', $val->mailaddress_mobile);
                $toAddressList = array_merge($toAddressList, $toAddressListM);
            }
            foreach ($toAddressList as $toAddress) {
                if (env('DEMAND_TO_ADDRESS')) { //TODO remove
                    $toAddress = env('DEMAND_TO_ADDRESS');
                }
                try {
                    MailHelper::sendRawMail($body, $subject, $from, trim($toAddress), $bcc);
                    Log::debug('___end send mail____');
                } catch (\Exception $e) {
                    Log::error($e->getMessage());
                    $result = false;
                }
            }
        }

        return $result;
    }

    /**
     * @param $demandId
     * @return mixed
     */
    public function getMailData($demandId)
    {
        return $mailData = $this->demandRepo->getMailData($demandId);
    }

    /**
     * @param $arr
     * @param $removeAttr
     * @return mixed
     */
    private function unsetMultiAttribute($arr, $removeAttr)
    {
        foreach ($removeAttr as $attr) {
            if (!array_key_exists($attr, $arr)) {
                continue;
            }
            unset($arr[$attr]);
        }

        return $arr;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function makeCommissionInfoData($data)
    {
        $arrayFill = array_fill(
            0,
            30,
            [
            "del_flg" => "0",
            "appointers" => '',
            "first_commission" => 0,
            "commission_note_sender" => '',
            "unit_price_calc_exclude" => "0",
            "commission_note_send_datetime" => '',
            "commit_flg" => 0,
            "lost_flg" => "0",
            "complete_date" => '',
            "commission_status" => '',
            "corp_claim_flg" => "0",
            "introduction_not" => "0",
            "send_mail_fax_datetime" => '',
            "commission_type" => '',
            "corp_id" => "",
            "id" => '',
            "select_commission_unit_price" => '',
            "select_commission_unit_price_rank" => '',
            "send_mail_fax_sender" => '',
            "send_mail_fax_othersend" => '',
            "order_fee_unit" => '',
            "send_mail_fax" => '',
            'demand_id' => 0,
            'mCorp' => [
                "corp_name" => "",
                "fax" => "",
                "mailaddress_pc" => "",
                "coordination_method" => "",
                "contactable_time" => "",
                "holiday" => "日",
                "commission_dial" => "",
            ],
            'mCorpNewYear' => [
                "label_01" => "",
                "label_02" => "",
                "label_03" => "",
                "label_04" => "",
                "label_05" => "",
                "label_06" => "",
                "status_01" => "",
                "status_02" => "",
                "status_03" => "",
                "status_04" => "",
                "status_05" => "",
                "status_06" => "",
                "note" => null,
            ],
            'mCorpCategory' => [
                "order_fee" => "",
                "order_fee_unit" => "",
                "note" => '',
            ],
            'affiliationInfo' => [
                'attention' => '',
            ],
            ]
        );
        $data = array_replace($arrayFill, $data);
        return $data;
    }

    /**
     * @param $attributes
     * @return bool
     */
    public function validateDemandInquiryAnswer($attributes)
    {
        if (!isset($attributes['demandInquiryAnswer'])) {
            return false;
        }
        return !isset($attributes['demandInquiryAnswer']['demand_id'])
                || empty($attributes['demandInquiryAnswer']['demand_id'])
                || !is_int($attributes['demandInquiryAnswer']['demand_id']
                || !is_int($attributes['demandInquiryAnswer']['inquiry_id']));
    }

    /**
     * @param $attributes
     * @return bool
     */
    public function validateDemandInfo($attributes)
    {
        $checkOrderDate = isset($attributes['demandInfo']['order_date'])
            ? $this->checkDateFormat($attributes['demandInfo']['order_date']) : true;
        $allValid = [
            $checkOrderDate,
            $this->checkFollowDate($attributes),
            $this->checkDemandStatus($attributes),
            $this->checkDemandStatusAdvance($attributes),
            $this->checkDemandStatusIntroduce($attributes),
            $this->checkDemandStatusIntroduceMail($attributes),
            $this->checkDemandStatusSelectionType($attributes),
            $this->checkDemandStatusConfirm($attributes),
            $this->checkOrderFailReason($attributes),
            $this->checkReservationDemandNotEmpty($attributes),
            $this->checkCrossSellSiteNotEmpty($attributes),
            $this->checkCrossSellGenreNotEmpty($attributes),
            $this->checkSourceDemandIdNotEmpty($attributes),
            $this->checkCustomerTel($attributes['demandInfo']['customer_tel']),
            $this->checkTel1($attributes),
            $this->checkContentsString($attributes['demandInfo']['contents']),
            $this->checkContactDesiredTime2($attributes),
            $this->checkContactDesiredTime3($attributes),
            $this->checkContactDesiredTime4($attributes),
            $this->checkRequireTo($attributes),
            $this->checkContactDesiredTime5($attributes),
            $this->checkContactDesiredTime6($attributes),
            $this->checkRequireFrom($attributes),
            $this->checkPetTombstoneDemandNotEmpty($attributes),
            $this->checkSmsDemandNotEmpty($attributes),
            $this->checkOrderNoMarriageNotEmpty($attributes),
            $this->checkJbrOrderNo($attributes),
            $this->checkJbrWorkContents($attributes),
            $this->checkJbrCategory($attributes),
            $this->checkJbrCategory2($attributes),
            $this->checkOrderFailDate($attributes),
            $this->checkSelectionSystem($attributes),
            $this->checkDoAuction($attributes),
            $this->checkDemandStatusIntroduceMail2($attributes)
        ];
        return !in_array(false, $allValid);
    }

    /**
     * @param $attributes
     * @return bool
     */
    private function checkDoAuction($attributes)
    {
        if (isset($attributes['demandInfo']['do_auction'])
            && !empty($attributes['demandInfo']['do_auction'])
            && $attributes['demandInfo']['selection_system'] != getDivValue('selection_type', 'auction_selection')
            && $attributes['demandInfo']['selection_system'] !=
                getDivValue('selection_type', 'automatic_auction_selection')
        ) {
            session()->flash('demand_errors.check_do_auction', __('demand.validation_error.check_do_auction'));
            return false;
        }
        return true;
    }

    /**
     * @param $attributes
     * @return bool
     */
    public function checkAuctionSettingGenre($attributes)
    {
        if ($attributes['demandInfo']['selection_system'] == getDivValue('selection_type', 'auction_selection')
            || $attributes['demandInfo']['selection_system'] ==
                getDivValue('selection_type', 'automatic_auction_selection')
            || $attributes['demandInfo']['selection_system'] == getDivValue('selection_type', 'auto_selection')
        ) {
            $auctionGenreData = $this->auctionGenreRepo->getFirstByGenreId($attributes['demandInfo']['genre_id']);
            if (empty($auctionGenreData)) {
                session()->flash('demand_errors.check_auction_setting_genre', __('demand.check_auction_setting_genre'));

                return false;
            }
        }

        return true;
    }

    /**
     * @param $attributes
     * @return bool
     */
    public function checkSelectionSystem($attributes)
    {
        $check = true;

        if (!empty($attributes['demandInfo']['do_auction'])) {
            if ($attributes['demandInfo']['do_auction'] == 1 || $attributes['demandInfo']['do_auction'] == 2) {
                if ($attributes['demandInfo']['selection_system'] == getDivValue('selection_type', 'auction_selection')
                    || $attributes['demandInfo']['selection_system'] ==
                        getDivValue('selection_type', 'automatic_auction_selection')) {
                    $genreData = $this->selectGenreRepository->findByGenreId($attributes['demandInfo']['genre_id']);

                    if (empty($genreData)) {
                        $check = false;
                    }

                    if ($genreData->select_type != getDivValue('selection_type', 'auction_selection')
                        && $genreData->select_type != getDivValue('selection_type', 'automatic_auction_selection')
                    ) {
                        $check = false;
                    } else {
                        $genrePrefectureData = $this->selectGenrePrefectureRepository->getByGenreIdAndPrefectureCd(
                            [
                                'genre_id' => $attributes['demandInfo']['genre_id'],
                                'address1' => $attributes['demandInfo']['address1']
                            ]
                        );
                        if (!empty($genrePrefectureData)
                            && $genrePrefectureData->selection_type != ""
                            && $genrePrefectureData->selection_type != null
                            && ($genrePrefectureData->selection_type !=
                                getDivValue('selection_type', 'auction_selection')
                            && $genrePrefectureData->selection_type !=
                                getDivValue('selection_type', 'automatic_auction_selection'))
                        ) {
                            $check = false;
                        }
                    }
                }
            }
        }

        if (!$check) {
            session()->flash(
                'demand_errors.check_selection_system',
                __('demand.validation_error.check_selection_system')
            );
            return false;
        }

        return true;
    }

    /**
     * @param $attributes
     * @return bool
     */
    public function checkOrderFailDate($attributes)
    {
        if (empty($attributes['demandInfo']['order_fail_date'])
            && $attributes['demandInfo']['demand_status'] == getDivValue('demand_status', 'order_fail')
        ) {
            session()->flash('demand_errors.check_date_format', __('demand.validation_error.check_date_format'));
            return false;
        }

        return true;
    }

    /**
     * @param $attribute
     * @return bool
     */
    public function checkDateFormat($attribute)
    {
        $datePart = explode('/', $attribute);
        $check = true;

        switch (true) {
            case count($datePart) < 3:
                $check = false;
                break;
            case strlen($datePart[0]) < 4:
                $check = false;
                break;
            case strlen($datePart[1]) < 2:
                $check = false;
                break;
            case strlen($datePart[2]) < 2:
                $check = false;
                break;
        }

        if (!$check) {
            session()->flash('demand_errors.check_date_format', __('demand.validation_error.check_date_format'));
            return false;
        }
        return true;
    }

    /**
     * @param $attributes
     * @return bool
     */
    public function checkJbrCategory2($attributes)
    {
        if (empty($attributes['demandInfo']['jbr_category'])) {
            $jbr = $this->checkJbrSite($attributes['demandInfo']['site_id']);

            if ($jbr) {
                session()->flash(
                    'demand_errors.check_jbr_category2',
                    __('demand.validation_error.check_jbr_not_empty')
                );
                return false;
            }
        }
        return true;
    }

    /**
     * @param $attributes
     * @return bool
     */
    public function checkJbrCategory($attributes)
    {
        if (empty($attributes['demandInfo']['jbr_category'])
            && $attributes['demandInfo']['jbr_work_contents'] == getDivValue('jbr_work', 'pest_extermination')) {
            session()->flash(
                'demand_errors.check_jbr_category',
                __('demand.validation_error.check_jbr_category_not_empty')
            );
            return false;
        }
        return true;
    }

    /**
     * @param $attributes
     * @return bool
     */
    public function checkJbrWorkContents($attributes)
    {
        if (empty($attributes['demandInfo']['jbr_work_contents'])) {
            $jbr = $this->checkJbrSite($attributes['demandInfo']['site_id']);
            if ($jbr) {
                session()->flash(
                    'demand_errors.check_jbr_work_contents',
                    __('demand.validation_error.check_jbr_not_empty')
                );
                return false;
            }
        }
        return true;
    }

    /**
     * @param $siteId
     * @return bool
     */
    private function checkJbrSite($siteId)
    {
        $rslt = false;
        if (empty($siteId)) {
            return $rslt;
        }
        $site = $this->mSiteRepo->findById($siteId);
        if ($site['MSite']['jbr_flg'] == 1) {
            $rslt = true;
        }
        return $rslt;
    }

    /**
     * @param $attributes
     * @return bool
     */
    public function checkJbrOrderNo($attributes)
    {
        if (empty($attributes['demandInfo']['jbr_order_no'])) {
            $jbr = $this->checkJbrSite($attributes['demandInfo']['site_id']);

            if ($jbr) {
                session()->flash('demand_errors.check_jbr_order_no', __('demand.validation_error.check_jbr_not_empty'));

                return false;
            }
        }

        return true;
    }

    /**
     * @param $attributes
     * @return bool
     */
    public function checkOrderNoMarriageNotEmpty($attributes)
    {
        $checkSites = array(477, 492, 906, 917, 460, 494, 727, 743, 758, 760, 907, 919);

        if ($attributes['demandInfo']['genre_id'] == 620
            || in_array($attributes['demandInfo']['site_id'], $checkSites)
        ) {
            if ($attributes['demandInfo']['order_no_marriage']  == null) {
                session()->flash(
                    'demand_errors.check_order_no_marriage_not_empty',
                    __('demand.validation_error.not_empty')
                );
                return false;
            }
        }
        return true;
    }

    /**
     * @param $attributes
     * @return bool
     */
    public function checkSmsDemandNotEmpty($attributes)
    {
        return true;
    }

    /**
     * @param $attributes
     * @return bool
     */
    private function checkPetTombstoneDemandNotEmpty($attributes)
    {
        if ($attributes['demandInfo']['genre_id'] == 509
            && $attributes['demandInfo']['pet_tombstone_demand']  == null
        ) {
            session()->flash(
                'demand_errors.pet_tombstone_demand',
                __('demand.validation_error.check_pet_tombstone_demand_not_empty')
            );
            return false;
        }
        return true;
    }

    /**
     * @param $attributes
     * @return bool
     */
    private function checkRequireFrom($attributes)
    {
        if (!isset($attributes['demandInfo']['contact_desired_time_to'])) {
            return true;
        }

        if (strlen($attributes['demandInfo']['contact_desired_time_to']) == 0) {
            return true;
        }

        if (!isset($attributes['demandInfo']['contact_desired_time_from'])) {
            session()->flash(
                'demand_errors.contact_desired_time_from',
                __('demand.validation_error.check_required_from')
            );
            return false;
        }

        if (strlen($attributes['demandInfo']['contact_desired_time_from']) == 0) {
            session()->flash(
                'demand_errors.contact_desired_time_from',
                __('demand.validation_error.check_required_from')
            );
            return false;
        }
        return true;
    }

    /**
     * @param $attributes
     * @return bool
     */
    private function checkContactDesiredTime6($attributes)
    {
        if (!empty($attributes['demandInfo']["contact_desired_time_to"])
            && !empty($attributes['demandInfo']["contact_desired_time_from"])
        ) {
            if (strtotime($attributes['demandInfo']["contact_desired_time_to"]) <
                strtotime($attributes['demandInfo']["contact_desired_time_from"])
            ) {
                session()->flash(
                    'demand_errors.check_contact_desired_time6',
                    __('demand.validation_error.past_date_time_2')
                );
                return false;
            }
        }
        return true;
    }

    /**
     * @param $attributes
     * @return bool
     */
    private function checkContactDesiredTime5($attributes)
    {
        if (!empty($attributes['demandInfo']["contact_desired_time_to"])
            && !empty($attributes['demandInfo']['do_auction'])
            && strtotime($attributes['demandInfo']["contact_desired_time_to"]) <
                strtotime(date('Y/m/d H:i'))
        ) {
            session()->flash(
                'demand_errors.check_contact_desired_time5',
                __('demand.validation_error.past_date_time')
            );
            return false;
        }
        return true;
    }

    /**
     * @param $attributes
     * @return bool
     */
    private function checkRequireTo($attributes)
    {
        if (!isset($attributes['demandInfo']['contact_desired_time_from'])) {
            return true;
        }

        if (strlen($attributes['demandInfo']['contact_desired_time_from']) == 0) {
            return true;
        }

        if (!isset($attributes['demandInfo']['contact_desired_time_to'])) {
            session()->flash('demand_errors.check_require_to', __('demand.validation_error.check_require_to'));

            return false;
        }

        if (strlen($attributes['demandInfo']['contact_desired_time_to']) == 0) {
            session()->flash('demand_errors.check_require_to', __('demand.validation_error.check_require_to'));

            return false;
        }

        return true;
    }

    /**
     * @param $attributes
     * @return bool
     */
    private function checkContactDesiredTime4($attributes)
    {
        if (!empty($attributes['demandInfo']["contact_desired_time_from"])
            && !empty($attributes['demandInfo']['do_auction'])
            && strtotime($attributes['demandInfo']["contact_desired_time_from"]) <
                strtotime(date('Y/m/d H:i'))) {
            session()->flash(
                'demand_errors.check_contact_desired_time4',
                __('demand.validation_error.past_date_time')
            );
            return false;
        }
        return true;
    }

    /**
     * @param $attributes
     * @return bool
     */
    private function checkContactDesiredTime3($attributes)
    {
        if (!empty($attributes['demandInfo']["contact_desired_time"])
            && !empty($attributes['demandInfo']['do_auction'])
            && strtotime($attributes['demandInfo']["contact_desired_time"]) < strtotime(date('Y/m/d H:i'))) {
            session()->flash(
                'demand_errors.check_contact_desired_time3',
                __('demand.validation_error.past_date_time')
            );
            return false;
        }
        return true;
    }

    /**
     * @param $attributes
     * @return bool
     */
    private function checkContactDesiredTime2($attributes)
    {
        if (empty($attributes['demandInfo']['contact_desired_time'])
            && empty($attributes['demandInfo']['contact_desired_time_from'])
        ) {
            foreach ($attributes['visitTime'] as $val) {
                if (!empty($val['visit_time'])
                    || (!empty($val['visit_time_from'])
                        && !empty($val['visit_time_to']))
                ) {
                    return true;
                }
            }

            session()->flash('demand_errors.check_contact_desired_time2', __('demand.validation_error.not_empty'));
            return false;
        }
        return true;
    }

    /**
     * @param $attribute
     * @return bool
     */
    private function checkContentsString($attribute)
    {
        if (!empty($attribute['demandInfo']['contents'])) {
            foreach (getDivList('word_ban') as $keyword) {
                if (strstr($attribute['demandInfo']['contents'], $keyword)) {
                    session()->flash(
                        'demand_errors.check_contents_string',
                        __('demand.validation_error.check_contents_string')
                    );
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * @param $attributes
     * @return bool
     */
    private function checkFollowDate($attributes)
    {
        $check = empty($attributes['demandInfo']['follow_date'])
                || $attributes['demandInfo']['follow_date'] instanceof \DateTime
                || (strtotime($attributes['demandInfo']['follow_date']) > strtotime(date('Y/m/d')));

        if (!$check) {
            session()->flash('demand_errors.follow_date', __('demand.validation_error.check_follow_date'));
            return false;
        }
        return true;
    }

    /**
     * @param $attributes
     * @return bool
     */
    private function checkDemandStatus($attributes)
    {
        if (isset($attributes['demandInfo']['selection_system'])
            && in_array(
                $attributes['demandInfo']['selection_system'],
                [
                    getDivValue('selection_type', 'auction_selection'),
                    getDivValue('selection_type', 'automatic_auction_selection')
                ]
            )
        ) {
            return true;
        }

        if (in_array(
            $attributes['demandInfo']['demand_status'],
            [getDivValue('demand_status', 'no_selection'), getDivValue('demand_status', 'no_guest')]
        )
        ) {
            return true;
        }

        for ($i = 0; $i < 30; $i++) {
            if (isset($attributes['commissionInfo'][$i])
                && !empty($attributes['commissionInfo'][$i]['corp_id'])
                && empty($attributes['commissionInfo'][$i]['lost_flg'])
            ) {
                return true;
            }
        }

        session()->flash('demand_errors.check_demand_status', __('demand.validation_error.check_demand_status'));

        return false;
    }

    /**
     * @param $attributes
     * @return bool
     */
    private function checkDemandStatusAdvance($attributes)
    {
        $demandStatus = $attributes['demandInfo']['demand_status'];

        if (isset($attributes['demandInfo']['selection_system'])
            && in_array(
                $attributes['demandInfo']['selection_system'],
                [
                    getDivValue('selection_type', 'auction_selection'),
                    getDivValue('selection_type', 'automatic_auction_selection')
                ]
            )
        ) {
            return true;
        }

        if (in_array(
            $demandStatus,
            [
                    getDivValue('demand_status', 'telephone_already'),
                    getDivValue('demand_status', 'information_sent')
                ]
        )
        ) {
            for ($i = 0; $i < 30; $i++) {
                if (isset($attributes['commissionInfo'][$i])) {
                    if (!empty($attributes['commissionInfo'][$i]['corp_id'])) {
                        if (empty($attributes['commissionInfo'][$i]['commit_flg'])
                            && empty($attributes['commissionInfo'][$i]['lost_flg'])
                        ) {
                            session()->flash(
                                'demand_errors.check_demand_status_advance',
                                __('demand.validation_error.check_demand_status_advance')
                            );
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    /**
     * @param $attributes
     * @return bool
     */
    private function checkDemandStatusIntroduce($attributes)
    {
        if (isset($attributes['demandInfo']['demand_status'])
            && $attributes['demandInfo']['demand_status'] == getDivValue('demand_status', 'no_selection')
        ) {
            $corpCnt = 0;
            $lostCnt = 0;
            $delCnt  = 0;

            for ($i = 0; $i < 30; $i++) {
                if (isset($attributes['commissionInfo']) && !empty($attributes['commissionInfo'][$i]['corp_id'])) {
                    $corpCnt++;
                }

                if (isset($attributes['commissionInfo'])
                    && !empty($attributes['commissionInfo'][$i]['corp_id'])
                    && $attributes['commissionInfo'][$i]['lost_flg'] == 1) {
                    $lostCnt++;
                }
                if (isset($attributes['commissionInfo'])
                    && !empty($attributes['commissionInfo'][$i]['corp_id'])
                    && $attributes['commissionInfo'][$i]['del_flg'] == 1) {
                    $delCnt++;
                }
            }

            if ($corpCnt != ($lostCnt + $delCnt)) {
                session()->flash(
                    'demand_errors.check_demand_status_introduce',
                    __('demand.validation_error.check_demand_status_introduce')
                );
                return false;
            }
        }
        return true;
    }

    /**
     * @param $attributes
     * @return bool
     */
    private function checkDemandStatusIntroduceMail($attributes)
    {
        if (isset($attributes['demandInfo']['send_commission_info'])
            && $attributes['demandInfo']['send_commission_info'] == 1) {
            return true;
        }

        if (isset($attributes['demandInfo']['demand_status']) && $attributes['demandInfo']['demand_status'] == getDivValue('demand_status', 'agency_before')) {
            for ($i = 0; $i < 30; $i++) {
                if (isset($attributes['demandInfo'][$i])
                    && !empty($attributes['demandInfo'][$i]['send_mail_fax'])
                    && $attributes['demandInfo'][$i]['send_mail_fax'] == 1
                    && ($attributes['demandInfo'][$i]['lost_flg'] == 0)
                    && ($attributes['demandInfo'][$i]['del_flg'] == 0)
                ) {
                    session()->flash(
                        'demand_errors.check_demand_status_introduce_email',
                        __('demand.validation_error.check_demand_status_introduce_email')
                    );
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Check demand status introduce mail
     *
     * @param $data
     * @return bool
     */
    public function checkDemandStatusIntroduceMail2($data)
    {
        if ($data['not-send'] == 0 && $data['send_commission_info'] == 0) {
        }
        if (isset($data['demandInfo']['selection_system'])
            && $data['demandInfo']['selection_system'] == getDivValue('selection_type', 'auction_selection')
        ) {
            return true;
        }

        if (isset($data['demandInfo']['selection_system'])
            && $data['demandInfo']['selection_system'] == getDivValue('selection_type', 'automatic_auction_selection')
        ) {
            return true;
        }

        if (isset($data['demandInfo']['demand_status'])
            && (
                $data['demandInfo']['demand_status'] == getDivValue('demand_status', 'telephone_already')
                || $data['demandInfo']['demand_status'] == getDivValue('demand_status', 'information_sent')
            )
        ) {
            for ($i = 0; $i < 30; $i++) {
                if (isset($data['commissionInfo'][$i])) {
                    if (($data['commissionInfo'][$i]['commit_flg'] == 1)
                        && (
                            ($data['commissionInfo'][$i]['send_mail_fax'] == 1)
                            || ($data['commissionInfo'][$i]['send_mail_fax_othersend'] == 1)
                        )
                    ) {
                        return true;
                    }
                }
            }
            session()->flash(
                'demand_errors.check_demand_status_introduce_email2',
                __('demand.validation_error.mail_not_select')
            );
            return false;
        }
        return true;
    }

    /**
     * @param $attributes
     * @return bool
     */
    private function checkDemandStatusSelectionType($attributes)
    {
        if (isset($attributes['demandInfo']['selection_system'])
            && $attributes['demandInfo']['selection_system'] != getDivValue('selection_type', 'auction_selection')) {
            return true;
        }

        if (isset($attributes['demandInfo']['selection_system'])
            && $attributes['demandInfo']['selection_system'] !=
                getDivValue('selection_type', 'automatic_auction_selection')
        ) {
            return true;
        }

        if (!empty($attributes['demandInfo']['do_auction'])
            && $attributes['demandInfo']['demand_status'] != getDivValue('demand_status', 'no_selection')
        ) {
            session()->flash(
                'demand_errors.check_demand_status_selection_type',
                __('demand.validation_error.check_demand_status_selection_type')
            );

            return false;
        }
        return true;
    }

    /**
     * @param $attributes
     * @return bool
     */
    private function checkDemandStatusConfirm($attributes)
    {
        $commissionFlgCount = 0;
        if (!empty($attributes['commissionInfo'])) {
            foreach ($attributes['commissionInfo'] as $commisionData) {
                if ($commisionData['corp_id']
                    && isset($commisionData['commit_flg'])
                    && $commisionData['commit_flg']) {
                    $commissionFlgCount = $commissionFlgCount + 1;
                }
            }
        }
        if (isset($attributes['quick_order_fail'])
            && $attributes['demandInfo']["quick_order_fail_reason"] != "") {
            return true;
        }
        if ($commissionFlgCount == 0) {
            return true;
        } else {
            switch ($attributes['demandInfo']['demand_status']) {
                case 4:
                case 5:
                case 6:
                    return true;
                    break;
                default:
                    session()->flash(
                        'demand_errors.check_demand_status_confirm',
                        __('demand.validation_error.check_demand_status_confirm')
                    );
                    return false;
                    break;
            }
        }
    }

    /**
     * @param $attributes
     * @return bool
     */
    private function checkOrderFailReason($attributes)
    {
        if (empty($attributes['demandInfo']['order_fail_reason'])
            && $attributes['demandInfo']['demand_status'] == getDivValue('demand_status', 'order_fail')
        ) {
            session()->flash(
                'demand_errors.check_order_fail_reason',
                __('demand.validation_error.check_order_fail_reason')
            );
            return false;
        }
        return true;
    }

    /**
     * @param $attributes
     * @return bool
     */
    private function checkReservationDemandNotEmpty($attributes)
    {
        if ($attributes['demandInfo']['site_id'] != 585) {
            if (isset($attributes['demandInfo']['reservation_demand'])
                && $attributes['demandInfo']['reservation_demand'] == null
            ) {
                session()->flash(
                    'demand_errors.check_reservation_demand_not_empty',
                    __('demand.validation_error.not_empty')
                );
                return false;
            }
        }
        return true;
    }

    /**
     * @param $attributes
     * @return bool
     */
    private function checkCrossSellSiteNotEmpty($attributes)
    {
        if (in_array($attributes['demandInfo']['site_id'], [861, 863, 889, 890, 1312, 1313, 1314])
            && $attributes['demandInfo']['cross_sell_source_site'] == null) {
            session()->flash(
                'demand_errors.check_cross_sell_site_not_empty',
                __('demand.validation_error.not_empty')
            );
            return false;
        }
        return true;
    }

    /**
     * @param $attributes
     * @return bool
     */
    private function checkCrossSellGenreNotEmpty($attributes)
    {
        if (in_array($attributes['demandInfo']['site_id'], [861, 863, 889, 890, 1312, 1313, 1314])
            && $attributes['demandInfo']['cross_sell_source_genre'] == null) {
            session()->flash(
                'demand_errors.check_cross_sell_genre_not_empty',
                __('demand.validation_error.not_empty')
            );
            return false;
        }
        return true;
    }

    /**
     * @param $attributes
     * @return bool
     */
    private function checkSourceDemandIdNotEmpty($attributes)
    {
        if (in_array($attributes['demandInfo']['site_id'], [861, 863, 889, 890, 1312, 1313, 1314])
            && $attributes['demandInfo']['source_demand_id'] == null
        ) {
            session()->flash(
                'demand_errors.check_source_demand_id_not_empty',
                __('demand.validation_error.not_empty')
            );
            return false;
        }
        return true;
    }

    /**
     * @param $attribute
     * @return bool
     */
    private function checkCustomerTel($attribute)
    {
        if (!empty($attribute) && !ctype_digit($attribute) && $attribute != '非通知') {
            session()->flash('demand_errors.check_customer_tel', __('demand.validation_error.check_customer_tel'));
            return false;
        }
        return true;
    }

    /**
     * @param $attributes
     * @return bool
     */
    private function checkTel1($attributes)
    {
        if (empty($attributes['demandInfo']['tel1'])
            && $attributes['demandInfo']['demand_status'] != getDivValue('demand_status', 'order_fail')
        ) {
            session()->flash('demand_errors.check_tel1', __('demand.validation_error.check_tel1'));
            return false;
        }
        return true;
    }

    /**
     * @param $data
     * @return bool
     */
    public function checkModifiedDemand($data)
    {
        // Only when updating (only when holding the item ID in the input data) Check
        if (isset($data['id']) && !empty($data['id'])) {
            //Retrieve current matter information record
            $currentData = $this->demandRepo->findById($data['id']);
            //Check the update date and time
            if ($data['modified'] != $currentData['DemandInfo']['modified']) {
                session()->flash('demand_errors.check_modified_demand', __('demand.check_modified_demand'));
                return false;
            }
        }
        return true;
    }

    /**
     * @return mixed
     */
    public function executeCheckFollowDate()
    {
        $data = ['follow_date' => null];

        return $this->demandRepo->updateExecuteFollowDate($data);
    }

    /**
     * @author thaihv
     * add some default data for demand input data
     * @param array $demandData request data
     * @param int $defaultValue
     */
    public function addDefaultValueForDemand($demandData, $defaultValue = 0)
    {
        $requestMerge = [
            'nighttime_takeover', 'mail_demand', 'cross_sell_implement', 'cross_sell_call', 'riro_kureka',
            'remand', 'sms_reorder', 'corp_change', 'low_accuracy', 'do_auction', 'follow', 'auction'
        ];

        foreach ($requestMerge as $merge) {
            if (!isset($demandData[$merge])) {
                $demandData[$merge] = $defaultValue;
            }
        }
        isset($demandData['priority']) ?? $demandData['priority'] = '';
        $demandData['pet_tombstone_demand'] = $demandData['pet_tombstone_demand'] ?? null;
        // remove demand_status
        // make sure that demand_status don't receive from view
        $demandData['selection_system'] = isset($demandData['selection_system']) ? $demandData['selection_system'] : 0;
        $demandData['do_auto_selection_category'] = 0;
        $demandData['do_auto_selection'] = 0;
        //Initialize the selection flag indicating that the member shop was designated by region × category
//        $demandData['selection_system_before'] = '';
        if (!isset($demandData['demand_status'])) {
            $demandData['demand_status'] = '';
        }
        return $demandData;
    }

    /**
     * @author thaihv
     * @param  array $data deamdInfo data
     * @return array all data processed
     */
    public function processDataWithoutQuickOrder($data)
    {
        $resultCrossSiteFlg = $this->mSiteRepo->getCrossSiteFlg(MSite::CROSS_FLAG);
        if ($data['demandInfo']['demand_status'] == getDivValue('demand_status', 'no_selection')
            && (!isset($data['quick_order_fail'])
            &&  $data["demandInfo"]["quick_order_fail_reason"] == "")
            && (!in_array($data["demandInfo"]["site_id"], $resultCrossSiteFlg))
            && (!in_array($data["demandInfo"]["site_id"], config('rits.arrSiteId')))
        ) {
            // Acquisition of selection targets
            $jisCd = $this->getTargetJisCd($data["demandInfo"]["address1"], null);
            $commissionConditions = [
                'category_id' => $data['demandInfo']['category_id'],
                'jis_cd' => $jisCd,
                'target_check' => ''
            ];
            // if empty corp => flash error
            $commissionCorps = $this->commissionService->getCorpList($commissionConditions, '');
            $newCommissionCorps = $this->commissionService->getCorpList($commissionConditions, 'new');
            /*
            Acquire prefecture and category of case
            Acquire eligible merchants
             */
            $add1 = $data["demandInfo"]["address1"];
            $category = $data["demandInfo"]['category_id'];
            $autoCommissonCorp = $this->autoCommissionCorpRepo->getAutoCommissionCorp($add1, $category, null, false);
            //Ref. Upper limit reference
            $autoCommissionSelectionLimit = 0;
            /**
             * if $data['selection_system'] == 2 || $data['selection_system'] == 3
             * return auction_selection_limit
             * other case => manual_selection_limit
             * on the site belongs to "Selection method" (選定方式) item when select site
             */
            try {
                $autoCommissionSelectionLimit = $this->mSiteRepo->getMaxLimitDemandCreate($data['demandInfo'], true);
            } catch (\Exception $e) {
                session()->flash('error_msg_input', $e->getMessage());
                return false;
            }
            //Whether the franchise chosen by the user was covered with an automatically selected franchise
            //Accept default default commission and default fee unit for specified category
            $defaultFee = $this->mCategoryRepo->getDefaultFee($data['demandInfo']['category_id']);
            /**
             * build data
             * autoCommissionCorpData
             * alreadyCommissions
             * autoCommissionSelectionLimitCount
             */
            $allData = $this->buildAutoCommissionCorpData(
                $data,
                $commissionCorps,
                $newCommissionCorps,
                $autoCommissonCorp,
                $defaultFee,
                $autoCommissionSelectionLimit
            );
            $autoCommissionCorpData = $allData['autoCommissionCorpData'];
            $alreadyCommissions = $allData['alreadyCommissions'];
            $autoCommissionSelectionLimitCount = $allData['autoCommissionSelectionLimitCount'];
            // check error restore
            if (empty($autoCommissionCorpData)) {
                /*On the screen, if it was indicated that there was an automatic
                supplier but it did not exist in some sort of irregular */
                //However, exclude cases that can not be determined because what was already selected by the user
                if (isset($data['display_auto_commission_message'])
                    && $data['display_auto_commission_message'] == "1"
                    && !$alreadyCommissions
                ) {
                    session()->flash('error_msg_input', 'again_enabled');
                    return false;
                }
            } else {
                $selectionSystem = getDivValue('selection_type', 'auto_selection');
                $informationSent = getDivValue('demand_status', 'information_sent');
                $agencyBefore = getDivValue('demand_status', 'agency_before');
                $manualSelection = getDivValue('selection_type', 'manual_selection');
                $data['restore_at_error'] = $this->buildRestoreAtError(
                    $selectionSystem,
                    $data['demandInfo'],
                    $autoCommissionSelectionLimitCount,
                    $data
                );
                //Store items to be restored when an error occurs
                $data['demandInfo'] = $this->updateDemandInfoDataByCorp(
                    $informationSent,
                    $agencyBefore,
                    $manualSelection,
                    $selectionSystem,
                    $data['demandInfo'],
                    $autoCommissionSelectionLimitCount
                );
                if ($autoCommissionSelectionLimitCount > 0) {
                    //Since there is an automatic destination, flag the mail transmission
                    $data['send_commission_info'] = 1;
                }
                $data['commissionInfo'] = array_merge($autoCommissionCorpData, $data['commissionInfo']);
            }
        }
        return $data;
    }


    /**
     * @author  thaihv
     * @param $address1
     * @param $address2
     * @return string
     */
    public function getTargetJisCd($address1, $address2)
    {
        if (!empty($address1)) {
            $address1 = getDivTextJP('prefecture_div', $address1);
        }
        $lowerAddress2 = '';
        $upperAddress2 = '';
        if (!empty($address2)) {
            $upperAddress2 = $address2;
            $upperAddress2 = str_replace('ヶ', 'ケ', $upperAddress2);
            $upperAddress2 = str_replace('ﾉ', 'ノ', $upperAddress2);
            $upperAddress2 = str_replace('ﾂ', 'ツ', $upperAddress2);
            $lowerAddress2 = $address2;
            $lowerAddress2 = str_replace('ケ', 'ヶ', $lowerAddress2);
            $lowerAddress2 = str_replace('ノ', 'ﾉ', $lowerAddress2);
            $lowerAddress2 = str_replace('ツ', 'ﾂ', $lowerAddress2);
        }
        $mPost = $this->mPostRepo->getJiscdByAddress($address1, $lowerAddress2, $upperAddress2);
        if ($mPost) {
            return $mPost->jis_cd;
        }
        return '';
    }
    /**
     * replaceSpace
     * @author thaihv
     * @param  array $demandInfo demand info data
     * @return array            demand info data
     */
    public function replaceSpace($demandInfo)
    {
        array_walk($demandInfo, function (&$demandItem) {
            $demandItem = preg_replace('/^[ 　]+/u', '', $demandItem);
            $demandItem = preg_replace('/[ 　]+$/u', '', $demandItem);
        });

        return $demandInfo;
    }
    /**
     * @author thaihv
     * @param  array $demandInfo demand info data
     * @return array             demand info data
     */
    public function checkDemanInfoDoAuction($demandInfo)
    {
        $arraySelection = [getDivValue('selection_type', 'auction_selection') ,
                getDivValue('selection_type', 'automatic_auction_selection')]; //[2,3]

        if (isset($demandInfo['selection_system'])
            && in_array($demandInfo['selection_system'], $arraySelection)
            && $demandInfo['selection_system_before'] == '') {
            $demandInfo['do_auction'] = 1;
        }

        return $demandInfo;
    }
    /**
     * @author thaihv
     * @param  array $demandInfoData demand info data
     * @return array                 demand info data
     */
    public function checkDoAutoSelection($demandInfoData)
    {
        if (isset($demandInfoData['selection_system'])
            && $demandInfoData['selection_system'] == getDivValue('selection_type', 'auto_selection')) {
            $demandInfoData['do_auto_selection'] = 1;
        }

        return $demandInfoData;
    }
    /**
     * update demand correspond content
     * @author  thaihv
     * @param  array $demandCorrespond demand correspond data
     * @return array                   demand correspond data
     */
    public function updateCorrespondContent($demandCorrespond)
    {
        if ($demandCorrespond['corresponding_contens'] != "") {
            $demandCorrespond['corresponding_contens'] = "ワンタッチ失注で登録\r\n"
                . $demandCorrespond['corresponding_contens'];//Orange-1155
            return $demandCorrespond;
        }

        $demandCorrespond['corresponding_contens'] = "ワンタッチ失注で登録";
        return $demandCorrespond;
    }
    /**
     * @author thaihv
     * @param  array $data all data
     * @return array
     */
    public function processQuickOrderFail($data)
    {
        if (!empty($data['quick_order_fail'])
            && $data['demandInfo']['quick_order_fail_reason'] !="") {
            $quickOrderFail = true;
            $data['demandInfo']['do_auction'] = 0;
            $data['demandInfo']['do_auto_selection'] = 0;
            // update demand info
            $data['demandInfo'] = $this->updateDemandInfoDataByQuickOrder($data['demandInfo']);
            //update demand correspond
            $data['demandCorrespond'] = $this->updateCorrespondContent($data['demandCorrespond']);
            //update commission info
            if (isset($data['commissionInfo'])) {
                $data['commissionInfo'] = $this->updateCommissionData(
                    $data['demandInfo']['category_id'],
                    $data['commissionInfo']
                );
            }
        }

        return $data;
    }
    /**
     * validate commission type div
     * @author  thaihv
     * @param  int $ctypeDiv  commission type div
     * @param  boolean $existsCommision exist commission
     * @param  int $demandStatus    demand status
     * @return boolean
     */
    public function validateCommissionTypeDiv($ctypeDiv, $existsCommision, $demandStatus)
    {
        $phoneReady = getDivValue('demand_status', 'telephone_already');
        $infoSent = getDivValue('demand_status', 'information_sent');
        if ($ctypeDiv == 2 && !$existsCommision && ( $demandStatus == $phoneReady ||$demandStatus == $infoSent)) {
            return false;
        }

        return true;
    }
    /**
     * validate selection systemtype
     * @author  thaihv
     * @param  array $data all data
     * @return boolean
     */
    public function validateSelectSystemType($data)
    {
        if (($data['demandInfo']['selection_system'] == 2 || $data['demandInfo']['selection_system'] == 3)
            && (isset($data['demandInfo']['auction']) && $data['demandInfo']['auction'] == 1)) {
            if ($data['demandInfo']['demand_status'] != 1 || $data['demandInfo']['do_auction'] != 2) {
                session()->flash('error_msg_input', __('demand.reBiddingSelect'));
                return false;
            }
        }
        if (($data['demandInfo']['selection_system'] == 2 || $data['demandInfo']['selection_system'] == 3)
            && (
                $data['demandInfo']['selection_system_before'] != 2
                && $data['demandInfo']['selection_system_before'] != 3
                && $data['demandInfo']['selection_system_before'] != ''
            )
            ) {
            if ($data['demandInfo']['demand_status'] != 1 || $data['demandInfo']['do_auction'] != 2) {
                session()->flash('error_msg_input', __('demand.reBiddingNotSelect'));
                return false;
            }
        }

        if ($data['demandInfo']['selection_system'] == getDivValue('selection_type', 'auto_selection')
            && !empty($data['demandInfo']['do_auto_selection'])) {
            if ($data['demandInfo']['demand_status'] != 1 && $data['demandInfo']['do_auto_selection_category'] != 1) {
                session()->flash('error_msg_input', __('demand.dataInconsistency'));
                return false;
            }
        }

        foreach ($data['commissionInfo'] as $commissionData) {
            if ($commissionData['corp_claim_flg'] && !$commissionData['commit_flg']) {
                session()->flash('error_msg_input', 'error corp_claim_flg');
                return false;
            }
        }

        return true;
    }
    /**
     * validate selection systemtype
     * @author  thaihv
     * @param  array $data all data
     * @return boolean
     */
    public function checkErrSendCommissionInfo($data)
    {
        if (isset($data['send_commission_info']) && !empty($data['send_commission_info'])) {
            $commissionFlgCount = 0;
            foreach ($data['commissionInfo'] as $commisionData) {
                if ($commisionData['corp_id'] && isset($commisionData['commit_flg']) && $commisionData['commit_flg']) {
                    $commissionFlgCount = $commissionFlgCount + 1;
                }
            }
            if ($commissionFlgCount == 0) {
                return true;
            }
        }

        return false;
    }
    /**
     * processing auction selection
     * @author thaihv
     * @param  array $data source data
     * @return array       data (auctionFlg, data, auctionNoneFlg,  hasStartTimeErr)
     */
    public function processAuctionSelection($data)
    {
        $auctionFlg = true;
        $auctionNoneFlg = true;
        $hasStartTimeErr = false;
        // Auction selection only
        $arraySelection = [getDivValue('selection_type', 'auction_selection') ,
                getDivValue('selection_type', 'automatic_auction_selection')]; //[2,3]
        if (isset($data['demandInfo']['selection_system'])
            && (in_array($data['demandInfo']['selection_system'], $arraySelection))
            && !empty($data['demandInfo']['do_auction'])
            && $data['demandInfo']['do_auto_selection_category'] == 0
        ) {
            // Only for auction process execution
            // ADD start In case of automatic consignment, auction is not selected
            // 【Bidding ceremony】 In case of re-bidding, recalculate priority
            if ($data['demandInfo']['do_auction'] == 2) {
                $data['demandInfo']['priority'] = '';
            }
            // Auction start date and time
            $data['demandInfo']['auction_start_time'] = date('Y-m-d H:i:s');
            // Auction closing date and time
            $data['demandInfo']['auction_deadline_time'] = '';
            // Proposal status
            $data['demandInfo']['demand_status'] = 3; //agency_before status
            // Get the minimum visit date and time, date of contact request
            $visitTimeList = array();

            foreach ($data['visitTime'] as $val) {
                if ($val['is_visit_time_range_flg'] == 0 && strlen($val['visit_time']) > 0) {
                    $visitTimeList[] = $val['visit_time'];
                }
                if ($val['is_visit_time_range_flg'] == 1 && strlen($val['visit_time_from']) > 0) {
                    $visitTimeList[] = $val['visit_time_from'];
                }
            }
            if (!empty($visitTimeList)) {
                // When to use the visit date and time
                $preferredDate = getMinVisitTime($visitTimeList);
                $data['demandInfo']['method'] = 'visit';
            } else {
                // When using the contact date and time desired
                if (isset($data['demandInfo']['is_contact_time_range_flg']) && $data['demandInfo']['is_contact_time_range_flg'] == 0) {
                    $preferredDate = $data['demandInfo']['contact_desired_time'];
                }
                if (isset($data['demandInfo']['is_contact_time_range_flg']) && $data['demandInfo']['is_contact_time_range_flg'] == 1) {
                    $preferredDate = $data['demandInfo']['contact_desired_time_from'];
                }
                $data['demandInfo']['method'] = 'tel';
            }

            $judgeResult = array(
                "result_flg"  => 0,// 0 = No change, 1 = Changed (It is subject to exclusion time in normal case)
                "result_date" => null// start date
            );

            $c = strtotime($data['demandInfo']['auction_start_time']);
            $p = strtotime($preferredDate);
            // If the case creation date and the desired date are reversed, manual selection is made
            if ($p < $c) {
                $auctionFlg = false;
                $auctionNoneFlg = false;
                $hasStartTimeErr = true;
            } else {
                // When the priority is not set yet
                $judgeResult = $this->buildJudeResult($data['demandInfo'], $preferredDate);

                if ($judgeResult['result_flg'] == 1) {
                    // Auction start date and time
                    if (!empty($judgeResult['result_date'])) {
                        $data['demandInfo']['auction_start_time'] = $judgeResult['result_date'];
                    }
                    //TODO : Process for recalculating priority <=> 優先度再計算用処理
                    //When the priority is changed, the determination for each priority is performed again
                    $judgeResult = judgeAuction(
                        $data['demandInfo']['auction_start_time'],
                        $preferredDate,
                        $data['demandInfo']['genre_id'],
                        $data['demandInfo']['address1'],
                        $data['demandInfo']['auction_deadline_time'],
                        $data['demandInfo']['priority']
                    );
                    if ($judgeResult['result_flg'] == 0) {
                        $auctionFlg = true;
                    } else {
                        $auctionFlg = false;
                    }
                }

                // Whether or not there is a member shop subject to auction (0 is false)
                $auctionNoneFlg = $this->auctionService->checkNumberAuctionInfos($data);
            }
            // Outside auction selection time or when there are 0 target franchise stores
            $data['demandInfo'] = $this->updateDemandInfoDataByFlg($data['demandInfo'], $auctionFlg, $auctionNoneFlg);
        }
        return [
                'data' => $data, 'auctionFlg' => $auctionFlg,
                'auctionNoneFlg' => $auctionNoneFlg, 'hasStartTimeErr' => $hasStartTimeErr
            ];
    }
    /**
     * processing data selection system
     * @author thaihv
     * @param  array $data source data
     * @return array       data
     */
    public function processDataWithSelectionSystem($data)
    {
        if ($data['demandInfo']['selection_system'] != getDivValue('selection_type', 'auto_selection')
            || $data['demandInfo']['demand_status'] != getDivValue('demand_status', 'no_selection')
            ) {
            return $data;
        }
        // In case of automatic selection, return to manual selection after selection
        $data['demandInfo']['selection_system'] = getDivValue('selection_type', 'manual_selection');
        // Only for automatic selection process execution
        // In case of automatic consignment, auction is not selected
        if (empty($data['demandInfo']['do_auto_selection'])
            || $data['demandInfo']['do_auto_selection_category'] != 0) {
            return $data;
        }
        $commissionInfos = array();
        $demandId = (array_key_exists('id', $data['demandInfo'])) ? $data['demandInfo']['id'] : null;
        // Move existing process to Component, move to AuctionInfoUtil -> update_auction_infos
        $autoCommissions = $this->auctionService->getAuctionInfoForAutoCommission($demandId, $data);

        $defaultFee = $this->mCategoryRepo->getDefaultFee($data['demandInfo']['category_id']);

        // Extract selected manual destination
        foreach ($data['commissionInfo'] as $key => $val) {
            if (!empty($val['corp_id'])) {
                $commissionInfos[] = $val;
            }
        }

        $isSelect = false;
        if (is_array($autoCommissions) && !empty($autoCommissions)) {
            $commissionInfos = $this->commissionService->buildCommissionData(
                $autoCommissions,
                $defaultFee,
                $commissionInfos
            );
        }
        if (!empty($commissionInfos)) {
            if ($isSelect) {
                $data['demandInfo']['demand_status'] = getDivValue('demand_status', 'agency_before');
            }
            $data['commissionInfo'] = $commissionInfos;
        }

        return $data;
    }

    /**
     * set pre demand
     * @param string $customerTel
     * @param string $siteTel
     * @return array
     */
    public function setPreDemand($customerTel, $siteTel)
    {
        $demandInfo = $this->demandRepo->getFirstDemandByTel($customerTel);
        $mSite = $this->mSiteRepo->getFirstSiteByTel($siteTel);
        if ($demandInfo) {
            $dStatus = $demandInfo->demand_status;
        } else {
            $dStatus = "";
        }

        if ($mSite) {
            $siteId = $mSite->id;
        } else {
            $siteId = "";
        }
        return [
            'customer_tel' => $customerTel,
            'site_id' => $siteId,
            'receptionist' => auth()->id(),
            'demand_status' => $dStatus,
            'commission_limitover_time' => 0
        ];
    }
}
