<?php

namespace App\Services;

use App\Repositories\SelectGenreRepositoryInterface;
use App\Repositories\Eloquent\MSiteRepository;
use App\Repositories\Eloquent\MSiteGenresRepository;

/**
 * DemandService
 *
 * @author cuongnguyenx
 */
class DemandService
{
    /**
     * @var SelectGenreRepositoryInterface
     */
    protected $selectGenreRepo;
    /**
     * @var MSiteRepository
     */
    protected $mSiteRepo;
    /**
     * @var MSiteGenresRepository
     */
    protected $mSiteGenresRepo;

    /**
     * DemandService constructor.
     *
     * @param SelectGenreRepositoryInterface $selectGenreRepo
     * @param MSiteRepository                $mSiteRepo
     * @param MSiteGenresRepository          $mSiteGenresRepo
     */
    public function __construct(
        SelectGenreRepositoryInterface $selectGenreRepo,
        MSiteRepository $mSiteRepo,
        MSiteGenresRepository $mSiteGenresRepo
    ) {
        $this->selectGenreRepo = $selectGenreRepo;
        $this->mSiteRepo = $mSiteRepo;
        $this->mSiteGenresRepo = $mSiteGenresRepo;
    }

    /**
     * Get selection system list
     *
     * @param  integer  $genreId
     * @param  $address1
     * @return array
     */
    public function getSelectionSystemList($genreId, $address1)
    {
        $selectionSystem = getDivValue('selection_type', 'manual_selection');
        $selectionSystem = !is_null($selectionSystem) ? $selectionSystem : 0;
        $result = [];

        if (!empty($address1)) {
            $genrePrefectureData = [];

            if (!empty($genrePrefectureData)) {
                if (!empty($genrePrefectureData['SelectGenrePrefecture']['selection_type'])) {
                    $selectionSystem = $genrePrefectureData['SelectGenrePrefecture']['selection_type'];
                }
            } else {
                $genreData = $this->selectGenreRepo->findByGenreId($genreId);

                if (!empty($genreData)) {
                    $selectionSystem = $genreData['SelectGenre']['select_type'];
                }
            }
        } else {
            $genreData = $this->selectGenreRepo->findByGenreId($genreId);
            $selectionSystem = (!empty($genreData)) ? $genreData->select_type : '';
        }

        $selectionTypeList = getDivList('rits.selection_type', 'auto_commission_corp');
        $manualSelection = getDivValue('selection_type', 'manual_selection');

        if (is_array($selectionTypeList)) {
            foreach ($selectionTypeList as $key => $val) {
                if ($key == $manualSelection || $key == $selectionSystem) {
                    $result[$key] = $val;
                }
            }
        }

        return $result;
    }

    /**
     * Get genre rank by site id
     *
     * @param  integer $siteId
     * @return NULL|[]
     */
    public function getGenreByRank($siteId)
    {
        if (empty($siteId)) {
            return null;
        }

        $site = $this->mSiteRepo->findById($siteId);

        if ($site->cross_site_flg == 1) {
            $siteId = null;
        }

        $rankList = $this->mSiteGenresRepo->getGenreRankBySiteId($siteId);

        return $rankList;
    }
}
