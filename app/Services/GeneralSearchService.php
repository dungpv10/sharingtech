<?php

namespace App\Services;

use App\Repositories\Eloquent\MItemRepository;
use App\Repositories\MAddress1RepositoryInterface;
use App\Repositories\MCategoryRepositoryInterface;
use App\Repositories\MCommissionTypeRepositoryInterface;
use App\Repositories\MGenresRepositoryInterface;
use App\Repositories\MItemRepositoryInterface;
use App\Repositories\MUserRepositoryInterface;
use App\Services\BaseService;
use App\Repositories\MSiteRepositoryInterface;
use App\Models\MUser;
use App\Services\MItemService;
use Illuminate\Support\Facades\Auth;
use App\Repositories\MGeneralSearchRepositoryInterface;
use App\Repositories\GeneralSearchConditionRepositoryInterface;
use App\Repositories\GeneralSearchItemRepositoryInterface;
use App\Repositories\PgDescriptionRepositoryInterface;
use DB;

class GeneralSearchService extends BaseService
{
    const FUNCTION_LIST = [
        null => '--機能名を選択して下さい--',
        0 => '案件管理',
        1 => '取次管理',
        2 => '請求管理',
        3 => '加盟店管理',
    ];

    const FUNCTION_CASE_MANAGEMENT = "0";

    const FUNCTION_AGENCY_MANAGEMENT = "1";

    const FUNCTION_CHARGE_MANAGEMENT = "2";

    const FUNCTION_MEMBER_MANAGEMENT = "3";
    /**
     * @var array
     */
    private $functionTables = array(
        array('demand_infos' => array(
            'id', 'follow_date', 'demand_status', 'order_fail_reason',
            'reservation_demand', 'mail_demand', 'nighttime_takeover', 'low_accuracy', 'remand', 'auction', 'priority',
            'immediately', 'corp_change', 'receive_datetime', 'site_id', 'genre_id',
            'category_id', 'cross_sell_source_site', 'cross_sell_source_genre', 'cross_sell_source_category', 'source_demand_id',
            'receptionist', 'customer_name', 'customer_tel', 'customer_mailaddress',
            'postcode', 'address1', 'address2', 'address3', 'address4', 'building',
            'room', 'tel1', 'tel2', 'contents', 'contact_desired_time', 'selection_system', 'pet_tombstone_demand', 'sms_demand', 'jbr_order_no',
            'jbr_work_contents', 'jbr_category', 'jbr_receipt_price',
            'mail', 'order_date', 'complete_date',
            'order_fail_date', 'jbr_estimate_status', 'jbr_receipt_status', 'development_request',
            'share_notice', 'del_flg', 'riro_kureka',
            'modified_user_id', 'modified', 'created_user_id', 'created', 'order_no_marriage',
            'order_loss', 'same_customer_demand_url', 'upload_estimate_file_name', 'upload_receipt_file_name',
            'cost_customer_question', 'cost_from', 'cost_to',
            'special_measures', 'cost_customer_answer', 'call_back_time', 'acceptance_status',
            'cross_sell_implement', 'cross_sell_call', 'commission_limitover_time', 'sms_reorder',
            'business_trip_amount', 'auction_deadline_time', 'auction_start_time', 'construction_class', 'contact_desired_time_from', 'contact_desired_time_to', 'customer_corp_name', 'nitoryu_flg'
        ),
            'visit_times' => array(
                'visit_time'
            )
        ),
        array('commission_infos' => array(
            'id', 'demand_id', 'corp_id', 'commit_flg', 'commission_type', 'lost_flg', 'appointers',
            'first_commission', 'corp_fee', 'waste_collect_oath', 'attention', 'commission_dial',
            'tel_commission_datetime', 'tel_commission_person', 'commission_fee_rate', 'commission_note_send_datetime',
            'commission_note_sender', 'commission_status', 'commission_order_fail_reason', 'complete_date', 'order_fail_date',
            'estimate_price_tax_exclude', 'construction_price_tax_exclude', 'construction_price_tax_include',
            'deduction_tax_include', 'deduction_tax_exclude', 'confirmd_fee_rate', 'unit_price_calc_exclude', 'introduction_free',
            'report_note', 'del_flg', 'checked_flg', 'irregular_fee_rate', 'irregular_fee', 'irregular_reason', 'falsity', 'follow_date',
            'introduction_not', 'lock_status', 'commission_status_last_updated', 'progress_reported', 'progress_report_datetime',
            'modified_user_id', 'modified', 'created_user_id', 'created',
            'send_mail_fax', 'send_mail_fax_datetime', 'select_commission_unit_price_rank', 'reform_upsell_ic', 'remand_flg',
            're_commission_exclusion_status', 're_commission_exclusion_user_id', 're_commission_exclusion_datetime', 'send_mail_fax_sender',
            'business_trip_amount', 'tel_support', 'visit_support', 'order_support', 'remand_reason', 'remand_correspond_person',
            'visit_desired_time', 'order_respond_datetime', 'select_commission_unit_price', 'send_mail_fax_othersend', 'ac_commission_exclusion_flg',
            'order_fee_unit', 'fee_billing_date'
        ),
            'bill_infos' => array(
                'auction_id'
            ),
            'demand_infos' => array(
                'contact_desired_time'
            ),
            'commission_tel_supports' => array(
                'correspond_status', 'order_fail_reason'
            ),
            'visit_times' => array(
                'visit_time'
            ),
            'commission_visit_supports' => array(
                'correspond_status', 'order_fail_reason'
            ),
            'commission_order_supports' => array(
                'correspond_datetime', 'correspond_status', 'order_fail_reason'
            ),
        ),
        array('bill_infos' => array(
            'id', 'demand_id', 'bill_status', 'irregular_fee_rate', 'irregular_fee', 'deduction_tax_include',
            'deduction_tax_exclude', 'indivisual_billing', 'comfirmed_fee_rate', 'fee_target_price', 'fee_tax_exclude',
            'tax', 'insurance_price', 'total_bill_price', 'fee_billing_date', 'fee_payment_date', 'fee_payment_price',
            'fee_payment_balance', 'report_note', 'commission_id',
            'modified_user_id', 'modified', 'created_user_id', 'created', 'business_trip_amount'
        ),
        ),
        array('m_corps' => array(
            'id', 'corp_name', 'corp_name_kana', 'official_corp_name', 'affiliation_status', 'responsibility', 'postcode',
            'address1', 'address2', 'address3', 'address4', 'building', 'room', 'trade_name1', 'trade_name2', 'commission_dial',
            'tel1', 'tel2', 'mobile_tel', 'fax', 'mailaddress_pc', 'mailaddress_mobile', 'url', 'target_range', 'available_time',
            'support24hour', 'contactable_time', 'free_estimate', 'portalsite', 'reg_send_date', 'reg_collect_date', 'ps_app_send_date',
            'ps_app_collect_date', 'coordination_method', 'prog_send_method',
            'prog_send_mail_address', 'prog_send_fax', 'prog_irregular', 'bill_send_method',
            'bill_send_address', 'bill_irregular', 'special_agreement', 'contract_date', 'order_fail_date', 'commission_ng_date', 'note',
            'document_send_request_date', 'follow_person', 'advertising_status', 'advertising_send_date',
            'progress_check_tel', 'progress_check_person', 'payment_site', 'del_flg', 'rits_person',
            'reg_send_method', 'geocode_lat', 'geocode_long', 'follow_date', 'corp_status', 'order_fail_reason', 'corp_commission_status',
            'listed_media', 'jbr_available_status', 'mailaddress_auction', 'auction_status', 'corp_commission_type',
            'corp_person', 'available_time_from', 'available_time_to', 'contactable_time_from', 'contactable_time_to',
            'contactable_support24hour', 'contactable_time_other', 'available_time_other', 'seikatsu110_id', 'mobile_mail_none', 'mobile_tel_type', 'auction_masking',
            'commission_accept_flg', 'commission_accept_date', 'commission_accept_user_id', 'representative_postcode', 'representative_address1',
            'representative_address2', 'representative_address3', 'refund_bank_name', 'refund_branch_name', 'refund_account_type', 'refund_account',
            'support_language_en', 'support_language_zh', 'support_language_employees', 'auto_call_flag',
        ),
            'affiliation_infos' => array(
                'id', 'corp_id', 'employees', 'max_commission', 'collection_method', 'collection_method_others', 'liability_insurance',
                'reg_follow_date1', 'reg_follow_date2', 'reg_follow_date3', 'waste_collect_oath', 'transfer_name', 'claim_count',
                'claim_history', 'commission_count', 'weekly_commission_count', 'orders_count', 'orders_rate',
                'construction_cost', 'fee', 'bill_price', 'payment_price', 'balance', 'construction_unit_price', 'commission_unit_price',
                'sf_construction_unit_price', 'sf_construction_count', 'reg_info', 'reg_pdf_path', 'attention',
                'corp_id', 'modified_user_id', 'modified', 'created_user_id', 'created',
                'credit_limit', 'listed_kind', 'default_tax', 'capital_stock', 'virtual_account', 'add_month_credit'
            ),
            'm_corp_categories' => array(
                'genre_id', 'category_id', 'order_fee', 'order_fee_unit', 'introduce_fee', 'introduce_fee_unit', 'note',
                'modified_user_id', 'modified', 'created_user_id', 'created', 'corp_commission_type'
            ),
            'm_target_areas' => array(
                'jis_cd', 'address1_cd'
            )
        )
    );
    /**
     * @var array
     */
    private $replaceFunctionTableColumnName = array(
        1 => array(
            'demand_infos.contact_desired_time' => '[電話対応]初回連絡希望日時',
            'commission_tel_supports.correspond_status' => '[電話対応]最新状況',
            'commission_tel_supports.order_fail_reason' => '[電話対応]失注理由',
            'visit_times.visit_time' => '[訪問対応]訪問日時',
            'commission_visit_supports.correspond_status' => '[訪問対応]最新状況',
            'commission_visit_supports.order_fail_reason' => '[訪問対応]失注理由',
            'commission_order_supports.correspond_datetime' => '[受注対応]受注対応日時',
            'commission_order_supports.correspond_status' => '[受注対応]最新状況',
            'commission_order_supports.order_fail_reason' => '[受注対応]失注理由',
            'bill_infos.auction_id' => '入札手数料'
        , 'commission_infos.fee_billing_date' => 'お試し手数料請求日'
        )
    );
    /**
     * @var array
     */
    private $transferReferenceTable = array(
        'demand_infos_demand_status' => array('v' => 'mItemDemandStatusList'),
        'demand_infos_order_fail_reason' => array('v' => 'mItemDemandOrderFailReasonList'),
        'demand_infos_site_id' => array('v' => 'mSiteList'),
        'demand_infos_genre_id' => array('v' => 'mGenreList'),
        'demand_infos_receptionist' => array('v' => 'mUserList'),
        'demand_infos_category_id' => array('v' => 'mCategoryList'),
        'demand_infos_cross_sell_source_site' => array('v' => 'mSiteList'),
        'demand_infos_cross_sell_source_genre' => array('v' => 'mGenreList'),
        'demand_infos_cross_sell_source_category' => array('v' => 'mCategoryList'),
        'demand_infos_jbr_work_contents' => array('v' => 'mItemWcontentsList'),
        'demand_infos_jbr_category' => array('v' => 'mItemJbrCategoryList'),
        'demand_infos_jbr_estimate_status' => array('v' => 'mItemJbrEstimateList'),
        'demand_infos_jbr_receipt_status' => array('v' => 'mItemJbrReceiptList'),
        'demand_corresponds_responders' => array('v' => 'mUserList'),
        'demand_infos_modified_user_id' => array('v' => 'mUserList2'),
        'demand_infos_created_user_id' => array('v' => 'mUserList2'),
        'demand_infos_address1' => array('v' => 'mAddress1List'),
        'demand_infos_pet_tombstone_demand' => array('v' => 'mItemPetTombstoneDemandList'),
        'demand_infos_sms_demand' => array('v' => 'mItemSmsDemandList'),
        'demand_infos_special_measures' => array('v' => 'mItemSpecialMeasuresList'),
        'demand_infos_acceptance_status' => array('v' => 'mItemAcceptanceStatusList'),
        'demand_infos_priority' => array('v' => 'demandInfosPriorityList'),
        'commission_infos_commission_type' => array('v' => 'mCommissionTypeList'),
        'commission_infos_appointers' => array('v' => 'mUserList'),
        'commission_infos_tel_commission_person' => array('v' => 'mUserList'),
        'commission_infos_commission_note_sender' => array('v' => 'mUserList'),
        'commission_infos_commission_status' => array('v' => 'mItemCommissionStatusList'),
        'commission_infos_commission_order_fail_reason' => array('v' => 'mItemCommissionOrderFailReasonList'),
        'commission_infos_modified_user_id' => array('v' => 'mUserList2'),
        'commission_infos_created_user_id' => array('v' => 'mUserList2'),
        'bill_infos_bill_status' => array('v' => 'mItemBillStatusList'),
        'bill_infos_modified_user_id' => array('v' => 'mUserList2'),
        'bill_infos_created_user_id' => array('v' => 'mUserList2'),
        'm_corps_address1' => array('v' => 'mAddress1List'),
        'm_corps_coordination_method' => array('v' => 'mItemCoordinationMethodList'),
        'm_corps_prog_send_method' => array('v' => 'mItemProgSendMethodList'),
        'm_corps_bill_send_method' => array('v' => 'mItemBillSendMethodList'),
        'm_corps_follow_person' => array('v' => 'mUserList'),
        'm_corps_advertising_status' => array('v' => 'mItemAdvertisingStatusList'),
        'm_corps_payment_site' => array('v' => 'mItemPaymentSiteList'),
        'm_corps_rits_person' => array('v' => 'mUserList'),
        'm_corps_corp_status' => array('v' => 'mCorpStatusList'),
        'm_corps_corp_commission_status' => array('v' => 'mCorpCorpCommissionStatusList'),
        'm_corps_affiliation_status' => array('v' => 'mCorpAffiliationStatusList'),
        'm_corps_reg_send_method' => array('v' => 'mItemRegSendMethodList'),
        'm_corps_corp_status' => array('v' => 'mItemCorpStatusList'),
        'm_corps_order_fail_reason' => array('v' => 'mItemCorpOrderFailReasonList'),
        'affiliation_infos_modified_user_id' => array('v' => 'mUserList2'),
        'affiliation_infos_created_user_id' => array('v' => 'mUserList2'),
        'affiliation_infos_liability_insurance' => array('v' => 'mItemLiabilityInsurance'),
        'affiliation_infos_waste_collect_oath' => array('v' => 'mItemWasteCollectOath'),
        'm_corp_categories_genre_id' => array('v' => 'mGenreList'),
        'm_corp_categories_category_id' => array('v' => 'mCategoryList'),
        'm_corp_categories_modified_user_id' => array('v' => 'mUserList2'),
        'm_corp_categories_created_user_id' => array('v' => 'mUserList2'),
        'm_corp_categories_order_fee_unit' => array('v' => 'mCorpCategoriesOrderFeeUnitList'),
        'm_corp_categories_corp_commission_type' => array('v' => 'mCorpCategoriesCorpCommissionTypeList'),
        'm_target_areas_jis_cd' => array('v' => 'mTargetAreasList'),
        'm_corps_auction_status' => array('v' => 'mCorpsAuctionStatusList'),
        'm_corps_corp_commission_type' => array('v' => 'mCorpsCorpCommissionType'),
        'commission_infos_reform_upsell_ic' => array('v' => 'commissionInfosReformUpsellIcList'),
        'commission_infos_re_commission_exclusion_status' => array('v' => 'commissionInfosReCommissionExclusionStatus'),
        'commission_infos_re_commission_exclusion_user_id' => array('v' => 'mUserList2'),
        'commission_infos_send_mail_fax_sender' => array('v' => 'mUserList'),
        'commission_tel_supports_correspond_status' => array('v' => 'commissionTelSupportsCorrespondStatus'),
        'commission_visit_supports_correspond_status' => array('v' => 'commissionVisitSupportsCorrespondStatus'),
        'commission_order_supports_correspond_status' => array('v' => 'commissionOrderSupportsCorrespondStatus'),
        'commission_tel_supports_order_fail_reason' => array('v' => 'commissionTelSupportsOrderFailReason'),
        'commission_visit_supports_order_fail_reason' => array('v' => 'commissionVisitSupportsOrderFailReason'),
        'commission_order_supports_order_fail_reason' => array('v' => 'commissionOrderSupportsOrderFailReason'),
        'commission_infos_irregular_reason' => array('v' => 'commissionInfosIrregularReasonList'),
        'm_corps_jbr_available_status' => array('v' => 'mCorpsJbrAvailableStatus'),
        'demand_infos_construction_class' => array('v' => 'demandInfosConstructionClass'),
        'm_corps_mobile_tel_type' => array('v' => 'mCorpsMobileTelType'),
        'm_corps_mobile_tel_type' => array('v' => 'mCorpsMobileTelType'),
        'm_corps_auction_masking' => array('v' => 'mCorpsAuctionMasking'),
        'm_corps_commission_accept_flg' => array('v' => 'mCorpsCommissionAcceptFlg'),
        'm_corps_commission_accept_user_id' => array('v' => 'mUserList2'),
        'm_corps_support_language_en' => array('v' => 'mCorpsSupportLanguageEn'),
        'm_corps_support_language_zh' => array('v' => 'mCorpsSupportLanguageZh'),
        'm_corps_representative_address1' => array('v' => 'mAddress1List'),
        'affiliation_infos_listed_kind' => array('v' => 'affiliationInfosListedKind'),
        'affiliation_infos_default_tax' => array('v' => 'affiliationInfosDefaultTax'),
        'commission_infos_order_fee_unit' => array('v' => 'mCorpCategoriesOrderFeeUnitList'),
        'demand_infos_selection_system' => array('v' => 'demandInfosSelectionSystem'),
        'm_corps_auto_call_flag' => array('v' => 'mCorpsAutoCallFlag'),
    );
    /**
     * @var array
     */
    private $replaceTableColumnName = array(
        1 => array(
            'demand_infos.contact_desired_time' => '[電話対応]初回連絡希望日時',
            'commission_tel_supports.correspond_status' => '[電話対応]最新状況',
            'commission_tel_supports.order_fail_reason' => '[電話対応]失注理由',
            'visit_times.visit_time' => '[訪問対応]訪問日時',
            'commission_visit_supports.correspond_status' => '[訪問対応]最新状況',
            'commission_visit_supports.order_fail_reason' => '[訪問対応]失注理由',
            'commission_order_supports.correspond_datetime' => '[受注対応]受注対応日時',
            'commission_order_supports.correspond_status' => '[受注対応]最新状況',
            'commission_order_supports.order_fail_reason' => '[受注対応]失注理由',
            'bill_infos.auction_id' => '入札手数料'
        , 'commission_infos.fee_billing_date' => 'お試し手数料請求日'
        )
    );
    /**
     * @var array
     */
    private $functionTablesJoinOrder = array(
        'demand_infos', 'visit_times', 'commission_infos', 'commission_tel_supports', 'commission_visit_supports', 'commission_order_supports', 'bill_infos', 'm_corps', 'affiliation_infos', 'm_corp_categories', 'm_target_areas',
    );
    /**
     * @var array
     */
    private $functionTablesJoinRule = array(
        'demand_infos' => array('cond' => null),
        'visit_times' => array(
            'cond' => 'demand_infos.id = visit_times.demand_id'
        ),
        'commission_infos' => array(
            'cond' => 'demand_infos.id = commission_infos.demand_id'
        ),
        'bill_infos' => array(
            'cond' => 'demand_infos.id = bill_infos.demand_id and commission_infos.id = bill_infos.commission_id and bill_infos.auction_id IS NULL'
        ),
        'm_corps' => array(
            'cond' => 'commission_infos.corp_id = m_corps.id'
        ),
        'affiliation_infos' => array(
            'cond' => 'm_corps.id = affiliation_infos.corp_id'
        ),
        'm_corp_categories' => array(
            'cond' => 'm_corps.id = m_corp_categories.corp_id'
        ),
        'm_target_areas' => array(
            'cond' => 'm_corp_categories.id = m_target_areas.corp_category_id'
        ),
        'commission_tel_supports' => array(
            'cond' => "commission_infos.id = commission_tel_supports.commission_id
				AND commission_tel_supports.id =
				(select id from commission_tel_supports ct where ct.commission_id = commission_infos.id order by ct.created desc limit 1)"
        ),
        'commission_visit_supports' => array(
            'cond' => "commission_infos.id = commission_visit_supports.commission_id
				AND commission_visit_supports.id =
				(select id from commission_visit_supports cv where cv.commission_id = commission_infos.id order by cv.created desc limit 1)"
        ),
        'commission_order_supports' => array(
            'cond' => "commission_infos.id = commission_order_supports.commission_id
				 AND commission_order_supports.id =
				(select id from commission_order_supports co where co.commission_id = commission_infos.id order by co.created desc limit 1)"
        ),
    );
    /**
     * @var MSiteRepositoryInterface
     */
    private $mSiteRepo;
    /**
     * @var MUser
     */
    private $mUser;
    /**
     * @var MGeneralSearchRepositoryInterface
     */
    public $mGeneralSearchRepo;
    /**
     * @var array
     */
    private $defaultSelectedItems = array('demand_infos.id');
    /**
     * @var GeneralSearchConditionRepositoryInterface
     */
    public $generalSearchConditionRepo;
    /**
     * @var GeneralSearchItemRepositoryInterface
     */
    public $generalSearchItemRepo;
    /**
     * @var MAddress1RepositoryInterface
     */
    private $mAddressRepo;
    /**
     * @var MGenresRepositoryInterface
     */
    private $mGenreRepo;
    /**
     * @var MCommissionTypeRepositoryInterface
     */
    private $mCommissionTypeRepo;
    /**
     * @var MCategoryRepositoryInterface
     */
    private $mCategoryRepo;
    /**
     * @var MItemRepositoryInterface
     */
    private $mItemRepo;
    /**
     * @var PgDescriptionRepositoryInterface
     */
    private $pgDescriptionRepo;
    /**
     * @var MItemService
     */
    private $mItemService;
    /**
     * @var MUserRepositoryInterface
     */
    private $mUserRepo;

    /**
     * variable communicate field table
     *
     * @var array
     */
    private $mSiteList;

    /**
     * @var array
     */
    private $mCommissionTypeList;
    /**
     * @var array
     */
    private $mGenreList;
    /**
     * @var array
     */
    private $mCategoryList;
    /**
     * @var array
     */
    private $mItemWcontentsList;
    /**
     * @var array
     */
    private $mItemPetTombstoneDemandList;
    /**
     * @var array
     */
    private $mItemSmsDemandList;
    /**
     * @var array
     */
    private $mItemJbrCategoryList;
    /**
     * @var array
     */
    private $mItemJbrEstimateList;
    /**
     * @var array
     */
    private $mItemJbrReceiptList;
    /**
     * @var array
     */
    private $mUserList;
    /**
     * @var array
     */
    private $mUserList2;
    /**
     * @var array
     */
    private $mAddress1List;
    /**
     * @var array
     */
    private $mItemBillStatusList;
    /**
     * @var array
     */
    private $mItemBillSendMethodList;
    /**
     * @var array
     */
    private $mItemCoordinationMethodList;
    /**
     * @var array
     */
    private $mItemProgSendMethodList;
    /**
     * @var array
     */
    private $mItemAdvertisingStatusList;
    /**
     * @var array
     */
    private $mItemPaymentSiteList;
    /**
     * @var array
     */
    private $mItemDemandStatusList;
    /**
     * @var array
     */
    private $mItemDemandOrderFailReasonList;
    /**
     * @var array
     */
    private $mItemCommissionStatusList;
    /**
     * @var array
     */
    private $mItemCommissionOrderFailReasonList;
    /**
     * @var array
     */
    private $mCorpStatusList;
    /**
     * @var array
     */
    private $mCorpAffiliationStatusList;
    /**
     * @var array
     */
    private $mCorpCorpCommissionStatusList;
    /**
     * @var array
     */
    private $mTargetAreasList;
    /**
     * @var array
     */
    private $mCorpCategoriesOrderFeeUnitList;
    /**
     * @var array
     */
    private $mCorpCategoriesCorpCommissionTypeList;
    /**
     * @var array
     */
    private $mItemRegSendMethodList;
    /**
     * @var array
     */
    private $mItemCorpStatusList;
    /**
     * @var array
     */
    private $mItemCorpOrderFailReasonList;
    /**
     * @var array
     */
    private $mItemSpecialMeasuresList;
    /**
     * @var array
     */
    private $demandInfosPriorityList;
    /**
     * @var array
     */
    private $mCorpsAuctionStatusList;
    /**
     * @var array
     */
    private $mCorpsCorpCommissionType;
    /**
     * @var array
     */
    private $commissionInfosReformUpsellIcList;
    /**
     * @var array
     */
    private $commissionInfosReCommissionExclusionStatus;
    /**
     * @var array
     */
    private $commissionTelSupportsCorrespondStatus;
    /**
     * @var array
     */
    private $commissionVisitSupportsCorrespondStatus;
    /**
     * @var array
     */
    private $commissionOrderSupportsCorrespondStatus;
    /**
     * @var array
     */
    private $commissionTelSupportsOrderFailReason;
    /**
     * @var array
     */
    private $commissionVisitSupportsOrderFailReason;
    /**
     * @var array
     */
    private $commissionOrderSupportsOrderFailReason;
    /**
     * @var array
     */
    private $commissionInfosIrregularReasonList;
    /**
     * @var array
     */
    private $mCorpsJbrAvailableStatus;
    /**
     * @var array
     */
    private $demandInfosSelectionSystem;
    /**
     * @var array
     */
    private $mCorpsAutoCallFlag;
    //end variable communicate field table
    // get csv title field
    /**
     * @var array
     */
    public $csvFormat = array(
        'default' => array());

    // end get csv title field

    /**
     * GeneralSearchService constructor.
     *
     * @param MSiteRepositoryInterface                  $mSiteRepo
     * @param MUser                                     $mUser
     * @param MGeneralSearchRepositoryInterface         $mGeneralSearchRepo
     * @param GeneralSearchItemRepositoryInterface      $generalSearchItemRepo
     * @param GeneralSearchConditionRepositoryInterface $generalSearchConditionRepo
     * @param MAddress1RepositoryInterface              $mAddressRepo
     * @param MGenresRepositoryInterface                $mGenreRepo
     * @param MCommissionTypeRepositoryInterface        $mCommissionTypeRepo
     * @param MCategoryRepositoryInterface              $mCategoryRepo
     * @param MItemRepositoryInterface                  $mItemRepo
     * @param MItemService                              $mItemService
     * @param MUserRepositoryInterface                  $mUserRepo
     */
    public function __construct(
        MSiteRepositoryInterface $mSiteRepo,
        MUser $mUser,
        MGeneralSearchRepositoryInterface $mGeneralSearchRepo,
        GeneralSearchItemRepositoryInterface $generalSearchItemRepo,
        GeneralSearchConditionRepositoryInterface $generalSearchConditionRepo,
        MAddress1RepositoryInterface $mAddressRepo,
        MGenresRepositoryInterface $mGenreRepo,
        MCommissionTypeRepositoryInterface $mCommissionTypeRepo,
        MCategoryRepositoryInterface $mCategoryRepo,
        MItemRepositoryInterface $mItemRepo,
        MItemService $mItemService,
        MUserRepositoryInterface $mUserRepo,
        PgDescriptionRepositoryInterface $pgDescriptionRepo
    ) {
        $this->mSiteRepo = $mSiteRepo;
        $this->mUser = $mUser;
        $this->mGeneralSearchRepo = $mGeneralSearchRepo;
        $this->generalSearchConditionRepo = $generalSearchConditionRepo;
        $this->generalSearchItemRepo = $generalSearchItemRepo;
        $this->mAddressRepo = $mAddressRepo;
        $this->mGenreRepo = $mGenreRepo;
        $this->mCommissionTypeRepo = $mCommissionTypeRepo;
        $this->mCategoryRepo = $mCategoryRepo;
        $this->mItemRepo = $mItemRepo;
        $this->mItemService = $mItemService;
        $this->mUserRepo = $mUserRepo;
        $this->pgDescriptionRepo = $pgDescriptionRepo;
        //get value for variable follow field
        $this->mSiteList = $this->mSiteRepo->getList();
        $this->mAddress1List = $this->mAddressRepo->getList();
        $this->mGenreList = $this->mGenreRepo->getList();
        $this->mCommissionTypeList = $this->mCommissionTypeRepo->getListCommissionTypeName();
        $this->mCategoryList = $this->mCategoryRepo->getList();
        $this->mItemDemandStatusList = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::PROPOSAL_STATUS));
        $this->mItemDemandOrderFailReasonList = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::REASON_FOR_LOST_NOTE));
        $this->mItemCommissionStatusList = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::COMMISSION_STATUS));
        $this->mItemCommissionOrderFailReasonList = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::REASON_FOR_LOSING_CONSENT));
        $this->mItemPetTombstoneDemandList = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::PET_TOMBSTONE_DEMAND));
        $this->mItemSmsDemandList = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::SMS_DEMAND));
        $this->mItemWcontentsList = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::JBR_WORK_CONTENTS));
        $this->mItemJbrCategoryList = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::JBR_CATEGORY));
        $this->mItemJbrEstimateList = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::JBR_ESTIMATE_STATUS));
        $this->mItemJbrReceiptList = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::JBR_RECEIPT_STATUS));
        $this->mUserList = $this->mUserRepo->dropDownUser()->toarray();
        $this->mUserList2 = $this->mUserRepo->dropDownUserList();
        $this->mItemBillStatusList = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::BILLING_STATUS));
        $this->mItemCoordinationMethodList = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::COORDINATION_METHOD));
        $this->mItemProgSendMethodList = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::PROG_SEND_METHOD));
        $this->mItemBillSendMethodList = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::BILL_SEND_METHOD));
        $this->mItemAdvertisingStatusList = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::ADVERTISEMENT_TYPE_SITE_SITUATION));
        $this->mItemPaymentSiteList = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::PAYMENT_SITE));
        $this->mCorpStatusList = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::CORP_STATUS));
        $this->mCorpAffiliationStatusList = array('0' => '未加盟', '1' => '加盟', '-1' => '解約');
        $this->mCorpCorpCommissionStatusList = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::CONTRACT_STATUS));
        $this->mTargetAreasList = getDivList('datacustom.prefecture_div');
        $this->mItemLiabilityInsurance = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::LIABILITY_INSURANCE));
        $this->mItemWasteCollectOath = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::WASTE_COLLECT_OATH));
        $this->mCorpCategoriesOrderFeeUnitList = array('0' => '円', '1' => '％');
        $this->mItemRegSendMethodList = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::REG_SEND_METHOD));
        $this->mItemCorpStatusList = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::CORP_STATUS));
        $this->mItemCorpOrderFailReasonList = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::LOSS_OF_DEVELOPMENT));
        $this->mItemSpecialMeasuresList = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::PROJECT_SPECIAL_MEASURES));
        $this->mItemAcceptanceStatusList = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::ACCEPTANCE_STATUS));
        $this->demandInfosPriorityList = array('0' => '-', '1' => '大至急', '2' => '至急', '3' => '通常');
        $this->mCorpsAuctionStatusList = array('1' => '通常選定＋入札式選定', '2' => '通常選定のみ', '3' => '入札式選定のみ');
        $this->mCorpsCorpCommissionType = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::CORPORATE_BROKERAGE_FORM));
        $this->commissionInfosReformUpsellIcList = array('1' => '申請', '2' => '認証', '3' => '非認証');
        $this->commissionInfosReCommissionExclusionStatus = array('0' => '', '1' => '成功', '2' => '失敗');
        $this->commissionTelSupportsCorrespondStatus = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::TELEPHONE_SUPPORT_STATUS));
        $this->commissionVisitSupportsCorrespondStatus = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::VISIT_SUPPORT_STATUS));
        $this->commissionOrderSupportsCorrespondStatus = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::ORDER_SUPPORT_STATUS));
        $commissionTelSupports = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::COMMISSION_TEL_SUPPORTS_ORDER_FAIL_REASON));
        $commissionVisitSupports = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::COMMISSION_VISIT_SUPPORTS_ORDER_FAIL_REASON));
        $commissionOrderSupports = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::COMMISSION_ORDER_SUPPORTS_ORDER_FAIL_REASON));
        $this->commissionTelSupportsOrderFailReason = array_merge(array(0 => ''), $commissionTelSupports);
        $this->commissionVisitSupportsOrderFailReason = array_merge(array(0 => ''), $commissionVisitSupports);
        $this->commissionOrderSupportsOrderFailReason = array_merge(array(0 => ''), $commissionOrderSupports);
        $this->commissionInfosIrregularReasonList = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::IRREGULAR_REASON));
        $this->mCorpsJbrAvailableStatus = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::JBR_STATUS));
        $this->demandInfosConstructionClass = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::BUILDING_TYPE));
        $mobileTelType = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::LIST_MOBILE_PHONE_TYPES));
        $this->mCorpsMobileTelType = array_merge(array('0' => 'なし'), $mobileTelType);
        $this->mCorpsAuctionMasking = getDivList('datacustom.auction_masking', 'demandlist');
        $this->mCorpsCommissionAcceptFlg = array(0 => '未契約', 1 => '契約完了', 2 => '契約未更新', 3 => '未更新STOP');
        $this->mCorpsSupportLanguageEn = array(0 => '未対応', 1 => '対応');
        $this->mCorpsSupportLanguageZh = array(0 => '未対応', 1 => '対応');
        $this->affiliationInfosListedKind = array('listed' => '上場', 'unlisted' => '非上場');
        $this->affiliationInfosDefaultTax = array('NULL' => '', '0' => '滞納なし', '1' => '滞納あり');
        $corpCommissionType = getDivList('datacustom.corp_commission_type');
        $this->mCorpCategoriesCorpCommissionTypeList = array_merge(array('0' => ''), $corpCommissionType);
        $this->demandInfosSelectionSystem = getDivList('datacustom.selection_type', 'demandlist');
        $this->mCorpsAutoCallFlag = $this->mItemService->prepareDataList($this->mItemRepo->getListByCategoryItem(MItemRepository::AUTO_CALL_CLASSIFICATION));
    }

    /**
     * @return array
     */
    public function dropDownFunctionId()
    {
        return self::FUNCTION_LIST;
    }

    /**
     * @return mixed
     */
    public function getMSiteList()
    {
        return $this->mSiteRepo->getList();
    }

    /**
     * @return array
     */
    public function getMUserlist()
    {
        return $this->mUser->dropDownUser();
    }

    /**
     * @return boolean
     */
    public function isEnabledDisplaySaveAndDel()
    {
        if (Auth::user()->auth == 'affiliation') {
            return false;
        }
        return true;
    }

    /**
     * @return boolean
     */
    public function isEnabledDisplayBillInfo()
    {
        if (Auth::user()->auth == 'system' || Auth::user()->auth == 'accounting'
            || Auth::user()->auth == 'accounting_admin' || Auth::user()->auth == 'admin'
            || Auth::user()->auth == 'popular'
        ) {
            return true;
        }
        return false;
    }

    /**
     * @param $functionId
     * @return string
     */
    public function getFunctionColumnList($functionId)
    {
        $result = $this->findFunctionTableColumn($functionId);
        $lists = array();
        foreach ($result as $row) {
            $key = $row->table_name . "." . $row->column_name;
            if (isset($this->replaceFunctionTableColumnName[$functionId][$key])) {
                $row->column_comment = $this->replaceFunctionTableColumnName[$functionId][$key];
            }
            if ($functionId == 1) {
                if ($row->table_name == 'visit_times' && $row->column_name == 'visit_time') {
                    $key = $key . '.' . $functionId;
                }
                if ($row->table_name == 'demand_infos' && $row->column_name == 'contact_desired_time') {
                    $key = $key . '.' . $functionId;
                }
            }
            $lists[] = "[\"" . $key . "\",\"" . $row->column_comment . "\"]";
        }
        return implode(',', $lists);
    }

    /**
     * @param $functionName
     * @return array
     */
    public function findFunctionTableColumn($functionName)
    {
        $retColumns = array();
        foreach ($this->functionTables[$functionName] as $key => $value) {
            $retColumns = array_merge($retColumns, $this->findTableColumn($key, $value));
        }
        return $retColumns;
    }

    /**
     * @param $tableName
     * @param $columns
     * @return array
     */
    private function findTableColumn($tableName, $columns)
    {
        $result = array();
        foreach ($this->pgDescriptionRepo->getColumnAlias($tableName, $columns) as $elem) {
            $object = (object) $elem;
            array_push($result, $object);
        }
        return $result;
    }

    /**
     * @param $mGeneralId
     * @return string
     */
    public function searchGeneralSearch($mGeneralId)
    {
        $results = $this->mGeneralSearchRepo->findGeneralSearch($mGeneralId);
        $results = array_shift($results);
        if (empty($results)) {
            return '';
        }
        $selectedItem = "";
        foreach ($results['gs_item'] as $gsItem) {
            if (strlen($selectedItem) > 0) {
                $selectedItem .= ",";
            }
            if ($gsItem['function_id'] == 1) {
                if ($gsItem['table_name'] == 'visit_times' && $gsItem['column_name'] == 'visit_time') {
                    $selectedItem .= '"' . $gsItem['table_name'] . "." . $gsItem['column_name'] . '.1"';
                } elseif ($gsItem['table_name'] == 'demand_infos' && $gsItem['column_name'] == 'contact_desired_time') {
                    $selectedItem .= '"' . $gsItem['table_name'] . "." . $gsItem['column_name'] . '.1"';
                } else {
                    $selectedItem .= '"' . $gsItem['table_name'] . "." . $gsItem['column_name'] . '"';
                }
            } else {
                $selectedItem .= '"' . $gsItem['table_name'] . "." . $gsItem['column_name'] . '"';
            }
        }
        return $selectedItem;
    }

    /**
     * @return string
     */
    public function getDefaultSelectedItem()
    {
        return '"' . implode('","', $this->defaultSelectedItems) . '"';
    }

    /**
     * @param $mGeneralId
     * @return array|string
     */
    public function getDataGeneralSearch($mGeneralId)
    {
        $dataResults = array();
        $dataMGeneralSearchCo = array();
        $results = $this->mGeneralSearchRepo->findGeneralSearch($mGeneralId);
        $results = array_shift($results);
        if (empty($results)) {
            return '';
        }
        $dataResults += array('MGeneralSearch' => [
            'id' => $results['id'],
            'definition_name' => $results['definition_name'],
            'auth_popular' => $results['auth_popular'],
            'auth_admin' => $results['auth_admin'],
            'auth_accounting' => $results['auth_accounting'],
            'modified_user_id' => $results['modified_user_id'],
            'modified' => $results['modified'],
            'created_user_id' => $results['created_user_id'],
            'created' => $results['created'],
            'auth_accounting_admin' => $results['auth_accounting_admin'],
        ]);
        $this->setMGeneralSearchCo($results, $dataMGeneralSearchCo);
        $dataResults += array('GeneralSearchCondition' => $dataMGeneralSearchCo);
        return $dataResults;
    }

    /**
     * @param $results
     * @param $dataMGeneralSearchCo
     * @return mixed
     */
    public function setMGeneralSearchCo($results, &$dataMGeneralSearchCo)
    {
        foreach ($results['gs_condition'] as $gsCondition) {
            switch ($gsCondition['condition_expression']) {
                case 0:
                case 1:
                    $dataMGeneralSearchCo[$gsCondition['condition_expression']][$gsCondition['table_name'] . "-" . $gsCondition['column_name']] = $gsCondition['condition_value'];
                    break;
                case 2:
                case 3:
                case 4:
                    $dataMGeneralSearchCo[$gsCondition['condition_expression']][$gsCondition['table_name'] . "-" . $gsCondition['column_name']] = explode('^', $gsCondition['condition_value']);
                    break;
                case 9:
                    if ($gsCondition['table_name'] . "-" . $gsCondition['column_name'] == 'm_target_areas-jis_cd') {
                        $dataMGeneralSearchCo[$gsCondition['condition_expression']][$gsCondition['table_name'] . "-" . $gsCondition['column_name']] = explode('^', $gsCondition['condition_value']);
                    } else {
                        $dataMGeneralSearchCo[$gsCondition['condition_expression']][$gsCondition['table_name'] . "-" . $gsCondition['column_name']] = $gsCondition['condition_value'];
                    }
                    break;
            }
        }
    }
    /**
     * @param $params
     * @return float
     */
    public function saveGeneralSearchAjax($params)
    {
        $data = $this->validateDataGeneralSearch($params);
        $id = "";
        if (isset($data['MGeneralSearch']['id'])) {
            $id = $data['MGeneralSearch']['id'];
            $this->generalSearchItemRepo->deleteById($id);
            $this->generalSearchConditionRepo->deleteById($id);
        }
        if (!empty($data['MGeneralSearch'])) {
            $this->mGeneralSearchRepo->insertGeneralSearch($data['MGeneralSearch']);
            $id = $this->mGeneralSearchRepo->getLastInsertID();
        }
        if (!empty($data['GeneralSearchItem'])) {
            $dataGeneralSearchItem = $this->addNewGenreId($data['GeneralSearchItem'], $id);
            $this->generalSearchItemRepo->insertGeneralSearch($dataGeneralSearchItem);
        }
        if (!empty($data['GeneralSearchCondition'])) {
            $dataGeneralSearchCondition = $this->addNewGenreId($data['GeneralSearchCondition'], $id);
            $this->generalSearchConditionRepo->insertGeneralSearch($dataGeneralSearchCondition);
        }
        if (strlen($id) === 0) {
            $id = $this->mGeneralSearchRepo->getLastInsertID();
        }
        return $id;
    }

    /**
     * @param $params
     * @return string
     */
    public function saveGeneralSearch($params)
    {
        $data = $this->validateDataGeneralSearch($params);
        $id = "";
        if (isset($data['MGeneralSearch']['id'])) {
            $id = $data['MGeneralSearch']['id'];
            $this->generalSearchItemRepo->deleteById($id);
            $this->generalSearchConditionRepo->deleteById($id);
        }
        if (!empty($data['MGeneralSearch'])) {
            $this->mGeneralSearchRepo->updateGeneralSearch($data['MGeneralSearch']);
        }
        if (!empty($data['GeneralSearchItem'])) {
            $this->generalSearchItemRepo->insertGeneralSearch($data['GeneralSearchItem']);
        }
        if (!empty($data['GeneralSearchCondition'])) {
            $this->generalSearchConditionRepo->insertGeneralSearch($data['GeneralSearchCondition']);
        }
        if (strlen($id) === 0) {
            $id = $this->mGeneralSearchRepo->getLastInsertID();
        }
        return $id;
    }

    /**
     * @param $params
     * @param $id
     * @return mixed
     */
    private function addNewGenreId($params, $id)
    {
        for ($i = 0; $i < count($params); $i++) {
            $params[$i]['general_search_id'] = $id;
        }
        return $params;
    }

    /**
     * @param $params
     * @return array
     * @throws \Exception
     */
    private function validateDataGeneralSearch($params)
    {
        $data = array();
        $data['MGeneralSearch'] = $this->validateDataMGeneralSearch($params);
        $data['GeneralSearchItem'] = $this->validateDataGeneralSearchItem($params);
        $data['GeneralSearchCondition'] = $this->validateDataGeneralSearchCondition($params);
        return $data;
    }

    /**
     * @param $params
     * @return array
     */
    private function validateDataMGeneralSearch($params)
    {
        $data = array();
        if (strlen($params['MGeneralSearch']['id']) > 0) {
            $data['id'] = $params['MGeneralSearch']['id'];
        }
        $data['definition_name'] = (isset($params['MGeneralSearch']['definition_name'])) ? $params['MGeneralSearch']['definition_name'] : "";
        $data['auth_popular'] = (isset($params['MGeneralSearch']['auth_popular'])) ? 1 : 0;
        $data['auth_admin'] = (isset($params['MGeneralSearch']['auth_admin'])) ? 1 : 0;
        $data['auth_accounting_admin'] = (isset($params['MGeneralSearch']['auth_accounting_admin'])) ? 1 : 0;
        $data['auth_accounting'] = (isset($params['MGeneralSearch']['auth_accounting'])) ? 1 : 0;
        return $data;
    }

    /**
     * @param $params
     * @return array
     * @throws \Exception
     */
    private function validateDataGeneralSearchItem($params)
    {
        $datas = array();

        if (!isset($params['GeneralSearchItem']['item'])) {
            throw new \Exception(trans('general_search.general_search_item_param_error'));
        }

        for ($i = 0; $i < count($params['GeneralSearchItem']['item']); $i++) {
            $elems = explode('.', $params['GeneralSearchItem']['item'][$i]);
            if (strlen($params['MGeneralSearch']['id']) > 0) {
                $data['general_search_id'] = $params['MGeneralSearch']['id'];
            }
            if (isset($elems[2])) {
                $data['function_id'] = $elems[2];
            } else {
                $data['function_id'] = $this->convertTableNameToFunctionId($elems[0]);
            }
            $data['table_name'] = $elems[0];
            $data['column_name'] = $elems[1];
            $data['output_order'] = $i;

            $datas[] = $data;
        }

        return $datas;
    }

    /**
     * @param $tableName
     * @return integer|null|string
     */
    private function convertTableNameToFunctionId($tableName)
    {

        foreach ($this->functionTables as $k => $v) {
            if (array_key_exists($tableName, $v)) {
                return $k;
            }
        }

        return null;
    }

    /**
     * @param $params
     * @return array
     */
    private function validateDataGeneralSearchCondition($params)
    {

        $datas = array();
        foreach ($params['GeneralSearchCondition'] as $conditionExpression => $val) {
            foreach ($val as $k => $v) {
                $e = $this->conditionExpression($conditionExpression, $v, $k);
                if (strlen($e) > 0) {
                    list($tableName, $columnName) = explode('-', $k);
                    if (strlen($params['MGeneralSearch']['id']) > 0) {
                        $data['general_search_id'] = $params['MGeneralSearch']['id'];
                    }
                    $data['table_name'] = $tableName;
                    $data['column_name'] = $columnName;
                    $data['condition_expression'] = $conditionExpression;
                    $data['condition_value'] = $e;
                    $data['condition_type'] = 0;

                    $datas[] = $data;
                }
            }
        }
        return $datas;
    }

    /**
     * @param $conditionExpression
     * @param $value
     * $param $key
     * @return array
     */
    public function conditionExpression($conditionExpression, $value, $key)
    {
        $elm = $value;
        switch ($conditionExpression) {
            case 2:
                $elm = $this->convertValue($value);
                break;
            case 3:
            case 4:
                $elm = (is_array($value)) ? implode('^', $value) : "";
                break;
            case 9:
                if ($key == 'm_target_areas-jis_cd') {
                    $elm = (is_array($value)) ? implode('^', $value) : "";
                }
                break;
        }
        return $elm;
    }
    /**
     * @param $value
     * @return float
     */
    public function convertValue($value)
    {
        return ($value[0] !== "" || $value[1] !== "") ? implode('^', $value) : "";
    }
    /**
     * @param $generalId
     * @return array
     */
    public function getCsvPreview($generalId)
    {
        $conditions = $this->getCsvPreviewCondition($generalId);
        $tableHeaders = $this->getCsvPreviewHeader($generalId);
        $tableData = $this->getCsvPreviewData($generalId);
        return array('conditions' => $conditions, 'headers' => $tableHeaders, 'datas' => $tableData);
    }

    /**
     * @param $generalId
     * @return array
     */
    private function getCsvPreviewCondition($generalId)
    {
        $result = $this->generalSearchConditionRepo->findGeneralSearchCondition($generalId);
        $conditions = array();
        foreach ($result as $elem) {
            if ($elem['column_name'] == 'free_text') {
                $elem['table_name'] = 'm_corps';
                $elem['column_name'] = 'note';
            }
            $comments = $this->findTableColumn($elem['table_name'], array($elem['column_name']));
            $title = $comments[0]->column_comment;
            $value = $elem['condition_value'];
            $key = $elem['table_name'] . "_" . $elem['column_name'];
            if (array_key_exists($key, $this->transferReferenceTable)) {
                $values = explode('^', $value);
                $value = "";
                foreach ($values as $v) {
                    if (is_array($this->{$this->transferReferenceTable[$key]['v']}) && array_key_exists($v, $this->{$this->transferReferenceTable[$key]['v']})) {
                        if (strlen($value) > 0) {
                            $value .= "^";
                        }
                        $value .= $this->{$this->transferReferenceTable[$key]['v']}[$v];
                    }
                }
            }
            if ($value != "^") {
                $conditions[] = array('title' => $title, 'value' => $value);
            }
        }
        return $conditions;
    }

    /**
     * @param $generalId
     * @return array
     */
    private function getCsvPreviewHeader($generalId)
    {
        $result = $this->generalSearchItemRepo->findGeneralSearchCondition($generalId);
        $headers = array();
        foreach ($result as $elem) {
            $comments = $this->findTableColumn($elem['table_name'], array($elem['column_name']));
            $key = $elem['table_name'] . '.' . $elem['column_name'];
            if (isset($this->replaceTableColumnName[$elem['function_id']][$key])) {
                $comments[0]->column_comment = $this->replaceTableColumnName[$elem['function_id']][$key];
            }
            $headers[] = array($comments[0]->column_comment);
        }
        return $headers;
    }

    /**
     * @param $generalId
     * @return array
     */
    private function getCsvPreviewData($generalId)
    {
        return $this->findGeneralSearchToCsv($generalId, 100);
    }

    /**
     * @param $generalId
     * @param $limit
     * @return array
     */
    public function findGeneralSearchToCsv($generalId, $limit = 0)
    {
        $this->setCsvFormat($generalId);
        $params = '';
        $sql = $this->buildQuery($generalId, $params, $limit);
        $result = $this->mGeneralSearchRepo->runQueryText($sql);
        $csvDatas = array();
        foreach ($result as $rrow) {
            $csvDatas[] = $this->getCsvDatasRow($rrow, $limit);
        }
        return $csvDatas;
    }

    /**
     * @param $rrow
     * @param $limit
     * @return array
     */
    public function getCsvDatasRow($rrow, $limit)
    {
        $csvDatasRow = array();
        $user = Auth::user()->auth;
        foreach ($rrow as $key => $value) {
            if (array_key_exists($key, $this->transferReferenceTable) && array_key_exists($value, $this->{$this->transferReferenceTable[$key]['v']})) {
                $value = $this->{$this->transferReferenceTable[$key]['v']}[$value];
            }
            if ($this->isMaskingAll($limit, $value, $user)) {
                $value = maskingAll($value);
            }
            $csvDatasRow[] = $value;
        }
        return $csvDatasRow;
    }

    /**
     * @param $limit
     * @param $key
     * @param $user
     * @return boolean
     */
    public function isMaskingAll($limit, $key, $user)
    {
        if ($limit == 0) {
            if ($this->isAccount($user) && ($key == 'demand_infos_address3' || $key == 'demand_infos_customer_tel' ||
                    $key == 'demand_infos_customer_name' || $key == 'demand_infos_tel1' ||
                    $key == 'demand_infos_tel2')) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $user
     * @return boolean
     */
    public function isAccount($user)
    {
        if ($user != 'system' && $user != 'admin' && $user != 'accounting_admin') {
            return true;
        }
        return false;
    }

    /**
     * @param $generalId
     */
    private function setCsvFormat($generalId)
    {
        $result = $this->generalSearchItemRepo->findGeneralSearchCondition($generalId);
        $this->csvFormat['default'] = array();
        foreach ($result as $elem) {
            $comments = $this->findTableColumn($elem['table_name'], array($elem['column_name']));
            $key = $elem['table_name'] . '.' . $elem['column_name'];
            if (isset($this->replaceTableColumnName[$elem['function_id']][$key])) {
                $comments[0]->column_comment = $this->replaceTableColumnName[$elem['function_id']][$key];
            }
            $this->csvFormat['default'][] = $comments[0]->column_comment;
        }
    }

    /**
     * @param $generalId
     * @param $params
     * @param integer   $limit
     * @return string
     */
    private function buildQuery($generalId, &$params, $limit = 0)
    {
        $result = $this->mGeneralSearchRepo->findGeneralSearch($generalId);

        if (count($result) == 0) {
            return "";
        }

        $tableDeep = 0;

        $columns = array();
        foreach ($result[0]['gs_item'] as $item) {
            $columns[] = $this->buildColumn($item);
            $tableDeep = (array_search($item['table_name'], $this->functionTablesJoinOrder) > $tableDeep) ?
                array_search($item['table_name'], $this->functionTablesJoinOrder) : $tableDeep;
        }

        $wheres = array('1 = 1');
        $params = array();
        $tableDeep = $this->getWhereCondition($result, $tableDeep, $wheres);

        $fromTable = "demand_infos ";
        for ($i = 1; $i < $tableDeep + 1; $i++) {
            $fromTable .= " left join " . $this->functionTablesJoinOrder[$i] . " ON " .
                $this->functionTablesJoinRule[$this->functionTablesJoinOrder[$i]]['cond'];
        }
        $wheres = array_filter(
            $wheres,
            function ($var) {
                if ($var != '()') {
                    return !empty($var);
                }
            }
        );
        $query = "select " . implode(',', $columns) . " from " . $fromTable . " where " . implode(' and ', $wheres) . (($limit > 0) ? " limit " . $limit : "");

        return $query;
    }
    /**
     * @param $result
     * @param $tableDeep
     * @param $wheres
     * @return float
     */
    public function getWhereCondition($result, $tableDeep, &$wheres)
    {
        foreach ($result[0]['gs_condition'] as $cond) {
            switch ($cond['condition_expression']) {
                case 0:
                    $wheres[] = $cond['table_name'] . "." . $cond['column_name'] . " = " . DB::raw("'".$cond['condition_value']."'") . "";
                    break;
                case 1:
                    $wheres[] = $cond['table_name'] . "." . $cond['column_name'] . " like '%" . $cond['condition_value'] . "%'";
                    break;
                case 2:
                    $this->getWhereConditionTow($cond, $wheres);
                    break;
                case 3:
                    $wheres[] = $cond['table_name'] . "." . $cond['column_name'] . " in (" . implode(',', explode('^', $cond['condition_value'])) . ")";
                    break;
                case 4:
                    $wheres[] = $cond['table_name'] . "." . $cond['column_name'] . " in ('" . implode("','", explode('^', $cond['condition_value'])) . "')";
                    break;
                case 9:
                    $this->getWhereConditionNigth($cond, $wheres, $tableDeep);
                    break;
            }
            $tableDeep = (array_search($cond['table_name'], $this->functionTablesJoinOrder) > $tableDeep) ?
                array_search($cond['table_name'], $this->functionTablesJoinOrder) : $tableDeep;
        }
        return $tableDeep;
    }

    /**
     * @param $cond
     * @param $wheres
     * @param $tableDeep
     * @return mixed
     */
    public function getWhereConditionNigth($cond, &$wheres, &$tableDeep)
    {
        if ($cond['table_name'] . "." . $cond['column_name'] == 'demand_infos.customer_tel') {
            $wheres[] = '(demand_infos.customer_tel = \'' . $cond['condition_value'] . '\' or demand_infos.tel1 = \'' . $cond['condition_value'] . '\')';
        }
        if ($cond['table_name'] . "." . $cond['column_name'] == 'm_corps.tel1') {
            $wheres[] = '(m_corps.commission_dial = \'' . $cond['condition_value'] . '\' or m_corps.tel1 = \'' . $cond['condition_value'] . '\' or m_corps.tel2 = \'' . $cond['condition_value'] . '\')';
        }
        if ($cond['table_name'] . "." . $cond['column_name'] == 'm_corps-free_text') {
            $wheres[] = '(m_corps.note like \'%' . $cond['condition_value'] . '%\' or affiliation_infos.attention like \'%' . $cond['condition_value'] . '%\')';
            $tableDeep = ($tableDeep < array_search('affiliation_infos', $this->functionTablesJoinOrder)) ? array_search('affiliation_infos', $this->functionTablesJoinOrder) : $tableDeep;
        }
        if ($cond['table_name'] . "." . $cond['column_name'] == 'money_corresponds.nominee') {
            $wheres[] = "m_corps.id in (select corp_id from money_corresponds where nominee LIKE '%" . $cond['condition_value'] . "%')";
        }
        if ($cond['table_name'] . "." . $cond['column_name'] == 'm_target_areas.jis_cd') {
            $wheres[] = "substring(m_target_areas.jis_cd, 1, 2)::integer in (" . implode(',', explode('^', $cond['condition_value'])) . ")";
            $tableDeep = ($tableDeep < array_search('m_target_areas', $this->functionTablesJoinOrder)) ? array_search('m_target_areas', $this->functionTablesJoinOrder) : $tableDeep;
        }
    }
    /**
     * @param $cond
     * @param $wheres
     * @return mixed
     */
    public function getWhereConditionTow($cond, &$wheres)
    {
        $spVal = explode('^', $cond['condition_value']);
        $where = "";
        if (strlen($spVal[0]) > 0) {
            $where .= $cond['table_name'] . "." . $cond['column_name'] . " >= " . DB::raw("'".$spVal[0]."'") . " ";
        }
        if (strlen($spVal[1]) > 0) {
            if (strlen($where) > 0) {
                $where .= " and ";
            }
            $where .= $cond['table_name'] . "." . $cond['column_name'] . " <= " . DB::raw("'".$spVal[1]."'") . "";
        }
        $wheres[] = "(" . $where . ")";
    }
    /**
     * @param $item
     * @return string
     */
    protected function buildColumn($item)
    {
        if ($item['table_name'] == 'demand_infos' && $item['column_name'] == 'commission_limitover_time') {
            $colData = <<< QUERY
                CASE WHEN demand_infos.commission_limitover_time > 0
                         THEN (demand_infos.commission_limitover_time / 60)::varchar || '時間' || (demand_infos.commission_limitover_time % 60)::varchar || '分'
                     ELSE NULL END
QUERY;
        } elseif ($item['table_name'] == 'affiliation_infos' && $item['column_name'] == 'default_tax') {
            $colData = <<<EOF
                CASE WHEN affiliation_infos.default_tax = false THEN '0'
                WHEN affiliation_infos.default_tax = true THEN '1'
                ELSE 'NULL' END
EOF;
        } elseif ($item['table_name'] == 'bill_infos' && $item['column_name'] == 'auction_id') {
            $colData = '(SELECT total_bill_price FROM bill_infos auction_bill_infos WHERE auction_bill_infos.commission_id = commission_infos.id AND auction_bill_infos.auction_id IS NOT NULL LIMIT 1)';
        } else {
            $colData = $item['table_name'] . "." . $item['column_name'];
        }
        return $colData . " as " . $item['table_name'] . "_" . $item['column_name'];
    }

    /**
     * @param $generalId
     */
    public function deleteGeneralSearchAll($generalId)
    {
        $this->generalSearchItemRepo->deleteById($generalId);
        $this->generalSearchConditionRepo->deleteById($generalId);
        $this->mGeneralSearchRepo->deleteGeneralSearch($generalId);
    }
}
