<?php

namespace App\Services;

use App\Repositories\ProgItemRepositoryInterface;

const PROGRESS_ITEM_ID = 1;


class ProgressManagementItemService
{
    /**
     * @var ProgItemRepositoryInterface
     */
    protected $progressItem;

    /**
     * ProgressManagementItemService constructor.
     *
     * @param ProgItemRepositoryInterface $progressItem
     */
    public function __construct(
        ProgItemRepositoryInterface $progressItem
    ) {
        $this->progressItem = $progressItem;
    }

    /**
     * @return mixed
     */
    public function getProgressItem()
    {
        return $this->progressItem->findById(PROGRESS_ITEM_ID);
    }

    /**
     * @param $id
     * @param $data
     * @return boolean
     */
    public function updateItem($id, $data)
    {
        try {
            $item = $this->progressItem->findById($id);
            if ($item !== null) {
                $data['modified_user_id'] = \Auth::user()->id;
                $data['modified'] = \Carbon\Carbon::now();
                $this->progressItem->update($item, $data);
                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
    }
}
