<?php

namespace App\Services\Report;

use App\Repositories\MCorpRepositoryInterface;
use App\Repositories\ReputationCheckRepositoryInterface;
use App\Services\BaseService;
use DB;
use Maatwebsite\Excel\Facades\Excel;

class ReportReputationFollowService extends BaseService
{
    /**
     * @var array
     */
    public static $csvFormatReputationChecks = [
        'id' => '企業ID',
        'official_corp_name' => '正式企業名',
        'corp_name_kana' => '企業名ふりがな',
        'last_reputation_date' => '風評チェック更新日時（前回）',
        'commission_dial' => '取次用ダイヤル'
    ];
    /**
     * @var MCorpRepositoryInterface
     */
    protected $mCorps;
    /**
     * @var ReputationCheckRepositoryInterface
     */
    protected $reputationChecks;

    /**
     * ReportReputationFollowService constructor.
     *
     * @param MCorpRepositoryInterface           $mCorpRepository
     * @param ReputationCheckRepositoryInterface $reputationCheckRepository
     */
    public function __construct(
        MCorpRepositoryInterface $mCorpRepository,
        ReputationCheckRepositoryInterface $reputationCheckRepository
    ) {
        $this->mCorps = $mCorpRepository;
        $this->reputationChecks = $reputationCheckRepository;
    }

    /**
     * prepare data for render view
     *
     * @return mixed
     */
    public function prepareListDataForView()
    {
        $result = $this->reputationChecks->getListCorpReport();
        $bAllowUpdate = false;
        $role = \Auth::user()->auth;
        if (in_array($role, array('system', 'admin', 'accounting_admin'))) {
            $bAllowUpdate = true;
        }
        $result['bAllowShowUpdate'] = $bAllowUpdate;
        return $result;
    }

    /**
     * get next or previous data for pagination
     *
     * @param  $page
     * @return mixed
     */
    public function getDataForPagination($page)
    {
        $result = $this->reputationChecks->getListCorpReport($page);
        $bAllowUpdate = false;
        $role = \Auth::user()->auth;
        if (in_array($role, array('system', 'admin', 'accounting_admin'))) {
            $bAllowUpdate = true;
        }
        $result['bAllowShowUpdate'] = $bAllowUpdate;
        return $result;
    }

    /**
     * render file csv to download
     *
     * @return \Maatwebsite\Excel\LaravelExcelWriter|null
     */
    public function getDataForDownloadCsv()
    {
        $result = $this->reputationChecks->getListCorpReportDownload();
        if (is_array($result) && count($result) > 0) {
            $file = $this->exportCsv($result);
            return $file;
        } else {
            return null;
        }
    }

    /**
     * @param $data
     * @return \Maatwebsite\Excel\LaravelExcelWriter
     */
    private function exportCsv($data)
    {
        $columnKeys = array_keys(ReportReputationFollowService::$csvFormatReputationChecks);
        $headerValues = array_values(ReportReputationFollowService::$csvFormatReputationChecks);
        $csvData = [];
        foreach ($data as $row) {
            $rData = [];
            foreach ($columnKeys as $k) {
                $rData[$k] = '';
                if (isset($row[$k])) {
                    $rData[$k] = $row[$k];
                }
            }
            $csvData[] = array_combine($headerValues, array_values($rData));
        }
        $fileName = 'reputation_follow_' . \Auth::user()->user_id;
        return Excel::create(
            $fileName,
            function ($excel) use ($csvData, $fileName) {
                $excel->sheet(
                    $fileName,
                    function ($sheet) use ($csvData) {
                        $sheet->fromArray($csvData);
                    }
                );
            }
        );
    }

    /**
     * update time or insert data
     *
     * @param  $listId
     * @return boolean
     */
    public function updateReputationFollowTime($listId)
    {
        try {
            DB::beginTransaction();
            foreach ($listId as $id) {
                if (!is_null($id) && strlen(trim($id)) > 0) {
                    $this->reputationChecks->updateDateTime((int)$id);
                }
            }
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            return false;
        }
    }
}
