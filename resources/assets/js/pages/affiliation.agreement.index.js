$(document).ready(function() {
    enable_upload_btn();
});

function subWinPreview(url){
    window.open(url, '_blank', 'width=1115, height=800, menubar=no, toolbar=no, scrollbars=yes , location=no, left=' + (screen.availWidth - 975));
}
$('#update-corp-agreement').on('click', function () {
    if ($(this).data('requestRunning')) {
        return;
    }
    $(this).data('requestRunning', true);
    ajaxFlg = $('#ajax-flg').val();
    if (ajaxFlg == 0) {
        return true;
    }

    corpId = $('#corp-id').data('corp-id');

    $.ajax({
        type: 'get',
        url: urlCheckAutoCommission,
        data: {
            'corp_id': corpId
        },
    }).done(function(res, textStatus, jqXHR) {
        $('#update-corp-agreement').data('requestRunning', false);
        if (res) {
            $('#ajax-flg').val(0);
            $("#update-corp-agreement-form").submit();
        } else {
            $('#ajax-flg').val(1);
            $('#checkModal').modal('show');
        }
    });
    return false;
});

function enable_upload_btn() {
    var isDisable = true;
    $(".upload_file_path").each(function() {
        if ($(this).val()) {
            document.getElementById("upload-file").disabled = false;
            return false;
        } else {
            document.getElementById("upload-file").disabled = true;
        }
    });
}

$('.btn-delete-file').on('click', function() {
    if ($(this).data('requestRunning')) {
        return;
    }
    $(this).data('requestRunning', true);
    fileId = $(this).data('id');
    $('#file-id').val(fileId);
    $("#upload-file-form").submit();
});