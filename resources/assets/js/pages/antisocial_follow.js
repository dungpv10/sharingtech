var AntisocialFollow = function() {
    function init() {
        $(document).on('change', '[name="check[]"]', function () {
            $('#update').prop('disabled', !$('[name="check[]"]').is(':checked'));
        });
        $(document).on('click', '#checkAll', function () {
            var texts = ["全加盟店を選択", "全加盟店を選択解除"];
            var t = $(this);
            var mode = (t.data('mode') + 1) % 2;
            $('[name="check[]"]').prop('checked', (mode === 1)).trigger('change');
            t.val(texts[mode]);
            t.text(texts[mode]).data('mode', mode);
        });
        var url = jQuery('.table-responsive').attr('data-url');
        var controlEl = {
            nextPage: '.next',
            prevPage: '.previous',
            resultArea: '.table-responsive',
        };
        ajaxCommon.search(url, controlEl);
    }
    return {
        init: init
    }
}();
$(document).ready(function () {
    AntisocialFollow.init();
});