var AuctionExclusion = function() {
    function insertSettingHoliday() {
        var groupVacation = $('#group-vacation').html();
        $('#addVacation').on('click', function() {
            $('#setting-vacation').append(groupVacation);
            Datetime.initForDatepicker();
        });
    }

    function initDate() {
        Datetime.initForDatepicker();
        Datetime.initForTimepicker();
    }

    function init() {
        insertSettingHoliday();
        initDate();
    }

    return {
        init: init
    }
}();
$(document).ready(function() {
    AuctionExclusion.init();
    FormUtil.validate('#form-auction-exclusion');
});
