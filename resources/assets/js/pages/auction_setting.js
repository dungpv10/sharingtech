var checkGet = function () {
    function validateGroupSelect(array) {
        array.forEach(function(element){
            addEventListen(element.item1, element.item2);
            addEventListen(element.item2, element.item1);
        })
    }

    function addEventListen(ele1, ele2) {
        processCheckVal(ele1, ele2);
        ['blur', 'change'].forEach(function(event){
            ele1.inputId.on(event,function(){
                processCheckVal(ele1, ele2, event);
            });
        });
    }

    function processCheckVal(ele1, ele2, event) {
        if (ele1.inputId.val() || ele2.inputId.val()) {
            if (ele1.inputId.val()) {
                processIsValid(ele2.inputId, event);
            }
            if (ele2.inputId.val()) {
                processIsValid(ele1.inputId, event);
            }
            ele2.feedbackId.empty();
            ele1.feedbackId.empty();
        } else {
            processIsInvalid(ele1.inputId);
            processIsInvalid(ele2.inputId);
        }
    }

    function processIsValid(element, event) {
        if (!element.hasClass('ignore')) {
            element.addClass('ignore');
        }
        if (element.hasClass('is-invalid')) {
            element.removeClass('is-invalid');
        }
        if (event === 'blur' && !element.hasClass('is-valid')) {
            element.addClass('is-valid');
        }
    }

    function processIsInvalid(element) {
        if (element.hasClass('ignore')) {
            element.removeClass('ignore');
        }
    }

    return {
        validateSelect: validateGroupSelect,
    }
}();


