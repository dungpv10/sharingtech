var BillDetail = function () {
    function redirectToBillList(selector) {
        var mcorpId = $(selector).data('mcorpid');
        $(selector).click(function() {
            window.location.href = mcorpId;
        });
    }

    function init() {
        redirectToBillList(".redirectToBillList");
    }

    return {
        init: init
    }
}();

