var Bill = function () {

    return {
        //main function to initiate the module
        init: function () {
            var idname;
            $('.datepicker').click(function () {
                idname = $(this).attr("id");
            });

            $('.datepicker').focus(function () {
                idname = $(this).attr("id");
            });

            var showAdditionalButton = function (input) {
                setTimeout(function() {
                    var buttonPane = $( input ).datepicker( "widget" ).find( ".ui-datepicker-buttonpane" );
                    var btn = $('<button class="ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all" type="button">2ヶ月後</button>');
                    btn.unbind("click").bind("click", function (id) {
                        var date = new Date();
                        var after_two_months = date.getFullYear()  + "/" + (date.getMonth() + 3) + "/" + date.getDate();
                        $('#'+idname).datepicker("setDate", after_two_months);
                    });
                    btn.appendTo( buttonPane );
                }, 1 );
            };

            $('#from_fee_billing_date').datepicker({
                showButtonPanel: true,
                beforeShow: showAdditionalButton,
                onChangeMonthYear: showAdditionalButton,
                onclick: showAdditionalButton
            });

            $('#to_fee_billing_date').datepicker({
                showButtonPanel: true,
                beforeShow: showAdditionalButton,
                onChangeMonthYear: showAdditionalButton,
                onclick: showAdditionalButton
            });
        }

    };

}();