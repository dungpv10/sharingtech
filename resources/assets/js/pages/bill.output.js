var BillOutput = function () {
    function initDatePicker() {
        $('.datepicker').datepicker({
            controlType: 'select',
            oneLine: true,
            timeText: '時間',
            currentText: '現時刻',
            closeText: '閉じる',
            locale: 'ja'
        });
    }

    return {
        init: initDatePicker
    }
}();

$(document).ready(function () {
    BillOutput.init();
});
