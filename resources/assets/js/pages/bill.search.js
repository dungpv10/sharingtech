let OnClick = function () {
    return {
        init: function (url_money_correspond, url_bill_download, url_bill_save) {
            $(document).on('click', '#history', function () {
                window.open(url_money_correspond, '_blank', 'width=800, height=500, menubar=no, toolbar=no, scrollbars=yes, left=' + (screen.availWidth - 800))
            });
            $(document).on('click', '#fee_all_check', function () {
                if ($("#fee_all_check").prop("checked") === true) {
                    $("input:checkbox[class='fee_target_checkbox']").prop('checked', true);
                    checkAllFee();
                    $("input:checkbox[id='all_check']").prop('checked', true);
                    checkAll();
                    let count = $("#count").val();
                    for (let i = 0; i <= count; i = i + 1) {
                        checkValue(i);
                    }
                }else {
                    checkAllFee();
                    $("input:checkbox[id='all_check']").prop('checked', false);
                    checkAll();
                    let count = $("#count").val();
                    for (let i = 0; i <= count; i = i + 1) {
                        checkValue(i);
                    }
                }
            });
            $(document).on('click', '#all_check', function () {
                checkAll();
            });

            function checkAllFee() {
                if ($("#fee_all_check").prop("checked") === true) {
                    $('input:checkbox[name="checkbox[]"]').each(function () {
                        this.checked = true;
                    });
                } else {
                    $('input:checkbox[name="checkbox[]"]').each(function () {
                        this.checked = false;
                    })
                }
            }

            function checkAll() {
                if ($("#all_check").prop("checked") === true) {
                    $('input:checkbox[name="target[]"]').each(function () {
                        this.checked = true;
                    });
                } else {
                    $('input:checkbox[name="target[]"]').each(function () {
                        this.checked = false;
                    })
                }
            }

            function checkValue(num) {
                if ($("#checkbox" + num).prop("checked") === true) {
                    let total_bill_price = $("#total_bill_price" + num).val();
                    $("#fee_payment_price" + num).val(total_bill_price);
                } else {
                    $("#fee_payment_price" + num).val(0);
                }
                changeValue(num);
            }

            function changeValue(num) {
                let total_bill_price = $("#total_bill_price" + num).val();
                let fee_payment_price = $("#fee_payment_price" + num).val();
                if (fee_payment_price.match(/^[0-9]+$/)) {
                    let fee_payment_balance = Number(total_bill_price) - Number(fee_payment_price);
                    $("#fee_payment_balance" + num).val(fee_payment_balance);
                    $("#fee_payment_balance_display" + num).html(separate(fee_payment_balance) + '円');
                    $("#target" + num).attr("checked", true);
                    aggregate();
                }

                $("input:checkbox[id='target" + num + "']").attr('checked', true);
            }

            function aggregate() {
                let count = $("#count").val();
                let i;
                let fee_payment_price;
                let fee_payment_balance;
                let all_fee_payment_price = 0;
                let all_fee_payment_balance = 0;
                for (i = 0; i <= count; i++) {
                    fee_payment_price = $("#fee_payment_price" + i).val();
                    fee_payment_balance = $("#fee_payment_balance" + i).val();
                    if (fee_payment_price.match(/^[0-9]+$/)) {
                        all_fee_payment_price = Number(all_fee_payment_price) + Number(fee_payment_price);
                    }
                    if (fee_payment_balance.match(/^[0-9]+$/)) {
                        all_fee_payment_balance = Number(all_fee_payment_balance) + Number(fee_payment_balance);
                    }
                }
                $("#all_fee_payment_price_display").html(separate(all_fee_payment_price) + '円');
                $("#all_fee_payment_balance_display").html(separate(all_fee_payment_balance) + '円');
            }

            $(document).on('click', '.fee_target_checkbox', function () {
                let count = $(this).val();
                $('input:checkbox[id="target'+ count + '"]').prop('checked', true);
                checkValue(count)
            });
            $(document).on('change', '.fee_target_input', function () {
                changeValue($(this).attr('data-fee'))
            });
           
            $(document).on('click', '#bill_download', function (e) {
                e.preventDefault();
                $(controlEl.formId).attr('action', url_bill_download).submit();
                $(controlEl.formId).attr('action', url_bill_save);
                });
                function separate(num) {
                    return String(num).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,');
                }
            }
        }
    }();
