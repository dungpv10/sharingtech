var CommissionSelectMCorp = function () {
    function unEscapeHTML(val) {
        return $('<div/>').html(val).text();
    }

    function selectItemInit() {
        $('.mCorpItem').click(function (e) {
           e.preventDefault();
            var corp_name = $(this).data('corp-name');
            var corp_id = $(this).data('corp-id');
            $(INPUT_CORP_NAME).val(unEscapeHTML(corp_name));
            $(INPUT_CORP_ID).val(unEscapeHTML(corp_id));
            $(MODAL).modal('hide');
        });
    }

    function renderList(listData, parentDiv) {
        var listItem = '';
        listData.forEach(function (data) {
            listItem += '<tr>\n' +
                '            <td>\n' +
                '                <a href="#"\n' +
                '                   id="mCorp' + data.id + '"\n' +
                '                   class="mCorpItem"\n' +
                '                   data-corp-name="' + data.official_corp_name + '"\n' +
                '                   data-corp-id="' + data.id + '">\n' +
                '                    ' + data.official_corp_name + '\n' +
                '                </a>\n' +
                '            </td>\n' +
                '            <td>(' + data.id + ')</td>\n' +
                '        </tr>';
        });
        $(parentDiv).empty();
        $(parentDiv).append(listItem);
    }

    function renderPagination(pagination, paginationDiv) {
        if (!(pagination.current_page == 1 && pagination.next_page_url == null)) {
            $(paginationDiv).empty();
            if (pagination.prev_page_url !== null) {
                $(paginationDiv).append('<li class="page-item"><a href="' + pagination.prev_page_url + '"  class="page-link" rel="prev" role="pagination">'+PREV_TEXT+'</a></li>');
            } else {
                $(paginationDiv).append('<li class="page-item disabled"><span class="page-link">'+PREV_TEXT+'</span></li>');
            }
            if (pagination.next_page_url !== null) {
                $(paginationDiv).append('<li class="page-item"><a href="' + pagination.next_page_url + '" class="page-link" rel="next" role="pagination">'+NEXT_TEXT+'</a></li>');
            } else {
                $(paginationDiv).append('<li class="disabled page-item"><span class="page-link">'+NEXT_TEXT+'</span></li>');
            }
            $('a[role=pagination]').click(function (e) {
                e.preventDefault();
                search($(this).attr('href'), $(FORM).serialize());
            });
        }
    }

    function search(url, data) {
        $.ajax({
            type: 'GET',
            url: url,
            data: data,
            success: function (e) {
                var response = JSON.parse(e);
                var resData = response.data;
                if (resData.length > 0) {
                    renderList(resData, LIST_DATA_SELECTOR);
                    renderPagination(response, PAGINATE_SELECTOR);
                    selectItemInit();
                }
            },
            error: function () {

            }
        });
    }

    function searchOnShowModal() {
        $(BTN_TOGGLE_MODAL).click(function (e) {
            var keyword = $(INPUT_CORP_NAME).val();
            if (keyword !== '') {
                $(INPUT_SEARCH_CORP).val(keyword);
                $(LIST_DATA_SELECTOR).empty();
                search(SEARCH_URL, $(FORM).serialize());
            }
        });
    }
    function searchEventInit() {
        $(BTN_SEARCH).click(function (e) {
            e.preventDefault();
            search(SEARCH_URL, $(FORM).serialize());
        });
    }
    function init() {
        search(SEARCH_URL, $(FORM).serialize());
        searchOnShowModal();
        searchEventInit();
    }
    return {
        init: init
    }
}();
jQuery(document).ready(function () {
    CommissionSelectMCorp.init();
});


