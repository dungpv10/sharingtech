var DemandInfo = function() {
    var tablet_width = 678;
    initPseudoScrollBar();

    function enableLostFlgStatus(el) {
        var tmpVar = jQuery("#demand_status").val();
        var elm = document.getElementById('lost_flg_filter');
        if (tmpVar == null) {
            if (true == elm.checked) {
                elm.click();
            }
            jQuery('#lost_flg_filter').prop("disabled", true);
        } else {
            if ((tmpVar.indexOf("1") != -1 && (tmpVar.length == 1)) ||
                ((tmpVar.indexOf("3") != -1) && (tmpVar.length == 1)) ||
                ((tmpVar.indexOf("3") != -1) && (tmpVar.indexOf("1") != -1) && (tmpVar.length == 2))) {
                jQuery('#lost_flg_filter').prop("disabled", false);
            } else {
                if (true == elm.checked) {
                    elm.click();
                }
                jQuery('#lost_flg_filter').prop("disabled", true);
            }
        }
    }

    function enableLostFlgStatusFirst(el) {
        var tmpVar = jQuery("#demand_status").val();
        var elm = document.getElementById('lost_flg_filter');
        if (tmpVar != null && tmpVar.length > 0) {
            if ((tmpVar.indexOf("1") != -1 && (tmpVar.length == 1)) ||
                ((tmpVar.indexOf("3") != -1) && (tmpVar.length == 1)) ||
                ((tmpVar.indexOf("3") != -1) && (tmpVar.indexOf("1") != -1) && (tmpVar.length == 2))) {
                jQuery('#lost_flg_filter').prop("disabled", false);
                if (jQuery('#lost_flg_filter').attr('data-check') == 'checked') {
                    elm.click();
                }
            } else {
                jQuery('#lost_flg_filter').prop("disabled", true);
            }
        } else {
            jQuery('#lost_flg_filter').prop("disabled", true);
        }
    }

    function sortSubmit(sort, direction) {
        $('#demand_info').append('<input type="hidden" name="data[sort]" value="' + sort + '">');
        $('#demand_info').append('<input type="hidden" name="data[direction]" value="' + direction + '">');
        $("#search").trigger("click");
    }

    function init() {
        enableLostFlgStatusFirst();
        jQuery("#demand_status").change(function() {
            enableLostFlgStatus();
        });
        jQuery(".sort").click(function() {
            sort = $(this).attr('data-sort');
            direction = $(this).attr('data-direction');
            sortSubmit(sort, direction);
        });
    }
    function initPseudoScrollBar() {
        if ($('.custom-scroll-x').length) {
            var table_scroll_width = $('.add-pseudo-scroll-bar').width();
            var table_offset_top = $('.custom-scroll-x').offset().top;
            var width_scroll = $('.custom-scroll-x').width();
    
            $('.scroll-bar').css('width', table_scroll_width);
            $('.pseudo-scroll-bar').css({ 'width': width_scroll, 'bottom': 0 });
            $('.pseudo-scroll-bar').scroll(function() {
                var left = Number($('.pseudo-scroll-bar').scrollLeft());
                $('.custom-scroll-x').scrollLeft(left);
            });
            $('.custom-scroll-x').scroll(function() {
                var left = Number($('.custom-scroll-x').scrollLeft());
                $('.pseudo-scroll-bar').scrollLeft(left);
            });
            $(window).on("scroll", function() {
                var display = $('.pseudo-scroll-bar').attr('data-display');
                if ($(window).width() < tablet_width) {
                    if ($(window).scrollTop() + $(window).height() > table_offset_top + $('.add-pseudo-scroll-bar').height() && display == 'true' || $(window).scrollTop() + $(window).height() < table_offset_top + 50 && display == 'true') {
                        $('.pseudo-scroll-bar').hide().attr('data-display', false);
                    } else if ($(window).scrollTop() + $(window).height() < table_offset_top + $('.add-pseudo-scroll-bar').height() && $(window).scrollTop() + $(window).height() > table_offset_top + 50 && display == 'false') {
                        $('.pseudo-scroll-bar').css('bottom', $('.fixed-button').outerHeight());
                        $('.pseudo-scroll-bar').show().attr('data-display', true);
                    }
                } else {
                    if ($(window).scrollTop() + $(window).height() > table_offset_top + $('.add-pseudo-scroll-bar').height() && display == 'true') {
                        $('.pseudo-scroll-bar').hide().attr('data-display', false);
                    } else if ($(window).scrollTop() + $(window).height() < table_offset_top + $('.add-pseudo-scroll-bar').height() && display == 'false') {
                        $('.pseudo-scroll-bar').show().attr('data-display', true);
                    }
                }
            });
        }
    }
    return {
        init: init
    }
}();
$(document).ready(function() {
    DemandInfo.init();
});
window.onbeforeunload = function() {
    window.scrollTo(0, 0);
};
