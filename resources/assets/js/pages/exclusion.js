var Exclusion = function () {
    function initDatePicker() {
        $('.datepicker').datepicker({
            controlType: 'select',
            oneLine: true,
            timeText: '時間',
            currentText: '現時刻',
            closeText: '閉じる',
            locale: 'ja'
        });
    }
    function initTimePicker() {
        $('.timepicker').timepicker({
            controlType: 'select',
            oneLine: true,
            hourText: '時',
            minuteText: '分',
            currentText: '現時刻',
            closeText: '閉じる',
            locale: 'ja'
        });
    }

    function init() {
        initDatePicker();
        initTimePicker();
    }
    return {
        init: init
    }
}();

$(document).ready(function () {
    Exclusion.init();
});
