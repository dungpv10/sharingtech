$('button[type="reset"]').click(function(e){
    e.preventDefault();
    $('.radio-asc').prop('checked', true);
    $('.radio-desc').prop('checked', false);
    $('.radio-asc:last').prop('checked', false);

    $('select:eq(0)').val('corp_name');
    $('select:eq(1)').val('');
    $('select:eq(2)').val('commission_rank');
    $('select:eq(3)').val('');
});

if(checkOrderBy){
    $('input[type="radio"]').prop('checked', false);
}
