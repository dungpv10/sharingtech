var jbrReceiptFollow = function() {

    var resultDiv = $('.content-ajax'),
        orderBy = '',
        sortType = '',
        from_date = '',
        to_date = '';
    var progress = new progressCommon();

    function initDatepicker() {
        $('.datepicker').each(function() {
            var idName = $(this).attr('id');
            var showAdditionalButton = function(input) {
                var buttonPane = $(input).datepicker("widget").find(".ui-datepicker-buttonpane");
                var btn = $('<button class="ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all" type="button">2ヶ月後</button>');
                btn.unbind("click").bind("click", function(id) {
                    var date = new Date();
                    var after_two_months = date.getFullYear() + "/" + (date.getMonth() + 3) + "/" + date.getDate();
                    $('#' + idName).datepicker("setDate", after_two_months);
                });
                btn.appendTo(buttonPane);
            };

            $(this).datepicker({
                showButtonPanel: true,
                beforeShow: showAdditionalButton,
                onChangeMonthYear: showAdditionalButton,
                onClose: function() {
                    $(this).trigger('blur');
                }
            });

            $(this).on('click', showAdditionalButton);
        });
    }

    function filterTable() {
        var currentPage = 1;
        $(window).on('hashchange', function() {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page == Number.NaN || page <= 1) {
                    return false;
                }
            }
        });

        $(document).on('click', '.sort', function(e) {
            e.preventDefault();
            detailSort = $(this).data('sort').split('-');
            orderBy = detailSort[0];
            sortType = detailSort[1];
            getPosts(currentPage);
        });
        $(document).on('click', '.next', function(e) {
            e.preventDefault();
            ++currentPage;
            getPosts(currentPage);
        });
        $(document).on('click', '.previous', function(e) {
            e.preventDefault();
            --currentPage;
            getPosts(currentPage);
        });

        $(document).on('click', '#search', function(e) {
            if ($('#search-form').valid()) {
                e.preventDefault();
                from_date = $('#from_date').val();
                to_date = $('#to_date').val();
                getPosts(currentPage);
            }
        });
    }

    function getPosts(currentPage) {
        var url = urlGetJbrList;
        page = currentPage;

        if (typeof page != 'undefined' && page > 1) {
            url = url + '?page=' + page + '&nameColumn=' + orderBy + '&order=' + sortType + '&from_date=' + from_date + '&to_date=' + to_date;
        } else {
            url = url + '?nameColumn=' + orderBy + '&order=' + sortType + '&from_date=' + from_date + '&to_date=' + to_date;
        }

        $.ajax({
            type: 'post',
            url: url,
            data: {},
            processData: false,
            xhr: function() {
                return progress.createXHR();
            },
            beforeSend: function() {
                progress.controlProgress(true);
            },
            complete: function() {
                progress.controlProgress(false);
            },
            success: function(data) {
                resultDiv.html(data);
            },
            error: function(err) {
                console.log('error');
            }
        });

    }

    function init() {
        initDatepicker();
        filterTable();
    }

    return {
        init: init
    }

}();

$(document).ready(function() {
    jbrReceiptFollow.init();
});