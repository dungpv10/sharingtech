var ReportSaleSupport = function () {
	function sortSubmit(sort, direction) {
        $('#sale_support_search_form').append('<input type="hidden" name="data[sort]" value="'+sort+'">');
        $('#sale_support_search_form').append('<input type="hidden" name="data[direction]" value="'+direction+'">');
        $('#sale_support_search_form').submit();
    }
 
    /**
     * Set function
     */
    function init() {
    	jQuery(".sort").click( function () {
            var sort = $(this).attr('data-sort');
            var direction = $(this).attr('data-direction');
            sortSubmit(sort, direction);
        });
    }

    return {
        init: init
    }
}();

$(document).ready(function () {
    ReportSaleSupport.init();
});