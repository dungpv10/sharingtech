var $selectAll = $('#select_all');
var $listSelect = $(".checkbox-selection");

var SelectionPrefecture = function () {

    function checkAll() {
        $selectAll.on("click", function(){
            $listSelect.prop('checked', $selectAll.prop('checked'));
        });
    }

    /**
     * Set function
     */
    function init() {
        checkAll();
    }

    return {
        init: init
    }
}();