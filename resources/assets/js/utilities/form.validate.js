var FormUtil = function () {
    return {
        validate: function (formSelector) {
            $(document).find(formSelector).each(function () {
                $(this).validate({
                    ignore: ".ignore",
                    onchange: function(element) {
                        // Hack method to validate form element without submit form for the first time, overriding default lazy validation
                        this.element(element);
                        toggleValidityStateForRequiredGroup($(element).parents('[data-group-required="true"]'), false);
                        toggleValidityStateForRequiredGroupFill($(element).parents('[data-group-fill-required="true"]'));
                        toggleValidityStateForRequiredOption($(element).parents('[data-rule-requiredOption="true"]'));
                    },
                    onblur: function(element) {
                        // Hack method to validate form element without submit form for the first time, overriding default lazy validation
                        this.element(element);
                        toggleValidityStateForRequiredGroup($(element).parents('[data-group-required="true"]'), false);
                        toggleValidityStateForRequiredGroupFill($(element).parents('[data-group-fill-required="true"]'));
                        toggleValidityStateForRequiredOption($(element).parents('[data-rule-requiredOption="true"]'));
                    },
                    onfocusout: function (element) {
                        // Hack method to validate form element without submit form for the first time, overriding default lazy validation
                        this.element(element);
                        toggleValidityStateForRequiredGroup($(element).parents('[data-group-required="true"]'), false);
                        toggleValidityStateForRequiredGroupFill($(element).parents('[data-group-fill-required="true"]'));
                        toggleValidityStateForRequiredOption($(element).parents('[data-rule-requiredOption="true"]'));
                    },
                    onclick: function (element) {
                        // Hack method to validate form element without submit form for the first time, overriding default lazy validation
                        this.element(element);
                        toggleValidityStateForRequiredGroup($(element).parents('[data-group-required="true"]'), false);
                        toggleValidityStateForRequiredGroupFill($(element).parents('[data-group-fill-required="true"]'));
                        toggleValidityStateForRequiredOption($(element).parents('[data-rule-requiredOption="true"]'));
                    },
                    onkeyup: function (element) {
                        // Hack method to validate form element without submit form for the first time, overriding default lazy validation
                        this.element(element);
                        toggleValidityStateForRequiredGroup($(element).parents('[data-group-required="true"]'), false);
                        toggleValidityStateForRequiredGroupFill($(element).parents('[data-group-fill-required="true"]'));
                        toggleValidityStateForRequiredOption($(element).parents('[data-rule-requiredOption="true"]'));
                    },
                    submitHandler: function (form) {
                        var validate1 = groupCheckedAtLeastOne(false);
                        var validate2 = groupFillAtLeastOne(false)
                        var validate3 = requiredOption(false);
                        if (validate1 && validate2 && validate3) {
                            form.submit();
                        } else {
                            focusWhenSubmit(formSelector);
                        }
                    },
                    invalidHandler: function () {
                        groupCheckedAtLeastOne(true);
                        groupFillAtLeastOne(true);
                        requiredOption(true);
                        focusWhenSubmit(formSelector);
                    },
                    errorClass: "invalid-feedback",
                    validClass: "valid-feedback",
                    errorPlacement: function (error, element) {
                        var errContainer = $(element).data('error-container');
                        if (errContainer) {
                            $(errContainer).append(error)
                        } else {
                            error.insertAfter(element);
                        }
                    },
                    highlight: function (element) {
                        $(element).addClass('is-invalid').removeClass('is-valid');
                    },
                    unhighlight: function (element) {
                        $(element).removeClass('is-invalid').addClass('is-valid');
                    }
                });
            });

            function focusWhenSubmit(form) {
                var itemFocus = 'nothing';
                $(form).find('input:not([type=hidden])').each(function (key, e) {
                    if ($(e).hasClass('is-invalid')) {
                        itemFocus = e;
                        return false;
                    }
                });
                if (itemFocus != 'nothing') {
                    $(itemFocus).focus();
                }
                return false
            }

            function groupCheckedAtLeastOne(isReturn) {
                var checkGroup = true,
                    arrFailGroup = [];
                $(document).find('[data-group-required=true]').each(function () {
                    checkGroup = toggleValidityStateForRequiredGroup($(this));

                    // if (!checkGroup) {
                    //     arrFailGroup.push($(this));
                    // }
                });

                if (!isReturn) {
                    // if (!checkGroup) {
                    //     arrFailGroup[0].find('input').first().focus();
                    // }
                    return checkGroup;
                }

                return false;
            }

            function groupFillAtLeastOne(isReturn) {
                var result = true,
                itemFocus = 'nothing';
                $(document).find('[data-group-fill-required=true]').each(function () {
                    checkGroup = toggleValidityStateForRequiredGroupFill($(this));

                    if (!checkGroup) {
                        result = false;
                        // $(this).find('input:not([type=hidden])').each(function (key, e) {
                        //     if (itemFocus == 'nothing') {
                        //         if ($(e).hasClass('is-invalid')) {
                        //             itemFocus = e;
                        //             return false;
                        //         }
                        //     } else {
                        //         return false;
                        //     }
                        // });
                    }
                });

                if (!isReturn) {
                    // if (!result && itemFocus != 'nothing') {
                    //     $(itemFocus).focus();
                    // }
                    return result;
                }

                return false;
            }

            function requiredOption(isReturn) {
                var result = true,
                itemFocus = 'nothing';
                $(document).find('[data-rule-requiredOption="true"]').each(function () {
                    var checkGroup = toggleValidityStateForRequiredOption($(this));

                    if (!checkGroup) {
                        result = false;
                        $(this).find('input:not([type=hidden])').each(function (key, e) {
                            if ($(e).hasClass('is-invalid')) {
                                var feedback = $(e).data('error-container');
                                var count = $(feedback).find('.invalid-feedback');
                                if (count > 1) {
                                    $(feedback).find('.invalid-feedback:not(:first-of-type)').remove();
                                }
                            }
                        });
                    }
                });

                if (!isReturn) {
                    // if (!result && itemFocus != 'nothing') {
                    //     $(itemFocus).focus();
                    // }
                    return result;
                }

                return false;
            }

            function toggleValidityStateForRequiredOption(element) {
                var result = true,
                    regEx = /^\d{2}\:\d{2}?$/,
                    itemFrom = element.find('input[from]').length ? element.find('input[from]')[0] : '',
                    itemTo = element.find('input[to]').length ? element.find('input[to]')[0] : '',
                    valFrom = itemFrom ? $(itemFrom).val().trim() : '',
                    valTo = itemTo ? $(itemTo).val().trim() : '';

                if (itemFrom != '' && itemTo != '') {
                    if (valFrom != '' && valTo == '') {
                        showFeedbackForRequiredOption(element, itemTo, true, 'error-container-option', $.validator.messages.requiredOption("TO"), false);
                        result = false
                    } else if (valFrom == '' && valTo != '') {
                        showFeedbackForRequiredOption(element, itemFrom, true, 'error-container-option', $.validator.messages.requiredOption("FROM"), false);
                        result = false
                    } else if (valFrom == '' && valTo == '') {
                        showFeedbackForRequiredOption(element, itemTo, false, 'error-container-option', '', false);
                        showFeedbackForRequiredOption(element, itemFrom, false, 'error-container-option', '', false);
                    } else if (valFrom != '' && valTo != '') {
                        if (regEx.test(valFrom) && regEx.test(valTo)) {
                            showFeedbackForRequiredOption(element, itemTo, false, 'error-container-option', '', false);
                            showFeedbackForRequiredOption(element, itemFrom, false, 'error-container-option', '', false);
                        } else {
                            if (!regEx.test(valTo)) {
                                showFeedbackForRequiredOption(element, itemTo, true, 'error-container',  $.validator.messages.invalid_time, false);
                            }
                            if (!regEx.test(valFrom)) {
                                showFeedbackForRequiredOption(element, itemFrom, true, 'error-container',  $.validator.messages.invalid_time, false);
                            }
                            result = false
                        }

                        if (regEx.test(valFrom)) {
                            showFeedbackForRequiredOption(element, itemFrom, false, 'error-container', '', false);
                        }
                        if (regEx.test(valTo)) {
                            showFeedbackForRequiredOption(element, itemTo, false, 'error-container', '', false);
                        }
                    }
                }
                return result;
            }

            function showFeedbackForRequiredOption (itemParent, item, isError, containerError, message, onlyFeedback) {
                if (!onlyFeedback) {
                    if (isError) {
                        item.setCustomValidity(' ');
                        $(item).addClass('is-invalid').removeClass('is-valid');
                    } else {
                        $(item).removeClass('is-invalid').addClass('is-valid');
                    }
                }
                var feedback = containerError.length ? $(item).data(containerError) : '',
                    error = isError ? '<label class="invalid-feedback d-block">' + message + '</label>' : '';
                if (feedback) {
                    $(feedback).empty();
                    if (isError && message) {
                        $(feedback).append(error)
                    }
                } else {
                    itemParent.children('.invalid-feedback').remove();
                    if (isError && message) {
                        itemParent.append(error);
                    }
                }
            }


            function toggleValidityStateForRequiredGroup(element, focus) {
                var result = true,
                    countChecked = element.find('input:checked').length;

                if (countChecked !== 0) {
                    element.find('input').each(function (e, val) {
                        val.setCustomValidity('');
                    });

                    element.children('.invalid-feedback').remove();
                    element.find('input').removeClass('is-invalid').addClass('is-valid');
                } else {

                    element.find('input').each(function (e, val) {
                        val.setCustomValidity(' ');
                    });
                    element.find('input').addClass('is-invalid').removeClass('is-valid');

                    // Handle error message position
                    var errContainer = element.data('error-container'),
                        error = '<label class="invalid-feedback d-block">' + $.validator.messages.requiredOne + '</label>';

                    element.children('.invalid-feedback').remove();

                    if (errContainer) {
                        $(errContainer).append(error)
                    } else {
                        element.append(error);
                    }

                    if (focus) {
                        element.find('input').first().focus();
                    }

                    result = false;
                }

                return result;
            }

            function toggleValidityStateForRequiredGroupFill(element) {
                var notError = true ,
                    hasFilled = false,
                    regEx = /^\d{4}\/\d{2}\/\d{2}?$/;

                element.find('input:not([type=hidden])').each(function (key, e) {
                    var value = $(e).val().trim();
                    if (value != '') {
                        hasFilled = true;
                        if (!regEx.test(value)) {
                            notError = false;
                            showFeedbackForRequiredOption($(e).parent(), e, true, '', $.validator.messages.date,false);
                        } else {
                            showFeedbackForRequiredOption($(e).parent(), e, false, '', '', false);
                        }
                    }
                });

                if (hasFilled) {
                    element.find('input:not([type=hidden])').each(function (key, e) {
                        if ($(e).val().trim() == '') {
                            showFeedbackForRequiredOption($(e).parent(), e, false, '', '', false);
                        }
                    });

                    showFeedbackForRequiredOption($(element).parent(), element, false, 'error-container', '', true);
                } else {
                    element.find('input:not([type=hidden])').each(function (key, e) {
                        showFeedbackForRequiredOption($(e).parent(), e, true, '', '', false);
                    });

                    showFeedbackForRequiredOption($(element).parent(), element, true, 'error-container', $.validator.messages.requiredAtLeast, true);
                }

                return (notError && hasFilled);
            }

        },

        convertToHalfWidth: function fullWidthNumConvert(fullWidthNum) {
            return fullWidthNum.replace(/[\uFF10-\uFF19]/g, function (m) {
                return String.fromCharCode(m.charCodeAt(0) - 0xfee0);
            });
        }
    }
}();

// Custom validator methods

// Datetime picker
$.validator.addMethod("datetimeCheck", function (value) {
    var regEx = /^\d{4}\/\d{2}\/\d{2}(\s\d{2}:\d{2})?$/;

    if (value.length && value.match(regEx) == null) {
        return false;
    }

    return true;
}, $.validator.messages.dateTime);

//Time picker
$.validator.addMethod("timeCheck", $.validator.methods.time, $.validator.messages.invalid_time);

//Date picker
$.validator.addMethod("dateCheck", function (value, element) {
    var regEx = /^\d{4}\/\d{2}\/\d{2}?$/;

    if (value.length && value.match(regEx) == null) {
        return false;
    }

    return this.optional(element) || !/Invalid|NaN/.test(new Date(value).toString())
}, $.validator.messages.date);

//Compare From with To
$.validator.addMethod("lessThanTime", function (value, element, param) {

    if ($(param).val()) {
        var target = $(param);

        if (this.settings.onfocusout && target.not(".validate-lessThanTime-blur").length) {
            target.addClass("validate-lessThanTime-blur").on("blur.validate-lessThanTime", function () {
                $(element).valid();
            });
        }
        return value <= target.val();
    }

    return true;
}, $.validator.messages.from_less_than_to);

//required From
$.validator.addMethod("requiredOptionFrom", function (value, element, param) {
    if (value.trim() == '' && $(param).val().trim() != '') {
        return false
    }
    return true;
}, $.validator.messages.requiredOption());

//required To
$.validator.addMethod("requiredOptionTo", function (value, element, param) {
    if (value.trim() == '' && $(param).val().trim() != '') {
        return false
    }
    return true;
}, $.validator.messages.requiredOption('TO'));

// Multiple email validation
$.validator.addMethod("multipleEmailCheck", function (value) {
    var regEx = /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/,
        arrEmail = value.split(';'),
        isValid = true;

    $.each(arrEmail, function (index, value) {
        if (value.trim().match(regEx) == null) {
            isValid = false;
            return;
        }
    });

    return isValid;
}, $.validator.messages.email);

// Al size number validation
$.validator.addMethod("numberAllSize", function (value, element) {
    value = FormUtil.convertToHalfWidth(value);
    return this.optional(element) || /^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(value);
}, $.validator.messages.number);


$.validator.addClassRules({
    "datetimepicker": {
        datetimeCheck: true
    },
    "timepicker": {
        timeCheck: true
    },
    "datepicker": {
        dateCheck: true
    },
    "datepicker_limit": {
        dateCheck: true
    },
    "multiple-email-validation": {
        multipleEmailCheck: true
    }
});

// Extends default validation method
$.validator.methods.min = function (value, element, param) {
    value = FormUtil.convertToHalfWidth(value);
    return this.optional(element) || value >= param;
}

$.validator.methods.max = function (value, element, param) {
    value = FormUtil.convertToHalfWidth(value);
    return this.optional(element) || value <= param;
}

$.validator.methods.range = function (value, element, param) {
    value = FormUtil.convertToHalfWidth(value);
    return this.optional(element) || (value >= param[0] && value <= param[1]);
}

$.validator.methods.email = function (value, element) {
    return this.optional(element) || /^\w+([-+.'][^\s]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value)
}

$.validator.methods.required = function (value, element, param) {
    value = value.trim();
    // Check if dependency is met
    if (!this.depend(param, element)) {
        return "dependency-mismatch";
    }
    if (element.nodeName.toLowerCase() === "select") {

        // Could be an array for select-multiple or a string, both are fine this way
        var val = $(element).val();
        return val && val.length > 0;
    }
    if (this.checkable(element)) {
        return this.getLength(value, element) > 0;
    }
    return value !== undefined && value !== null && value.length > 0;
}
