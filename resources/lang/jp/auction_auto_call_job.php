<?php

return [
    'job_start' => 'オートコール処理(AuctionAutoCallTask) 開始',
    'job_end' => 'オートコール対象加盟店なし',
    'job_error' => 'AuctionAutoCallTask execute error. ',
    'call_api_failed' => 'コンタクトの開始に失敗',
    'call_api_error' => 'オートコールを実行',
    'api_empty' => 'AuctionAutoCallTask URL is empty. (api.add_contact)',
    'connect_api' => '指定したURLへ接続を開始',
    'cant_get_data_api' => 'AuctionAutoCallTask file_get_contents error',
    'update_data_failed' => 'AuctionAutoCallTask execute error when update Auction Info id: ',
];
