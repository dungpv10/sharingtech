<?php

return [
    'target_demand_flag_label' => '営業支援対象案件フラグマスタ',
    'id' => 'ID',
    'name' => '名称',
    'proposal_flag' => '営業支援対象案件フラグ',
    'excluded_check' => '（チェックで除外）',
];
