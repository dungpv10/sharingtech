@if(count($genreAreaList) == 0)
    <p class="border border-thick border-note bg-note text--orange-light p-2">
        {{ __('affiliation.no_applicable_genre_category') }}
    </p>
@else
    <table class="table table-bordered">
        <thead class="thead-light">
        <tr>
            <th>
                {{ __('affiliation.genre') }}
            </th>
            <th>{{ __('affiliation.category') }}</th>
            <th>
                {!! trans('affiliation.corresponding_area_edit_by_category') !!}
            </th>
            @if ( $userAuth != 'affiliation' || $mCorpsCorpCommissionType != 2 )
                <th>{{ __('affiliation.expertise') }}</th>
            @endif

            <th>{{ __('affiliation.fee') }}</th>
            <th>{{ __('affiliation.unit') }}</th>
            <th>{{ __('affiliation.note') }}</th>
            @if ( $userAuth != 'affiliation' )
                <th> {{ __('affiliation.intermediary_method') }}</th>
            @endif
        </tr>
        </thead>
        <tbody>
        @php
            $introAutoFlg = false;
        @endphp
        @foreach ($genreAreaList as $genre)
            @if ($genre['disable_flg'] == false)
                <tr>
                    <td data-label="{{ __('affiliation.genre') }}">
                        <p class="m-0">
                            {{ $genre['genre_name'] }}
                            @if ( $userAuth == 'affiliation' && $genre['m_sites_commission_type'] == 2 )
                            <span class="text-danger">{{trans('common.asterisk')}}1</span>
                            @php
                                $introAutoFlg = true;
                            @endphp
                            @endif
                        </p>
                        <div class="text-right d-md-none">
                            <a target="_blank" href="{{ action('Affiliation\AffiliationTargetAreaController@targetArea', ['id' => $genre['id']]) }}" class="btn btn--gradient-orange btn-sm border text-white">
                                <strong>{{ __('common.edit') }}</strong>
                            </a>
                        </div>
                    </td>
                    <td data-label="{{ __('affiliation.category') }}">{{ $genre['category_name'] }}</td>

                    <td class="text-center d-none d-md-table-cell"
                        data-label="{{ __('affiliation.corresponding_area_edit_by_category') }}">
                        <a href="{{ action('Affiliation\AffiliationTargetAreaController@targetArea', ['id' => $genre['id']]) }}"
                           target="_blank"
                           class="btn btn--gradient-default btn-sm border text-dark"><strong>{{ __('common.edit') }}</strong>
                        </a>
                    </td>
                    @if ( $userAuth != 'affiliation' || $mCorpsCorpCommissionType != 2 )
                        <td class="text-md-center" data-label="{{ __('affiliation.expertise') }}">
                            {{ $genre['select_list'] }}
                        </td>
                    @endif
                    <td class="text-md-center" data-label="{{ __('affiliation.fee') }}">
                        {{--Category A show order_fee, B show introduce_fee--}}
                        @if($category == 'A')
                            {{ $genre['order_fee'] }}
                        @elseif ($category == 'B')
                            {{ $genre['introduce_fee'] }}
                        @endif
                    </td>
                    <td class="text-md-center" data-label="{{ __('affiliation.unit') }}">
                        @if($category == 'A')
                            @if(isset($genre['order_fee_unit']))
                                @if($genre['order_fee_unit'] == 0)
                                    {{ __('affiliation.yen') }}
                                @else
                                    {{ __('affiliation.percent') }}
                                @endif
                            @endif
                        @elseif ($category == 'B')
                            {{ __('affiliation.yen') }}
                        @endif
                    </td>
                    <td class="text-md-center" data-label="{{ __('affiliation.note') }}">
                        {{ $genre['note'] }}
                    </td>
                    @if ( $userAuth != 'affiliation' )
                        <form action="{{ action('Affiliation\AffiliationController@updateStatusMCorpCategory', ['id' => $genre['id']]) }}"
                              id="updateStatus_{{ $genre['id'] }}" method="post">
                            <td class="d-none d-md-table-cell" data-label="{{ __('affiliation.intermediary_method') }}">
                                <select name="auction_status" class="custom-select custom-select-sm">
                                    <option value="0">{{ __('affiliation.not_set') }}</option>
                                    @foreach($auctionDeliveryStatusList as $auctionDeliveryStatusKey => $auctionDeliveryStatusValue)
                                        @php
                                            $selected = '';
                                        @endphp
                                        @if($auctionDeliveryStatusKey == $genre['auction_status'])
                                            @php
                                                $selected = 'selected="selected"';
                                            @endphp
                                        @endif
                                        <option value="{{ $auctionDeliveryStatusKey }}" {{ $selected }}>{{ $auctionDeliveryStatusValue }}</option>
                                    @endforeach
                                </select>
                                <input type="hidden" name="corpId" value="{{ $corpId }}">
                                {{ csrf_field() }}
                                <button form="updateStatus_{{ $genre['id'] }}"
                                        class="mt-1"
                                        type="submit">
                                    {{ __('affiliation.change') }}
                                </button>
                            </td>
                        </form>
                    @endif
                </tr>
                <tr class="d-md-none row-divider">
                </tr>
            @endif
        @endforeach
        </tbody>
    </table>
    @if ($introAutoFlg)
    <p class="text-danger">{{ __('affiliation.intro_auto_message') }}</p>
    @endif
@endif
