<!-- Modal -->
<div class="modal fade" id="historyModal" tabindex="-1" role="dialog" aria-labelledby="historyModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="container">
                    <div class="modal-body">
                            <div id="contents">
                                <div id="main">
                                    <h3>{{ __('affiliation_detail.affiliation_history_label') }}</h3>
                                    <form id="formAffiliationHistory" method="post">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <div class="row">
                                                    <label class="col-md-2" for="popup-responders">{{ __('affiliation_detail.responders') }}</label>
                                                    <select class="col-md-9 form-control" name="data_history[responders]" id="popup-responders" data-rule-required="true">
                                                            <option value="">{{ trans('affiliation_detail.none_value') }}</option>
                                                            @if(!empty($userList))
                                                                @foreach($userList as $key => $value)
                                                                    <option value="{{ $key }}">{{ $value }}</option>
                                                                @endforeach
                                                            @endif
                                                    </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-2" for="popup-content">{{ __('affiliation_detail.corresponding_contents') }}</label>
                                                <textarea class="col-md-9 form-control modal-area" name="data_history[corresponding_contens]" id="popup-content" data-rule-required="true" data-rule-maxlength="1000"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-2" for="popup-datetime">{{ __('affiliation_detail.correspond_datetime') }}</label>
                                                <input type="text" class="col-md-9 form-control datetimepicker" name="data_history[correspond_datetime]" id="popup-datetime">
                                            </div>
                                        </div>
                                        <input type="hidden" name="affiliation_id" value="{{ $mCorp->id }}">
                                        <button type="button" class="btn btn-primary" id="cancel-history">{{ __('common.cancel') }}</button>
                                        <button type="submit" class="btn btn-primary" id="submit-history">{{ __('common.edit') }}</button>
                                    </form>
                                </div>
                            </div>
                        </div>
            </div>
            
        </div>
    </div>
</div>