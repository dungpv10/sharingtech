<div class="modal modal-global fade" id="add-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    @lang('agreement_admin.register_license_information')
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>

            <form id="add-license-form">
                <div class="modal-body">
                    <div class="form-group px-3 mb-sm-0">
                        <div class="row">
                            <div class="col-sm-3 d-flex align-items-center modal-label">
                                <label for="add-license-name">
                                    @lang('agreement_admin.license_name') *
                                </label>
                            </div>
                            <div class="col-sm-9 d-flex align-items-center">
                                <input type="text" id="add-license-name" name="add-license-name"
                                       data-rule-required="true" data-rule-maxlength="200"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group px-3 mb-sm-0">
                        <div class="row">
                            <div class="col-sm-3 d-flex align-items-center modal-label">
                                <label for="add-certificate-required-flag">
                                    @lang('agreement_admin.certificate_required_flag')
                                </label>
                            </div>
                            <div class="col-sm-9 d-flex align-items-center">
                                <input type="checkbox" id="add-certificate-required-flag" name="add-certificate-required-flag"/>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <div class="modal-footer justify-content-center p-4">
                <button type="button"
                        class="btn btn--gradient-green w-45 text-white"
                        id="add-license-button">
                    @lang('agreement_admin.btn_update')
                </button>
                <button type="button"
                        class="btn btn--gradient-default border w-45 text-secondary"
                        data-dismiss="modal">
                    @lang('agreement_admin.btn_cancel')
                </button>
            </div>

        </div>
    </div>
</div>
