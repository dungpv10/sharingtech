<div class="modal modal-global fade" id="detail-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    @lang('agreement_admin.reference_license_information')
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>

            <div class="modal-body">
                <div class="form-group px-3 mb-sm-0">
                    <div class="row">
                        <div class="col-sm-3 d-flex align-items-center modal-label">
                            <label for="detail-license-id">
                                @lang('agreement_admin.license_id')
                            </label>
                        </div>
                        <div class="col-sm-9 d-flex align-items-center">
                            <div id="detail-license-id" name="detail-license-id"></div>
                        </div>
                    </div>
                </div>
                <div class="form-group px-3 mb-sm-0">
                    <div class="row border-top-0">
                        <div class="col-sm-3 d-flex align-items-center modal-label">
                            <label for="detail-license-name">
                                @lang('agreement_admin.license_name')
                            </label>
                        </div>
                        <div class="col-sm-9 d-flex align-items-center">
                            <div id="detail-license-name" class="my-2" name="detail-license-name"></div>
                        </div>
                    </div>
                </div>
                <div class="form-group px-3 mb-sm-0">
                    <div class="row border-top-0">
                        <div class="col-sm-3 d-flex align-items-center modal-label">
                            <label for="detail-certificate-required-flag">
                                @lang('agreement_admin.certificate_required_flag')
                            </label>
                        </div>
                        <div class="col-sm-9 d-flex align-items-center">
                            <div id="detail-certificate-required-flag" name="detail-certificate-required-flag"></div>
                        </div>
                    </div>
                </div>
                <div class="form-group px-3 mb-0">
                    <div class="row border-top-0">
                        <div class="col-sm-3 d-flex align-items-center modal-label">
                            <label for="detail-update-date">
                                @lang('agreement_admin.update_date')
                            </label>
                        </div>
                        <div class="col-sm-9 d-flex align-items-center">
                            <div id="detail-update-date" name="detail-update-date"></div>
                        </div>
                    </div>
                </div>
                <div class="form-group px-3 mb-0">
                    <div class="row border-top-0">
                        <div class="col-sm-3 d-flex align-items-center modal-label">
                            <label for="detail-update-user-id">
                                @lang('agreement_admin.update_user_id')
                            </label>
                        </div>
                        <div class="col-sm-9 d-flex align-items-center">
                            <div id="detail-update-user-id" name="detail-update-user-id"></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
