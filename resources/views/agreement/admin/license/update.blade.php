<div class="modal modal-global fade" id="update-dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    @lang('agreement_admin.update_license_information')
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>

            <form id="update-license-form">
                <div class="modal-body">
                    <div class="form-group px-3 mb-sm-0">
                        <div class="row">
                            <div class="col-sm-3 d-flex align-items-center modal-label">
                                <label for="update-license-id">
                                    @lang('agreement_admin.license_id')
                                </label>
                            </div>
                            <div class="col-sm-9 d-flex align-items-center">
                                <div id="update-license-id" name="update-license-id"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group px-3 mb-sm-0">
                        <div class="row">
                            <div class="col-sm-3 d-flex align-items-center modal-label">
                                <label for="update-license-name">
                                    @lang('agreement_admin.license_name') *
                                </label>
                            </div>
                            <div class="col-sm-9 d-flex align-items-center">
                                <input type="text" id="update-license-name" name="update-license-name"
                                       data-rule-required="true" data-rule-maxlength="200"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group px-3 mb-sm-0">
                        <div class="row">
                            <div class="col-sm-3 d-flex align-items-center modal-label">
                                <label for="update-certificate-required-flag">
                                    @lang('agreement_admin.certificate_required_flag')
                                </label>
                            </div>
                            <div class="col-sm-9 d-flex align-items-center">
                                <input type="checkbox" id="update-certificate-required-flag" name="update-certificate-required-flag"/>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <div class="modal-footer justify-content-center p-4">
                <button type="button"
                        class="btn btn--gradient-green w-45 text-white"
                        id="update-license-button">
                    @lang('agreement_admin.btn_update')
                </button>
                <button type="button"
                        class="btn btn--gradient-default border w-45 text-secondary"
                        data-dismiss="modal">
                    @lang('agreement_admin.btn_cancel')
                </button>
            </div>

        </div>
    </div>
</div>
