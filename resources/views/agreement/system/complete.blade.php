@extends('layouts.app')

@section('content')
    <div class="agreement-system">
        <div class="text-center mb-4">
            <h3>
                @lang('agreement_system.complete_note')
            </h3>
            @lang('agreement_system.complete_register')
        </div>

    </div>
@endsection
@section('script')
@endsection

