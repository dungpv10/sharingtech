@extends('layouts.app')

@section('content')
    <section class="agreement-system">
        @include('agreement.system.progress')
        <h2>@lang('agreement_system.contract_contents')</h2>
        <form id="confirmFormId" method="post" action="{{route('agreementSystem.postConfirm')}}">
            {{ csrf_field() }}
            @include('agreement.system.confirm_agreement')
            <br/><br/><br/><br/>
            @include('agreement.system.confirm_base_information')
            <br>
            @include('agreement.system.confirm_area')
            <br>
            @include('agreement.system.confirm_category')
            <br><br><br>
            @include('agreement.system.confirm_revise')
            <div>
                <input id="acceptedCheck" type="checkbox"/> @lang('agreement_system.i_agree_above_content')
            </div>
            <div align="center">
                <button id="back_button" class="btn btn--gradient-default btn-lg"
                        type="button">@lang('agreement_system.btn.return')</button>
                <button id="btnApplicationId" class="btn btn--gradient-default btn-lg"
                        type="button">@lang('agreement_system.btn_application')</button>
            </div>
        </form>
        @include('agreement.system.area_dialog')
    </section>
@endsection
@section('script')
    <script>
        var urlBackConfirm = '{{route('agreementSystem.back.getConfirm')}}';
        var alertConfirmAgreement = '@lang('agreement_system.alert_confirm_agreement')';
    </script>
    <script src="{{ mix('js/pages/step_confirm_agreement_system.js')}}"></script>
    <script src="{{ mix('js/pages/step3_agreement_system.js')}}"></script>
    <script>
        jQuery(document).ready(function () {
            StepConfirmAgreementSystem.init();
            Step3AgreementSystem.onViewConfigurationArea();
        });
    </script>
@endsection
