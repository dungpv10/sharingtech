@extends('layouts.app')

@section('content')
    <div class="autocommission-corp_add">
        <input type="hidden" value="{{csrf_token()}}" id="csrf-token" name="token">
        <a class="link-orange"
           href="{{action('Auction\AuctionController@index')}}">{{__('auto_commission_corp.link_to_auto_commission_corp')}}</a>
        <label class="form-category__label mt-2"> {{__('auto_commission_corp.label_view_selection')}} </label>
        <div class="row">
            <div class="col-12 text-sm-center py-2 bg--yellow-light">{{__('auto_commission_corp.note_view_selection')}}</div>
        </div>
        <div class="row">
            <div class="col-sm-6 text-sm-center py-2 bg--yellow-light">{{__('auto_commission_corp.genre')}}</div>
            <div class="col-sm-6 py-2">
                {!! Form::select('genre_name', $data['listGenre'], null,['class' => 'col-sm-3 col-lg-3', 'id' =>
                'genre_id'])!!}
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 text-sm-center py-2 bg--yellow-light">{{__('auto_commission_corp.category')}}</div>
            <div class="col-sm-6 py-2">
                {!! Form::select('category_name[]',[], null,['class' => 'col-sm-3 col-lg-3', 'id' => 'category_id',
                'multiple' => true]) !!}
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 text-sm-center py-2 bg--yellow-light">{{__('auto_commission_corp.prefectures')}}</div>
            <div class="col-sm-6 py-2">
                {!! Form::select('perf_name', $data['listPref'], null,['class' => 'col-sm-3 col-lg-3', 'id' =>
                'perf_id', 'multiple' => true])!!}
            </div>
        </div>
        <div id="selection-1" class="row p-2">
            <div class="col-xl-6 py-1 header">
                <label class="col-sm-5 col-form-label font-weight-bold">{{__('auto_commission_corp.list_transaction')}}</label>
                <button class="btn col-sm-3 px-0 py-1"
                        id="commission_to_selected">{{__('auto_commission_corp.move_to_selection')}}</button>
                <button class="btn col-sm-3 px-0 py-1"
                        id="commission_to_unselected">{{__('auto_commission_corp.move_to_unselected')}}</button>
            </div>
            <div class="col-xl-6 py-1 header">
                <label class="col-sm-3 col-form-label">{{__('auto_commission_corp.change_priority')}}:</label>
                <button class="btn col-sm-4 px-0 py-1 up-priority"><i class="fa fa-arrow-circle-up"
                                                                      aria-hidden="true"></i>
                    {{__('auto_commission_corp.upward')}}
                </button>
                <button class="btn col-sm-4 px-0 py-1 down-priority"><i class="fa fa-arrow-circle-down"
                                                                        aria-hidden="true"></i>
                    {{__('auto_commission_corp.downward')}}
                </button>
            </div>
            <select class="col-12 border-top-0 body" id="list_corp_automatic" title="" multiple
                    name="commissionCorp[]"></select>
        </div>
        <div id="selection-2" class="row p-2">
            <div class="col-xl-6 py-1 header">
                <label class="col-sm-5 col-form-label font-weight-bold">{{__('auto_commission_corp.auto_selection')}}</label>
                <button class="btn col-sm-3 px-0 py-1"
                        id="selected_to_commission">{{__('auto_commission_corp.move_to_selection')}}</button>
                <button class="btn col-sm-3 px-0 py-1"
                        id="selected_to_unselected">{{__('auto_commission_corp.move_to_unselected')}}</button>
            </div>
            <div class="col-xl-6 py-1 header">
                <label class="col-sm-3 col-form-label">{{__('auto_commission_corp.change_priority')}}:</label>
                <button class="btn col-sm-4 px-0 py-1 up-priority"><i class="fa fa-arrow-circle-up"
                                                                      aria-hidden="true"></i>
                    {{__('auto_commission_corp.upward')}}
                </button>
                <button class="btn col-sm-4 px-0 py-1 down-priority"><i class="fa fa-arrow-circle-down"
                                                                        aria-hidden="true"></i>
                    {{__('auto_commission_corp.downward')}}
                </button>
            </div>
            <select class="col-12 border-top-0 body" id="list_corp_selected" title="" multiple
                    name="selectedCorp[]"></select>
        </div>
        <div id="selection-3" class="row p-2">
            <div class="col-xl-6 py-1 header">
                <label class="col-sm-5 col-form-label font-weight-bold">{{__('auto_commission_corp.unselected_list')}}</label>
                <button class="btn col-sm-3 px-0 py-1"
                        id="unselected_to_commission">{{__('auto_commission_corp.move_to_selection')}}</button>
                <button class="btn col-sm-3 px-0 py-1"
                        id="unselected_to_selected">{{__('auto_commission_corp.move_to_unselected')}}</button>
            </div>
            <div class="col-xl-6 py-1 header"></div>
            <select class="col-12 border-top-0 body" id="list_corp_unselected" title="" multiple
                    name="unselectedCorp[]"></select>
            <div class="col-sm-4 col-lg-2 px-0 py-1">
                <select class="custom-select" title="" id="select_search_type">
                    <option selected value="0">{{__('auto_commission_corp.search_by_name')}}</option>
                    <option value="1">{{__('auto_commission_corp.search_by_id')}}</option>
                </select>
            </div>
            <div class="offset-lg-10 offset-sm-9"></div>
            <div class="col-xl-8 px-0 py-1">
                <input type="text" class="form-control" id="text_search_input"
                       placeholder="{{__('auto_commission_corp.placeholder_input_search')}}" aria-label="placeholder">
            </div>
            <div class="offset-xl-4"></div>
            <button class="btn btn--gradient-default px-3 py-1"
                    id="btn_get_corp">{{__('auto_commission_corp.get_corp')}}</button>
            <div id="corp_search_message"><span id="ajax_message" class="ajax_none"></span></div>
        </div>
        <div class="row">
            <div class="col-sm-6 py-2 text-center text-sm-right">
                <button class="btn btn--gradient-default py-1 px-5"
                        id="btnBackIndex">{{__('auto_commission_corp.return')}}</button>
            </div>
            <div class="col-sm-6 py-2 text-center text-sm-left">
                <button class="btn btn--gradient-default py-1 px-5" id="btn_register_corp"
                        data-url-editcrop="{{route('autoCommissionCorp.cropSelectRegister')}}">
                    {{__('auto_commission_corp.submit')}}
                </button>
            </div>
        </div>
        <div id="page-data" data-text-selectall="{{__('auto_commission_corp.select_all')}}"
             data-text-unselectall="{{__('auto_commission_corp.unselect_all')}}"
             data-text-none-select="{{__('auto_commission_corp.none')}}"
             data-ajax-load="{{__('auto_commission_corp.ajax_message_loading')}}"
             data-ajax-success="{{__('auto_commission_corp.ajax_message_success')}}"
             data-ajax-fail="{{__('auto_commission_corp.ajax_message_fail')}}"
             data-url-category="{{route('autoCommissionCorp.getCategoryByGenreId')}}"
             data-url-searchcorp="{{route('autoCommissionCorp.searchCorpByCatePref')}}"
             data-url-back="{{route('autoCommissionCorp.index')}}"
             data-url-listcorp="{{route('autoCommissionCorp.getListCorpByGenreCatePref')}}">
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ mix('js/lib/jquery.multiselect.js') }}"></script>
    <script src="{{ mix('js/lib/jquery.multiselect.filter.js') }}"></script>
    <script src="{{ mix('js/pages/auto_commission_corp_select.js') }}"></script>
@endsection()