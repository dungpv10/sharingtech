@if($isRoleAffiliation)
    @if($isMobile)
        <div id="search-cal-box">
            <div class="d-table-cell w-20 align-middle calendar-directional">
                <div id="cal-go-prev" class="text-center">
                    <span class="fa fa-chevron-left"></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div id="cell-cal">
                    </div>
                </div>
            </div>
            <div class="d-table-cell w-20 align-middle calendar-directional">
                <div id="cal-go-next" class="text-center">
                    <span class="fa fa-chevron-right"></span>
                </div>
            </div>
        </div>
        @else
        <div id="search-cal-box">
            <div class="d-table-cell w-20 align-middle calendar-directional">
                <div id="cal-go-prev" class="text-center">
                    <span class="fa fa-chevron-left"></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div id="cell-cal1">
                    </div>
                </div>
                <div class="col-md-4">
                    <div id="cell-cal2">
                    </div>
                </div>
                <div class="col-md-4">
                    <div id="cell-cal3">
                    </div>
                </div>

            </div>
            <div class="d-table-cell w-20 align-middle calendar-directional">
                <div id="cal-go-next" class="text-center">
                    <span class="fa fa-chevron-right"></span>
                </div>
            </div>
        </div>
        @endif
@endif
