<div class="table-responsive">
    <table class="table table-list table-bordered">
        <thead>
        <tr>
            <th  class="text-center font-weight-bold w-20">@lang("daily_list.col1")</th>
            <th class="text-center font-weight-bold">@lang("daily_list.col2")</th>
        </tr>
        </thead>
        <tbody>
        @foreach($files as $key => $file)
            <tr>
                <td class="text-center">{{$key + 1}}</td>
                <td>
                    <a href="{{$file["path"]}}">{{$file["filename"]}}</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>