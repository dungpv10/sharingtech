
<div class="col-12 p-0">
    <div class="row header-field mb-3">
        <div class="col-12">
            <strong class="pull-left mr-4"> 案件ID: </strong>
            <strong class="pull-left mr-4">案件状況: </strong>
            <strong class="pull-right">閲覧数：0件</strong>
        </div>
    </div>
    <div class="d-flex justify-content-end mb-3">
        <button class="btn btn--gradient-green btn--w-normal ml-4">登録</button>
    </div>
    @if(Session::has('message'))
    <div class="row header-field my-4 border-2 border border-warning">
        <div class="col-12">
            <strong style="color: #f27b07"> {{ Session::get('message') }} </strong>
        </div>
    </div>
    @endif

    @if(session('error'))
            <p class="alert alert-danger my-2">{{ session('error') }}</p>
    @endif
    @if(Session::has('error_msg_input'))
            <p class="alert alert-danger my-2">{{ Session::get('error_msg_input') }}</p>
    @elseif ($errors->any() || Session::get('demand_errors'))
            <p class="alert alert-danger my-2">{{ __('demand.error_miss_input') }}</p>
    @endif
    <div class="d-flex justify-content-end mb-3">
        <a href="#commissioninfo" class="text--orange text--underline ml-4">▼取次先情報 </a>
        <a href="#jbrdemandinfo" class="text--orange text--underline ml-4">▼JBR様案件情報 </a>
        <a href="#demandstatus" class="text--orange text--underline ml-4">▼案件状況 </a>
        <a href="#introductioninfo" class="text--orange text--underline ml-4">▼紹介先情報 </a>
        <a href="#correspondsinfo" class="text--orange text--underline ml-4">▼対応履歴情報</a>
    </div>
</div>
