<div class="modal modal-global fade dialog-modal" id="modal-dialog" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3>入力エラー</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer justify-content-center">
                <button type="button" class="btn btn--gradient-default" data-dismiss="modal">閉じる</button>
            </div>
        </div>
    </div>
</div>
