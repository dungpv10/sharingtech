<div class="form-category mb-4" id="correspondsinfo">
    @include('demand.create.anchor_top')
    <label class="form-category__label">対応履歴情報</label>
    <div class="form-category__body clearfix">
        <div class="form-table mb-4">
            <div class="row mx-0 border ">
                <div class="col-12 col-lg-6 row m-0 p-0">
                    <div class="col-12 col-lg-6 px-0">
                        <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                            <label class="m-0">
                                <strong>対応者</strong>
                            </label>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 py-2">
                        {!! Form::select('demandCorrespond[responders]', $userDropDownList, Auth::user()->id, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-12 col-lg-6 row m-0 p-0">
                    <div class="col-12 col-lg-6  px-0">
                        <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                            <label class="m-0">
                                <strong>対応日時</strong>
                            </label>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 py-2">
                        <div class="form-group d-flex justify-content-around align-items-center mb-lg-0">
                            {!! Form::text('demandCorrespond[correspond_datetime]', dateTimeNowFormat(), ['class' => 'form-control datetimepicker']) !!}
                        </div>
                    </div>

                </div>
            </div>
            <div class="row mx-0 border ">
                <div class="col-12 row m-0 p-0">
                    <div class="col-12 col-lg-3 px-0">
                        <div class="form__label form__label--white-light p-3 h-100 border-bottom">
                            <label class="m-0">
                                <strong>対応内容</strong>
                            </label>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 py-2">
                        {!! Form::textarea('demandCorrespond[corresponding_contens]', '初回登録', ['class' => 'form-control']) !!}
                        @if ($errors->has('demandCorrespond.corresponding_contens'))
                        <label class="invalid-feedback d-block">{{$errors->first('demandCorrespond.corresponding_contens')}}</label>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="table-responsive">

            <table class="table table-list table-bordered" >
                <thead>

                <th align="center" width="50px">No</th>
                <th align="center" width="100px">担当者</th>
                <th align="center" width="120px">対応日時</th>
                <th align="left">対応内容</th>

                </thead>
                <tbody>
                </tbody>
            </table>



        </div>
    </div>
</div>
