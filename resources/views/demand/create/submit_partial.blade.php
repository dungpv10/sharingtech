<div class="row justify-content-center">
    <div class="col-12 col-lg-3">
        <button class="btn btn--gradient-green w-100">登録</button>
        <div class="custom-control mx-auto custom-checkbox my-1 ">
            {!! Form::checkbox('not-send', 1, false, ['class' => 'custom-control-input', 'id' => 'not-send']) !!}
            <label class="custom-control-label" for="not-send">メール/FAX送信不要</label>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <h5 class="m-0 text-right text-dark"><strong>閲覧数：0件</strong></h5>
    </div>
</div>
