<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <td>所在地</td>
                    <td>
                        {{ $mCorp->address1_jp  }}{{ $mCorp->address2 }}{{ $mCorp->address3 }}
{{--                        {{ $mCorp->address3 != '' && $mCorp->address4 != '' && strpos($mCorp->address3, $mCorp->address4) ? '' : $mCorp->address4 }}--}}
                    </td>
                </tr>

                <tr>
                    <td>FAX番号</td>
                    <td>
                        {{ $mCorp->fax }}
                    </td>
                </tr>
                <tr>
                    <td>PCメール</td>
                    <td>

                        @foreach($mCorp->email_by_array as $mail)
                            <a href="callto:{{ $mail }}">{{ $mail }}</a> <br/>
                        @endforeach
                    </td>
                </tr>

                <tr>
                    <td>顧客情報連絡手段</td>
                    <td>
                        {{ $mCorp->text_coordination }}
                    </td>
                </tr>
                <tr>
                    <td>連絡可能時間</td>
                    <td>
                        @if($mCorp->contactable_support24hour)
                            24時間
                        @else
                            {{ $mCorp->contactable_time_from . ' - ' . $mCorp->contactable_time_to }}
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>休業日</td>
                    <td>
                        {{ $mCorp->holiday1 }}
                    </td>
                </tr>
                <tr>
                    <td>受注手数料</td>
                    <td>
                        {!! $feeCommission !!}
                    </td>
                </tr>

                <tr>
                    <td>受注メモ</td>
                    <td>
                        {!! !empty($feeData->note) ? $feeData->note : ''  !!}
                    </td>
                </tr>

            </tbody>
        </table>
    </div>
</div>
