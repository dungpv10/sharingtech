@section('script')
    <script src="{{ mix('js/lib/jquery-ui-timepicker-addon.js') }}"></script>
    <script src="{{ mix('js/lib/jquery.multiselect.js') }}"></script>
    <script src="{{ mix('js/lib/jquery.multiselect.filter.js') }}"></script>
    <script src="{{ mix('js/pages/demand_detail_header.js') }}"></script>
    <script src="{{ mix('js/utilities/st.common.js') }}"></script>
    <script src="{{ mix('js/pages/demands/process.js') }}"></script>

    <script>
        $(document).ready(function () {
            var copyDemandUrl = "{{ route('demand.detail.copy', ['demandId' => $demand->id]) }}";
            processDemandModule.copyDemand(copyDemandUrl);
            var crossDemandUrl = "{{ route('demand.detail.cross', ['demandId' => $demand->id]) }}";
            processDemandModule.crossDemand(crossDemandUrl);
        });
    </script>

    <script src="{{ mix('js/pages/demands/template.js') }}"></script>

    <script>
        let initTime = function () {
            $('.datetimepicker').datetimepicker({
                controlType: 'select',
                oneLine: true,
                timeText: '時間',
                hourText: '時',
                minuteText: '分',
                currentText: '現時刻',
                closeText: '閉じる',
                locale: 'ja'
            });
            $(".multiple_check_filter").multiselect({
                minWidth: 200,
                selectedList: 50,
                checkAllText: "全選択",
                uncheckAllText: "選択解除",
                noneSelectedText: "--なし--",
                multiple: false,
            }).multiselectfilter({
                label: '',
                width: 95
            });
        };
        initTime();

        const uploadAttachedFileUrl = "{{ route('demand.upload_attached_file', $demand->id) }}";
        const deleteAttachedFileUrl = "{{ route('demand.delete_attached_file') }}";

        const getInquiryItemDataUrl = "{{ route('ajax.get_inquiry_item_data') }}";
        const getCategoryListByGenreIdUrl = "{{ route('ajax.get_category_by_genre_id') }}";
        const getGenreListBySiteId = "{{ route('ajax.get_genre_list_by_site_id') }}";
        const getSiteDataUrl = "{{ route('ajax.demand.site_data') }}";
        const getSelectionSystemList = "{{ route('ajax.demand.selection_system_list') }}";
        const getAddressByZipUrl = "{{ route('ajax.searchAddressByZip') }}";
        const getBusinessTripMountUrl = "{{ route('ajax.travel_expenses') }}";
        const getUserListUrl = "{{ route('demand.get_user_list') }}";
        const getDefaultFeeUrl = "{{ route('demand.get_default_fee') }}";

        const postCountBrowseUrl = "{{ route('ajax.demand.count_browse', $demand->id) }}";
        const getWriteBrowseUrl = "{{ route('ajax.demand.write_browse', $demand->id) }}";


        const currentUserId = "{{ Auth::user()->id }}";
    </script>

    <script>

        const triggerSectionFiles = $('.trigger-section-file');
        const btnSubmitAttachedFile = $('#submit-form-attached-file');
        const btnDeleteAttachedFile = $('.btn-delete-attached-file');
        const btnResetAttachedFile = $('.reset-default-attached-file');


        const demandDetailObj = {
            addMinutes: function (minutes = 15) {
                const now = new Date();
                now.setMinutes(now.getMinutes() + minutes);
                let hours = now.getHours();
                let min = now.getMinutes();
                let time = this.dateNowFormat() + ' ' + this.toDoubleDigits(hours) + ':' + this.toDoubleDigits(min);
                contactDesiredTime.val(time);
            },

            toDoubleDigits: function (myNumber) {
                return ("0" + myNumber).slice(-2);
            },

            middleNightCheckbox: function (checkbox) {
                let hours = (new Date()).getHours();
                if (hours >= 21 || hours < 7) {
                    $(checkbox).prop('checked', true);
                }
            },

            getUserList: function () {
                return this.callAjaxApi('get', getUserListUrl, {}, (response) = > {
                    let chainHtmlNoteSender = this.renderHtml(response.data);
                let chainHtmlApointers = this.renderHtml(response.data, true, currentUserId);

                $('.commission_note_sender').html(chainHtmlNoteSender);
                $('.appointers').html(chainHtmlApointers);
                initTime();
            })
                ;
            },

            changeSiteId: function () {
                let siteId = siteElm.val();
                this.callAjaxApi('get', getGenreListBySiteId, {site_id: siteId}, (response) = > {
                    let genresList = response.data;
                let chainHtml = this.renderHtml(genresList);
                genreIdElm.html(chainHtml);
                visitTimeDiv.css({display: 'block'});
                hiddenTime.css({display: 'block'});
                if (listSiteId.indexOf(parseInt(siteId)) !== -1) {
                    crossSellSourceSite.attr('disabled', false);
                    crossSellSourceGenre.attr('disabled', false);
                } else {
                    crossSellSourceSite.attr('disabled', true);
                    crossSellSourceGenre.attr('disabled', true);
                }
            })
                ;
                this.getCategoryList();
                this.getSiteData(siteId);
            },
            getCategoryList: function () {
                this.callAjaxApi('get', getCategoryListByGenreIdUrl, {genreId: null}, (response) = > {
                    let categories = response.data;
                let chainHtml = this.renderHtml(categories);
                categoryElm.html(chainHtml);
            })
                ;
            },
            renderHtml: function (items, optionDefault = true, defaultSelected = '') {
                let chainHtml = optionDefault ? `<option value="">--なし--</option>` : '';
                for (let i in items) {
                    let category = items[i];
                    chainHtml += defaultSelected === parseInt(i) ? `<option selected value=${i}>${category}</option>` : `<option value=${i}>${category}</option>`;
                }
                return chainHtml;
            },

            changeCategory: function () {

            },

            resetRadio: function (btnRest) {
                btnRest.parents('.group-radio-pet-tomb').find('input[type="radio"]').prop('checked', false);
            },

            submitUploadAttachFile: function (btn, url) {
                $(btn).parents('form').attr('action', url).submit();
            },

            deleteAttachedFile: function (btnDelete) {
                this.callAjaxApi('post', deleteAttachedFileUrl, {attached_id: $(btnDelete).data('attached_id')}, function (response) {
                    $(btnDelete).parents('tr').remove();
                });
            },

            resetAttachedFileValue: function (btnReset) {
                let parent = $(btnReset).parent().prev('.trigger-section-file');
                let file = parent.find('input[type="file"]');
                file.replaceWith(file.val('')).clone(true);
                parent.find('.reset-file-name').html('No file choosen');
            },

            enableTextBox: function (radioElm, enableElm, disabledElm) {
                if ($(radioElm).is(':checked')) {
                    let parent = $(radioElm).parents('.py-2');
                    parent.find('.' + enableElm).val('').attr('disabled', false);
                    parent.find('.' + disabledElm).val('').attr('disabled', true);
                }
            },

            changeGenreSelection: function (genreId, renderElm = $('#category_id')) {
                let _this = this;
                this.callAjaxApi('get', getCategoryListByGenreIdUrl, {genreId: genreId}, (response) = > {
                    let categories = response.data;
                let chainHtml = this.renderHtml(categories);
                renderElm.html(chainHtml);
                renderElm.trigger('change');
                let siteId = $('#site_id').val();
                let beforeDemand = $('#before_demandinfo_genre_id');
                let sourceDemandId = $('#source_demand_id');

                _this.callAjaxApi('get', getInquiryItemDataUrl, {genreId: genreId}, function (response) {
                    let cross_contents = response.data.inquiry_item;
                    if (siteId === 861) {
                        let beforeDemandInfoGenreId = beforeDemand.val();
                        if (genreId !== '' && beforeDemandInfoGenreId !== '') {
                            let cross_contents = "クロスセル案件番号：" + sourceDemandId + " ジャンル：" + genreIdElm.find('option:selected').text() + "　同じ加盟店を希望or違う加盟店でもよい";
                            beforeDemand.val(genreId);
                            $("#demand-content").val(cross_contents);
                        }
                    }
                    $("#demand-content").val(cross_contents);

                    if (genreId !== '') {
                        _this.callAjaxApi('get', getInquiryItemDataUrl, {genreId: genreId}, function (response) {
                            $('#attention').val(response.data ? response.data.attention : '');
                        })
                    }
                    beforeDemand.val(genreId);
                });
            })
                ;
            },

            getInquiryItemData: function () {

            },

            triggerFile: function (section) {
                section.find('input[type="file"]').trigger('click');
            },

            writeBrowse: function () {
                this.callAjaxApi('get', getWriteBrowseUrl, {}, (response) => {
                    demandDetailObj.countBrowse();
            })
                ;
            },

            countBrowse: function () {
                this.callAjaxApi('post', postCountBrowseUrl, {}, function (response) {
                    $('.total-current-views').html(response.data);
                });
            },

            getSiteData: function (siteId) {
                this.getSiteDataByContent(siteId, 1, (response) = > {
                    const url = response.data ? response.data.site_url : '';
                $('#site_url').html(url).attr('href', 'http://' + url);
            })
                ;
                this.getSiteDataByContent(siteId, 2, (response) = > {
                    if(response.data
            )
                {
                    const commissionTypeName = response.data.m_commission_type ? response.data.m_commission_type.commission_type_name : '';
                    $('#commission_type_data').html(commissionTypeName);
                    $('#commission_type_data_hidden').val(commissionTypeName);
                    $('#commission_type_div').val(response.data.m_commission_type.commission_type_div);
                    if (response.data.m_commission_type.commission_type_name !== 1) {
                        $('#agency_info').css({display: 'block'});
                    }
                }
            })
                ;
            },

            getSiteDataByContent: function (siteId, contentType, callback) {
                return this.callAjaxApi('get', getSiteDataUrl, {
                    site_id: siteId,
                    content: contentType
                }, (response) = > callback(response)
            )
                ;
            },

            getSelectionSystemList: function (genreId, address1) {
                this.callAjaxApi('get', getSelectionSystemList, {
                    genre_id: genreId,
                    address1: address1
                }, (response) = > {
                    const selectionType = response.data.selection_system;
                const defaultValue = response.data.default_value;

                selectionSystemElm.html(this.renderHtml(selectionType, false, defaultValue));
                selectionSystemElm.trigger('change');

                let siteId = siteElm.val();
                let selectSystemValue = selectionSystemElm.val();
                if (siteId === 861) {
                    if (selectSystemValue.size() > 0) {
                        selectionSystemElm.val(selectSystemValue);
                    }
                }

                if (defineListSiteIds.indexOf(parseInt(siteId)) !== -1) {
                    selectionSystemElm.find('option').each(function (index, option) {
                        if ($(this).is(':selected')) {
                            $(this).removeAttr('selected');
                        }
                        if ($(this).text() === '入札式+自動') {
                            $(this).css('display', 'none');
                        }
                    });
                }
            })
                ;
            },

            callAjaxApi: function (method, url, data = {}, successCallback = (response) = > {},
            errorCallback = (error) = > {}
        )
        {
            $.ajax({type: method, url: url, data: data, success: successCallback});
        }
        ,

        //get_business_trip_amount
        travelExpress : function (genreId, address) {
            this.callAjaxApi('get', getBusinessTripMountUrl, {
                genre_id: genreId,
                address: address
            }, function (response) {
                businessTripMount.val(response.data.business_trip_amount);
            });
        }
        ,
        searchAddressByZip: function (zip) {
            this.callAjaxApi('get', getAddressByZipUrl, {zip: zip}, (response) = > {
                if(
            !$.isEmptyObject(response)
        )
            {
                let genId = genreIdElm.val(), address = address1Elm.val();
                this.travelExpress(genId, address);
                address1Elm.val(parseInt(response.m_posts_jis_cd));
                address1Elm.trigger('change');
                address2Elm.val(response.address2);
                address3Elm.val(response.address3);

                if (businessTripMount.val() === '') return;
                const genreId = genreIdElm.val();
                const address1 = address1Elm.val();

                if (genreId !== '' && address1 !== '') {
                    this.travelExpress(genreId, address1);
                }

                if (selectionSystemElm.val() === '') return;
                this.getSelectionSystemList(genreId, address1);
            }
        })
            ;
        }
        ,
        changeOrderFailReason: function (statusObj) {
            let demandStatus = parseInt($(statusObj).val());
            if ([6, 9].indexOf(demandStatus) !== -1) {
                selectionSystemElm.val(0);
                const genreId = genreIdElm.val();
                const address1 = address1Elm.val();
                this.travelExpress(genreId, address1);
            }

            if (demandStatus === 6) {
                orderFailReason.attr('disabled', false);
                let orderFailDateVal = orderFailDate.val();
                if (orderFailDateVal === '') {
                    orderFailDate.val('' + this.dateNowFormat() + '');
                }
            } else {
                orderFailReason.attr('disabled', true);
                orderFailDate.val('');
            }


        }
        ,
        dateNowFormat()
        {
            let date = new Date();
            let dd = date.getDate();
            let mm = date.getMonth() + 1;
            let yyyy = date.getFullYear();
            dd = dd < 10 ? '0' + dd : dd;
            mm = mm < 10 ? '0' + mm : mm;
            return yyyy + '/' + mm + '/' + dd;
        }
        ,

        showDialog : function (header, content) {
            modalDialog.find('h3').html(header);
            modalDialog.find('.modal-body').html(content);
            modalDialog.modal({show: true});
        }
        ,

        quickOrderFail: function () {
            let quickOrderFail = quickOrderFailReason.val(), genreId = genreIdElm.val(), categoryId = categoryElm.val();
            if (quickOrderFail === '' || genreId === '' || categoryId === '') {
                this.showDialog('入力エラー', 'ワンタッチ失注登録理由,ジャンル,カテゴリを選択して下さい。');
                return false;
            }
            $('#hidQuickOrderFail').val('1');
            $(window).off('beforeunload');
            categoryElm.parents('form').submit();
        }
        ,
        getDefaultFee(callback)
        {
            this.callAjaxApi('GET', getDefaultFeeUrl, {category_id: categoryElm.val()}, function (response) {
                callback(response.data.default_fee);
            });
        }
        ,

        removeDateTimePicker(elementClass, elementExcept)
        {
            $(elementClass).not(elementExcept).val('');
        }
        }
        ;

        demandDetailObj.middleNightCheckbox(middleNightCheckbox);

        demandDetailObj.getSiteData(siteElm.val());

        destinationCompanyBtn.click(function (e) {
            e.preventDefault();
            let urlData = $(this).data('url_data').split('?')[0];
            let localStorage = window.localStorage;
            let mCorpsArr = $.makeArray(JSON.parse(localStorage.getItem('m_corps')));
            let m = $.map(mCorpsArr, function (m_corp, index) {
                return JSON.parse(m_corp).corp_id;
            });

            let mCorpsId = m.join(',');

            let queryString = $.param({
                'data[no]': -1,
                'data[site_id]': siteElm.val(),
                'data[category_id]': categoryElm.val(),
                'data[postcode]': postcodeElm.val(),
                'data[address1]': address1Elm.val(),
                'data[address2]': address2Elm.val(),
                'data[corp_name]': '',
                'data[commition_info_count]': 0,
                'data[exclude_corp_id]': mCorpsId,
                'data[genre_id]': genreIdElm.val(),
                'data[view]': true
            });

            $(this).data('url_data', [urlData, queryString].join('?'));
            let address1 = address1Elm.val(), category = categoryElm.val(), address2 = address2Elm.val();
            if (address1 === '' || address1 === '--なし--' || category === '' || (address1 !== '不明' && address2 === '')) {
                demandDetailObj.showDialog('', '都道府県、市区町村、カテゴリを選択してください。');
                return false;
            }
        });


        btnQuickOrderFail.click(function (e) {
            e.preventDefault();
            demandDetailObj.quickOrderFail();
        });

        demandStatusElm.change(function (e) {
            e.preventDefault();
            demandDetailObj.changeOrderFailReason(this);
        });
        address1Elm.change(function () {
//Change when searchAddress By zip code
        });

        demandDetailObj.writeBrowse();

        demandDetailObj.getSiteData(siteElm.val());


        btnSearchAddressByZip.click(function (e) {
            e.preventDefault();
            let postcode = encodeURI(postcodeElm.val());
            demandDetailObj.searchAddressByZip(postcode);
        });

        categoryElm.change(function (e) {
            demandDetailObj.changeCategory();
        });

        btnResetRadio.click(function (e) {
            e.preventDefault();
            demandDetailObj.resetRadio($(this));
        });
        modalPopup.on('show.bs.modal', function (e) {
            let buttonTrigger = $(e.relatedTarget);
            let modal = $(this);
            modal.find('.modal-body').load(buttonTrigger.data('url_data'));
        });

        triggerSectionFiles.each(function (index, element) {
            $(element).find('button').click((e) => {
                e.preventDefault();
            demandDetailObj.triggerFile($(this));
        });
        });

        $('input[type="file"]').change(function (e) {
            e.preventDefault();
            let fileName = $(this).val().toString().split("\\").pop();
            $(this).next().next().html(fileName);
        });

        btnSubmitAttachedFile.click(function (e) {
            e.preventDefault();
            demandDetailObj.submitUploadAttachFile(this, uploadAttachedFileUrl);
        });

        btnDeleteAttachedFile.click(function (e) {
            e.preventDefault();
            demandDetailObj.deleteAttachedFile(this);
        });

        btnResetAttachedFile.click(function (e) {
            e.preventDefault();
            demandDetailObj.resetAttachedFileValue(this);
        });

        radioAbsoluteTime.click(function (e) {
            demandDetailObj.enableTextBox(this, 'txt_absolute_time', 'txt_range_time');
        });

        radioRangeTime.click(function () {
            demandDetailObj.enableTextBox(this, 'txt_range_time', 'txt_absolute_time');
        });

        genreIdElm.change(function (e) {
            e.preventDefault();
            let genreId = $(this).val();
            let address1 = $('#address1').val();
            demandDetailObj.changeGenreSelection(genreId);
            demandDetailObj.getSelectionSystemList(genreId, address1);
        });

        btnPlus15Minutes.click(function (e) {
            e.preventDefault();
            demandDetailObj.addMinutes();
        });

    </script>
@stop
