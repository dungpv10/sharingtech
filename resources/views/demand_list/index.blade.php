@extends('layouts.app')
@section('style')
    <link href="{{ mix('css/lib/dataTables.bootstrap4.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="demand-list">
        <form id="demand_info" class="form-horizontal fieldset-custom" method="POST"
              action="{{ route('demandlist.search') }}">
            {{ csrf_field() }}
            <fieldset>
                <legend class="fs-13">{{ trans('demandlist.search_condition') }}</legend>
                <div class="form-container bg-update-box border-update-box p-2">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-inline mb-2">
                                <label class="col-12 col-sm-3 col-md-3 col-lg-4"
                                       for="corp_name">{{ trans('demandlist.company_name') }}</label>
                                <div class="col-12 col-sm-9 col-md-9 col-lg-8 p-0">
                                    {{Form::input('text', 'data[corp_name]', isset($conditions['corp_name']) ? $conditions['corp_name'] : null, ['id' => 'corp_name', 'class' => 'form-control w-100'])}}
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-inline mb-2">
                                <label class="col-12 col-sm-3 col-md-3 col-lg-4"
                                       for="corp_name_kana">{{ trans('demandlist.company_name_furigana') }}</label>
                                <div class="col-12 col-sm-9 col-md-9 col-lg-8 p-0">
                                    {{Form::input('text', 'data[corp_name_kana]', isset($conditions['corp_name_kana']) ? $conditions['corp_name_kana'] : null, ['id' => 'corp_name_kana', 'class' => 'form-control w-100'])}}
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-inline mb-2">
                                <label class="col-12 col-sm-3 col-md-3 col-lg-4"
                                       for="customer_tel">{{ trans('demandlist.customer_phone_number') }}</label>
                                <div class="col-12 col-sm-9 col-md-9 col-lg-8 p-0">
                                    {{Form::input('text', 'data[customer_tel]', isset($conditions['customer_tel']) ? $conditions['customer_tel'] : null, ['id' => 'customer_tel', 'class' => 'form-control w-100'])}}
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-inline mb-2">
                                <label class="col-12 col-sm-3 col-md-3 col-lg-4"
                                       for="customer_name">{{ trans('demandlist.customer_name') }}</label>
                                <div class="col-12 col-sm-9 col-md-9 col-lg-8 p-0">
                                    {{Form::input('text', 'data[customer_name]', null, ['id' => 'customer_name', 'class' => 'form-control w-100'])}}
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-inline mb-2">
                                <label class="col-12 col-sm-3 col-md-3 col-lg-4"
                                       for="opportunity_id">{{ trans('demandlist.opportunity_id') }}</label>
                                <div class="col-12 col-sm-9 col-md-9 col-lg-8 p-0">
                                    {{Form::input('text', 'data[id]', null, ['id' => 'id', 'class' => 'form-control w-100', 'data-rule-number'=>'true'])}}
                                    @if ($errors->has('data.id'))
                                        <p class="form-control-feedback text-danger my-2 has-danger">{{$errors->first('data.id')}}</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-inline mb-2">
                                <label class="col-12 col-sm-3 col-md-3 col-lg-4"
                                       for="demand_status">{{ trans('demandlist.proposal_status') }}</label>
                                <div class="col-12 col-sm-9 col-md-9 col-lg-8 p-0">
                                    {{Form::select('data[demand_status][]',$itemLists, isset($conditions['demand_status']) ? $conditions['demand_status'] : null,['id'=>'demand_status', 'multiple'=>'multiple', 'class'=>'multiple_check_filter w-100', 'style' => 'display:none'])}}
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-inline mb-2">
                                <label class="col-12 col-sm-3 col-md-3 col-lg-4"
                                       for="site_id">{{ trans('demandlist.site_name') }}</label>
                                <div class="col-12 col-sm-9 col-md-9 col-lg-8 p-0">
                                    {{Form::select('data[site_id][]',$siteLists, isset($conditions['site_id']) ? $conditions['site_id'] : null,['id'=>'site_id', 'multiple'=>'multiple', 'class'=>'multiple_check_filter w-100' , 'style' => 'display:none'])}}
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-inline mb-2">
                                <label class="col-12 col-sm-3 col-md-3 col-lg-4"
                                       for="jbr_order_no">{{ trans('demandlist.JBR_reception_like_no') }}</label>
                                <div class="col-12 col-sm-9 col-md-9 col-lg-8 p-0">
                                    {{Form::input('text', 'data[jbr_order_no]', null, ['id' => 'jbr_order_no', 'class' => 'form-control w-100'])}}
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-inline mb-2">
                                <label class="col-12 col-sm-3 col-md-3 col-lg-4"
                                       for="from_contact_desired_time">{{ trans('demandlist.contact_deadline_date_and_time') }}</label>
                                <div class="d-flex col-12 col-sm-9 col-md-9 col-lg-8 p-0">
                                    <div class="d-inline-flex flex-column">
                                        <input name="data[from_contact_desired_time]"
                                               class="form-control datetimepicker w-100"
                                               type="text"
                                               id="from_contact_desired_time"
                                               value="{{ isset($conditions['from_contact_desired_time']) ? $conditions['from_contact_desired_time'] : '' }}"
                                               data-rule-lessThanTime="#to_contact_desired_time">
                                    </div>
                                    @if ($errors->has('data.from_contact_desired_time'))
                                        <p class="form-control-feedback text-danger my-2 has-danger">{{$errors->first('data.from_contact_desired_time')}}</p>
                                    @endif
                                    <label class="px-2">{{trans('common.wavy_seal')}}</label>
                                    <div class="d-inline-flex flex-column">
                                        <input name="data[to_contact_desired_time]"
                                               class="form-control datetimepicker w-100"
                                               type="text"
                                               id="to_contact_desired_time"
                                               value="{{ isset($conditions['to_contact_desired_time']) ? $conditions['to_contact_desired_time'] : '' }}">
                                    </div>
                                    @if ($errors->has('data.to_contact_desired_time'))
                                        <p class="form-control-feedback text-danger my-2 has-danger">{{$errors->first('data.to_contact_desired_time')}}</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-inline mb-2">
                                <label class="col-12 col-sm-3 col-md-3 col-lg-4"
                                       for="lost_flg_filter">{{ trans('demandlist.excludes_before_disapproval') }}</label>
                                <div class="custom-control custom-checkbox mr-sm-2">
                                    {{Form::input('checkbox', 'data[lost_flg_filter]', null, ['id' => 'lost_flg_filter', 'class' => 'custom-control-input', 'data-check' => (isset($conditions['lost_flg_filter']) && $conditions['lost_flg_filter']=='on') ? 'checked' : ''])}}
                                    <label class="custom-control-label custome-label" for="lost_flg_filter"></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-inline mb-2">
                                <label class="col-12 col-sm-3 col-md-3 col-lg-4"
                                       for="to_receive_datetime">{{ trans('demandlist.reception_date_and_time') }}</label>
                                <div class="d-flex col-12 col-sm-9 col-md-9 col-lg-8 p-0">
                                    <div class="d-inline-flex flex-column">
                                        <input name="data[from_receive_datetime]"
                                               class="form-control datetimepicker w-100"
                                               type="text"
                                               id="from_receive_datetime"
                                               value="{{ isset($conditions['from_receive_datetime']) ? $conditions['from_receive_datetime'] : '' }}"
                                               data-rule-lessThanTime="#to_receive_datetime">
                                        @if ($errors->has('data.from_receive_datetime'))
                                            <p class="form-control-feedback text-danger my-2 has-danger">{{$errors->first('data.from_receive_datetime')}}</p>
                                        @endif
                                    </div>
                                    <label class="px-2">{{trans('common.wavy_seal')}}</label>
                                    <div class="d-inline-flex flex-column">
                                        <input name="data[to_receive_datetime]"
                                               class="form-control datetimepicker w-100"
                                               type="text" id="to_receive_datetime"
                                               value="{{ isset($conditions['to_receive_datetime']) ? $conditions['to_receive_datetime'] : '' }}">
                                        @if ($errors->has('data.to_receive_datetime'))
                                            <p class="form-control-feedback text-danger my-2 has-danger">{{$errors->first('data.to_receive_datetime')}}</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="d-flex flex-column flex-sm-row">
                                <input type="button"
                                       class="btn btn--gradient-orange remove-effect-btn col-lg-2 col-sm-3 mb-1 mb-sm-0"
                                       onclick="location.href='{{ route('demand.get.create') }}';"
                                       value="{{ trans('demandlist.sign_up') }}">
                                <button name="submit" id="search"
                                        class="btn btn--gradient-orange  remove-effect-btn col-lg-2 col-sm-3 mx-sm-2 mb-1 mb-sm-0"
                                        type="submit"
                                        value="search">{{ trans('demandlist.search') }}</button>
                                @if (!$defaultDisplay)
                                    @if($auth=='system' || $auth=='admin' || $auth=='accounting_admin')
                                        <button name="submit"
                                                class="btn btn--gradient-orange remove-effect-btn col-lg-2 col-sm-3"
                                                type="submit"
                                                value="csv">{{ trans('demandlist.CSV_output') }}</button>
                                    @endif
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
            @if (!$defaultDisplay)
                @if(isset($demandInfos) && count($demandInfos) > 0)
                    <p class="mt-2 mb-0">{{ trans('antisocial_follow.total_number').$demandInfos->total().trans('antisocial_follow.matter') }}</p>
                @else
                    <p class="mt-2 mb-0">{{ trans('antisocial_follow.total_number').'0'.trans('antisocial_follow.matter') }}</p>
                @endif
                <div class="custom-scroll-x">
                    <table class="table custom-border add-pseudo-scroll-bar mb-0" id='table-demandlist'
                           data-url='{{ Route::current()->parameter('id') }}'>
                        <thead>
                        <tr class="text-center bg-yellow-light">
                            <th class="p-1 align-middle fix-w-100">{{ trans('demandlist.opportunity_id') }}
                                <div class="sortInner">
                                    <i class="triangle-up up sort mx-1" aria-hidden="true" data-sort="demand_infos.id"
                                       data-direction="asc"></i>
                                    <i class="triangle-down down sort" aria-hidden="true" data-sort="demand_infos.id"
                                       data-direction="desc"></i>
                                </div>
                            </th>
                            <th class="p-1 align-middle fix-w-100">{{ trans('demandlist.urgent') }}
                                <div class="sortInner">
                                    <i class="triangle-up up sort mx-1" aria-hidden="true"
                                       data-sort="demand_infos.immediately" data-direction="asc"></i>
                                    <i class="triangle-down down sort" aria-hidden="true"
                                       data-sort="demand_infos.immediately" data-direction="desc"></i>
                                </div>
                            </th>
                            <th class="p-1 align-middle fix-w-100">{{ trans('demandlist.demand_status') }}
                                <div class="sortInner">
                                    <i class="triangle-up up sort mx-1" aria-hidden="true"
                                       data-sort="demand_infos.demand_status" data-direction="asc"></i>
                                    <i class="triangle-down down sort" aria-hidden="true"
                                       data-sort="demand_infos.demand_status" data-direction="desc"></i>
                                </div>
                            </th>
                            <th class="p-1 align-middle fix-w-100">{{ trans('demandlist.customer_name') }}
                                <div class="sortInner">
                                    <i class="triangle-up up sort mx-1" aria-hidden="true"
                                       data-sort="demand_infos.customer_name" data-direction="asc"></i>
                                    <i class="triangle-down down sort" aria-hidden="true"
                                       data-sort="demand_infos.customer_name" data-direction="desc"></i>
                                </div>
                            </th>
                            <th class="p-1 align-middle fix-w-100">{{ trans('demandlist.corporate_name') }}
                                <div class="sortInner">
                                    <i class="triangle-up up sort mx-1" aria-hidden="true"
                                       data-sort="demand_infos.customer_corp_name" data-direction="asc"></i>
                                    <i class="triangle-down down sort" aria-hidden="true"
                                       data-sort="demand_infos.customer_corp_name" data-direction="desc"></i>
                                </div>
                            </th>
                            <th class="p-1 align-middle fix-w-100">{{ trans('demandlist.site_name') }}
                                <div class="sortInner">
                                    <i class="triangle-up up sort mx-1" aria-hidden="true" data-sort="m_sites.site_name"
                                       data-direction="asc"></i>
                                    <i class="triangle-down down sort" aria-hidden="true" data-sort="m_sites.site_name"
                                       data-direction="desc"></i>
                                </div>
                            </th>
                            <th class="p-1 align-middle fix-w-100">{{ trans('demandlist.category') }}
                                <div class="sortInner">
                                    <i class="triangle-up up sort mx-1" aria-hidden="true"
                                       data-sort="m_categories.category_name" data-direction="asc"></i>
                                    <i class="triangle-down down sort" aria-hidden="true"
                                       data-sort="m_categories.category_name" data-direction="desc"></i>
                                </div>
                            </th>
                            <th class="p-1 align-middle fix-w-100">{{ trans('demandlist.JBR_reception_like_no') }}
                                <div class="sortInner">
                                    <i class="triangle-up up sort mx-1" aria-hidden="true"
                                       data-sort="demand_infos.jbr_order_no" data-direction="asc"></i>
                                    <i class="triangle-down down sort" aria-hidden="true"
                                       data-sort="demand_infos.jbr_order_no" data-direction="desc"></i>
                                </div>
                            </th>
                            <th class="p-1 align-middle fix-w-100">{{ trans('demandlist.power_receiving') }}
                                <div class="sortInner">
                                    <i class="triangle-up up sort mx-1" aria-hidden="true"
                                       data-sort="demand_infos.receive_datetime" data-direction="asc"></i>
                                    <i class="triangle-down down sort" aria-hidden="true"
                                       data-sort="demand_infos.receive_datetime" data-direction="desc"></i>
                                </div>
                            </th>
                            <th class="p-1 align-middle fix-w-100">{{ trans('demandlist.contact_deadline_date_and_time') }}
                                <div class="sortInner">
                                    <i class="triangle-up up sort mx-1" aria-hidden="true"
                                       data-sort="demand_infos.contact_desired_time" data-direction="asc"></i>
                                    <i class="triangle-down down sort" aria-hidden="true"
                                       data-sort="demand_infos.contact_desired_time" data-direction="desc"></i>
                                </div>
                            </th>
                            <th class="p-1 align-middle fix-w-100">{{ trans('demandlist.selection_method') }}
                                <div class="sortInner">
                                    <i class="triangle-up up sort mx-1" aria-hidden="true"
                                       data-sort="demand_infos.selection_system" data-direction="asc"></i>
                                    <i class="triangle-down down sort" aria-hidden="true"
                                       data-sort="demand_infos.selection_system" data-direction="desc"></i>
                                </div>
                            </th>
                        </tr>
                        </thead>
                        @if(isset($demandInfos) && count($demandInfos) > 0)
                            @foreach($demandInfos as $demandInfo)
                                <tr>
                                    <td class="p-1 align-middle fix-w-100 text-wrap text-center">
                                        <a class="highlight-link" target="_blank"
                                           href="{{ route('demand.detail', ['id' => $demandInfo->id]) }}">
                                            {{ $demandInfo->id }}
                                        </a>
                                    </td>
                                    <td class="p-1 align-middle fix-w-100 text-wrap">
                                        @if (!empty($demandInfo->immediately))
                                            {{ trans('demandlist'.getDivTextJP('checkbox_div', $demandInfo->immediately)) }}
                                        @endif
                                    </td>
                                    <td class="p-1 align-middle fix-w-100 text-wrap">{{ getDropText(\App\Repositories\Eloquent\MItemRepository::PROPOSAL_STATUS, $demandInfo->demand_status) }}</td>
                                    <td class="p-1 align-middle fix-w-100 text-wrap">{{ $demandInfo->customer_name }}</td>
                                    <td class="p-1 align-middle fix-w-100 text-wrap">{{ $demandInfo->customer_corp_name }}</td>
                                    <td class="p-1 align-middle fix-w-100 text-wrap">{{ $demandInfo->site_name }}</td>
                                    <td class="p-1 align-middle fix-w-100 text-wrap">{{ $demandInfo->category_name }}</td>
                                    <td class="p-1 align-middle fix-w-100 text-wrap">{{ $demandInfo->jbr_order_no }}</td>
                                    <td class="p-1 align-middle fix-w-100 text-wrap text-center">{{ dateTimeFormat($demandInfo->receive_datetime) }}</td>
                                    <td class="p-1 align-middle fix-w-100 text-wrap text-center">{{ getContactDesiredTime($demandInfo) }}</td>
                                    <td class="p-1 align-middle fix-w-100 text-wrap">
                                        @php
                                            if ($demandInfo->selection_system == 2) {
                                                echo trans('demandlist.bidding_ceremony_manual');
                                            }elseif ($demandInfo->selection_system == 3){
                                                echo trans('demandlist.bidding_ceremony_automatic');
                                            }elseif ($demandInfo->selection_system == 4) {
                                                echo trans('demandlist.automatic');
                                            }else {
                                                echo trans('demandlist.manual');
                                            }
                                        @endphp
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </table>
                </div>
                @if(!empty($demandInfos))
                    {{ $demandInfos->links('demand_list.demandlist_pagination') }}
                @endif
                <div class="pseudo-scroll-bar" data-display="false">
                    <div class="scroll-bar"></div>
                </div>
            @endif
        </form>
    </div>
@endsection
@section('script')
    <script src="{{ mix('js/lib/jquery.multiselect.js') }}"></script>
    <script src="{{ mix('js/lib/jquery.multiselect.filter.js') }}"></script>
    <script src="{{ mix('js/lib/jquery-ui-timepicker-addon.js') }}"></script>
    <script src="{{ mix('js/lib/jquery.validate.min.js') }}"></script>
    <script src="{{ mix('js/lib/localization/jquery.validate.messages_ja.js') }}"></script>
    <script src="{{ mix('js/lib/additional-methods.min.js') }}"></script>
    <script src="{{ mix('js/utilities/form.validate.js') }}"></script>
    <script src="{{ mix('js/lib/custom.js') }}"></script>
    <script src="{{ mix('js/pages/demandinfo.js') }}"></script>
    <script>
        FormUtil.validate('#demand_info');
    </script>
@endsection
