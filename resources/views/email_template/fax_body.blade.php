--{{ $data['boundary']."\n" }}
Content-Type: text/plain; charset=ISO-2022-JP

[PASSWD]Mj4yhWiU
[FAX]{{ $data['fax']}}

[RNAME]{{ $data['corpname']}}

[SCODE1]
[SCODE2]
[SCODE3]
[RMAIL]

[JIKAN]
[GENKO]Attach
[QUALITY]Fine
[PAPERSIZE]A4
[DIRECTION]
[FONTSIZE]
[FONTTYPE]
[RTRYINTERVAL]

--{{ $data['boundary']."\n"}}
Content-Type: application/pdf; name={{ $data['filebase']."\n"}}
Content-Disposition: attachment; filename={{ $data['filebase']."\n"}}
Content-Transfer-Encoding: base64

{{ $data['file_contents']."\n"}}
--{{ $data['boundary']}}--