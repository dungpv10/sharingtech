@if ($paginator->hasPages())
    <div class="dataTables_paginate">
        @if ($paginator->onFirstPage())
            <a class="paginate_button disabled"  rel="prev">
                &lt; @lang('mcorp_list.prev')
            </a>
        @else
            <a data-cur-page="{{ $paginator->currentPage() }}" class="paginate_button previous active" rel="prev">
                &lt; @lang('mcorp_list.prev')
            </a>
        @endif
        <span class="pl-3 pr-3"></span>
        @if ($paginator->hasMorePages())
            <a data-cur-page="{{ $paginator->currentPage() }}" class="paginate_button next active" rel="next">
                @lang('mcorp_list.next') &gt;
            </a>
        @else
            <a class="paginate_button disabled"  rel="next">
                @lang('mcorp_list.next') &gt;
            </a>
        @endif
    </div>
@else
    <div class="dataTables_paginate">
        <a class="paginate_button disabled"  rel="prev">
            &lt; @lang('mcorp_list.prev')
        </a>
        <span class="pl-3 pr-3"></span>
        <a class="paginate_button disabled"  rel="next">
            @lang('mcorp_list.next') &gt;
        </a>
    </div>
@endif
