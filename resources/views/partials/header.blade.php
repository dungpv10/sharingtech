<header>
    @php
        $logined = Auth::user();
        $menu = [];

        if (!empty($logined)) {
            $menu = getMenuByRole($logined->auth);
        }

        $isPageCustom = false;
        switch (Route::current()->getName()) {
            case 'bill.moneyCorrespond':
            case 'auction.proposal':
            case 'auction.support':
                $isPageCustom = true;
                break;
            default:
                $isPageCustom = false;
                break;
        }
    @endphp

    <nav class="navbar navbar-expand-lg navbar-light">
        @if(!$isPageCustom)
        <div class="container px-3 pb-2" id="hearder-logo-app">

            {{--Logo brand--}}
            <a class="navbar-brand" href="{{ url('/') }}">
                {{--{{ config('app.name', 'Laravel') }}--}}
                <span class="sr-only">Sharing Tech</span>
                <img src="{{asset('assets/img/orange.png')}}" alt="Logo" class="img-fluid">
            </a>

            @auth
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#app-navbar-collapse" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <ul class="navbar-text d-none d-lg-block m-0">
                    <li>
                        ユーザID：{{ $logined->user_id }}
                    </li>
                    <li>
                        ユーザ名：{{ $logined->user_name }}
                    </li>
                    <li>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                            ログアウト
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            @endauth
        </div>

        <div class="collapse navbar-collapse d-lg-block w-100 mt-lg-2" id="app-navbar-collapse">
            <div class="container">
                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav justify-content-lg-start w-100">
                    <!-- Authentication Links -->
                    @auth
                        @foreach ($menu as $key => $mnu)
                        <li>
                            <a href="{{!empty($mnu['route']) ? route($mnu['route']) : ''}}">{{trans($mnu['name'])}}</a>
                        </li>
                        @endforeach
                        <li class="dropdown-divider d-lg-none"></li>
                        <li class="d-lg-none">
                            <p class="m-0 text-muted">
                                <em>ユーザID：{{ $logined->user_id }}</em> <br>
                                <em>ユーザ名：{{ $logined->user_name }}</em>
                            </p>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                            document.getElementById('logout-form-mobile').submit();">
                                ログアウト
                            </a>

                            <form id="logout-form-mobile" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    @endauth
                </ul>
            </div>
        </div>
        @endif
    </nav>
</header>
