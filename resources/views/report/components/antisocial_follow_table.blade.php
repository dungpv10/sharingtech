<table class="table table-bordered">
    <thead>
    <tr class="text-center">
        <td class="p-1">{{ trans('antisocial_follow.merchant_id') }}</td>
        <td class="p-1">{{ trans('antisocial_follow.company_name') }}</td>
        <td class="p-1">{{ trans('antisocial_follow.last_time') }}</td>
        <td class="p-1">{{ trans('antisocial_follow.scheduled_month') }}</td>
        <td class="p-1">{{ trans('antisocial_follow.procedure_dial') }}</td>
        @if ($isUpdateAuthority)
            <td class="p-1">{{ trans('antisocial_follow.confirmation') }}</td>
        @endif
    </tr>
    </thead>
    <tbody class="text-center">
    @if (isset($results) && count($results) > 0)
        @foreach ($results as $key => $result)
            <tr>
                <td class="text-right p-1">
                    <a href="{{ url('/affiliation/detail/'.$result->mcorp_id) }}" class="text--orange">{{ $result->mcorp_id }}</a>
                </td>
                <td class="p-1">{{ $result->official_corp_name }}</td>
                <td class="p-1">{{ $result->max }}</td>
                <td class="p-1">{{ $result->concat }}</td>
                <td class="p-1">@php echo (ctype_digit($result->commission_dial)) ? '<a href="callto:'.$result->commission_dial.'">'.$result->commission_dial.'</a>' : ''; @endphp</td>
                @if ($isUpdateAuthority)
                    <td class="p-1">
                        {{ Form::checkbox('check[]', $result->mcorp_id, null,['id' => 'r_checkbox_'.$key]) }}
                    </td>
                @endif
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
{{ $results->links('pagination.nextprevajax') }}
