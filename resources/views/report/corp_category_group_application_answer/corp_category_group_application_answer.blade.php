@extends('layouts.app')
@section('style')
@endsection
@section('content')
    <h3>{{ __('report_corp_cate_group_app_answer.corp_category_application_answer') }}({{ __('report_corp_cate_group_app_answer.answer') }})</h3>
    @include('report.corp_category_group_application_answer.search')
    <div class="searchResult">
        @include('report.corp_category_group_application_answer.show_report')
    </div>
@endsection

@section('script')
    <script>
        var url_report_search = '{{ route('report.search.corp.category.group.application.answer') }}';
        var controlEl = {
            searchEl: '#searchButton',
            sort: [],
            formId: '#searchForm',
            resultArea: '.searchResult',
            nextPage: '.next',
            prevPage: '.previous'
        };
    </script>
    <script src="{{ mix('js/lib/jquery.validate.min.js') }}"></script>
    <script src="{{ mix('js/utilities/form.validate.js') }}"></script>
    <script src="{{ asset('js/utilities/st.common.js') }}"></script>
    <script>
        ajaxCommon.search(url_report_search, controlEl);
    </script>
@endsection
