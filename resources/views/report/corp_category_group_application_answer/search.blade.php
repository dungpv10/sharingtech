<div id="search">
    <div id="contents">
        <div id="main">
            <fieldset style="padding:15px 15px 15px 15px;margin:5px 15px 15px 15px;">
                <legend>{{ __('report_corp_cate_group_app_answer.search_condition') }}</legend>
                <table border="0" style="width:100%;background-color:#eaeaea;padding:5px 5px 5px 5px;border:solid 2px #cccccc;border-bottom: none;">
                    <form id="searchForm" action="{{ URL::route('report.get.corp.category.group.application.answer') }}" method="get">
                        <tr>
                            <td colspan="1" align="left" width="128px">{{ __('report_corp_cate_group_app_answer.corp_id') }}</td>
                            <td align="left">
                                <input type="text" name="corp_id" id="corp_id" style="width: 150px">
                            </td>
                            <td colspan="1" align="left" width="128px">{{ __('report_corp_cate_group_app_answer.corp_name') }}</td>
                            <td align="left">
                                <input type="text" name="corp_name" id="corp_name" style="width: 150px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" align="left" width="128px">{{ __('report_corp_cate_group_app_answer.group_id') }}</td>
                            <td align="left">
                                <input type="text" name="group_id" id="group_id" style="width: 150px">
                            </td>
                            <td colspan="1" align="left" width="128px">{{ __('report_corp_cate_group_app_answer.application_date') }}</td>
                            <td align="left">
                                <input type="text" name="application_date_from" id="application_date_from" style="width: 150px">
                                {{trans('common.wavy_seal')}}
                                <input type="text" name="application_date_to" id="application_date_to" style="width: 150px">
                            </td>
                        </tr>
                    </form>

                    <form id="downloadCsv" action="{{ URL::route('export.csv.corp.category.group.application.answer') }}" method="post" multiple="multipart/form-data" accept-charset="UTF-8">
                        {{ csrf_field() }}
                    </form>

                    <tr>
                        <td colspan="4" style="padding-top: 10px;">
                            <button type="button" id="searchButton" class="btn btn-default">{{ __('report_corp_cate_group_app_answer.searchForm') }}</button>
                            <button type="submit" id="downloadCsv" form="downloadCsv" class="btn btn-default">{{ __('report_corp_cate_group_app_answer.downloadCsv') }}</button>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
    </div>
</div>
