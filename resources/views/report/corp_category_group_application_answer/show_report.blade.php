<!-- Search details -->

@if (isset($results))
    <div class="row table-result-search">
        <span class="count-total">{{ __('common.total') }} {{ $results->total() }} {{ __('common.item') }}</span>
        <div class="table-responsive">
            <table class="table table-bordered" id="searchData">
                <thead>
                    <tr class="text-center bg-yellow-light">
                        <th class="p-1 align-middle">@lang('report_corp_cate_group_app_answer.default_value_label')</th>
                        <th class="p-1 align-middle">@lang('report_corp_cate_group_app_answer.default_value_label')</th>
                        <th class="p-1 align-middle">@lang('report_corp_cate_group_app_answer.m_corps_id')</th>
                        <th class="p-1 align-middle">@lang('report_corp_cate_group_app_answer.created_user_id')</th>
                        <th class="p-1 align-middle">@lang('report_corp_cate_group_app_answer.corp_category_application_answer_created')</th>
                        <th class="p-1 align-middle">@lang('report_corp_cate_group_app_answer.application_count')</th>
                        <th class="p-1 align-middle">@lang('report_corp_cate_group_app_answer.application_count_check')</th>
                    </tr>
                </thead>
                <tbody>
                @if(!empty($results))
                    @foreach($results as $key => $data)
                        @php
                            if($data->unapproved_count > 0) {
                                $status = -1;
                            } elseif($data->approval_count == $data->application_count) {
                                $status = 1;
                            } elseif($data->reject_count == $data->application_count) {
                                $status = 2;
                            } else {
                                $status = 0;
                            }
                        @endphp

                        <tr>
                            <td align="center">{{ $data->cid }}</td>
                            <td align="center">{{ __('report_corp_cate_group_app_answer.default_value') }}</td>
                            <td align="center">
                                <a href="{{ URL::route('affiliation.detail.edit', $data->m_corps_id) }}" target="_blank">{{ $data->official_corp_name }}</a>
                            <td align="center" style="word-wrap:break-word">{{ $data->created_user_id }}</td>
                            <td align="center">{{ $data->created }}</td>
                            <td align="center">
                                <a href="{{ URL::route('report.get.corp.category.application.answer', $data->cid) }}"
                                   target="_blank">{{ $data->application_count }}</a>
                            </td>
                            <td align="center" bgcolor="@if($status == -1) #ffffc0 @elseif($status == 1) #81F79F @elseif($status == 2) #F5A9A9 @else #fff @endif">
                                {{ $propriety[$status] }}
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
            @if(!empty($results))
                {{ $results->links('common.pagination') }}
            @endif
        </div>
    </div>
@endif