@extends('layouts.app')

@section('content')
    @if(session()->has('error'))
        <div class="box__mess box--error">
            {!! session('error') !!}
        </div>
    @endif
    @if(session()->has('success'))
        <div class="box__mess box--success">
            {!! session('success') !!}
        </div>
    @endif
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>@lang('selection.selection_label')</h3>
            </div>
            <div class="col-md-12">
                <form action="{{ route('selection.post') }}" method="post" accept-charset="utf-8" class="">
                    {{ csrf_field() }}
                    @foreach($genres->chunk(3) as $key => $groupGenres)
                        <div class="row mt-2">
                            @foreach($groupGenres as $genre)
                                <div class="box col-md-4 form-group">
                                    <a @if(isset($genre->select_type) && (in_array($genre->select_type, [2,3,4]))) href="{{ route('selection.prefecture', $genre->genre_id) }}" @endif>{{ $genre->genre_name }}</a>
                                    <br>
                                    <select class="form-control" name="data[{{ $key }}][select_type]">
                                        @if($selectionType)
                                            @foreach($selectionType as $k => $v)
                                                <option @if($k == $genre->select_type) selected @endif value="{{ $k }}">{{ $v }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    <input type="hidden" value="{{ $genre->id }}" name="data[{{ $key }}][id]">
                                    <input type="hidden" value="{{ $genre->genre_id }}" name="data[{{ $key }}][genre_id]">
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                    <div class="d-flex flex-column flex-sm-row justify-content-md-center">
                        <a href="{{ route('admin.index') }}" class="btn btn-primary mt-2 mr-md-1" role="button">@lang('common.return_button')</a>
                        <button type="submit" class="btn btn--gradient-orange mt-2 ml-md-1" type="button">@lang('common.save_button')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
