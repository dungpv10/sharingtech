<div class="table-responsive">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th class="text-center p-1" width="15%">@lang("target_area_detail.col1")</th>
                <th class="text-center p-1">@lang("target_area_detail.col2")</th>
            </tr>
        </thead>
        <tbody>
            @foreach($results as $result)
                <tr>
                    <td>{{$result->category_name}}</td>
                    <td>{{$result->address}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>