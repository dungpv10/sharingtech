@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div>
                <div>
                    <span>(@lang('user.input_require'))</span>
                </div>
                <div>
                    @if(Session::has('message'))
                        {{Session::get('message')}}
                    @endif
                </div>
                <div>
                    <h3>@lang('user.info_user')</h3>
                    @component("user.components._form_detail", [
                        "authList" => $authList,
                        "routeAction" => route('user.edit', $id),
                        "data" => $dataUser,
                        "dataMcorp" => $dataMcorp
                    ])
                    @endcomponent
                </div>
            </div>
        </div>
    </div>
    @include('commission_select.m_corp_display')
@endsection

