let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .copy('resources/assets/js/lib/*.js', 'public/js/lib')
   .copy('resources/assets/js/lib/localization/*.js', 'public/js/lib/localization')
   .copy('resources/assets/js/pages/*.js', 'public/js/pages')
   .copy('resources/assets/js/pages/helpers/*.js', 'public/js/pages/helpers')
   .copy('resources/assets/js/utilities/*.js', 'public/js/utilities')
   .copy('resources/assets/js/constant/*.js', 'public/js/constant')
   .copy('resources/assets/js/pages/demands/*.js', 'public/js/pages/demands')
   .copy('resources/assets/js/pages/commission/*.js', 'public/js/pages/commission')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .copy('resources/assets/sass/lib/*.css', 'public/css/lib')
   .sass('node_modules/font-awesome/scss/font-awesome.scss', 'public/css/lib/font-awesome.css')
   .sass('resources/assets/sass/theme/bootstrap.custom.scss', 'public/css/lib/bootstrap.css');

mix.version();
